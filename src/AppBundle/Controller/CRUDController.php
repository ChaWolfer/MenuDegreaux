<?php
// src/AppBundle/Controller/CRUDController.php

namespace AppBundle\Controller;

use Sonata\AdminBundle\Controller\CRUDController as BaseController;
use Symfony\Component\HttpFoundation\Request;

class CRUDController extends BaseController
{
    public function batchActionMergeIsRelevant(array $selectedIds, $allEntitiesSelected, Request $request = null)
    {
        // here you have access to all POST parameters, if you use some custom ones
        // POST parameters are kept even after the confirmation page.
        $parameterBag = $request->request;

        // check that a target has been chosen
        if (!$parameterBag->has('targetId')) {
            return 'flash_batch_merge_no_target';
        }

        $targetId = $parameterBag->get('targetId');

        // if all entities are selected, a merge can be done
        if ($allEntitiesSelected) {
            return true;
        }

        // filter out the target from the selected models
        $selectedIds = array_filter($selectedIds,
            function($selectedId) use($targetId){
                return $selectedId !== $targetId;
            }
        );

        // if at least one but not the target model is selected, a merge can be done.
        return count($selectedIds) > 0;
    }

    // ...
}
