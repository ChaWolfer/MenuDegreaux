<?php

/* @FOSUser/Security/login.html.twig */
class __TwigTemplate_a5a2cc59147294f55f320b0a30f4a6e6ff514205c5bb8381e49409bac3b04789 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("::base.html.twig", "@FOSUser/Security/login.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "::base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9097c250e8eeb29efc289aabdcfa719847eadf20dfab37b40654c47f73ec7fd0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9097c250e8eeb29efc289aabdcfa719847eadf20dfab37b40654c47f73ec7fd0->enter($__internal_9097c250e8eeb29efc289aabdcfa719847eadf20dfab37b40654c47f73ec7fd0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@FOSUser/Security/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9097c250e8eeb29efc289aabdcfa719847eadf20dfab37b40654c47f73ec7fd0->leave($__internal_9097c250e8eeb29efc289aabdcfa719847eadf20dfab37b40654c47f73ec7fd0_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_23608f95d83162c6bec4df28e0d8b4c203d3827996ba6bd0312724d8a70c4438 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_23608f95d83162c6bec4df28e0d8b4c203d3827996ba6bd0312724d8a70c4438->enter($__internal_23608f95d83162c6bec4df28e0d8b4c203d3827996ba6bd0312724d8a70c4438_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Se connecter";
        
        $__internal_23608f95d83162c6bec4df28e0d8b4c203d3827996ba6bd0312724d8a70c4438->leave($__internal_23608f95d83162c6bec4df28e0d8b4c203d3827996ba6bd0312724d8a70c4438_prof);

    }

    // line 5
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_33290127a47fbd0dfcc6e732803097cc3989c2ce4b6fcf52398be9339f0cc7c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33290127a47fbd0dfcc6e732803097cc3989c2ce4b6fcf52398be9339f0cc7c5->enter($__internal_33290127a47fbd0dfcc6e732803097cc3989c2ce4b6fcf52398be9339f0cc7c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "login";
        
        $__internal_33290127a47fbd0dfcc6e732803097cc3989c2ce4b6fcf52398be9339f0cc7c5->leave($__internal_33290127a47fbd0dfcc6e732803097cc3989c2ce4b6fcf52398be9339f0cc7c5_prof);

    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        $__internal_1054653a5e76e4a001469818de8596176574b6a13ccaa5c69148759f1a262225 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1054653a5e76e4a001469818de8596176574b6a13ccaa5c69148759f1a262225->enter($__internal_1054653a5e76e4a001469818de8596176574b6a13ccaa5c69148759f1a262225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 8
        echo "
    ";
        // line 9
        echo twig_include($this->env, $context, "@FOSUser/Security/login_content.html.twig");
        echo "
";
        
        $__internal_1054653a5e76e4a001469818de8596176574b6a13ccaa5c69148759f1a262225->leave($__internal_1054653a5e76e4a001469818de8596176574b6a13ccaa5c69148759f1a262225_prof);

    }

    public function getTemplateName()
    {
        return "@FOSUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 9,  66 => 8,  60 => 7,  48 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '::base.html.twig' %}

{% block title %}Se connecter{% endblock %}

{% block body_id 'login' %}

{% block main %}

    {{ include('@FOSUser/Security/login_content.html.twig') }}
{% endblock main %}
", "@FOSUser/Security/login.html.twig", "/home/lievininqd/restauration/app/Resources/FOSUserBundle/views/Security/login.html.twig");
    }
}
