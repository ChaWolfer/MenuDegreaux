<?php

/* base.html.twig */
class __TwigTemplate_3f5de3df2b57d7ff71025057b7a04ba4e340ff60d7796d945f1e950045b8cad2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4d73635dada8065b22d9632b829cc3643feada7d17a1c23451c85859ea1ab2d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b4d73635dada8065b22d9632b829cc3643feada7d17a1c23451c85859ea1ab2d->enter($__internal_b4d73635dada8065b22d9632b829cc3643feada7d17a1c23451c85859ea1ab2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.png"), "html", null, true);
        echo "\" >
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/css_sass/css/style.css"), "html", null, true);
        echo "\">
        ";
        // line 19
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>

    <body id=\"";
        // line 24
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 26
        $this->displayBlock('header', $context, $blocks);
        // line 143
        echo "
        <div class=\"container body-container\">
            ";
        // line 145
        $this->displayBlock('body', $context, $blocks);
        // line 159
        echo "        </div>

        ";
        // line 161
        $this->displayBlock('footer', $context, $blocks);
        // line 172
        echo "
        ";
        // line 173
        $this->displayBlock('javascripts', $context, $blocks);
        // line 182
        echo "

    </body>
</html>
";
        
        $__internal_b4d73635dada8065b22d9632b829cc3643feada7d17a1c23451c85859ea1ab2d->leave($__internal_b4d73635dada8065b22d9632b829cc3643feada7d17a1c23451c85859ea1ab2d_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_394e568a762dc08d27692f9b4cde6e9c231bb6f0c0dafa660fd8899ab0b0f1dc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_394e568a762dc08d27692f9b4cde6e9c231bb6f0c0dafa660fd8899ab0b0f1dc->enter($__internal_394e568a762dc08d27692f9b4cde6e9c231bb6f0c0dafa660fd8899ab0b0f1dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Menu Degreaux";
        
        $__internal_394e568a762dc08d27692f9b4cde6e9c231bb6f0c0dafa660fd8899ab0b0f1dc->leave($__internal_394e568a762dc08d27692f9b4cde6e9c231bb6f0c0dafa660fd8899ab0b0f1dc_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_33df400895b0fe09a39df003f9352b66611cc2a1e2ce9989b660721ce7f7ae48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33df400895b0fe09a39df003f9352b66611cc2a1e2ce9989b660721ce7f7ae48->enter($__internal_33df400895b0fe09a39df003f9352b66611cc2a1e2ce9989b660721ce7f7ae48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "
        ";
        
        $__internal_33df400895b0fe09a39df003f9352b66611cc2a1e2ce9989b660721ce7f7ae48->leave($__internal_33df400895b0fe09a39df003f9352b66611cc2a1e2ce9989b660721ce7f7ae48_prof);

    }

    // line 24
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_9c85d2fd1edb538a657d895f5ce6195c7aa326e9b324547c4ab327c55870314f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c85d2fd1edb538a657d895f5ce6195c7aa326e9b324547c4ab327c55870314f->enter($__internal_9c85d2fd1edb538a657d895f5ce6195c7aa326e9b324547c4ab327c55870314f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_9c85d2fd1edb538a657d895f5ce6195c7aa326e9b324547c4ab327c55870314f->leave($__internal_9c85d2fd1edb538a657d895f5ce6195c7aa326e9b324547c4ab327c55870314f_prof);

    }

    // line 26
    public function block_header($context, array $blocks = array())
    {
        $__internal_157379256bb5cc820bcefefdacf8a11aa3c3663d1808ea0694094669e59e620d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_157379256bb5cc820bcefefdacf8a11aa3c3663d1808ea0694094669e59e620d->enter($__internal_157379256bb5cc820bcefefdacf8a11aa3c3663d1808ea0694094669e59e620d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 27
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                ";
        // line 45
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 132
        echo "




                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_157379256bb5cc820bcefefdacf8a11aa3c3663d1808ea0694094669e59e620d->leave($__internal_157379256bb5cc820bcefefdacf8a11aa3c3663d1808ea0694094669e59e620d_prof);

    }

    // line 45
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_dfc1f953388b7a56c06e4a8eb9ff1fb1638f8b8561f404aa29eb815337dedbea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfc1f953388b7a56c06e4a8eb9ff1fb1638f8b8561f404aa29eb815337dedbea->enter($__internal_dfc1f953388b7a56c06e4a8eb9ff1fb1638f8b8561f404aa29eb815337dedbea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 46
        echo "
                                    ";
        // line 47
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 48
            echo "                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      ";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/profile"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/resseting"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    ";
            // line 64
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
                // line 65
                echo "                                                        <li>
                                                            <a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/register"), "html", null, true);
                echo "\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    ";
            }
            // line 71
            echo "                                                  </ul>
                                                </li>

                                    ";
        }
        // line 75
        echo "
                                    <li>
                                        <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    ";
        // line 98
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 99
            echo "                                        <li>
                                            <a href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("{_locale}adminSonata"), "html", null, true);
            echo "\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    ";
        }
        // line 106
        echo "
                                    ";
        // line 107
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 108
            echo "                                        <li>
                                            <a href=\"";
            // line 109
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                ";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                        ";
        } else {
            // line 115
            echo "                                        <li>
                                            <a href=\"";
            // line 116
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                ";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                    ";
        }
        // line 122
        echo "
                                    ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "all", array()));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 124
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 125
                echo "                                            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                                ";
                // line 126
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                            </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "
                                ";
        
        $__internal_dfc1f953388b7a56c06e4a8eb9ff1fb1638f8b8561f404aa29eb815337dedbea->leave($__internal_dfc1f953388b7a56c06e4a8eb9ff1fb1638f8b8561f404aa29eb815337dedbea_prof);

    }

    // line 145
    public function block_body($context, array $blocks = array())
    {
        $__internal_003b4b7a048303eb8eecf3db76a71dc7d661844c2e7482336d59b7765bbaef85 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_003b4b7a048303eb8eecf3db76a71dc7d661844c2e7482336d59b7765bbaef85->enter($__internal_003b4b7a048303eb8eecf3db76a71dc7d661844c2e7482336d59b7765bbaef85_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 146
        echo "                ";
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        ";
        // line 149
        $this->displayBlock('main', $context, $blocks);
        // line 151
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        ";
        // line 154
        $this->displayBlock('sidebar', $context, $blocks);
        // line 156
        echo "                    </div>
                </div>
            ";
        
        $__internal_003b4b7a048303eb8eecf3db76a71dc7d661844c2e7482336d59b7765bbaef85->leave($__internal_003b4b7a048303eb8eecf3db76a71dc7d661844c2e7482336d59b7765bbaef85_prof);

    }

    // line 149
    public function block_main($context, array $blocks = array())
    {
        $__internal_0f7aa197be6e12715bec5f05b64f23e508a10447042a6e7308c2eae7e1ebf141 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0f7aa197be6e12715bec5f05b64f23e508a10447042a6e7308c2eae7e1ebf141->enter($__internal_0f7aa197be6e12715bec5f05b64f23e508a10447042a6e7308c2eae7e1ebf141_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 150
        echo "                        ";
        
        $__internal_0f7aa197be6e12715bec5f05b64f23e508a10447042a6e7308c2eae7e1ebf141->leave($__internal_0f7aa197be6e12715bec5f05b64f23e508a10447042a6e7308c2eae7e1ebf141_prof);

    }

    // line 154
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_35308d4361cb6de7dad08766d42137d73b5705ac9d1924446f2dcca82522e9b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_35308d4361cb6de7dad08766d42137d73b5705ac9d1924446f2dcca82522e9b4->enter($__internal_35308d4361cb6de7dad08766d42137d73b5705ac9d1924446f2dcca82522e9b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 155
        echo "                        ";
        
        $__internal_35308d4361cb6de7dad08766d42137d73b5705ac9d1924446f2dcca82522e9b4->leave($__internal_35308d4361cb6de7dad08766d42137d73b5705ac9d1924446f2dcca82522e9b4_prof);

    }

    // line 161
    public function block_footer($context, array $blocks = array())
    {
        $__internal_d3e46e69608696146449acfd712b98e13f43dd5968aa285de3645db058a6e943 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d3e46e69608696146449acfd712b98e13f43dd5968aa285de3645db058a6e943->enter($__internal_d3e46e69608696146449acfd712b98e13f43dd5968aa285de3645db058a6e943_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 162
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; ";
        // line 166
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_d3e46e69608696146449acfd712b98e13f43dd5968aa285de3645db058a6e943->leave($__internal_d3e46e69608696146449acfd712b98e13f43dd5968aa285de3645db058a6e943_prof);

    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_b7d1ffe65e5f22d24393a819d89062a894892fd1aac01c5a6cd0da51ce0eadd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b7d1ffe65e5f22d24393a819d89062a894892fd1aac01c5a6cd0da51ce0eadd9->enter($__internal_b7d1ffe65e5f22d24393a819d89062a894892fd1aac01c5a6cd0da51ce0eadd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 174
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_b7d1ffe65e5f22d24393a819d89062a894892fd1aac01c5a6cd0da51ce0eadd9->leave($__internal_b7d1ffe65e5f22d24393a819d89062a894892fd1aac01c5a6cd0da51ce0eadd9_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  495 => 180,  491 => 179,  487 => 178,  483 => 177,  479 => 176,  475 => 175,  470 => 174,  464 => 173,  451 => 166,  445 => 162,  439 => 161,  432 => 155,  426 => 154,  419 => 150,  413 => 149,  404 => 156,  402 => 154,  397 => 151,  395 => 149,  388 => 146,  382 => 145,  374 => 130,  368 => 129,  359 => 126,  354 => 125,  349 => 124,  345 => 123,  342 => 122,  335 => 118,  330 => 116,  327 => 115,  320 => 111,  315 => 109,  312 => 108,  310 => 107,  307 => 106,  298 => 100,  295 => 99,  293 => 98,  269 => 77,  265 => 75,  259 => 71,  251 => 66,  248 => 65,  246 => 64,  239 => 60,  231 => 55,  223 => 50,  219 => 48,  217 => 47,  214 => 46,  208 => 45,  191 => 132,  189 => 45,  172 => 31,  166 => 27,  160 => 26,  149 => 24,  141 => 20,  135 => 19,  123 => 6,  112 => 182,  110 => 173,  107 => 172,  105 => 161,  101 => 159,  99 => 145,  95 => 143,  93 => 26,  88 => 24,  84 => 22,  82 => 19,  78 => 18,  73 => 16,  69 => 15,  65 => 14,  61 => 13,  57 => 12,  53 => 11,  49 => 10,  42 => 6,  35 => 2,  32 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>{% block title %}Menu Degreaux{% endblock %}</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.png') }}\" >
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-flatly-3.3.7.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome-4.6.3.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-lato.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-datetimepicker.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/highlight-solarized-light.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-tagsinput.css') }}\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/css_sass/css/style.css') }}\">
        {% block stylesheets %}

        {% endblock %}
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">

        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                {% block header_navigation_links %}

                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"{{ asset('/{_locale}/profile') }}\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"{{ asset('/{_locale}/resseting') }}\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    {% if is_granted(\"ROLE_ADMIN\") %}
                                                        <li>
                                                            <a href=\"{{ asset('/{_locale}/register') }}\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    {% endif %}
                                                  </ul>
                                                </li>

                                    {% endif %}

                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li>
                                            <a href=\"{{ asset('{_locale}adminSonata') }}\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_logout') }}\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                        {% else %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_login') }}\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                {{ 'layout.login'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% for type, messages in app.session.flashBag.all %}
                                        {% for message in messages %}
                                            <div class=\"{{ type }}\">
                                                {{ message|trans({}, 'FOSUserBundle') }}
                                            </div>
                                        {% endfor %}
                                    {% endfor %}

                                {% endblock %}





                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                {{ include('default/_flash_messages.html.twig') }}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        {% block main %}
                        {% endblock %}
                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        {% block sidebar %}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; {{ 'now'|date('Y') }} - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            <script src=\"{{ asset('js/jquery-2.2.4.min.js') }}\"></script>
            <script src=\"{{ asset('js/moment.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-3.3.7.min.js') }}\"></script>
            <script src=\"{{ asset('js/highlight.pack.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-datetimepicker.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-tagsinput.min.js') }}\"></script>
            <script src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}


    </body>
</html>
", "base.html.twig", "/home/lievininqd/restauration/app/Resources/views/base.html.twig");
    }
}
