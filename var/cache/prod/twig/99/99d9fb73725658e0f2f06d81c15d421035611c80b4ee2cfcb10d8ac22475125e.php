<?php

/* SonataAdminBundle:CRUD:base_edit.html.twig */
class __TwigTemplate_3399f98579db504a991ec6898c7ec51ae99fe630fc56c70ce8283515f39db900 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $_trait_0 = $this->loadTemplate("SonataAdminBundle:CRUD:base_edit_form.html.twig", "SonataAdminBundle:CRUD:base_edit.html.twig", 33);
        // line 33
        if (!$_trait_0->isTraitable()) {
            throw new Twig_Error_Runtime('Template "'."SonataAdminBundle:CRUD:base_edit_form.html.twig".'" cannot be used as a trait.');
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        if (!isset($_trait_0_blocks["form"])) {
            throw new Twig_Error_Runtime(sprintf('Block "form" is not defined in trait "SonataAdminBundle:CRUD:base_edit_form.html.twig".'));
        }

        $_trait_0_blocks["parentForm"] = $_trait_0_blocks["form"]; unset($_trait_0_blocks["form"]);

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            array(
                'title' => array($this, 'block_title'),
                'navbar_title' => array($this, 'block_navbar_title'),
                'actions' => array($this, 'block_actions'),
                'tab_menu' => array($this, 'block_tab_menu'),
                'form' => array($this, 'block_form'),
            )
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:base_edit.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_55c2221f77ff40160ea3ed2aedfe648dbb9f92a1d08f828605366832a0a7caf8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_55c2221f77ff40160ea3ed2aedfe648dbb9f92a1d08f828605366832a0a7caf8->enter($__internal_55c2221f77ff40160ea3ed2aedfe648dbb9f92a1d08f828605366832a0a7caf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_edit.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_55c2221f77ff40160ea3ed2aedfe648dbb9f92a1d08f828605366832a0a7caf8->leave($__internal_55c2221f77ff40160ea3ed2aedfe648dbb9f92a1d08f828605366832a0a7caf8_prof);

    }

    // line 14
    public function block_title($context, array $blocks = array())
    {
        $__internal_720ffad2ad4bf988a8a66cd903fcf768cf30d4ad7ef5cfdff852def9230180e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_720ffad2ad4bf988a8a66cd903fcf768cf30d4ad7ef5cfdff852def9230180e1->enter($__internal_720ffad2ad4bf988a8a66cd903fcf768cf30d4ad7ef5cfdff852def9230180e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 15
        echo "    ";
        // line 16
        echo "    ";
        if ( !(null === ((array_key_exists("objectId", $context)) ? (_twig_default_filter((isset($context["objectId"]) ? $context["objectId"] : $this->getContext($context, "objectId")), $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"))) : ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"))))) {
            // line 17
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_edit", array("%name%" => twig_truncate_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "toString", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), 15)), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        } else {
            // line 19
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_create", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        
        $__internal_720ffad2ad4bf988a8a66cd903fcf768cf30d4ad7ef5cfdff852def9230180e1->leave($__internal_720ffad2ad4bf988a8a66cd903fcf768cf30d4ad7ef5cfdff852def9230180e1_prof);

    }

    // line 23
    public function block_navbar_title($context, array $blocks = array())
    {
        $__internal_36c50196c1f48fdecf45ec491c4211569b2db307d3836a1a8394de77549bda57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_36c50196c1f48fdecf45ec491c4211569b2db307d3836a1a8394de77549bda57->enter($__internal_36c50196c1f48fdecf45ec491c4211569b2db307d3836a1a8394de77549bda57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "navbar_title"));

        // line 24
        echo "    ";
        $this->displayBlock("title", $context, $blocks);
        echo "
";
        
        $__internal_36c50196c1f48fdecf45ec491c4211569b2db307d3836a1a8394de77549bda57->leave($__internal_36c50196c1f48fdecf45ec491c4211569b2db307d3836a1a8394de77549bda57_prof);

    }

    // line 27
    public function block_actions($context, array $blocks = array())
    {
        $__internal_91ecb0b6b9eeda11294349fef1caaa193ffe9803339ffa10bbd434826d8d2013 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_91ecb0b6b9eeda11294349fef1caaa193ffe9803339ffa10bbd434826d8d2013->enter($__internal_91ecb0b6b9eeda11294349fef1caaa193ffe9803339ffa10bbd434826d8d2013_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 28
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:base_edit.html.twig", 28)->display($context);
        
        $__internal_91ecb0b6b9eeda11294349fef1caaa193ffe9803339ffa10bbd434826d8d2013->leave($__internal_91ecb0b6b9eeda11294349fef1caaa193ffe9803339ffa10bbd434826d8d2013_prof);

    }

    // line 31
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_db94264c0feb8538be6d88682f0d87d664f20ff2fc15f67164454a64c26a4951 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_db94264c0feb8538be6d88682f0d87d664f20ff2fc15f67164454a64c26a4951->enter($__internal_db94264c0feb8538be6d88682f0d87d664f20ff2fc15f67164454a64c26a4951_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "sidemenu", array(0 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action"))), "method"), array("currentClass" => "active", "template" => $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
        
        $__internal_db94264c0feb8538be6d88682f0d87d664f20ff2fc15f67164454a64c26a4951->leave($__internal_db94264c0feb8538be6d88682f0d87d664f20ff2fc15f67164454a64c26a4951_prof);

    }

    // line 35
    public function block_form($context, array $blocks = array())
    {
        $__internal_63d8acbedd926e2066b1216d5b54917b55a9d1e552c54fd80c0cbe21e4dd4060 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63d8acbedd926e2066b1216d5b54917b55a9d1e552c54fd80c0cbe21e4dd4060->enter($__internal_63d8acbedd926e2066b1216d5b54917b55a9d1e552c54fd80c0cbe21e4dd4060_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form"));

        // line 36
        echo "    ";
        $this->displayBlock("parentForm", $context, $blocks);
        echo "
";
        
        $__internal_63d8acbedd926e2066b1216d5b54917b55a9d1e552c54fd80c0cbe21e4dd4060->leave($__internal_63d8acbedd926e2066b1216d5b54917b55a9d1e552c54fd80c0cbe21e4dd4060_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  130 => 36,  124 => 35,  112 => 31,  105 => 28,  99 => 27,  89 => 24,  83 => 23,  72 => 19,  66 => 17,  63 => 16,  61 => 15,  55 => 14,  40 => 12,  12 => 33,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block title %}
    {# NEXT_MAJOR: remove default filter #}
    {% if objectId|default(admin.id(object)) is not null %}
        {{ \"title_edit\"|trans({'%name%': admin.toString(object)|truncate(15) }, 'SonataAdminBundle') }}
    {% else %}
        {{ \"title_create\"|trans({}, 'SonataAdminBundle') }}
    {% endif %}
{% endblock %}

{% block navbar_title %}
    {{ block('title') }}
{% endblock %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}{{ knp_menu_render(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonata_admin.adminPool.getTemplate('tab_menu_template')}, 'twig') }}{% endblock %}

{% use 'SonataAdminBundle:CRUD:base_edit_form.html.twig' with form as parentForm %}

{% block form %}
    {{ block('parentForm') }}
{% endblock %}
", "SonataAdminBundle:CRUD:base_edit.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_edit.html.twig");
    }
}
