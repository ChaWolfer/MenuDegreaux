<?php

/* menu/scolaire.html.twig */
class __TwigTemplate_c078334ae5f9df002a0a6bbaf22d18c0c84554750fb697166dc4e60af2efff31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/scolaire.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d4de3d34c514ff562a66278cda68ca4d55249e973aef3a894a95d8e1273c7d6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d4de3d34c514ff562a66278cda68ca4d55249e973aef3a894a95d8e1273c7d6->enter($__internal_9d4de3d34c514ff562a66278cda68ca4d55249e973aef3a894a95d8e1273c7d6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "menu/scolaire.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9d4de3d34c514ff562a66278cda68ca4d55249e973aef3a894a95d8e1273c7d6->leave($__internal_9d4de3d34c514ff562a66278cda68ca4d55249e973aef3a894a95d8e1273c7d6_prof);

    }

    // line 5
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_f3270e311fa0d06737b6c696f9753f3e8197321cfd89f7a492cd1a452eadaa82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f3270e311fa0d06737b6c696f9753f3e8197321cfd89f7a492cd1a452eadaa82->enter($__internal_f3270e311fa0d06737b6c696f9753f3e8197321cfd89f7a492cd1a452eadaa82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_scolaire";
        
        $__internal_f3270e311fa0d06737b6c696f9753f3e8197321cfd89f7a492cd1a452eadaa82->leave($__internal_f3270e311fa0d06737b6c696f9753f3e8197321cfd89f7a492cd1a452eadaa82_prof);

    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        $__internal_a12db8416a4cb4ac846d0935cd3bb874428825223862cba2df5a1ebc6884e9bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a12db8416a4cb4ac846d0935cd3bb874428825223862cba2df5a1ebc6884e9bc->enter($__internal_a12db8416a4cb4ac846d0935cd3bb874428825223862cba2df5a1ebc6884e9bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 8
        echo "<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>


            </div>



";
        
        $__internal_a12db8416a4cb4ac846d0935cd3bb874428825223862cba2df5a1ebc6884e9bc->leave($__internal_a12db8416a4cb4ac846d0935cd3bb874428825223862cba2df5a1ebc6884e9bc_prof);

    }

    // line 93
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_87fbb9a28424c4ef8e27d930d1f95268b0fdd19d4d6d39f977b3914d4da26117 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_87fbb9a28424c4ef8e27d930d1f95268b0fdd19d4d6d39f977b3914d4da26117->enter($__internal_87fbb9a28424c4ef8e27d930d1f95268b0fdd19d4d6d39f977b3914d4da26117_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 94
        echo "<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"";
        // line 98
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
        Résident
    </a>
</div>



";
        
        $__internal_87fbb9a28424c4ef8e27d930d1f95268b0fdd19d4d6d39f977b3914d4da26117->leave($__internal_87fbb9a28424c4ef8e27d930d1f95268b0fdd19d4d6d39f977b3914d4da26117_prof);

    }

    public function getTemplateName()
    {
        return "menu/scolaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 108,  224 => 103,  216 => 98,  210 => 94,  204 => 93,  186 => 81,  182 => 80,  178 => 79,  174 => 78,  160 => 67,  156 => 66,  152 => 65,  148 => 64,  131 => 50,  127 => 49,  123 => 48,  119 => 47,  105 => 36,  101 => 35,  97 => 34,  93 => 33,  79 => 22,  75 => 21,  71 => 20,  67 => 19,  54 => 8,  48 => 7,  36 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}



{% block body_id 'menu_scolaire' %}

{% block main %}
<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.lundiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.lundiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mardiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mardiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mercrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mercrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediDessert }}</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.jeudiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.jeudiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.vendrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.vendrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediDessert }}</div>
                        </div>
                    </div>
                </div>


            </div>



{% endblock %}

{% block sidebar %}
<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"{{ path('externe') }}\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"{{ path('scolaire') }}\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"{{ path('resident') }}\" >
        Résident
    </a>
</div>



{% endblock %}
", "menu/scolaire.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\menu\\scolaire.html.twig");
    }
}
