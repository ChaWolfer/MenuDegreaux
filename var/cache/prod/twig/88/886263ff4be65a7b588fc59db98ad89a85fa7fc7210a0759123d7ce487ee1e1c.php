<?php

/* base.html.twig */
class __TwigTemplate_6eb9f46ca64833dad4c0fc6d24a1431bc8b43ae6459bc32cb3c144646aa936e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Roboto:300\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.png"), "html", null, true);
        echo "\" >
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/css_sass/css/style.css"), "html", null, true);
        echo "\">
        ";
        // line 19
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>

    <body id=\"";
        // line 24
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 26
        $this->displayBlock('header', $context, $blocks);
        // line 145
        echo "
        <div class=\"container body-container\">
            ";
        // line 147
        $this->displayBlock('body', $context, $blocks);
        // line 161
        echo "        </div>

        ";
        // line 163
        $this->displayBlock('footer', $context, $blocks);
        // line 174
        echo "
        ";
        // line 175
        $this->displayBlock('javascripts', $context, $blocks);
        // line 184
        echo "

    </body>
</html>
";
    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        echo "Menu Degreaux";
    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        // line 20
        echo "
        ";
    }

    // line 24
    public function block_body_id($context, array $blocks = array())
    {
    }

    // line 26
    public function block_header($context, array $blocks = array())
    {
        // line 27
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                ";
        // line 45
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 134
        echo "




                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
    }

    // line 45
    public function block_header_navigation_links($context, array $blocks = array())
    {
        // line 46
        echo "
                                    ";
        // line 47
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 48
            echo "                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      ";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/profile"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/resseting"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    ";
            // line 64
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
                // line 65
                echo "                                                        <li>
                                                            <a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/{_locale}/register"), "html", null, true);
                echo "\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    ";
            }
            // line 71
            echo "                                                  </ul>
                                                </li>

                                    ";
        }
        // line 75
        echo "
                                    <li>
                                        <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\">
                                                Homepage
                                            </i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"> </i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\">
                                            </i>
                                             Imprimer
                                        </a>
                                    </li>




                                    ";
        // line 100
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 101
            echo "                                        <li>
                                            <a href=\"";
            // line 102
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("{_locale}adminSonata"), "html", null, true);
            echo "\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    ";
        }
        // line 108
        echo "
                                    ";
        // line 109
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 110
            echo "                                        <li>
                                            <a href=\"";
            // line 111
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                ";
            // line 113
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                        ";
        } else {
            // line 117
            echo "                                        <li>
                                            <a href=\"";
            // line 118
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                ";
            // line 120
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                    ";
        }
        // line 124
        echo "
                                    ";
        // line 125
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : null), "session", array()), "flashBag", array()), "all", array()));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 126
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 127
                echo "                                            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                                ";
                // line 128
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                            </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 131
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 132
        echo "
                                ";
    }

    // line 147
    public function block_body($context, array $blocks = array())
    {
        // line 148
        echo "                ";
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "
                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-9\">
                        ";
        // line 151
        $this->displayBlock('main', $context, $blocks);
        // line 153
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-sm-3\">
                        ";
        // line 156
        $this->displayBlock('sidebar', $context, $blocks);
        // line 158
        echo "                    </div>
                </div>
            ";
    }

    // line 151
    public function block_main($context, array $blocks = array())
    {
        // line 152
        echo "                        ";
    }

    // line 156
    public function block_sidebar($context, array $blocks = array())
    {
        // line 157
        echo "                        ";
    }

    // line 163
    public function block_footer($context, array $blocks = array())
    {
        // line 164
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; ";
        // line 168
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
    }

    // line 175
    public function block_javascripts($context, array $blocks = array())
    {
        // line 176
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 181
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 182
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  434 => 182,  430 => 181,  426 => 180,  422 => 179,  418 => 178,  414 => 177,  409 => 176,  406 => 175,  396 => 168,  390 => 164,  387 => 163,  383 => 157,  380 => 156,  376 => 152,  373 => 151,  367 => 158,  365 => 156,  360 => 153,  358 => 151,  351 => 148,  348 => 147,  343 => 132,  337 => 131,  328 => 128,  323 => 127,  318 => 126,  314 => 125,  311 => 124,  304 => 120,  299 => 118,  296 => 117,  289 => 113,  284 => 111,  281 => 110,  279 => 109,  276 => 108,  267 => 102,  264 => 101,  262 => 100,  236 => 77,  232 => 75,  226 => 71,  218 => 66,  215 => 65,  213 => 64,  206 => 60,  198 => 55,  190 => 50,  186 => 48,  184 => 47,  181 => 46,  178 => 45,  164 => 134,  162 => 45,  145 => 31,  139 => 27,  136 => 26,  131 => 24,  126 => 20,  123 => 19,  117 => 6,  109 => 184,  107 => 175,  104 => 174,  102 => 163,  98 => 161,  96 => 147,  92 => 145,  90 => 26,  85 => 24,  81 => 22,  79 => 19,  75 => 18,  70 => 16,  66 => 15,  62 => 14,  58 => 13,  54 => 12,  50 => 11,  46 => 10,  39 => 6,  32 => 2,  29 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "base.html.twig", "/home/lievininqd/restauration/app/Resources/views/base.html.twig");
    }
}
