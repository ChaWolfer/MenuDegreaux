<?php

/* ::base.html.twig */
class __TwigTemplate_f32803aa4b14d10b841599649a817dba174c6bf2f25eea2a0576223c725fcc24 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_504d2233e2a734bf4a9e8fdefcec858acfc0b84438dcf4bf0bdc7d2ea7cf946e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_504d2233e2a734bf4a9e8fdefcec858acfc0b84438dcf4bf0bdc7d2ea7cf946e->enter($__internal_504d2233e2a734bf4a9e8fdefcec858acfc0b84438dcf4bf0bdc7d2ea7cf946e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array()), "html", null, true);
        echo "\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>";
        // line 6
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        // line 10
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.png"), "html", null, true);
        echo "\" >
        <link rel=\"stylesheet\" href=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-flatly-3.3.7.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-awesome-4.6.3.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 13
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/font-lato.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-datetimepicker.min.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/highlight-solarized-light.css"), "html", null, true);
        echo "\">
        <link rel=\"stylesheet\" href=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/bootstrap-tagsinput.css"), "html", null, true);
        echo "\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/css_sass/css/style.css"), "html", null, true);
        echo "\">
        ";
        // line 19
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 22
        echo "    </head>

    <body id=\"";
        // line 24
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">

        ";
        // line 26
        $this->displayBlock('header', $context, $blocks);
        // line 143
        echo "
        <div class=\"container body-container\">
            ";
        // line 145
        $this->displayBlock('body', $context, $blocks);
        // line 159
        echo "        </div>

        ";
        // line 161
        $this->displayBlock('footer', $context, $blocks);
        // line 172
        echo "
        ";
        // line 173
        $this->displayBlock('javascripts', $context, $blocks);
        // line 182
        echo "

    </body>
</html>
";
        
        $__internal_504d2233e2a734bf4a9e8fdefcec858acfc0b84438dcf4bf0bdc7d2ea7cf946e->leave($__internal_504d2233e2a734bf4a9e8fdefcec858acfc0b84438dcf4bf0bdc7d2ea7cf946e_prof);

    }

    // line 6
    public function block_title($context, array $blocks = array())
    {
        $__internal_cf85292c8e278ac6926c936029074067be77d8669c02179908615be01ff5008c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf85292c8e278ac6926c936029074067be77d8669c02179908615be01ff5008c->enter($__internal_cf85292c8e278ac6926c936029074067be77d8669c02179908615be01ff5008c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Menu Degreaux";
        
        $__internal_cf85292c8e278ac6926c936029074067be77d8669c02179908615be01ff5008c->leave($__internal_cf85292c8e278ac6926c936029074067be77d8669c02179908615be01ff5008c_prof);

    }

    // line 19
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_25290533a1af01487aafe45597d087d65abf6f4f8ce0ef5f224d9a6deaac4961 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25290533a1af01487aafe45597d087d65abf6f4f8ce0ef5f224d9a6deaac4961->enter($__internal_25290533a1af01487aafe45597d087d65abf6f4f8ce0ef5f224d9a6deaac4961_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 20
        echo "
        ";
        
        $__internal_25290533a1af01487aafe45597d087d65abf6f4f8ce0ef5f224d9a6deaac4961->leave($__internal_25290533a1af01487aafe45597d087d65abf6f4f8ce0ef5f224d9a6deaac4961_prof);

    }

    // line 24
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_f29ede77e2a5423de24cbd58fc483f810b511f86c098576e89303dbe94ce4a5e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f29ede77e2a5423de24cbd58fc483f810b511f86c098576e89303dbe94ce4a5e->enter($__internal_f29ede77e2a5423de24cbd58fc483f810b511f86c098576e89303dbe94ce4a5e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_f29ede77e2a5423de24cbd58fc483f810b511f86c098576e89303dbe94ce4a5e->leave($__internal_f29ede77e2a5423de24cbd58fc483f810b511f86c098576e89303dbe94ce4a5e_prof);

    }

    // line 26
    public function block_header($context, array $blocks = array())
    {
        $__internal_e21004d78927358a9450d53c195828400542d1713b7344d654137a1b0b0888ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e21004d78927358a9450d53c195828400542d1713b7344d654137a1b0b0888ef->enter($__internal_e21004d78927358a9450d53c195828400542d1713b7344d654137a1b0b0888ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 27
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                ";
        // line 45
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 132
        echo "




                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_e21004d78927358a9450d53c195828400542d1713b7344d654137a1b0b0888ef->leave($__internal_e21004d78927358a9450d53c195828400542d1713b7344d654137a1b0b0888ef_prof);

    }

    // line 45
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_6de9411d91c70346a10b595a38fbbe1a0af63c31df8259e51e3f047864f0c3e4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6de9411d91c70346a10b595a38fbbe1a0af63c31df8259e51e3f047864f0c3e4->enter($__internal_6de9411d91c70346a10b595a38fbbe1a0af63c31df8259e51e3f047864f0c3e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 46
        echo "
                                    ";
        // line 47
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 48
            echo "                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      ";
            // line 50
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logged_in_as", array("%username%" => $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()), "username", array())), "FOSUserBundle"), "html", null, true);
            echo "
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/profile"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"";
            // line 60
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/resseting"), "html", null, true);
            echo "\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    ";
            // line 64
            if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_ADMIN")) {
                // line 65
                echo "                                                        <li>
                                                            <a href=\"";
                // line 66
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/register"), "html", null, true);
                echo "\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    ";
            }
            // line 71
            echo "                                                  </ul>
                                                </li>

                                    ";
        }
        // line 75
        echo "
                                    <li>
                                        <a href=\"";
        // line 77
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    ";
        // line 98
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_USER")) {
            // line 99
            echo "                                        <li>
                                            <a href=\"";
            // line 100
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("/adminSonata"), "html", null, true);
            echo "\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    ";
        }
        // line 106
        echo "
                                    ";
        // line 107
        if ($this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("IS_AUTHENTICATED_REMEMBERED")) {
            // line 108
            echo "                                        <li>
                                            <a href=\"";
            // line 109
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_logout");
            echo "\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                ";
            // line 111
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.logout", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                        ";
        } else {
            // line 115
            echo "                                        <li>
                                            <a href=\"";
            // line 116
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("fos_user_security_login");
            echo "\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                ";
            // line 118
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("layout.login", array(), "FOSUserBundle"), "html", null, true);
            echo "
                                            </a>
                                        </li>
                                    ";
        }
        // line 122
        echo "
                                    ";
        // line 123
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "session", array()), "flashBag", array()), "all", array()));
        foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
            // line 124
            echo "                                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["messages"]);
            foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                // line 125
                echo "                                            <div class=\"";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\">
                                                ";
                // line 126
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($context["message"], array(), "FOSUserBundle"), "html", null, true);
                echo "
                                            </div>
                                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 129
            echo "                                    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 130
        echo "
                                ";
        
        $__internal_6de9411d91c70346a10b595a38fbbe1a0af63c31df8259e51e3f047864f0c3e4->leave($__internal_6de9411d91c70346a10b595a38fbbe1a0af63c31df8259e51e3f047864f0c3e4_prof);

    }

    // line 145
    public function block_body($context, array $blocks = array())
    {
        $__internal_3e7180892180080d4184885e87e385a15b36030cb571035bd71c63957c9458ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3e7180892180080d4184885e87e385a15b36030cb571035bd71c63957c9458ef->enter($__internal_3e7180892180080d4184885e87e385a15b36030cb571035bd71c63957c9458ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 146
        echo "                ";
        echo twig_include($this->env, $context, "default/_flash_messages.html.twig");
        echo "
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        ";
        // line 149
        $this->displayBlock('main', $context, $blocks);
        // line 151
        echo "                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        ";
        // line 154
        $this->displayBlock('sidebar', $context, $blocks);
        // line 156
        echo "                    </div>
                </div>
            ";
        
        $__internal_3e7180892180080d4184885e87e385a15b36030cb571035bd71c63957c9458ef->leave($__internal_3e7180892180080d4184885e87e385a15b36030cb571035bd71c63957c9458ef_prof);

    }

    // line 149
    public function block_main($context, array $blocks = array())
    {
        $__internal_5023c951f26beaed30d4ffaa147da4cf0bf0dc128345ac08daf92b90380e7ad9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5023c951f26beaed30d4ffaa147da4cf0bf0dc128345ac08daf92b90380e7ad9->enter($__internal_5023c951f26beaed30d4ffaa147da4cf0bf0dc128345ac08daf92b90380e7ad9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 150
        echo "                        ";
        
        $__internal_5023c951f26beaed30d4ffaa147da4cf0bf0dc128345ac08daf92b90380e7ad9->leave($__internal_5023c951f26beaed30d4ffaa147da4cf0bf0dc128345ac08daf92b90380e7ad9_prof);

    }

    // line 154
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_172e25475195d36e4df25704ae7c11e81999dffba687551d943a4ae8668068c9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_172e25475195d36e4df25704ae7c11e81999dffba687551d943a4ae8668068c9->enter($__internal_172e25475195d36e4df25704ae7c11e81999dffba687551d943a4ae8668068c9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 155
        echo "                        ";
        
        $__internal_172e25475195d36e4df25704ae7c11e81999dffba687551d943a4ae8668068c9->leave($__internal_172e25475195d36e4df25704ae7c11e81999dffba687551d943a4ae8668068c9_prof);

    }

    // line 161
    public function block_footer($context, array $blocks = array())
    {
        $__internal_5161abd8dfe6ffcc64f06a6c611caf0aa8f70b05363ab1b62c2b5d52fb1d38c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5161abd8dfe6ffcc64f06a6c611caf0aa8f70b05363ab1b62c2b5d52fb1d38c7->enter($__internal_5161abd8dfe6ffcc64f06a6c611caf0aa8f70b05363ab1b62c2b5d52fb1d38c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 162
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; ";
        // line 166
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_5161abd8dfe6ffcc64f06a6c611caf0aa8f70b05363ab1b62c2b5d52fb1d38c7->leave($__internal_5161abd8dfe6ffcc64f06a6c611caf0aa8f70b05363ab1b62c2b5d52fb1d38c7_prof);

    }

    // line 173
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_ebb3a8cc71b13e3d1f753f8e7625dd4f3d3068786ec2530d60746c92974c97a7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ebb3a8cc71b13e3d1f753f8e7625dd4f3d3068786ec2530d60746c92974c97a7->enter($__internal_ebb3a8cc71b13e3d1f753f8e7625dd4f3d3068786ec2530d60746c92974c97a7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 174
        echo "            <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/jquery-2.2.4.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 175
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/moment.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 176
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-3.3.7.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 177
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/highlight.pack.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 178
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-datetimepicker.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 179
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/bootstrap-tagsinput.min.js"), "html", null, true);
        echo "\"></script>
            <script src=\"";
        // line 180
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/main.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_ebb3a8cc71b13e3d1f753f8e7625dd4f3d3068786ec2530d60746c92974c97a7->leave($__internal_ebb3a8cc71b13e3d1f753f8e7625dd4f3d3068786ec2530d60746c92974c97a7_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  495 => 180,  491 => 179,  487 => 178,  483 => 177,  479 => 176,  475 => 175,  470 => 174,  464 => 173,  451 => 166,  445 => 162,  439 => 161,  432 => 155,  426 => 154,  419 => 150,  413 => 149,  404 => 156,  402 => 154,  397 => 151,  395 => 149,  388 => 146,  382 => 145,  374 => 130,  368 => 129,  359 => 126,  354 => 125,  349 => 124,  345 => 123,  342 => 122,  335 => 118,  330 => 116,  327 => 115,  320 => 111,  315 => 109,  312 => 108,  310 => 107,  307 => 106,  298 => 100,  295 => 99,  293 => 98,  269 => 77,  265 => 75,  259 => 71,  251 => 66,  248 => 65,  246 => 64,  239 => 60,  231 => 55,  223 => 50,  219 => 48,  217 => 47,  214 => 46,  208 => 45,  191 => 132,  189 => 45,  172 => 31,  166 => 27,  160 => 26,  149 => 24,  141 => 20,  135 => 19,  123 => 6,  112 => 182,  110 => 173,  107 => 172,  105 => 161,  101 => 159,  99 => 145,  95 => 143,  93 => 26,  88 => 24,  84 => 22,  82 => 19,  78 => 18,  73 => 16,  69 => 15,  65 => 14,  61 => 13,  57 => 12,  53 => 11,  49 => 10,  42 => 6,  35 => 2,  32 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html lang=\"{{ app.request.locale }}\">
    <head>
        <meta charset=\"UTF-8\" />
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"/>
        <title>{% block title %}Menu Degreaux{% endblock %}</title>
        <!-- police -->
        <link href=\"https://fonts.googleapis.com/css?family=Dosis\" rel=\"stylesheet\">
        <!-- Favicon -->
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.png') }}\" >
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-flatly-3.3.7.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-awesome-4.6.3.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/font-lato.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-datetimepicker.min.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/highlight-solarized-light.css') }}\">
        <link rel=\"stylesheet\" href=\"{{ asset('css/bootstrap-tagsinput.css') }}\">
        <!-- style perso -->
        <link rel=\"stylesheet\" href=\"{{ asset('css/css_sass/css/style.css') }}\">
        {% block stylesheets %}

        {% endblock %}
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">

        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Menu Degreaux
                            </a>
                            <button type=\"button\" class=\"navbar-toggle\"
                                    data-toggle=\"collapse\"
                                    data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Menu</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">
                                {% block header_navigation_links %}

                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li class=\"dropdown\">
                                                  <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\" role=\"button\" aria-haspopup=\"true\" aria-expanded=\"false\">
                                                      {{ 'layout.logged_in_as'|trans({'%username%': app.user.username}, 'FOSUserBundle') }}
                                                       <span class=\"caret\"></span>
                                                   </a>
                                                  <ul class=\"dropdown-menu\">
                                                    <li>
                                                        <a href=\"{{ asset('/profile') }}\">
                                                            <i class=\"fa fa-user\" aria-hidden=\"true\"></i> Profil
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href=\"{{ asset('/resseting') }}\">
                                                            <i class=\"fa fa-key\" aria-hidden=\"true\"></i> Changer le mot de passe
                                                        </a>
                                                    </li>
                                                    {% if is_granted(\"ROLE_ADMIN\") %}
                                                        <li>
                                                            <a href=\"{{ asset('/register') }}\">
                                                                <i class=\"fa fa-user-plus\" aria-hidden=\"true\"></i> Ajouter un utilisateur
                                                            </a>
                                                        </li>
                                                    {% endif %}
                                                  </ul>
                                                </li>

                                    {% endif %}

                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" aria-hidden=\"true\"></i>
                                            Homepage
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"http://lievin.fr/\" target=\"_blank\">
                                            <i class=\"fa fa-reply \" aria-hidden=\"true\"></i>
                                            Ville de Liévin
                                        </a>
                                    </li>
                                    <li>
                                        <a href=\"javascript:window.print()\">
                                            <i class=\"fa fa-print \" aria-hidden=\"true\"></i>
                                             Imprimer
                                        </a>
                                    </li>




                                    {% if is_granted(\"ROLE_USER\") %}
                                        <li>
                                            <a href=\"{{ asset('/adminSonata') }}\">

                                                <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> Gestion des menus
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% if is_granted(\"IS_AUTHENTICATED_REMEMBERED\") %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_logout') }}\">
                                                <i class=\"fa fa-sign-out\" aria-hidden=\"true\"></i>
                                                {{ 'layout.logout'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                        {% else %}
                                        <li>
                                            <a href=\"{{ path('fos_user_security_login') }}\">
                                                <i class=\"fa fa-sign-in\" aria-hidden=\"true\"></i>
                                                {{ 'layout.login'|trans({}, 'FOSUserBundle') }}
                                            </a>
                                        </li>
                                    {% endif %}

                                    {% for type, messages in app.session.flashBag.all %}
                                        {% for message in messages %}
                                            <div class=\"{{ type }}\">
                                                {{ message|trans({}, 'FOSUserBundle') }}
                                            </div>
                                        {% endfor %}
                                    {% endfor %}

                                {% endblock %}





                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                {{ include('default/_flash_messages.html.twig') }}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-md-9 col-xs-12\">
                        {% block main %}
                        {% endblock %}
                    </div>

                    <div id=\"sidebar\" class=\"col-md-3 col-xs-12\">
                        {% block sidebar %}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-12 col-xs-12\">
                            <p>&copy; {{ 'now'|date('Y') }} - Liévin</p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            <script src=\"{{ asset('js/jquery-2.2.4.min.js') }}\"></script>
            <script src=\"{{ asset('js/moment.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-3.3.7.min.js') }}\"></script>
            <script src=\"{{ asset('js/highlight.pack.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-datetimepicker.min.js') }}\"></script>
            <script src=\"{{ asset('js/bootstrap-tagsinput.min.js') }}\"></script>
            <script src=\"{{ asset('js/main.js') }}\"></script>
        {% endblock %}


    </body>
</html>
", "::base.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/base.html.twig");
    }
}
