<?php

/* menu/scolaire.html.twig */
class __TwigTemplate_776348dda5bb6e5a7c8043e35e28e6a8f99fa2027e5fd02b6320bd7a88f8bd65 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "menu/scolaire.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_body_id($context, array $blocks = array())
    {
        echo "menu_scolaire";
    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        // line 8
        echo "<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "lundiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "lundiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "lundiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mardiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mardiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mardiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : null), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>


            </div>



";
    }

    // line 93
    public function block_sidebar($context, array $blocks = array())
    {
        // line 94
        echo "<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"";
        // line 98
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
        Résident
    </a>
</div>



";
    }

    public function getTemplateName()
    {
        return "menu/scolaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 108,  203 => 103,  195 => 98,  189 => 94,  186 => 93,  171 => 81,  167 => 80,  163 => 79,  159 => 78,  145 => 67,  141 => 66,  137 => 65,  133 => 64,  116 => 50,  112 => 49,  108 => 48,  104 => 47,  90 => 36,  86 => 35,  82 => 34,  78 => 33,  64 => 22,  60 => 21,  56 => 20,  52 => 19,  39 => 8,  36 => 7,  30 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("", "menu/scolaire.html.twig", "/home/lievininqd/restauration/app/Resources/views/menu/scolaire.html.twig");
    }
}
