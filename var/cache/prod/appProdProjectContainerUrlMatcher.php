<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appProdProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appProdProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        // security_login
        if (preg_match('#^/(?P<_locale>fr)/login$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'security_login')), array (  '_controller' => 'AppBundle\\Controller\\SecurityController::loginAction',  '_locale' => 'fr',));
        }

        // security_logout
        if (preg_match('#^/(?P<_locale>fr)/logout$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'security_logout')), array (  '_controller' => 'AppBundle\\Controller\\SecurityController::logoutAction',  '_locale' => 'fr',));
        }

        // externe
        if (preg_match('#^/(?P<_locale>fr)/menu/externe$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_externe;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'externe')), array (  '_controller' => 'AppBundle\\Controller\\menuController::ExterneAction',  '_locale' => 'fr',));
        }
        not_externe:

        // scolaire
        if (preg_match('#^/(?P<_locale>fr)/menu/scolaire$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_scolaire;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'scolaire')), array (  '_controller' => 'AppBundle\\Controller\\menuController::ScolaireAction',  '_locale' => 'fr',));
        }
        not_scolaire:

        // resident
        if (preg_match('#^/(?P<_locale>fr)/menu/resident$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_resident;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'resident')), array (  '_controller' => 'AppBundle\\Controller\\menuController::ResidentAction',  '_locale' => 'fr',));
        }
        not_resident:

        // homepage
        if (preg_match('#^/(?P<_locale>fr)?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'homepage')), array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\TemplateController::templateAction',  'template' => 'menu/homepage.html.twig',  '_locale' => 'fr',));
        }

        // sonata_admin_redirect
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/?$#s', $pathinfo, $matches)) {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'sonata_admin_redirect');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_redirect')), array (  '_controller' => 'Symfony\\Bundle\\FrameworkBundle\\Controller\\RedirectController::redirectAction',  'route' => 'sonata_admin_dashboard',  'permanent' => 'true',));
        }

        // sonata_admin_dashboard
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/dashboard$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_dashboard')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::dashboardAction',));
        }

        // sonata_admin_retrieve_form_element
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/core/get\\-form\\-field\\-element$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_retrieve_form_element')), array (  '_controller' => 'sonata.admin.controller.admin:retrieveFormFieldElementAction',));
        }

        // sonata_admin_append_form_element
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/core/append\\-form\\-field\\-element$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_append_form_element')), array (  '_controller' => 'sonata.admin.controller.admin:appendFormFieldElementAction',));
        }

        // sonata_admin_short_object_information
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/core/get\\-short\\-object\\-description(?:\\.(?P<_format>html|json))?$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_short_object_information')), array (  '_controller' => 'sonata.admin.controller.admin:getShortObjectDescriptionAction',  '_format' => 'html',));
        }

        // sonata_admin_set_object_field_value
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/core/set\\-object\\-field\\-value$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_set_object_field_value')), array (  '_controller' => 'sonata.admin.controller.admin:setObjectFieldValueAction',));
        }

        // sonata_admin_search
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/search$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_search')), array (  '_controller' => 'Sonata\\AdminBundle\\Controller\\CoreController::searchAction',));
        }

        // sonata_admin_retrieve_autocomplete_items
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/core/get\\-autocomplete\\-items$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'sonata_admin_retrieve_autocomplete_items')), array (  '_controller' => 'sonata.admin.controller.admin:retrieveAutocompleteItemsAction',));
        }

        // admin_app_menumidi_list
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menumidi/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_list')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.menumidi',  '_sonata_name' => 'admin_app_menumidi_list',));
        }

        // admin_app_menumidi_create
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menumidi/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_create')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.menumidi',  '_sonata_name' => 'admin_app_menumidi_create',));
        }

        // admin_app_menumidi_batch
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menumidi/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_batch')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.menumidi',  '_sonata_name' => 'admin_app_menumidi_batch',));
        }

        // admin_app_menumidi_edit
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menumidi/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_edit')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.menumidi',  '_sonata_name' => 'admin_app_menumidi_edit',));
        }

        // admin_app_menumidi_delete
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menumidi/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_delete')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.menumidi',  '_sonata_name' => 'admin_app_menumidi_delete',));
        }

        // admin_app_menumidi_show
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menumidi/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_show')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.menumidi',  '_sonata_name' => 'admin_app_menumidi_show',));
        }

        // admin_app_menumidi_export
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menumidi/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menumidi_export')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.menumidi',  '_sonata_name' => 'admin_app_menumidi_export',));
        }

        // admin_app_menusoir_list
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menusoir/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_list')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.menusoir',  '_sonata_name' => 'admin_app_menusoir_list',));
        }

        // admin_app_menusoir_create
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menusoir/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_create')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.menusoir',  '_sonata_name' => 'admin_app_menusoir_create',));
        }

        // admin_app_menusoir_batch
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menusoir/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_batch')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.menusoir',  '_sonata_name' => 'admin_app_menusoir_batch',));
        }

        // admin_app_menusoir_edit
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menusoir/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_edit')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.menusoir',  '_sonata_name' => 'admin_app_menusoir_edit',));
        }

        // admin_app_menusoir_delete
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menusoir/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_delete')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.menusoir',  '_sonata_name' => 'admin_app_menusoir_delete',));
        }

        // admin_app_menusoir_show
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menusoir/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_show')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.menusoir',  '_sonata_name' => 'admin_app_menusoir_show',));
        }

        // admin_app_menusoir_export
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menusoir/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menusoir_export')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.menusoir',  '_sonata_name' => 'admin_app_menusoir_export',));
        }

        // admin_app_menuscolaire_list
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menuscolaire/list$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_list')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::listAction',  '_sonata_admin' => 'admin.menuscolaire',  '_sonata_name' => 'admin_app_menuscolaire_list',));
        }

        // admin_app_menuscolaire_create
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menuscolaire/create$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_create')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::createAction',  '_sonata_admin' => 'admin.menuscolaire',  '_sonata_name' => 'admin_app_menuscolaire_create',));
        }

        // admin_app_menuscolaire_batch
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menuscolaire/batch$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_batch')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::batchAction',  '_sonata_admin' => 'admin.menuscolaire',  '_sonata_name' => 'admin_app_menuscolaire_batch',));
        }

        // admin_app_menuscolaire_edit
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menuscolaire/(?P<id>[^/]++)/edit$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_edit')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::editAction',  '_sonata_admin' => 'admin.menuscolaire',  '_sonata_name' => 'admin_app_menuscolaire_edit',));
        }

        // admin_app_menuscolaire_delete
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menuscolaire/(?P<id>[^/]++)/delete$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_delete')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::deleteAction',  '_sonata_admin' => 'admin.menuscolaire',  '_sonata_name' => 'admin_app_menuscolaire_delete',));
        }

        // admin_app_menuscolaire_show
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menuscolaire/(?P<id>[^/]++)/show$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_show')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::showAction',  '_sonata_admin' => 'admin.menuscolaire',  '_sonata_name' => 'admin_app_menuscolaire_show',));
        }

        // admin_app_menuscolaire_export
        if (preg_match('#^/(?P<_locale>[^/]++)/adminSonata/app/menuscolaire/export$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'admin_app_menuscolaire_export')), array (  '_controller' => 'AppBundle\\Controller\\CRUDController::exportAction',  '_sonata_admin' => 'admin.menuscolaire',  '_sonata_name' => 'admin_app_menuscolaire_export',));
        }

        if (0 === strpos($pathinfo, '/log')) {
            if (0 === strpos($pathinfo, '/login')) {
                // fos_user_security_login
                if ($pathinfo === '/login') {
                    if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                        $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                        goto not_fos_user_security_login;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::loginAction',  '_route' => 'fos_user_security_login',);
                }
                not_fos_user_security_login:

                // fos_user_security_check
                if ($pathinfo === '/login_check') {
                    if ($this->context->getMethod() != 'POST') {
                        $allow[] = 'POST';
                        goto not_fos_user_security_check;
                    }

                    return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::checkAction',  '_route' => 'fos_user_security_check',);
                }
                not_fos_user_security_check:

            }

            // fos_user_security_logout
            if ($pathinfo === '/logout') {
                if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                    $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                    goto not_fos_user_security_logout;
                }

                return array (  '_controller' => 'FOS\\UserBundle\\Controller\\SecurityController::logoutAction',  '_route' => 'fos_user_security_logout',);
            }
            not_fos_user_security_logout:

        }

        // fos_user_profile_show
        if (preg_match('#^/(?P<_locale>[^/]++)/profile/?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_fos_user_profile_show;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'fos_user_profile_show');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_profile_show')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::showAction',));
        }
        not_fos_user_profile_show:

        // fos_user_profile_edit
        if (preg_match('#^/(?P<_locale>[^/]++)/profile/edit$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_profile_edit;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_profile_edit')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ProfileController::editAction',));
        }
        not_fos_user_profile_edit:

        // fos_user_registration_register
        if (preg_match('#^/(?P<_locale>[^/]++)/register/?$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_registration_register;
            }

            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'fos_user_registration_register');
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_register')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::registerAction',));
        }
        not_fos_user_registration_register:

        // fos_user_registration_check_email
        if (preg_match('#^/(?P<_locale>[^/]++)/register/check\\-email$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_fos_user_registration_check_email;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_check_email')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::checkEmailAction',));
        }
        not_fos_user_registration_check_email:

        // fos_user_registration_confirm
        if (preg_match('#^/(?P<_locale>[^/]++)/register/confirm/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_fos_user_registration_confirm;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirm')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmAction',));
        }
        not_fos_user_registration_confirm:

        // fos_user_registration_confirmed
        if (preg_match('#^/(?P<_locale>[^/]++)/register/confirmed$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_fos_user_registration_confirmed;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_registration_confirmed')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\RegistrationController::confirmedAction',));
        }
        not_fos_user_registration_confirmed:

        // fos_user_resetting_request
        if (preg_match('#^/(?P<_locale>[^/]++)/resetting/request$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_fos_user_resetting_request;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_request')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::requestAction',));
        }
        not_fos_user_resetting_request:

        // fos_user_resetting_send_email
        if (preg_match('#^/(?P<_locale>[^/]++)/resetting/send\\-email$#s', $pathinfo, $matches)) {
            if ($this->context->getMethod() != 'POST') {
                $allow[] = 'POST';
                goto not_fos_user_resetting_send_email;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_send_email')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::sendEmailAction',));
        }
        not_fos_user_resetting_send_email:

        // fos_user_resetting_check_email
        if (preg_match('#^/(?P<_locale>[^/]++)/resetting/check\\-email$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'HEAD'));
                goto not_fos_user_resetting_check_email;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_check_email')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::checkEmailAction',));
        }
        not_fos_user_resetting_check_email:

        // fos_user_resetting_reset
        if (preg_match('#^/(?P<_locale>[^/]++)/resetting/reset/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_resetting_reset;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_resetting_reset')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ResettingController::resetAction',));
        }
        not_fos_user_resetting_reset:

        // fos_user_change_password
        if (preg_match('#^/(?P<_locale>[^/]++)/profile/change\\-password$#s', $pathinfo, $matches)) {
            if (!in_array($this->context->getMethod(), array('GET', 'POST', 'HEAD'))) {
                $allow = array_merge($allow, array('GET', 'POST', 'HEAD'));
                goto not_fos_user_change_password;
            }

            return $this->mergeDefaults(array_replace($matches, array('_route' => 'fos_user_change_password')), array (  '_controller' => 'FOS\\UserBundle\\Controller\\ChangePasswordController::changePasswordAction',));
        }
        not_fos_user_change_password:

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
