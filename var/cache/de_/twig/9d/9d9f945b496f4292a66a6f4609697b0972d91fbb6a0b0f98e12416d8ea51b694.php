<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_d7d2ed1995880bb02993edef7de01de1a28251b4b68fd5dc3d04ac0ae72b0780 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_02d7af25649324be24c3ab948c9bced01f5fc18406fd80ab39ccb07060ae4efd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02d7af25649324be24c3ab948c9bced01f5fc18406fd80ab39ccb07060ae4efd->enter($__internal_02d7af25649324be24c3ab948c9bced01f5fc18406fd80ab39ccb07060ae4efd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        $__internal_ff1785cff531781abd7d471640c22bd0f8cabd57ba23bf5c80153c8a1a1c08c8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff1785cff531781abd7d471640c22bd0f8cabd57ba23bf5c80153c8a1a1c08c8->enter($__internal_ff1785cff531781abd7d471640c22bd0f8cabd57ba23bf5c80153c8a1a1c08c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_02d7af25649324be24c3ab948c9bced01f5fc18406fd80ab39ccb07060ae4efd->leave($__internal_02d7af25649324be24c3ab948c9bced01f5fc18406fd80ab39ccb07060ae4efd_prof);

        
        $__internal_ff1785cff531781abd7d471640c22bd0f8cabd57ba23bf5c80153c8a1a1c08c8->leave($__internal_ff1785cff531781abd7d471640c22bd0f8cabd57ba23bf5c80153c8a1a1c08c8_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\search_widget.html.php");
    }
}
