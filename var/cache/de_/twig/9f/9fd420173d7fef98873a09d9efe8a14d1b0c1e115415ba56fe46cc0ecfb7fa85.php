<?php

/* @KnpMenu/menu.html.twig */
class __TwigTemplate_77878a9a6fd61d80e76db437b5590ff0784dc5aa6d9d10dc5e67323b7102ac28 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knp_menu.html.twig", "@KnpMenu/menu.html.twig", 1);
        $this->blocks = array(
            'label' => array($this, 'block_label'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knp_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d685dd883cc69a12765ae2acb5b1a355c99536ff5ee4610312e3488731e9eaf5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d685dd883cc69a12765ae2acb5b1a355c99536ff5ee4610312e3488731e9eaf5->enter($__internal_d685dd883cc69a12765ae2acb5b1a355c99536ff5ee4610312e3488731e9eaf5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@KnpMenu/menu.html.twig"));

        $__internal_e23edad5d4354d6f9a7ee02501891295e06f6847bcef813d93049ff54b8cc770 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e23edad5d4354d6f9a7ee02501891295e06f6847bcef813d93049ff54b8cc770->enter($__internal_e23edad5d4354d6f9a7ee02501891295e06f6847bcef813d93049ff54b8cc770_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@KnpMenu/menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d685dd883cc69a12765ae2acb5b1a355c99536ff5ee4610312e3488731e9eaf5->leave($__internal_d685dd883cc69a12765ae2acb5b1a355c99536ff5ee4610312e3488731e9eaf5_prof);

        
        $__internal_e23edad5d4354d6f9a7ee02501891295e06f6847bcef813d93049ff54b8cc770->leave($__internal_e23edad5d4354d6f9a7ee02501891295e06f6847bcef813d93049ff54b8cc770_prof);

    }

    // line 3
    public function block_label($context, array $blocks = array())
    {
        $__internal_2bbd81ae9206bb426338281006a4889b017571a4aee3828dbaaf1d60db06f2b6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2bbd81ae9206bb426338281006a4889b017571a4aee3828dbaaf1d60db06f2b6->enter($__internal_2bbd81ae9206bb426338281006a4889b017571a4aee3828dbaaf1d60db06f2b6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_820348f5cb5f982264247af5067eb4df1956739f63643f19324195e53cc0ca62 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_820348f5cb5f982264247af5067eb4df1956739f63643f19324195e53cc0ca62->enter($__internal_820348f5cb5f982264247af5067eb4df1956739f63643f19324195e53cc0ca62_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        // line 4
        $context["translation_domain"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "translation_domain", 1 => "messages"), "method");
        // line 5
        $context["label"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "label", array());
        // line 6
        if ( !((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) {
            // line 7
            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "translation_params", 1 => array()), "method"), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
        }
        // line 9
        if (($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "allow_safe_labels", array()) && $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "extra", array(0 => "safe_label", 1 => false), "method"))) {
            echo (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"));
        } else {
            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
        }
        
        $__internal_820348f5cb5f982264247af5067eb4df1956739f63643f19324195e53cc0ca62->leave($__internal_820348f5cb5f982264247af5067eb4df1956739f63643f19324195e53cc0ca62_prof);

        
        $__internal_2bbd81ae9206bb426338281006a4889b017571a4aee3828dbaaf1d60db06f2b6->leave($__internal_2bbd81ae9206bb426338281006a4889b017571a4aee3828dbaaf1d60db06f2b6_prof);

    }

    public function getTemplateName()
    {
        return "@KnpMenu/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 9,  55 => 7,  53 => 6,  51 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'knp_menu.html.twig' %}

{% block label %}
    {%- set translation_domain = item.extra('translation_domain', 'messages') -%}
    {%- set label = item.label -%}
    {%- if translation_domain is not same as(false) -%}
        {%- set label = label|trans(item.extra('translation_params', {}), translation_domain) -%}
    {%- endif -%}
    {%- if options.allow_safe_labels and item.extra('safe_label', false) %}{{ label|raw }}{% else %}{{ label }}{% endif -%}
{% endblock %}
", "@KnpMenu/menu.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\knplabs\\knp-menu-bundle\\Resources\\views\\menu.html.twig");
    }
}
