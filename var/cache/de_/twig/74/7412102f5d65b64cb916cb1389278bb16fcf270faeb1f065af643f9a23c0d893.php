<?php

/* :buttons:delete_button.html.twig */
class __TwigTemplate_9ed58eb814ad50471da03dc5d45a01a45beae8a7444119c74aed8af9bd3b4ca9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_073e5cd53f8da443ba943c1b734d96d53d90801422af1118ccc542ffe2067e0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_073e5cd53f8da443ba943c1b734d96d53d90801422af1118ccc542ffe2067e0f->enter($__internal_073e5cd53f8da443ba943c1b734d96d53d90801422af1118ccc542ffe2067e0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":buttons:delete_button.html.twig"));

        $__internal_7f423b115061d17f69fce3613b0ba22d3ec9a2227fe2ebbdca8c9578696167cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f423b115061d17f69fce3613b0ba22d3ec9a2227fe2ebbdca8c9578696167cf->enter($__internal_7f423b115061d17f69fce3613b0ba22d3ec9a2227fe2ebbdca8c9578696167cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":buttons:delete_button.html.twig"));

        // line 1
        if (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasAccess", array(0 => "delete", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method") && $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasRoute", array(0 => "delete"), "method"))) {
            // line 2
            echo "
    <a href=\"";
            // line 3
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
            echo "\" class=\"btn btn-sm btn-danger delete_link\" title=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action_delete", array(), "SonataAdminBundle"), "html", null, true);
            echo "\">

        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>

        Supprimer

    </a>

";
        }
        
        $__internal_073e5cd53f8da443ba943c1b734d96d53d90801422af1118ccc542ffe2067e0f->leave($__internal_073e5cd53f8da443ba943c1b734d96d53d90801422af1118ccc542ffe2067e0f_prof);

        
        $__internal_7f423b115061d17f69fce3613b0ba22d3ec9a2227fe2ebbdca8c9578696167cf->leave($__internal_7f423b115061d17f69fce3613b0ba22d3ec9a2227fe2ebbdca8c9578696167cf_prof);

    }

    public function getTemplateName()
    {
        return ":buttons:delete_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 3,  27 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if admin.hasAccess('delete', object) and admin.hasRoute('delete') %}

    <a href=\"{{ admin.generateObjectUrl('delete', object) }}\" class=\"btn btn-sm btn-danger delete_link\" title=\"{{ 'action_delete'|trans({}, 'SonataAdminBundle') }}\">

        <i class=\"fa fa-times\" aria-hidden=\"true\"></i>

        Supprimer

    </a>

{% endif %}
", ":buttons:delete_button.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/buttons/delete_button.html.twig");
    }
}
