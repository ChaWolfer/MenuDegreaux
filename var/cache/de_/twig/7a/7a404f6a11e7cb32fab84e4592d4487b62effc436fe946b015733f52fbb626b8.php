<?php

/* :form:layout.html.twig */
class __TwigTemplate_291dc11fe9afcde94cafbeb6046b65924fe8a64d8bfe0b942ec0f414c9117277 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("bootstrap_3_layout.html.twig", ":form:layout.html.twig", 1);
        $this->blocks = array(
            'form_errors' => array($this, 'block_form_errors'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "bootstrap_3_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_85124f4af952648b0ffa092bf34b1c5812111b39bc124800efe2d9fddfb0b012 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_85124f4af952648b0ffa092bf34b1c5812111b39bc124800efe2d9fddfb0b012->enter($__internal_85124f4af952648b0ffa092bf34b1c5812111b39bc124800efe2d9fddfb0b012_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":form:layout.html.twig"));

        $__internal_4f6549a7f515c17f6aa9fb7eb23dc5517e3daf94bda01d8dd4b80bc49ea92404 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4f6549a7f515c17f6aa9fb7eb23dc5517e3daf94bda01d8dd4b80bc49ea92404->enter($__internal_4f6549a7f515c17f6aa9fb7eb23dc5517e3daf94bda01d8dd4b80bc49ea92404_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":form:layout.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_85124f4af952648b0ffa092bf34b1c5812111b39bc124800efe2d9fddfb0b012->leave($__internal_85124f4af952648b0ffa092bf34b1c5812111b39bc124800efe2d9fddfb0b012_prof);

        
        $__internal_4f6549a7f515c17f6aa9fb7eb23dc5517e3daf94bda01d8dd4b80bc49ea92404->leave($__internal_4f6549a7f515c17f6aa9fb7eb23dc5517e3daf94bda01d8dd4b80bc49ea92404_prof);

    }

    // line 5
    public function block_form_errors($context, array $blocks = array())
    {
        $__internal_3fe39e11c379a14105b66292f95560c7da6208ee137d1e9d70f0f53baf2b4c20 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3fe39e11c379a14105b66292f95560c7da6208ee137d1e9d70f0f53baf2b4c20->enter($__internal_3fe39e11c379a14105b66292f95560c7da6208ee137d1e9d70f0f53baf2b4c20_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        $__internal_bb995080b72d741d2d3342443a5389c3a938a6de8b22dde6f9fa62aaa27f1044 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb995080b72d741d2d3342443a5389c3a938a6de8b22dde6f9fa62aaa27f1044->enter($__internal_bb995080b72d741d2d3342443a5389c3a938a6de8b22dde6f9fa62aaa27f1044_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "form_errors"));

        // line 6
        if ((twig_length_filter($this->env, (isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors"))) > 0)) {
            // line 7
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "<span class=\"help-block\">";
            } else {
                echo "<div class=\"alert alert-danger\">";
            }
            // line 8
            echo "        <ul class=\"list-unstyled\">";
            // line 9
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : $this->getContext($context, "errors")));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 11
                echo "            <li><span class=\"fa fa-exclamation-triangle\"></span> ";
                echo twig_escape_filter($this->env, $this->getAttribute($context["error"], "message", array()), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 13
            echo "</ul>
        ";
            // line 14
            if ($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "parent", array())) {
                echo "</span>";
            } else {
                echo "</div>";
            }
        }
        
        $__internal_bb995080b72d741d2d3342443a5389c3a938a6de8b22dde6f9fa62aaa27f1044->leave($__internal_bb995080b72d741d2d3342443a5389c3a938a6de8b22dde6f9fa62aaa27f1044_prof);

        
        $__internal_3fe39e11c379a14105b66292f95560c7da6208ee137d1e9d70f0f53baf2b4c20->leave($__internal_3fe39e11c379a14105b66292f95560c7da6208ee137d1e9d70f0f53baf2b4c20_prof);

    }

    public function getTemplateName()
    {
        return ":form:layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 14,  71 => 13,  63 => 11,  59 => 9,  57 => 8,  51 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'bootstrap_3_layout.html.twig' %}

{# Errors #}

{% block form_errors -%}
    {% if errors|length > 0 -%}
        {% if form.parent %}<span class=\"help-block\">{% else %}<div class=\"alert alert-danger\">{% endif %}
        <ul class=\"list-unstyled\">
        {%- for error in errors -%}
            {# use font-awesome icon library #}
            <li><span class=\"fa fa-exclamation-triangle\"></span> {{ error.message }}</li>
        {%- endfor -%}
        </ul>
        {% if form.parent %}</span>{% else %}</div>{% endif %}
    {%- endif %}
{%- endblock form_errors %}
", ":form:layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/form/layout.html.twig");
    }
}
