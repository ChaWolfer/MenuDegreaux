<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_71393c3412be821119e73505ea5cdf68e00bbec2692fb48722d588a0140fd086 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2b1e598a1f939d32c05fb0311930efd48f094bbaa951ff9eeb999d2cf38dd0b3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2b1e598a1f939d32c05fb0311930efd48f094bbaa951ff9eeb999d2cf38dd0b3->enter($__internal_2b1e598a1f939d32c05fb0311930efd48f094bbaa951ff9eeb999d2cf38dd0b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $__internal_29988fbe9369b0bfec02e00651c730bc10b2cacee6d13b3b32edbf0954b0ebfc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_29988fbe9369b0bfec02e00651c730bc10b2cacee6d13b3b32edbf0954b0ebfc->enter($__internal_29988fbe9369b0bfec02e00651c730bc10b2cacee6d13b3b32edbf0954b0ebfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2b1e598a1f939d32c05fb0311930efd48f094bbaa951ff9eeb999d2cf38dd0b3->leave($__internal_2b1e598a1f939d32c05fb0311930efd48f094bbaa951ff9eeb999d2cf38dd0b3_prof);

        
        $__internal_29988fbe9369b0bfec02e00651c730bc10b2cacee6d13b3b32edbf0954b0ebfc->leave($__internal_29988fbe9369b0bfec02e00651c730bc10b2cacee6d13b3b32edbf0954b0ebfc_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_e249cafee2db4f5ca9d419054176352a0a1ff386ce47a3da697e1f392d887dca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e249cafee2db4f5ca9d419054176352a0a1ff386ce47a3da697e1f392d887dca->enter($__internal_e249cafee2db4f5ca9d419054176352a0a1ff386ce47a3da697e1f392d887dca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_95310b87484e7765a90dfe3a46eda98958bc4c692068c884fa84adcf5619da52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95310b87484e7765a90dfe3a46eda98958bc4c692068c884fa84adcf5619da52->enter($__internal_95310b87484e7765a90dfe3a46eda98958bc4c692068c884fa84adcf5619da52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
";
        
        $__internal_95310b87484e7765a90dfe3a46eda98958bc4c692068c884fa84adcf5619da52->leave($__internal_95310b87484e7765a90dfe3a46eda98958bc4c692068c884fa84adcf5619da52_prof);

        
        $__internal_e249cafee2db4f5ca9d419054176352a0a1ff386ce47a3da697e1f392d887dca->leave($__internal_e249cafee2db4f5ca9d419054176352a0a1ff386ce47a3da697e1f392d887dca_prof);

    }

    // line 136
    public function block_title($context, array $blocks = array())
    {
        $__internal_bece920ce899a41dbaf7d570c7ae83261b3262600cefe50a64669571d703d0bc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_bece920ce899a41dbaf7d570c7ae83261b3262600cefe50a64669571d703d0bc->enter($__internal_bece920ce899a41dbaf7d570c7ae83261b3262600cefe50a64669571d703d0bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_7e4ea72faeeefa43b9b3a8231f4bf1766e39c9832cfb5916078f627154940810 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e4ea72faeeefa43b9b3a8231f4bf1766e39c9832cfb5916078f627154940810->enter($__internal_7e4ea72faeeefa43b9b3a8231f4bf1766e39c9832cfb5916078f627154940810_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 137
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_7e4ea72faeeefa43b9b3a8231f4bf1766e39c9832cfb5916078f627154940810->leave($__internal_7e4ea72faeeefa43b9b3a8231f4bf1766e39c9832cfb5916078f627154940810_prof);

        
        $__internal_bece920ce899a41dbaf7d570c7ae83261b3262600cefe50a64669571d703d0bc->leave($__internal_bece920ce899a41dbaf7d570c7ae83261b3262600cefe50a64669571d703d0bc_prof);

    }

    // line 140
    public function block_body($context, array $blocks = array())
    {
        $__internal_1db321f1a6386257c8fac7761cf82df974323553daaf25bea2fb2bcf69b8a3a6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1db321f1a6386257c8fac7761cf82df974323553daaf25bea2fb2bcf69b8a3a6->enter($__internal_1db321f1a6386257c8fac7761cf82df974323553daaf25bea2fb2bcf69b8a3a6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_56b9cbff4ae2e5bd21170ca11ff3e0a080d02d72cc857e56c1fa6d446a660e4f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56b9cbff4ae2e5bd21170ca11ff3e0a080d02d72cc857e56c1fa6d446a660e4f->enter($__internal_56b9cbff4ae2e5bd21170ca11ff3e0a080d02d72cc857e56c1fa6d446a660e4f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 141
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 141)->display($context);
        
        $__internal_56b9cbff4ae2e5bd21170ca11ff3e0a080d02d72cc857e56c1fa6d446a660e4f->leave($__internal_56b9cbff4ae2e5bd21170ca11ff3e0a080d02d72cc857e56c1fa6d446a660e4f_prof);

        
        $__internal_1db321f1a6386257c8fac7761cf82df974323553daaf25bea2fb2bcf69b8a3a6->leave($__internal_1db321f1a6386257c8fac7761cf82df974323553daaf25bea2fb2bcf69b8a3a6_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 141,  217 => 140,  200 => 137,  191 => 136,  51 => 4,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <style>
        .sf-reset .traces {
            padding-bottom: 14px;
        }
        .sf-reset .traces li {
            font-size: 12px;
            color: #868686;
            padding: 5px 4px;
            list-style-type: decimal;
            margin-left: 20px;
        }
        .sf-reset #logs .traces li.error {
            font-style: normal;
            color: #AA3333;
            background: #f9ecec;
        }
        .sf-reset #logs .traces li.warning {
            font-style: normal;
            background: #ffcc00;
        }
        /* fix for Opera not liking empty <li> */
        .sf-reset .traces li:after {
            content: \"\\00A0\";
        }
        .sf-reset .trace {
            border: 1px solid #D3D3D3;
            padding: 10px;
            overflow: auto;
            margin: 10px 0 20px;
        }
        .sf-reset .block-exception {
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
            border-radius: 16px;
            margin-bottom: 20px;
            background-color: #f6f6f6;
            border: 1px solid #dfdfdf;
            padding: 30px 28px;
            word-wrap: break-word;
            overflow: hidden;
        }
        .sf-reset .block-exception div {
            color: #313131;
            font-size: 10px;
        }
        .sf-reset .block-exception-detected .illustration-exception,
        .sf-reset .block-exception-detected .text-exception {
            float: left;
        }
        .sf-reset .block-exception-detected .illustration-exception {
            width: 152px;
        }
        .sf-reset .block-exception-detected .text-exception {
            width: 670px;
            padding: 30px 44px 24px 46px;
            position: relative;
        }
        .sf-reset .text-exception .open-quote,
        .sf-reset .text-exception .close-quote {
            font-family: Arial, Helvetica, sans-serif;
            position: absolute;
            color: #C9C9C9;
            font-size: 8em;
        }
        .sf-reset .open-quote {
            top: 0;
            left: 0;
        }
        .sf-reset .close-quote {
            bottom: -0.5em;
            right: 50px;
        }
        .sf-reset .block-exception p {
            font-family: Arial, Helvetica, sans-serif;
        }
        .sf-reset .block-exception p a,
        .sf-reset .block-exception p a:hover {
            color: #565656;
        }
        .sf-reset .logs h2 {
            float: left;
            width: 654px;
        }
        .sf-reset .error-count, .sf-reset .support {
            float: right;
            width: 170px;
            text-align: right;
        }
        .sf-reset .error-count span {
             display: inline-block;
             background-color: #aacd4e;
             -moz-border-radius: 6px;
             -webkit-border-radius: 6px;
             border-radius: 6px;
             padding: 4px;
             color: white;
             margin-right: 2px;
             font-size: 11px;
             font-weight: bold;
        }

        .sf-reset .support a {
            display: inline-block;
            -moz-border-radius: 6px;
            -webkit-border-radius: 6px;
            border-radius: 6px;
            padding: 4px;
            color: #000000;
            margin-right: 2px;
            font-size: 11px;
            font-weight: bold;
        }

        .sf-reset .toggle {
            vertical-align: middle;
        }
        .sf-reset .linked ul,
        .sf-reset .linked li {
            display: inline;
        }
        .sf-reset #output-content {
            color: #000;
            font-size: 12px;
        }
        .sf-reset #traces-text pre {
            white-space: pre;
            font-size: 12px;
            font-family: monospace;
        }
    </style>
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
