<?php

/* FOSUserBundle:Group:show_content.html.twig */
class __TwigTemplate_b19ff07b5188a7530f7afe217b1354fda2510774021149957de57f3ca2a8d97b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_873eb254dec256c329fd8a4585697d38d92e1c2d0cdf13e00580da60af961d3b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_873eb254dec256c329fd8a4585697d38d92e1c2d0cdf13e00580da60af961d3b->enter($__internal_873eb254dec256c329fd8a4585697d38d92e1c2d0cdf13e00580da60af961d3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        $__internal_5c1a2fb96a38793812d04341c5c39761b2e8ffa75acd273fb206b57aed06c990 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c1a2fb96a38793812d04341c5c39761b2e8ffa75acd273fb206b57aed06c990->enter($__internal_5c1a2fb96a38793812d04341c5c39761b2e8ffa75acd273fb206b57aed06c990_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show_content.html.twig"));

        // line 2
        echo "
<div class=\"fos_user_group_show\">
    <p>";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("group.show.name", array(), "FOSUserBundle"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["group"]) ? $context["group"] : $this->getContext($context, "group")), "getName", array(), "method"), "html", null, true);
        echo "</p>
</div>
";
        
        $__internal_873eb254dec256c329fd8a4585697d38d92e1c2d0cdf13e00580da60af961d3b->leave($__internal_873eb254dec256c329fd8a4585697d38d92e1c2d0cdf13e00580da60af961d3b_prof);

        
        $__internal_5c1a2fb96a38793812d04341c5c39761b2e8ffa75acd273fb206b57aed06c990->leave($__internal_5c1a2fb96a38793812d04341c5c39761b2e8ffa75acd273fb206b57aed06c990_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show_content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  29 => 4,  25 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}

<div class=\"fos_user_group_show\">
    <p>{{ 'group.show.name'|trans }}: {{ group.getName() }}</p>
</div>
", "FOSUserBundle:Group:show_content.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/show_content.html.twig");
    }
}
