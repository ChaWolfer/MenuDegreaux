<?php

/* SonataBlockBundle:Block:block_core_rss.html.twig */
class __TwigTemplate_bbcf3fc48dc9c0a0dcfc7da0ee72a625ec662f7f2718f9f52ddfac584063623f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 11
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_core_rss.html.twig", 11);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_067e94d633aa6a4466b6790d7ab0f3845ba673af655d7f2ccb5bc5dcf23c8bb2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_067e94d633aa6a4466b6790d7ab0f3845ba673af655d7f2ccb5bc5dcf23c8bb2->enter($__internal_067e94d633aa6a4466b6790d7ab0f3845ba673af655d7f2ccb5bc5dcf23c8bb2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_rss.html.twig"));

        $__internal_747a28b7b6799b5883c5ace24539984949a3d6c322fe42e2c90703389adaca27 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_747a28b7b6799b5883c5ace24539984949a3d6c322fe42e2c90703389adaca27->enter($__internal_747a28b7b6799b5883c5ace24539984949a3d6c322fe42e2c90703389adaca27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_rss.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_067e94d633aa6a4466b6790d7ab0f3845ba673af655d7f2ccb5bc5dcf23c8bb2->leave($__internal_067e94d633aa6a4466b6790d7ab0f3845ba673af655d7f2ccb5bc5dcf23c8bb2_prof);

        
        $__internal_747a28b7b6799b5883c5ace24539984949a3d6c322fe42e2c90703389adaca27->leave($__internal_747a28b7b6799b5883c5ace24539984949a3d6c322fe42e2c90703389adaca27_prof);

    }

    // line 13
    public function block_block($context, array $blocks = array())
    {
        $__internal_1c3e53b348202bbdf04b3125f66ffdfe493462fd0533a526d9cdbef232fdfe9b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c3e53b348202bbdf04b3125f66ffdfe493462fd0533a526d9cdbef232fdfe9b->enter($__internal_1c3e53b348202bbdf04b3125f66ffdfe493462fd0533a526d9cdbef232fdfe9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_99e2f455116887a87c648073412770a7f7670bb6c0e5c5c5ee9b9a9a93d29215 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_99e2f455116887a87c648073412770a7f7670bb6c0e5c5c5ee9b9a9a93d29215->enter($__internal_99e2f455116887a87c648073412770a7f7670bb6c0e5c5c5ee9b9a9a93d29215_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 14
        echo "    <h3 class=\"sonata-feed-title\">";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "title", array()), "html", null, true);
        echo "</h3>

    <div class=\"sonata-feeds-container\">
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["feeds"]) ? $context["feeds"] : $this->getContext($context, "feeds")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["feed"]) {
            // line 18
            echo "            <div>
                <strong><a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->getAttribute($context["feed"], "link", array()), "html", null, true);
            echo "\" rel=\"nofollow\" title=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["feed"], "title", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["feed"], "title", array()), "html", null, true);
            echo "</a></strong>
                <div>";
            // line 20
            echo $this->getAttribute($context["feed"], "description", array());
            echo "</div>
            </div>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 23
            echo "                No feeds available.
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['feed'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "    </div>
";
        
        $__internal_99e2f455116887a87c648073412770a7f7670bb6c0e5c5c5ee9b9a9a93d29215->leave($__internal_99e2f455116887a87c648073412770a7f7670bb6c0e5c5c5ee9b9a9a93d29215_prof);

        
        $__internal_1c3e53b348202bbdf04b3125f66ffdfe493462fd0533a526d9cdbef232fdfe9b->leave($__internal_1c3e53b348202bbdf04b3125f66ffdfe493462fd0533a526d9cdbef232fdfe9b_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_core_rss.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 25,  79 => 23,  71 => 20,  63 => 19,  60 => 18,  55 => 17,  48 => 14,  39 => 13,  18 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% extends sonata_block.templates.block_base %}

{% block block %}
    <h3 class=\"sonata-feed-title\">{{ settings.title }}</h3>

    <div class=\"sonata-feeds-container\">
        {% for feed in feeds %}
            <div>
                <strong><a href=\"{{ feed.link}}\" rel=\"nofollow\" title=\"{{ feed.title }}\">{{ feed.title }}</a></strong>
                <div>{{ feed.description|raw }}</div>
            </div>
        {% else %}
                No feeds available.
        {% endfor %}
    </div>
{% endblock %}
", "SonataBlockBundle:Block:block_core_rss.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_core_rss.html.twig");
    }
}
