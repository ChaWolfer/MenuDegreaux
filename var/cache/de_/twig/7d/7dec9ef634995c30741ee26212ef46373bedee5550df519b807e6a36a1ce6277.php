<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_0caf08314b712f688a9647a452ff3470bf8ba54ef5b9893e31aeac120a1d43f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_50b8e32bb0e03187cb330c632b78d4c4078e14011fcea4b095f2da08d895e87d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_50b8e32bb0e03187cb330c632b78d4c4078e14011fcea4b095f2da08d895e87d->enter($__internal_50b8e32bb0e03187cb330c632b78d4c4078e14011fcea4b095f2da08d895e87d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        $__internal_805657c2fa6db292ad84d0fc5191307c3d9475e88d46d5dfa37a9959fcc69a3b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_805657c2fa6db292ad84d0fc5191307c3d9475e88d46d5dfa37a9959fcc69a3b->enter($__internal_805657c2fa6db292ad84d0fc5191307c3d9475e88d46d5dfa37a9959fcc69a3b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_50b8e32bb0e03187cb330c632b78d4c4078e14011fcea4b095f2da08d895e87d->leave($__internal_50b8e32bb0e03187cb330c632b78d4c4078e14011fcea4b095f2da08d895e87d_prof);

        
        $__internal_805657c2fa6db292ad84d0fc5191307c3d9475e88d46d5dfa37a9959fcc69a3b->leave($__internal_805657c2fa6db292ad84d0fc5191307c3d9475e88d46d5dfa37a9959fcc69a3b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\integer_widget.html.php");
    }
}
