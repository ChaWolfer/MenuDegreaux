<?php

/* @SonataBlock/Block/block_exception_debug.html.twig */
class __TwigTemplate_304d9972f98d1a940a502cfb5b6938d962a60f75574e3c20cfa9a6c141c8a0eb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "@SonataBlock/Block/block_exception_debug.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b8509fbf7b94fd74e00ef653f316aade1a5d4cd4f870fc8293c658b7cc97b37 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5b8509fbf7b94fd74e00ef653f316aade1a5d4cd4f870fc8293c658b7cc97b37->enter($__internal_5b8509fbf7b94fd74e00ef653f316aade1a5d4cd4f870fc8293c658b7cc97b37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_exception_debug.html.twig"));

        $__internal_4a1eacde2a5c1b60c7ad7a6405f2f129ddf619966439ccec84c09adfde5a3c59 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a1eacde2a5c1b60c7ad7a6405f2f129ddf619966439ccec84c09adfde5a3c59->enter($__internal_4a1eacde2a5c1b60c7ad7a6405f2f129ddf619966439ccec84c09adfde5a3c59_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_exception_debug.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5b8509fbf7b94fd74e00ef653f316aade1a5d4cd4f870fc8293c658b7cc97b37->leave($__internal_5b8509fbf7b94fd74e00ef653f316aade1a5d4cd4f870fc8293c658b7cc97b37_prof);

        
        $__internal_4a1eacde2a5c1b60c7ad7a6405f2f129ddf619966439ccec84c09adfde5a3c59->leave($__internal_4a1eacde2a5c1b60c7ad7a6405f2f129ddf619966439ccec84c09adfde5a3c59_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_b39fc0ac2ef237cd0ed1dc50e2ea00f7b6ec24010fd2ec34f37cf6fa830740b8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b39fc0ac2ef237cd0ed1dc50e2ea00f7b6ec24010fd2ec34f37cf6fa830740b8->enter($__internal_b39fc0ac2ef237cd0ed1dc50e2ea00f7b6ec24010fd2ec34f37cf6fa830740b8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_ac432eedde1835bc76712f44b91a50eace29c370d058bad6a455b47ac670882c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ac432eedde1835bc76712f44b91a50eace29c370d058bad6a455b47ac670882c->enter($__internal_ac432eedde1835bc76712f44b91a50eace29c370d058bad6a455b47ac670882c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\" ";
        if ((isset($context["forceStyle"]) ? $context["forceStyle"] : $this->getContext($context, "forceStyle"))) {
            echo "style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"";
        }
        echo ">

        ";
        // line 18
        echo "        ";
        if ((isset($context["forceStyle"]) ? $context["forceStyle"] : $this->getContext($context, "forceStyle"))) {
            // line 19
            echo "            <link href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception_layout.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css"), "html", null, true);
            echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        ";
        }
        // line 22
        echo "        ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "@SonataBlock/Block/block_exception_debug.html.twig", 22)->display($context);
        // line 23
        echo "    </div>
";
        
        $__internal_ac432eedde1835bc76712f44b91a50eace29c370d058bad6a455b47ac670882c->leave($__internal_ac432eedde1835bc76712f44b91a50eace29c370d058bad6a455b47ac670882c_prof);

        
        $__internal_b39fc0ac2ef237cd0ed1dc50e2ea00f7b6ec24010fd2ec34f37cf6fa830740b8->leave($__internal_b39fc0ac2ef237cd0ed1dc50e2ea00f7b6ec24010fd2ec34f37cf6fa830740b8_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_exception_debug.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 23,  69 => 22,  64 => 20,  59 => 19,  56 => 18,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <div class=\"cms-block-exception\" {% if forceStyle %}style=\"overflow-y: scroll; min-width: 300px; max-height: 300px;\"{% endif %}>

        {# this is dirty but the alternative would require a new block-optimized exception css #}
        {% if forceStyle %}
            <link href=\"{{ asset('bundles/framework/css/exception_layout.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
            <link href=\"{{ asset('bundles/framework/css/exception.css') }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
        {% endif %}
        {% include 'TwigBundle:Exception:exception.html.twig' %}
    </div>
{% endblock %}
", "@SonataBlock/Block/block_exception_debug.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_exception_debug.html.twig");
    }
}
