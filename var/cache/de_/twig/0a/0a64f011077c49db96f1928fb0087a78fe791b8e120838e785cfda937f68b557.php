<?php

/* knp_menu.html.twig */
class __TwigTemplate_1d03fcffcfc2cdbd32ef78dc74324b8992dae44020b665daeaafa7ec6759338b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("knp_menu_base.html.twig", "knp_menu.html.twig", 1);
        $this->blocks = array(
            'compressed_root' => array($this, 'block_compressed_root'),
            'root' => array($this, 'block_root'),
            'list' => array($this, 'block_list'),
            'children' => array($this, 'block_children'),
            'item' => array($this, 'block_item'),
            'linkElement' => array($this, 'block_linkElement'),
            'spanElement' => array($this, 'block_spanElement'),
            'label' => array($this, 'block_label'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knp_menu_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d87a49d23553a0ec28b71acba7968bb0168a2e8311a90537192ac5681ccdcbb0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d87a49d23553a0ec28b71acba7968bb0168a2e8311a90537192ac5681ccdcbb0->enter($__internal_d87a49d23553a0ec28b71acba7968bb0168a2e8311a90537192ac5681ccdcbb0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu.html.twig"));

        $__internal_24a42440be5047456e56362a28e0dcc79dd3ab40a302d92fce7fb021b6a14a05 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24a42440be5047456e56362a28e0dcc79dd3ab40a302d92fce7fb021b6a14a05->enter($__internal_24a42440be5047456e56362a28e0dcc79dd3ab40a302d92fce7fb021b6a14a05_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d87a49d23553a0ec28b71acba7968bb0168a2e8311a90537192ac5681ccdcbb0->leave($__internal_d87a49d23553a0ec28b71acba7968bb0168a2e8311a90537192ac5681ccdcbb0_prof);

        
        $__internal_24a42440be5047456e56362a28e0dcc79dd3ab40a302d92fce7fb021b6a14a05->leave($__internal_24a42440be5047456e56362a28e0dcc79dd3ab40a302d92fce7fb021b6a14a05_prof);

    }

    // line 11
    public function block_compressed_root($context, array $blocks = array())
    {
        $__internal_6325903ad06c194b0dee0aecb608348dc019395cd7b1714fce2408268e02fd80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6325903ad06c194b0dee0aecb608348dc019395cd7b1714fce2408268e02fd80->enter($__internal_6325903ad06c194b0dee0aecb608348dc019395cd7b1714fce2408268e02fd80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "compressed_root"));

        $__internal_1572a5d5ee2e70439d7fcdf1a0199940042a4e81df5a9fe14a60b738aa2b8546 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1572a5d5ee2e70439d7fcdf1a0199940042a4e81df5a9fe14a60b738aa2b8546->enter($__internal_1572a5d5ee2e70439d7fcdf1a0199940042a4e81df5a9fe14a60b738aa2b8546_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "compressed_root"));

        // line 12
        ob_start();
        // line 13
        $this->displayBlock("root", $context, $blocks);
        echo "
";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_1572a5d5ee2e70439d7fcdf1a0199940042a4e81df5a9fe14a60b738aa2b8546->leave($__internal_1572a5d5ee2e70439d7fcdf1a0199940042a4e81df5a9fe14a60b738aa2b8546_prof);

        
        $__internal_6325903ad06c194b0dee0aecb608348dc019395cd7b1714fce2408268e02fd80->leave($__internal_6325903ad06c194b0dee0aecb608348dc019395cd7b1714fce2408268e02fd80_prof);

    }

    // line 17
    public function block_root($context, array $blocks = array())
    {
        $__internal_aa149a5763a94ecb6959f4ea896e00d18969c76d7e435cceaa640e7ed0c20d9b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aa149a5763a94ecb6959f4ea896e00d18969c76d7e435cceaa640e7ed0c20d9b->enter($__internal_aa149a5763a94ecb6959f4ea896e00d18969c76d7e435cceaa640e7ed0c20d9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "root"));

        $__internal_dfaf555f3a9d0ac6e63c1f1e4625a9201d96fbf7d748ac5969d8832eea643b07 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dfaf555f3a9d0ac6e63c1f1e4625a9201d96fbf7d748ac5969d8832eea643b07->enter($__internal_dfaf555f3a9d0ac6e63c1f1e4625a9201d96fbf7d748ac5969d8832eea643b07_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "root"));

        // line 18
        $context["listAttributes"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttributes", array());
        // line 19
        $this->displayBlock("list", $context, $blocks);
        
        $__internal_dfaf555f3a9d0ac6e63c1f1e4625a9201d96fbf7d748ac5969d8832eea643b07->leave($__internal_dfaf555f3a9d0ac6e63c1f1e4625a9201d96fbf7d748ac5969d8832eea643b07_prof);

        
        $__internal_aa149a5763a94ecb6959f4ea896e00d18969c76d7e435cceaa640e7ed0c20d9b->leave($__internal_aa149a5763a94ecb6959f4ea896e00d18969c76d7e435cceaa640e7ed0c20d9b_prof);

    }

    // line 22
    public function block_list($context, array $blocks = array())
    {
        $__internal_2c312e8e2a0a07e746955e0ea1c4df959e8a77a6f473d836e38cc3a5b8df9f68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c312e8e2a0a07e746955e0ea1c4df959e8a77a6f473d836e38cc3a5b8df9f68->enter($__internal_2c312e8e2a0a07e746955e0ea1c4df959e8a77a6f473d836e38cc3a5b8df9f68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        $__internal_1df78a9919b1b39e1f30f3896f70a6b3a4c8b5e28c622883d6df5c1b8ac10f01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1df78a9919b1b39e1f30f3896f70a6b3a4c8b5e28c622883d6df5c1b8ac10f01->enter($__internal_1df78a9919b1b39e1f30f3896f70a6b3a4c8b5e28c622883d6df5c1b8ac10f01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        // line 23
        if ((($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "hasChildren", array()) &&  !($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "depth", array()) === 0)) && $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "displayChildren", array()))) {
            // line 24
            echo "    ";
            $context["knp_menu"] = $this;
            // line 25
            echo "    <ul";
            echo $context["knp_menu"]->getattributes((isset($context["listAttributes"]) ? $context["listAttributes"] : $this->getContext($context, "listAttributes")));
            echo ">
        ";
            // line 26
            $this->displayBlock("children", $context, $blocks);
            echo "
    </ul>
";
        }
        
        $__internal_1df78a9919b1b39e1f30f3896f70a6b3a4c8b5e28c622883d6df5c1b8ac10f01->leave($__internal_1df78a9919b1b39e1f30f3896f70a6b3a4c8b5e28c622883d6df5c1b8ac10f01_prof);

        
        $__internal_2c312e8e2a0a07e746955e0ea1c4df959e8a77a6f473d836e38cc3a5b8df9f68->leave($__internal_2c312e8e2a0a07e746955e0ea1c4df959e8a77a6f473d836e38cc3a5b8df9f68_prof);

    }

    // line 31
    public function block_children($context, array $blocks = array())
    {
        $__internal_fd93ddf38f91eba4ad59761e1375a88230ac037214124ae7c3d58733911dc8ce = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fd93ddf38f91eba4ad59761e1375a88230ac037214124ae7c3d58733911dc8ce->enter($__internal_fd93ddf38f91eba4ad59761e1375a88230ac037214124ae7c3d58733911dc8ce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "children"));

        $__internal_84ea3989ed9575ad76ca14e3a20760105fc61b045161652a4d822f793ef4eece = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_84ea3989ed9575ad76ca14e3a20760105fc61b045161652a4d822f793ef4eece->enter($__internal_84ea3989ed9575ad76ca14e3a20760105fc61b045161652a4d822f793ef4eece_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "children"));

        // line 33
        $context["currentOptions"] = (isset($context["options"]) ? $context["options"] : $this->getContext($context, "options"));
        // line 34
        $context["currentItem"] = (isset($context["item"]) ? $context["item"] : $this->getContext($context, "item"));
        // line 36
        if ( !(null === $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "depth", array()))) {
            // line 37
            $context["options"] = twig_array_merge((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), array("depth" => ($this->getAttribute((isset($context["currentOptions"]) ? $context["currentOptions"] : $this->getContext($context, "currentOptions")), "depth", array()) - 1)));
        }
        // line 40
        if (( !(null === $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "matchingDepth", array())) && ($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "matchingDepth", array()) > 0))) {
            // line 41
            $context["options"] = twig_array_merge((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), array("matchingDepth" => ($this->getAttribute((isset($context["currentOptions"]) ? $context["currentOptions"] : $this->getContext($context, "currentOptions")), "matchingDepth", array()) - 1)));
        }
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["currentItem"]) ? $context["currentItem"] : $this->getContext($context, "currentItem")), "children", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 44
            echo "    ";
            $this->displayBlock("item", $context, $blocks);
            echo "
";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 47
        $context["item"] = (isset($context["currentItem"]) ? $context["currentItem"] : $this->getContext($context, "currentItem"));
        // line 48
        $context["options"] = (isset($context["currentOptions"]) ? $context["currentOptions"] : $this->getContext($context, "currentOptions"));
        
        $__internal_84ea3989ed9575ad76ca14e3a20760105fc61b045161652a4d822f793ef4eece->leave($__internal_84ea3989ed9575ad76ca14e3a20760105fc61b045161652a4d822f793ef4eece_prof);

        
        $__internal_fd93ddf38f91eba4ad59761e1375a88230ac037214124ae7c3d58733911dc8ce->leave($__internal_fd93ddf38f91eba4ad59761e1375a88230ac037214124ae7c3d58733911dc8ce_prof);

    }

    // line 51
    public function block_item($context, array $blocks = array())
    {
        $__internal_f59455ec9b6540ca7e794f44b8aff9d6ebb6997698c3d3664a945989f124daec = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f59455ec9b6540ca7e794f44b8aff9d6ebb6997698c3d3664a945989f124daec->enter($__internal_f59455ec9b6540ca7e794f44b8aff9d6ebb6997698c3d3664a945989f124daec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item"));

        $__internal_97de93e469a91b0fa96b0243e0ff1a58e1aba8fc33df0d688975eeb989ebf10b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97de93e469a91b0fa96b0243e0ff1a58e1aba8fc33df0d688975eeb989ebf10b->enter($__internal_97de93e469a91b0fa96b0243e0ff1a58e1aba8fc33df0d688975eeb989ebf10b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item"));

        // line 52
        if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "displayed", array())) {
            // line 54
            $context["classes"] = (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attribute", array(0 => "class"), "method"))) ? (array(0 => $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attribute", array(0 => "class"), "method"))) : (array()));
            // line 55
            if ($this->getAttribute((isset($context["matcher"]) ? $context["matcher"] : $this->getContext($context, "matcher")), "isCurrent", array(0 => (isset($context["item"]) ? $context["item"] : $this->getContext($context, "item"))), "method")) {
                // line 56
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "currentClass", array())));
            } elseif ($this->getAttribute(            // line 57
(isset($context["matcher"]) ? $context["matcher"] : $this->getContext($context, "matcher")), "isAncestor", array(0 => (isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), 1 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "matchingDepth", array())), "method")) {
                // line 58
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "ancestorClass", array())));
            }
            // line 60
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "actsLikeFirst", array())) {
                // line 61
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "firstClass", array())));
            }
            // line 63
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "actsLikeLast", array())) {
                // line 64
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "lastClass", array())));
            }
            // line 66
            echo "
    ";
            // line 68
            echo "    ";
            if (($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "hasChildren", array()) &&  !($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "depth", array()) === 0))) {
                // line 69
                echo "        ";
                if (( !twig_test_empty($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "branch_class", array())) && $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "displayChildren", array()))) {
                    // line 70
                    $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "branch_class", array())));
                    // line 71
                    echo "        ";
                }
                // line 72
                echo "    ";
            } elseif ( !twig_test_empty($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "leaf_class", array()))) {
                // line 73
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "leaf_class", array())));
            }
            // line 76
            $context["attributes"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attributes", array());
            // line 77
            if ( !twig_test_empty((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")))) {
                // line 78
                $context["attributes"] = twig_array_merge((isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")), array("class" => twig_join_filter((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), " ")));
            }
            // line 81
            echo "    ";
            $context["knp_menu"] = $this;
            // line 82
            echo "    <li";
            echo $context["knp_menu"]->getattributes((isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")));
            echo ">";
            // line 83
            if (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "uri", array())) && ( !$this->getAttribute((isset($context["matcher"]) ? $context["matcher"] : $this->getContext($context, "matcher")), "isCurrent", array(0 => (isset($context["item"]) ? $context["item"] : $this->getContext($context, "item"))), "method") || $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "currentAsLink", array())))) {
                // line 84
                echo "        ";
                $this->displayBlock("linkElement", $context, $blocks);
            } else {
                // line 86
                echo "        ";
                $this->displayBlock("spanElement", $context, $blocks);
            }
            // line 89
            $context["childrenClasses"] = (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttribute", array(0 => "class"), "method"))) ? (array(0 => $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttribute", array(0 => "class"), "method"))) : (array()));
            // line 90
            $context["childrenClasses"] = twig_array_merge((isset($context["childrenClasses"]) ? $context["childrenClasses"] : $this->getContext($context, "childrenClasses")), array(0 => ("menu_level_" . $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "level", array()))));
            // line 91
            $context["listAttributes"] = twig_array_merge($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttributes", array()), array("class" => twig_join_filter((isset($context["childrenClasses"]) ? $context["childrenClasses"] : $this->getContext($context, "childrenClasses")), " ")));
            // line 92
            echo "        ";
            $this->displayBlock("list", $context, $blocks);
            echo "
    </li>
";
        }
        
        $__internal_97de93e469a91b0fa96b0243e0ff1a58e1aba8fc33df0d688975eeb989ebf10b->leave($__internal_97de93e469a91b0fa96b0243e0ff1a58e1aba8fc33df0d688975eeb989ebf10b_prof);

        
        $__internal_f59455ec9b6540ca7e794f44b8aff9d6ebb6997698c3d3664a945989f124daec->leave($__internal_f59455ec9b6540ca7e794f44b8aff9d6ebb6997698c3d3664a945989f124daec_prof);

    }

    // line 97
    public function block_linkElement($context, array $blocks = array())
    {
        $__internal_f799293370a6d26d70e9ce83a0de7d640d969cae9df19f90a77fa02504337a6b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f799293370a6d26d70e9ce83a0de7d640d969cae9df19f90a77fa02504337a6b->enter($__internal_f799293370a6d26d70e9ce83a0de7d640d969cae9df19f90a77fa02504337a6b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "linkElement"));

        $__internal_7d44c62a11691441c5c2633e4875ed2548f050023874fefb89aa177a541e7ebc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d44c62a11691441c5c2633e4875ed2548f050023874fefb89aa177a541e7ebc->enter($__internal_7d44c62a11691441c5c2633e4875ed2548f050023874fefb89aa177a541e7ebc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "linkElement"));

        $context["knp_menu"] = $this;
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "uri", array()), "html", null, true);
        echo "\"";
        echo $context["knp_menu"]->getattributes($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "linkAttributes", array()));
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</a>";
        
        $__internal_7d44c62a11691441c5c2633e4875ed2548f050023874fefb89aa177a541e7ebc->leave($__internal_7d44c62a11691441c5c2633e4875ed2548f050023874fefb89aa177a541e7ebc_prof);

        
        $__internal_f799293370a6d26d70e9ce83a0de7d640d969cae9df19f90a77fa02504337a6b->leave($__internal_f799293370a6d26d70e9ce83a0de7d640d969cae9df19f90a77fa02504337a6b_prof);

    }

    // line 99
    public function block_spanElement($context, array $blocks = array())
    {
        $__internal_908b1e5e004f4883cd22f0ed0f4b9105906ce5de9511a9f26fe0651183257274 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_908b1e5e004f4883cd22f0ed0f4b9105906ce5de9511a9f26fe0651183257274->enter($__internal_908b1e5e004f4883cd22f0ed0f4b9105906ce5de9511a9f26fe0651183257274_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "spanElement"));

        $__internal_3887e511cce3a5d442017f5402adbc5ce6af13144ba6306cd386971e629ad2bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3887e511cce3a5d442017f5402adbc5ce6af13144ba6306cd386971e629ad2bd->enter($__internal_3887e511cce3a5d442017f5402adbc5ce6af13144ba6306cd386971e629ad2bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "spanElement"));

        $context["knp_menu"] = $this;
        echo "<span";
        echo $context["knp_menu"]->getattributes($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "labelAttributes", array()));
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</span>";
        
        $__internal_3887e511cce3a5d442017f5402adbc5ce6af13144ba6306cd386971e629ad2bd->leave($__internal_3887e511cce3a5d442017f5402adbc5ce6af13144ba6306cd386971e629ad2bd_prof);

        
        $__internal_908b1e5e004f4883cd22f0ed0f4b9105906ce5de9511a9f26fe0651183257274->leave($__internal_908b1e5e004f4883cd22f0ed0f4b9105906ce5de9511a9f26fe0651183257274_prof);

    }

    // line 101
    public function block_label($context, array $blocks = array())
    {
        $__internal_a2c40b6556f018c1e77470a54b7793982e88610c673e4c5995fcfa6912c9e341 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a2c40b6556f018c1e77470a54b7793982e88610c673e4c5995fcfa6912c9e341->enter($__internal_a2c40b6556f018c1e77470a54b7793982e88610c673e4c5995fcfa6912c9e341_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        $__internal_b740bfde9e97b61cfa2d65793d8d6b4a91c131d51b2cff862c656be33400c768 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b740bfde9e97b61cfa2d65793d8d6b4a91c131d51b2cff862c656be33400c768->enter($__internal_b740bfde9e97b61cfa2d65793d8d6b4a91c131d51b2cff862c656be33400c768_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "label"));

        if (($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "allow_safe_labels", array()) && $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "getExtra", array(0 => "safe_label", 1 => false), "method"))) {
            echo $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "label", array());
        } else {
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "label", array()), "html", null, true);
        }
        
        $__internal_b740bfde9e97b61cfa2d65793d8d6b4a91c131d51b2cff862c656be33400c768->leave($__internal_b740bfde9e97b61cfa2d65793d8d6b4a91c131d51b2cff862c656be33400c768_prof);

        
        $__internal_a2c40b6556f018c1e77470a54b7793982e88610c673e4c5995fcfa6912c9e341->leave($__internal_a2c40b6556f018c1e77470a54b7793982e88610c673e4c5995fcfa6912c9e341_prof);

    }

    // line 3
    public function getattributes($__attributes__ = null, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "attributes" => $__attributes__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            $__internal_e25f368dae7f94d82845e8ed0784a7ce51c0bb1a61ee3e959f346252edc7d175 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $__internal_e25f368dae7f94d82845e8ed0784a7ce51c0bb1a61ee3e959f346252edc7d175->enter($__internal_e25f368dae7f94d82845e8ed0784a7ce51c0bb1a61ee3e959f346252edc7d175_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "attributes"));

            $__internal_371d3afa251e43f58794ae47409c1e57156a449cea769311344f8f123ccc638b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $__internal_371d3afa251e43f58794ae47409c1e57156a449cea769311344f8f123ccc638b->enter($__internal_371d3afa251e43f58794ae47409c1e57156a449cea769311344f8f123ccc638b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "attributes"));

            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")));
            foreach ($context['_seq'] as $context["name"] => $context["value"]) {
                // line 5
                if (( !(null === $context["value"]) &&  !($context["value"] === false))) {
                    // line 6
                    echo sprintf(" %s=\"%s\"", $context["name"], ((($context["value"] === true)) ? (twig_escape_filter($this->env, $context["name"])) : (twig_escape_filter($this->env, $context["value"]))));
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            
            $__internal_371d3afa251e43f58794ae47409c1e57156a449cea769311344f8f123ccc638b->leave($__internal_371d3afa251e43f58794ae47409c1e57156a449cea769311344f8f123ccc638b_prof);

            
            $__internal_e25f368dae7f94d82845e8ed0784a7ce51c0bb1a61ee3e959f346252edc7d175->leave($__internal_e25f368dae7f94d82845e8ed0784a7ce51c0bb1a61ee3e959f346252edc7d175_prof);

        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "knp_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  385 => 6,  383 => 5,  379 => 4,  361 => 3,  339 => 101,  316 => 99,  291 => 97,  276 => 92,  274 => 91,  272 => 90,  270 => 89,  266 => 86,  262 => 84,  260 => 83,  256 => 82,  253 => 81,  250 => 78,  248 => 77,  246 => 76,  243 => 73,  240 => 72,  237 => 71,  235 => 70,  232 => 69,  229 => 68,  226 => 66,  223 => 64,  221 => 63,  218 => 61,  216 => 60,  213 => 58,  211 => 57,  209 => 56,  207 => 55,  205 => 54,  203 => 52,  194 => 51,  184 => 48,  182 => 47,  165 => 44,  148 => 43,  145 => 41,  143 => 40,  140 => 37,  138 => 36,  136 => 34,  134 => 33,  125 => 31,  111 => 26,  106 => 25,  103 => 24,  101 => 23,  92 => 22,  82 => 19,  80 => 18,  71 => 17,  58 => 13,  56 => 12,  47 => 11,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'knp_menu_base.html.twig' %}

{% macro attributes(attributes) %}
{% for name, value in attributes %}
    {%- if value is not none and value is not same as(false) -%}
        {{- ' %s=\"%s\"'|format(name, value is same as(true) ? name|e : value|e)|raw -}}
    {%- endif -%}
{%- endfor -%}
{% endmacro %}

{% block compressed_root %}
{% spaceless %}
{{ block('root') }}
{% endspaceless %}
{% endblock %}

{% block root %}
{% set listAttributes = item.childrenAttributes %}
{{ block('list') -}}
{% endblock %}

{% block list %}
{% if item.hasChildren and options.depth is not same as(0) and item.displayChildren %}
    {% import _self as knp_menu %}
    <ul{{ knp_menu.attributes(listAttributes) }}>
        {{ block('children') }}
    </ul>
{% endif %}
{% endblock %}

{% block children %}
{# save current variables #}
{% set currentOptions = options %}
{% set currentItem = item %}
{# update the depth for children #}
{% if options.depth is not none %}
{% set options = options|merge({'depth': currentOptions.depth - 1}) %}
{% endif %}
{# update the matchingDepth for children #}
{% if options.matchingDepth is not none and options.matchingDepth > 0 %}
{% set options = options|merge({'matchingDepth': currentOptions.matchingDepth - 1}) %}
{% endif %}
{% for item in currentItem.children %}
    {{ block('item') }}
{% endfor %}
{# restore current variables #}
{% set item = currentItem %}
{% set options = currentOptions %}
{% endblock %}

{% block item %}
{% if item.displayed %}
{# building the class of the item #}
    {%- set classes = item.attribute('class') is not empty ? [item.attribute('class')] : [] %}
    {%- if matcher.isCurrent(item) %}
        {%- set classes = classes|merge([options.currentClass]) %}
    {%- elseif matcher.isAncestor(item, options.matchingDepth) %}
        {%- set classes = classes|merge([options.ancestorClass]) %}
    {%- endif %}
    {%- if item.actsLikeFirst %}
        {%- set classes = classes|merge([options.firstClass]) %}
    {%- endif %}
    {%- if item.actsLikeLast %}
        {%- set classes = classes|merge([options.lastClass]) %}
    {%- endif %}

    {# Mark item as \"leaf\" (no children) or as \"branch\" (has children that are displayed) #}
    {% if item.hasChildren and options.depth is not same as(0) %}
        {% if options.branch_class is not empty and item.displayChildren %}
            {%- set classes = classes|merge([options.branch_class]) %}
        {% endif %}
    {% elseif options.leaf_class is not empty %}
        {%- set classes = classes|merge([options.leaf_class]) %}
    {%- endif %}

    {%- set attributes = item.attributes %}
    {%- if classes is not empty %}
        {%- set attributes = attributes|merge({'class': classes|join(' ')}) %}
    {%- endif %}
{# displaying the item #}
    {% import _self as knp_menu %}
    <li{{ knp_menu.attributes(attributes) }}>
        {%- if item.uri is not empty and (not matcher.isCurrent(item) or options.currentAsLink) %}
        {{ block('linkElement') }}
        {%- else %}
        {{ block('spanElement') }}
        {%- endif %}
{# render the list of children#}
        {%- set childrenClasses = item.childrenAttribute('class') is not empty ? [item.childrenAttribute('class')] : [] %}
        {%- set childrenClasses = childrenClasses|merge(['menu_level_' ~ item.level]) %}
        {%- set listAttributes = item.childrenAttributes|merge({'class': childrenClasses|join(' ') }) %}
        {{ block('list') }}
    </li>
{% endif %}
{% endblock %}

{% block linkElement %}{% import _self as knp_menu %}<a href=\"{{ item.uri }}\"{{ knp_menu.attributes(item.linkAttributes) }}>{{ block('label') }}</a>{% endblock %}

{% block spanElement %}{% import _self as knp_menu %}<span{{ knp_menu.attributes(item.labelAttributes) }}>{{ block('label') }}</span>{% endblock %}

{% block label %}{% if options.allow_safe_labels and item.getExtra('safe_label', false) %}{{ item.label|raw }}{% else %}{{ item.label }}{% endif %}{% endblock %}
", "knp_menu.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\knplabs\\knp-menu\\src\\Knp\\Menu\\Resources\\views\\knp_menu.html.twig");
    }
}
