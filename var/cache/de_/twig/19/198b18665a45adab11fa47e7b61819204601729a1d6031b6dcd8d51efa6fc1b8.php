<?php

/* @SonataCore/Form/color.html.twig */
class __TwigTemplate_f0f038e6655de1af27affc818a6d29db1b45211954ceeae9ce92ee9296ac8c5e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonata_type_color_widget' => array($this, 'block_sonata_type_color_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d2f9b8b0b5227acee3d11aaf37c22f39025c676b5b89c945e742eaa9daa0b4d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1d2f9b8b0b5227acee3d11aaf37c22f39025c676b5b89c945e742eaa9daa0b4d->enter($__internal_1d2f9b8b0b5227acee3d11aaf37c22f39025c676b5b89c945e742eaa9daa0b4d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataCore/Form/color.html.twig"));

        $__internal_2a00a88e6636287af93933f42f35ee670c401b1ae80935d80d0d451192bc89b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2a00a88e6636287af93933f42f35ee670c401b1ae80935d80d0d451192bc89b0->enter($__internal_2a00a88e6636287af93933f42f35ee670c401b1ae80935d80d0d451192bc89b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataCore/Form/color.html.twig"));

        // line 11
        $this->displayBlock('sonata_type_color_widget', $context, $blocks);
        
        $__internal_1d2f9b8b0b5227acee3d11aaf37c22f39025c676b5b89c945e742eaa9daa0b4d->leave($__internal_1d2f9b8b0b5227acee3d11aaf37c22f39025c676b5b89c945e742eaa9daa0b4d_prof);

        
        $__internal_2a00a88e6636287af93933f42f35ee670c401b1ae80935d80d0d451192bc89b0->leave($__internal_2a00a88e6636287af93933f42f35ee670c401b1ae80935d80d0d451192bc89b0_prof);

    }

    public function block_sonata_type_color_widget($context, array $blocks = array())
    {
        $__internal_02b837c6a0ca36d7576c0b1a85ee2bb8592f2191a7541c6e93f569c2d9f2cdf8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_02b837c6a0ca36d7576c0b1a85ee2bb8592f2191a7541c6e93f569c2d9f2cdf8->enter($__internal_02b837c6a0ca36d7576c0b1a85ee2bb8592f2191a7541c6e93f569c2d9f2cdf8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_widget"));

        $__internal_dacb2c55e396eddd4857057af93f504c8f18a317456cd6f150253a542d56e332 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dacb2c55e396eddd4857057af93f504c8f18a317456cd6f150253a542d56e332->enter($__internal_dacb2c55e396eddd4857057af93f504c8f18a317456cd6f150253a542d56e332_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_widget"));

        // line 12
        echo "    ";
        ob_start();
        // line 13
        echo "        <input type=\"color\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\" ";
        }
        echo "/>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_dacb2c55e396eddd4857057af93f504c8f18a317456cd6f150253a542d56e332->leave($__internal_dacb2c55e396eddd4857057af93f504c8f18a317456cd6f150253a542d56e332_prof);

        
        $__internal_02b837c6a0ca36d7576c0b1a85ee2bb8592f2191a7541c6e93f569c2d9f2cdf8->leave($__internal_02b837c6a0ca36d7576c0b1a85ee2bb8592f2191a7541c6e93f569c2d9f2cdf8_prof);

    }

    public function getTemplateName()
    {
        return "@SonataCore/Form/color.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  47 => 13,  44 => 12,  26 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% block sonata_type_color_widget %}
    {% spaceless %}
        <input type=\"color\" {{ block('widget_attributes') }} {% if value is not empty %}value=\"{{ value }}\" {% endif %}/>
    {% endspaceless %}
{% endblock sonata_type_color_widget %}
", "@SonataCore/Form/color.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\core-bundle\\Resources\\views\\Form\\color.html.twig");
    }
}
