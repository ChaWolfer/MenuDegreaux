<?php

/* TwigBundle:Exception:error.rdf.twig */
class __TwigTemplate_99588a785c513c6403c407f65f9f28166526bbc2d1e702867b5b7dc13b06c016 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_13d192f410a48d37bbf03c6e5548719ad856b61aafddfda516d2ee0af51b359f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_13d192f410a48d37bbf03c6e5548719ad856b61aafddfda516d2ee0af51b359f->enter($__internal_13d192f410a48d37bbf03c6e5548719ad856b61aafddfda516d2ee0af51b359f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        $__internal_cf3101ecd135e2a206ab3964ca48b15124d584541b9b4668336d2127e28fd3d7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cf3101ecd135e2a206ab3964ca48b15124d584541b9b4668336d2127e28fd3d7->enter($__internal_cf3101ecd135e2a206ab3964ca48b15124d584541b9b4668336d2127e28fd3d7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/error.xml.twig");
        echo "
";
        
        $__internal_13d192f410a48d37bbf03c6e5548719ad856b61aafddfda516d2ee0af51b359f->leave($__internal_13d192f410a48d37bbf03c6e5548719ad856b61aafddfda516d2ee0af51b359f_prof);

        
        $__internal_cf3101ecd135e2a206ab3964ca48b15124d584541b9b4668336d2127e28fd3d7->leave($__internal_cf3101ecd135e2a206ab3964ca48b15124d584541b9b4668336d2127e28fd3d7_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/error.xml.twig') }}
", "TwigBundle:Exception:error.rdf.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.rdf.twig");
    }
}
