<?php

/* @Twig/Exception/error.js.twig */
class __TwigTemplate_a1c7aab0578aeaabb52e5a05eb6fc1fd6beae067041d201300ef70f62bc52ef2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_258c0a6e2789c09962a899c28224c4b9b73721e552a4e00dad61ff2e8ce79b86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_258c0a6e2789c09962a899c28224c4b9b73721e552a4e00dad61ff2e8ce79b86->enter($__internal_258c0a6e2789c09962a899c28224c4b9b73721e552a4e00dad61ff2e8ce79b86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        $__internal_8046742fd552775c6c0c1df04cca396096d2da66f6ac5bef270771b4e4d9c51a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8046742fd552775c6c0c1df04cca396096d2da66f6ac5bef270771b4e4d9c51a->enter($__internal_8046742fd552775c6c0c1df04cca396096d2da66f6ac5bef270771b4e4d9c51a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "js", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "js", null, true);
        echo "

*/
";
        
        $__internal_258c0a6e2789c09962a899c28224c4b9b73721e552a4e00dad61ff2e8ce79b86->leave($__internal_258c0a6e2789c09962a899c28224c4b9b73721e552a4e00dad61ff2e8ce79b86_prof);

        
        $__internal_8046742fd552775c6c0c1df04cca396096d2da66f6ac5bef270771b4e4d9c51a->leave($__internal_8046742fd552775c6c0c1df04cca396096d2da66f6ac5bef270771b4e4d9c51a_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.js.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.js.twig");
    }
}
