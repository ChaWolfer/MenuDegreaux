<?php

/* SonataAdminBundle:CRUD:edit_integer.html.twig */
class __TwigTemplate_d5f884795424839a2d4936ef5e0e5686409688303c4cba25f8a8f73e16d2a7a8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:edit_integer.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c282b9567b1fb3d3a6dc887e98f3dc72c92df93f99ee8e5c78d490b4e1758582 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c282b9567b1fb3d3a6dc887e98f3dc72c92df93f99ee8e5c78d490b4e1758582->enter($__internal_c282b9567b1fb3d3a6dc887e98f3dc72c92df93f99ee8e5c78d490b4e1758582_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_integer.html.twig"));

        $__internal_9f6e8decf2ddf21a789c04e5a606ce9e0e4c1d49b0edeb16edd4271999acff8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f6e8decf2ddf21a789c04e5a606ce9e0e4c1d49b0edeb16edd4271999acff8c->enter($__internal_9f6e8decf2ddf21a789c04e5a606ce9e0e4c1d49b0edeb16edd4271999acff8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_integer.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c282b9567b1fb3d3a6dc887e98f3dc72c92df93f99ee8e5c78d490b4e1758582->leave($__internal_c282b9567b1fb3d3a6dc887e98f3dc72c92df93f99ee8e5c78d490b4e1758582_prof);

        
        $__internal_9f6e8decf2ddf21a789c04e5a606ce9e0e4c1d49b0edeb16edd4271999acff8c->leave($__internal_9f6e8decf2ddf21a789c04e5a606ce9e0e4c1d49b0edeb16edd4271999acff8c_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_d5f994267e6d87a6f0daf44dc3a93266267b0b49e67e8f58e1d2faee2ca4886a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d5f994267e6d87a6f0daf44dc3a93266267b0b49e67e8f58e1d2faee2ca4886a->enter($__internal_d5f994267e6d87a6f0daf44dc3a93266267b0b49e67e8f58e1d2faee2ca4886a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_ef48d1664a132c54d92c5b2f38ada56a483d52bcf6c0a7c13757f8e80e599090 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef48d1664a132c54d92c5b2f38ada56a483d52bcf6c0a7c13757f8e80e599090->enter($__internal_ef48d1664a132c54d92c5b2f38ada56a483d52bcf6c0a7c13757f8e80e599090_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_ef48d1664a132c54d92c5b2f38ada56a483d52bcf6c0a7c13757f8e80e599090->leave($__internal_ef48d1664a132c54d92c5b2f38ada56a483d52bcf6c0a7c13757f8e80e599090_prof);

        
        $__internal_d5f994267e6d87a6f0daf44dc3a93266267b0b49e67e8f58e1d2faee2ca4886a->leave($__internal_d5f994267e6d87a6f0daf44dc3a93266267b0b49e67e8f58e1d2faee2ca4886a_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_integer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_integer.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit_integer.html.twig");
    }
}
