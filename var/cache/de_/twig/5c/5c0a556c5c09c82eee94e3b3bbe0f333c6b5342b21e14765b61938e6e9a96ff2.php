<?php

/* SonataAdminBundle:CRUD:show_html.html.twig */
class __TwigTemplate_7b3c48f2e90f81257e33e84af3c2a2ddd5f344f3fa39094b763b600eecd68c76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_html.html.twig", 1);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2a465d6f6ec00c3e1312aaad7fd25daeb190d7242ea6a0376804786afea328b9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2a465d6f6ec00c3e1312aaad7fd25daeb190d7242ea6a0376804786afea328b9->enter($__internal_2a465d6f6ec00c3e1312aaad7fd25daeb190d7242ea6a0376804786afea328b9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_html.html.twig"));

        $__internal_0bd78a1e5f888662d61dbceff0323a291513fb91b7df058a8d1eb7c255652d6a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0bd78a1e5f888662d61dbceff0323a291513fb91b7df058a8d1eb7c255652d6a->enter($__internal_0bd78a1e5f888662d61dbceff0323a291513fb91b7df058a8d1eb7c255652d6a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_html.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2a465d6f6ec00c3e1312aaad7fd25daeb190d7242ea6a0376804786afea328b9->leave($__internal_2a465d6f6ec00c3e1312aaad7fd25daeb190d7242ea6a0376804786afea328b9_prof);

        
        $__internal_0bd78a1e5f888662d61dbceff0323a291513fb91b7df058a8d1eb7c255652d6a->leave($__internal_0bd78a1e5f888662d61dbceff0323a291513fb91b7df058a8d1eb7c255652d6a_prof);

    }

    // line 3
    public function block_field($context, array $blocks = array())
    {
        $__internal_c047e87bf01b1a745768a3da9310384aea129692313c7ebea834efcfa0a0d9d8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c047e87bf01b1a745768a3da9310384aea129692313c7ebea834efcfa0a0d9d8->enter($__internal_c047e87bf01b1a745768a3da9310384aea129692313c7ebea834efcfa0a0d9d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_5eedcb5d135eebe8ab2e815ba380ced0454b9d98cc017df5fd77932e7151d5db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5eedcb5d135eebe8ab2e815ba380ced0454b9d98cc017df5fd77932e7151d5db->enter($__internal_5eedcb5d135eebe8ab2e815ba380ced0454b9d98cc017df5fd77932e7151d5db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 4
        if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 5
            echo "&nbsp;
    ";
        } else {
            // line 7
            if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "truncate", array(), "any", true, true)) {
                // line 8
                $context["truncate"] = $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "truncate", array());
                // line 9
                echo "            ";
                $context["length"] = (($this->getAttribute((isset($context["truncate"]) ? $context["truncate"] : null), "length", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["truncate"]) ? $context["truncate"] : null), "length", array()), 30)) : (30));
                // line 10
                echo "            ";
                $context["preserve"] = (($this->getAttribute((isset($context["truncate"]) ? $context["truncate"] : null), "preserve", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["truncate"]) ? $context["truncate"] : null), "preserve", array()), false)) : (false));
                // line 11
                echo "            ";
                $context["separator"] = (($this->getAttribute((isset($context["truncate"]) ? $context["truncate"] : null), "separator", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["truncate"]) ? $context["truncate"] : null), "separator", array()), "...")) : ("..."));
                // line 12
                echo "            ";
                echo twig_truncate_filter($this->env, strip_tags((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), (isset($context["length"]) ? $context["length"] : $this->getContext($context, "length")), (isset($context["preserve"]) ? $context["preserve"] : $this->getContext($context, "preserve")), (isset($context["separator"]) ? $context["separator"] : $this->getContext($context, "separator")));
            } else {
                // line 14
                if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "strip", array(), "any", true, true)) {
                    // line 15
                    $context["value"] = strip_tags((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
                }
                // line 17
                echo (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"));
                echo "
        ";
            }
            // line 19
            echo "    ";
        }
        
        $__internal_5eedcb5d135eebe8ab2e815ba380ced0454b9d98cc017df5fd77932e7151d5db->leave($__internal_5eedcb5d135eebe8ab2e815ba380ced0454b9d98cc017df5fd77932e7151d5db_prof);

        
        $__internal_c047e87bf01b1a745768a3da9310384aea129692313c7ebea834efcfa0a0d9d8->leave($__internal_c047e87bf01b1a745768a3da9310384aea129692313c7ebea834efcfa0a0d9d8_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_html.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 19,  77 => 17,  74 => 15,  72 => 14,  68 => 12,  65 => 11,  62 => 10,  59 => 9,  57 => 8,  55 => 7,  51 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field%}
    {%- if value is empty -%}
        &nbsp;
    {% else %}
        {%- if field_description.options.truncate is defined -%}
            {% set truncate = field_description.options.truncate %}
            {% set length = truncate.length|default(30) %}
            {% set preserve = truncate.preserve|default(false) %}
            {% set separator = truncate.separator|default('...') %}
            {{ value|striptags|truncate(length, preserve, separator)|raw }}
        {%- else -%}
            {%- if field_description.options.strip is defined -%}
                {% set value = value|striptags %}
            {%- endif -%}
            {{ value|raw }}
        {% endif %}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_html.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_html.html.twig");
    }
}
