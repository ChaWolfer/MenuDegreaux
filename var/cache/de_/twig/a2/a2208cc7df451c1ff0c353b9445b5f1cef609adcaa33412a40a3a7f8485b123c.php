<?php

/* SonataCoreBundle:Form:color.html.twig */
class __TwigTemplate_6df42ab446e6155cdabe1655541ad82460a95fadb7d480ebc00de33c416c26e9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'sonata_type_color_widget' => array($this, 'block_sonata_type_color_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_20dd835e667471d20e03f8d2faf39a4a60a62c11709c9efaa5a08c05a833c39f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_20dd835e667471d20e03f8d2faf39a4a60a62c11709c9efaa5a08c05a833c39f->enter($__internal_20dd835e667471d20e03f8d2faf39a4a60a62c11709c9efaa5a08c05a833c39f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataCoreBundle:Form:color.html.twig"));

        $__internal_458795d69349af2ee3a636d7e06c20129a5bf79ce97bff21bb7617eb66db8d13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_458795d69349af2ee3a636d7e06c20129a5bf79ce97bff21bb7617eb66db8d13->enter($__internal_458795d69349af2ee3a636d7e06c20129a5bf79ce97bff21bb7617eb66db8d13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataCoreBundle:Form:color.html.twig"));

        // line 11
        $this->displayBlock('sonata_type_color_widget', $context, $blocks);
        
        $__internal_20dd835e667471d20e03f8d2faf39a4a60a62c11709c9efaa5a08c05a833c39f->leave($__internal_20dd835e667471d20e03f8d2faf39a4a60a62c11709c9efaa5a08c05a833c39f_prof);

        
        $__internal_458795d69349af2ee3a636d7e06c20129a5bf79ce97bff21bb7617eb66db8d13->leave($__internal_458795d69349af2ee3a636d7e06c20129a5bf79ce97bff21bb7617eb66db8d13_prof);

    }

    public function block_sonata_type_color_widget($context, array $blocks = array())
    {
        $__internal_ad050f50930c4e6204bf453ff2d56ecc23a0685cffc798367cdaa543fa4d94ba = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad050f50930c4e6204bf453ff2d56ecc23a0685cffc798367cdaa543fa4d94ba->enter($__internal_ad050f50930c4e6204bf453ff2d56ecc23a0685cffc798367cdaa543fa4d94ba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_widget"));

        $__internal_8a451604d748ba96656db49f88033b2657a553e9cf53fc0775a88ea7e21c1a76 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a451604d748ba96656db49f88033b2657a553e9cf53fc0775a88ea7e21c1a76->enter($__internal_8a451604d748ba96656db49f88033b2657a553e9cf53fc0775a88ea7e21c1a76_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_type_color_widget"));

        // line 12
        echo "    ";
        ob_start();
        // line 13
        echo "        <input type=\"color\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        echo " ";
        if ( !twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            echo "value=\"";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "\" ";
        }
        echo "/>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_8a451604d748ba96656db49f88033b2657a553e9cf53fc0775a88ea7e21c1a76->leave($__internal_8a451604d748ba96656db49f88033b2657a553e9cf53fc0775a88ea7e21c1a76_prof);

        
        $__internal_ad050f50930c4e6204bf453ff2d56ecc23a0685cffc798367cdaa543fa4d94ba->leave($__internal_ad050f50930c4e6204bf453ff2d56ecc23a0685cffc798367cdaa543fa4d94ba_prof);

    }

    public function getTemplateName()
    {
        return "SonataCoreBundle:Form:color.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  47 => 13,  44 => 12,  26 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% block sonata_type_color_widget %}
    {% spaceless %}
        <input type=\"color\" {{ block('widget_attributes') }} {% if value is not empty %}value=\"{{ value }}\" {% endif %}/>
    {% endspaceless %}
{% endblock sonata_type_color_widget %}
", "SonataCoreBundle:Form:color.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\core-bundle/Resources/views/Form/color.html.twig");
    }
}
