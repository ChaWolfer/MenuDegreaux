<?php

/* SonataBlockBundle:Block:block_no_page_available.html.twig */
class __TwigTemplate_771b32d2b0107329ecf5ec8695ae3869465492f8f29c53dc3dec8efffbc50ca7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45a969bcfbc2b5a40126b8323360c2c034e69c9619c96f86dc068052139fd91b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45a969bcfbc2b5a40126b8323360c2c034e69c9619c96f86dc068052139fd91b->enter($__internal_45a969bcfbc2b5a40126b8323360c2c034e69c9619c96f86dc068052139fd91b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_no_page_available.html.twig"));

        $__internal_c43865efc719fcc504c79b2d546d3c4d9916d0a97b94f5a1c02d1f3ba684828f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c43865efc719fcc504c79b2d546d3c4d9916d0a97b94f5a1c02d1f3ba684828f->enter($__internal_c43865efc719fcc504c79b2d546d3c4d9916d0a97b94f5a1c02d1f3ba684828f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_no_page_available.html.twig"));

        
        $__internal_45a969bcfbc2b5a40126b8323360c2c034e69c9619c96f86dc068052139fd91b->leave($__internal_45a969bcfbc2b5a40126b8323360c2c034e69c9619c96f86dc068052139fd91b_prof);

        
        $__internal_c43865efc719fcc504c79b2d546d3c4d9916d0a97b94f5a1c02d1f3ba684828f->leave($__internal_c43865efc719fcc504c79b2d546d3c4d9916d0a97b94f5a1c02d1f3ba684828f_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_no_page_available.html.twig";
    }

    public function getDebugInfo()
    {
        return array ();
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
", "SonataBlockBundle:Block:block_no_page_available.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_no_page_available.html.twig");
    }
}
