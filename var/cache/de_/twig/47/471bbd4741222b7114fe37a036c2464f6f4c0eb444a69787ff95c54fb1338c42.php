<?php

/* SonataAdminBundle:CRUD:show.html.twig */
class __TwigTemplate_8c62b02db5d977a81d5d794a11f3328dfa274beba4c959104df0d5def8f18904 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show.html.twig", "SonataAdminBundle:CRUD:show.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_df4b0b9a400482679e427d2704454e55e1225e48e6c3a991633c13379275f53f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df4b0b9a400482679e427d2704454e55e1225e48e6c3a991633c13379275f53f->enter($__internal_df4b0b9a400482679e427d2704454e55e1225e48e6c3a991633c13379275f53f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show.html.twig"));

        $__internal_8a3e003714207a2f23f9de17e5080cd2ad7d9598724ddf11fc60ef086369caca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a3e003714207a2f23f9de17e5080cd2ad7d9598724ddf11fc60ef086369caca->enter($__internal_8a3e003714207a2f23f9de17e5080cd2ad7d9598724ddf11fc60ef086369caca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_df4b0b9a400482679e427d2704454e55e1225e48e6c3a991633c13379275f53f->leave($__internal_df4b0b9a400482679e427d2704454e55e1225e48e6c3a991633c13379275f53f_prof);

        
        $__internal_8a3e003714207a2f23f9de17e5080cd2ad7d9598724ddf11fc60ef086369caca->leave($__internal_8a3e003714207a2f23f9de17e5080cd2ad7d9598724ddf11fc60ef086369caca_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show.html.twig' %}
", "SonataAdminBundle:CRUD:show.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show.html.twig");
    }
}
