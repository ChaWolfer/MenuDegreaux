<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_faf3ef998e337cd241eabc2629c801ee6e3dfe11c518ef12e59890075d0ea177 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edafc50a053b7fa1102e227d1ccfef5cd5805165f22202b64310b6c9fd0ce52c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edafc50a053b7fa1102e227d1ccfef5cd5805165f22202b64310b6c9fd0ce52c->enter($__internal_edafc50a053b7fa1102e227d1ccfef5cd5805165f22202b64310b6c9fd0ce52c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_30cb2f69c967639ca3705fac5b628814256917ef8442d7cba0b71c8211626fdc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_30cb2f69c967639ca3705fac5b628814256917ef8442d7cba0b71c8211626fdc->enter($__internal_30cb2f69c967639ca3705fac5b628814256917ef8442d7cba0b71c8211626fdc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_edafc50a053b7fa1102e227d1ccfef5cd5805165f22202b64310b6c9fd0ce52c->leave($__internal_edafc50a053b7fa1102e227d1ccfef5cd5805165f22202b64310b6c9fd0ce52c_prof);

        
        $__internal_30cb2f69c967639ca3705fac5b628814256917ef8442d7cba0b71c8211626fdc->leave($__internal_30cb2f69c967639ca3705fac5b628814256917ef8442d7cba0b71c8211626fdc_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
