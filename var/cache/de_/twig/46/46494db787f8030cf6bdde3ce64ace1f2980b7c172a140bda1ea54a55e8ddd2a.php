<?php

/* knp_menu_base.html.twig */
class __TwigTemplate_3af69f20ab5389e1b0c774c3aa9653e7113cd9d5a435d21f3840c90ac8a5fbd5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e81d1ec69c6c95249117ff6dc6b21993f3a5514c6d5beb2dc2405cc2bc988b9e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e81d1ec69c6c95249117ff6dc6b21993f3a5514c6d5beb2dc2405cc2bc988b9e->enter($__internal_e81d1ec69c6c95249117ff6dc6b21993f3a5514c6d5beb2dc2405cc2bc988b9e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu_base.html.twig"));

        $__internal_e857c7d10f95ceae50771773a8f8228397db11ac4a4ee6662194832ddc28056e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e857c7d10f95ceae50771773a8f8228397db11ac4a4ee6662194832ddc28056e->enter($__internal_e857c7d10f95ceae50771773a8f8228397db11ac4a4ee6662194832ddc28056e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "knp_menu_base.html.twig"));

        // line 1
        if ($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "compressed", array())) {
            $this->displayBlock("compressed_root", $context, $blocks);
        } else {
            $this->displayBlock("root", $context, $blocks);
        }
        
        $__internal_e81d1ec69c6c95249117ff6dc6b21993f3a5514c6d5beb2dc2405cc2bc988b9e->leave($__internal_e81d1ec69c6c95249117ff6dc6b21993f3a5514c6d5beb2dc2405cc2bc988b9e_prof);

        
        $__internal_e857c7d10f95ceae50771773a8f8228397db11ac4a4ee6662194832ddc28056e->leave($__internal_e857c7d10f95ceae50771773a8f8228397db11ac4a4ee6662194832ddc28056e_prof);

    }

    public function getTemplateName()
    {
        return "knp_menu_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% if options.compressed %}{{ block('compressed_root') }}{% else %}{{ block('root') }}{% endif %}
", "knp_menu_base.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\knplabs\\knp-menu\\src\\Knp\\Menu\\Resources\\views\\knp_menu_base.html.twig");
    }
}
