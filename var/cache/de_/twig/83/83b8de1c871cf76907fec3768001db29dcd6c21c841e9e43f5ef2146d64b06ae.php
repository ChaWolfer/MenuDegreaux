<?php

/* FOSUserBundle:Group:show.html.twig */
class __TwigTemplate_1a8757381c6a251c8935c2a29fc0926ff5c37c506148ca5651e9413448983930 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Group:show.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f0256ea9860a77a61a6cc7a9e3a3f99e5a1b3220123389d0b5200a636e233a93 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f0256ea9860a77a61a6cc7a9e3a3f99e5a1b3220123389d0b5200a636e233a93->enter($__internal_f0256ea9860a77a61a6cc7a9e3a3f99e5a1b3220123389d0b5200a636e233a93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $__internal_1b735072106659c539d4d3fe95ad18931a35c6678b43829d121db5657af04342 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1b735072106659c539d4d3fe95ad18931a35c6678b43829d121db5657af04342->enter($__internal_1b735072106659c539d4d3fe95ad18931a35c6678b43829d121db5657af04342_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Group:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f0256ea9860a77a61a6cc7a9e3a3f99e5a1b3220123389d0b5200a636e233a93->leave($__internal_f0256ea9860a77a61a6cc7a9e3a3f99e5a1b3220123389d0b5200a636e233a93_prof);

        
        $__internal_1b735072106659c539d4d3fe95ad18931a35c6678b43829d121db5657af04342->leave($__internal_1b735072106659c539d4d3fe95ad18931a35c6678b43829d121db5657af04342_prof);

    }

    // line 3
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_f87faf38f08033ab52c84ae4a66781b55b07ae74f31ad79c8c02da70519e2595 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f87faf38f08033ab52c84ae4a66781b55b07ae74f31ad79c8c02da70519e2595->enter($__internal_f87faf38f08033ab52c84ae4a66781b55b07ae74f31ad79c8c02da70519e2595_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_33f5412b6d3bb75a79faab0ebd873e47dd97ff0211e40fce8628b946c51533f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_33f5412b6d3bb75a79faab0ebd873e47dd97ff0211e40fce8628b946c51533f9->enter($__internal_33f5412b6d3bb75a79faab0ebd873e47dd97ff0211e40fce8628b946c51533f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 4
        $this->loadTemplate("@FOSUser/Group/show_content.html.twig", "FOSUserBundle:Group:show.html.twig", 4)->display($context);
        
        $__internal_33f5412b6d3bb75a79faab0ebd873e47dd97ff0211e40fce8628b946c51533f9->leave($__internal_33f5412b6d3bb75a79faab0ebd873e47dd97ff0211e40fce8628b946c51533f9_prof);

        
        $__internal_f87faf38f08033ab52c84ae4a66781b55b07ae74f31ad79c8c02da70519e2595->leave($__internal_f87faf38f08033ab52c84ae4a66781b55b07ae74f31ad79c8c02da70519e2595_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Group:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% block fos_user_content %}
{% include \"@FOSUser/Group/show_content.html.twig\" %}
{% endblock fos_user_content %}
", "FOSUserBundle:Group:show.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Group/show.html.twig");
    }
}
