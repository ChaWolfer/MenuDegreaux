<?php

/* TwigBundle:Exception:error.html.twig */
class __TwigTemplate_42d807988b4b442d51de3b5cbfca2d121a5e0a3ec03c2d688f0c06913ff8288a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 11
        $this->parent = $this->loadTemplate("base.html.twig", "TwigBundle:Exception:error.html.twig", 11);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e24f5e09fd39a8bedce67a4f753c004c7c3ff73d72a8bb188872f3cd32253b28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e24f5e09fd39a8bedce67a4f753c004c7c3ff73d72a8bb188872f3cd32253b28->enter($__internal_e24f5e09fd39a8bedce67a4f753c004c7c3ff73d72a8bb188872f3cd32253b28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.html.twig"));

        $__internal_dabe8dc72ba855f7be23ac833d7217d1cd9e3b85b7dcd0de878a2ec6999832f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dabe8dc72ba855f7be23ac833d7217d1cd9e3b85b7dcd0de878a2ec6999832f0->enter($__internal_dabe8dc72ba855f7be23ac833d7217d1cd9e3b85b7dcd0de878a2ec6999832f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e24f5e09fd39a8bedce67a4f753c004c7c3ff73d72a8bb188872f3cd32253b28->leave($__internal_e24f5e09fd39a8bedce67a4f753c004c7c3ff73d72a8bb188872f3cd32253b28_prof);

        
        $__internal_dabe8dc72ba855f7be23ac833d7217d1cd9e3b85b7dcd0de878a2ec6999832f0->leave($__internal_dabe8dc72ba855f7be23ac833d7217d1cd9e3b85b7dcd0de878a2ec6999832f0_prof);

    }

    // line 13
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_c8610d7a33da95dda75f3c6ac384ab7ae0423e020bfd5d74c1f47643226ff5aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c8610d7a33da95dda75f3c6ac384ab7ae0423e020bfd5d74c1f47643226ff5aa->enter($__internal_c8610d7a33da95dda75f3c6ac384ab7ae0423e020bfd5d74c1f47643226ff5aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_93e5cb00b70e940130b416dfcab6c5ff3f0b36b803135513e79607d07a3327a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_93e5cb00b70e940130b416dfcab6c5ff3f0b36b803135513e79607d07a3327a9->enter($__internal_93e5cb00b70e940130b416dfcab6c5ff3f0b36b803135513e79607d07a3327a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "error";
        
        $__internal_93e5cb00b70e940130b416dfcab6c5ff3f0b36b803135513e79607d07a3327a9->leave($__internal_93e5cb00b70e940130b416dfcab6c5ff3f0b36b803135513e79607d07a3327a9_prof);

        
        $__internal_c8610d7a33da95dda75f3c6ac384ab7ae0423e020bfd5d74c1f47643226ff5aa->leave($__internal_c8610d7a33da95dda75f3c6ac384ab7ae0423e020bfd5d74c1f47643226ff5aa_prof);

    }

    // line 15
    public function block_main($context, array $blocks = array())
    {
        $__internal_fe412b1dd3bfa0ee4ba44d4f4f2ce1c8d02f0bfe0782a8c97e381f32bad1aecd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_fe412b1dd3bfa0ee4ba44d4f4f2ce1c8d02f0bfe0782a8c97e381f32bad1aecd->enter($__internal_fe412b1dd3bfa0ee4ba44d4f4f2ce1c8d02f0bfe0782a8c97e381f32bad1aecd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_c5769220a60670185e34de2c6818170672bdd558adf32945eaddca65ec73e595 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c5769220a60670185e34de2c6818170672bdd558adf32945eaddca65ec73e595->enter($__internal_c5769220a60670185e34de2c6818170672bdd558adf32945eaddca65ec73e595_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 16
        echo "    <h1 class=\"text-danger\">";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.name", array("%status_code%" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")))), "html", null, true);
        echo "</h1>

    <p class=\"lead\">
        ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.description", array("%status_code%" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")))), "html", null, true);
        echo "
    </p>
    <p>
        ";
        // line 22
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.suggestion", array("%url%" => $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index")));
        echo "
    </p>
";
        
        $__internal_c5769220a60670185e34de2c6818170672bdd558adf32945eaddca65ec73e595->leave($__internal_c5769220a60670185e34de2c6818170672bdd558adf32945eaddca65ec73e595_prof);

        
        $__internal_fe412b1dd3bfa0ee4ba44d4f4f2ce1c8d02f0bfe0782a8c97e381f32bad1aecd->leave($__internal_fe412b1dd3bfa0ee4ba44d4f4f2ce1c8d02f0bfe0782a8c97e381f32bad1aecd_prof);

    }

    // line 26
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_8a19d0695a32a0f48dd6355c38fd9719807e27ccba64a0b7c958ce6db4e0fc4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a19d0695a32a0f48dd6355c38fd9719807e27ccba64a0b7c958ce6db4e0fc4e->enter($__internal_8a19d0695a32a0f48dd6355c38fd9719807e27ccba64a0b7c958ce6db4e0fc4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_af5c5a3d8da8ae8ff1cbfd04ad79fd87b1a6a1d1901b08f9e06297f412952920 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_af5c5a3d8da8ae8ff1cbfd04ad79fd87b1a6a1d1901b08f9e06297f412952920->enter($__internal_af5c5a3d8da8ae8ff1cbfd04ad79fd87b1a6a1d1901b08f9e06297f412952920_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 27
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 29
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_af5c5a3d8da8ae8ff1cbfd04ad79fd87b1a6a1d1901b08f9e06297f412952920->leave($__internal_af5c5a3d8da8ae8ff1cbfd04ad79fd87b1a6a1d1901b08f9e06297f412952920_prof);

        
        $__internal_8a19d0695a32a0f48dd6355c38fd9719807e27ccba64a0b7c958ce6db4e0fc4e->leave($__internal_8a19d0695a32a0f48dd6355c38fd9719807e27ccba64a0b7c958ce6db4e0fc4e_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 29,  104 => 27,  95 => 26,  82 => 22,  76 => 19,  69 => 16,  60 => 15,  42 => 13,  11 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
    This template is used to render any error different from 403, 404 and 500.

    This is the simplest way to customize error pages in Symfony applications.
    In case you need it, you can also hook into the internal exception handling
    made by Symfony. This allows you to perform advanced tasks and even recover
    your application from some errors.
    See http://symfony.com/doc/current/cookbook/controller/error_pages.html
#}

{% extends 'base.html.twig' %}

{% block body_id 'error' %}

{% block main %}
    <h1 class=\"text-danger\">{{ 'http_error.name'|trans({ '%status_code%': status_code }) }}</h1>

    <p class=\"lead\">
        {{ 'http_error.description'|trans({ '%status_code%': status_code }) }}
    </p>
    <p>
        {{ 'http_error.suggestion'|trans({ '%url%': path('blog_index') })|raw }}
    </p>
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}
", "TwigBundle:Exception:error.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources/TwigBundle/views/Exception/error.html.twig");
    }
}
