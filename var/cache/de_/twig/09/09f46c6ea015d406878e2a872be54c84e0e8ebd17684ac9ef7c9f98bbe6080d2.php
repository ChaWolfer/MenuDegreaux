<?php

/* SonataAdminBundle:CRUD:list_array.html.twig */
class __TwigTemplate_aa54f0a8fb263e0fd683f34796d9ad8b0b30ed7b686a7d3e7d307abb2bad9b7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 13
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_array.html.twig", 13);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ca963e51fcc2fb0862041a37f7a5530d0dcd9dfabadaa7b5b97988f9b96d92df = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ca963e51fcc2fb0862041a37f7a5530d0dcd9dfabadaa7b5b97988f9b96d92df->enter($__internal_ca963e51fcc2fb0862041a37f7a5530d0dcd9dfabadaa7b5b97988f9b96d92df_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_array.html.twig"));

        $__internal_f95108fb4bf324f2c100ea7368ae6a23b13d3bdee1e4de6073883326d3b11486 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f95108fb4bf324f2c100ea7368ae6a23b13d3bdee1e4de6073883326d3b11486->enter($__internal_f95108fb4bf324f2c100ea7368ae6a23b13d3bdee1e4de6073883326d3b11486_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_array.html.twig"));

        // line 11
        $context["list"] = $this->loadTemplate("SonataAdminBundle:CRUD:base_array_macro.html.twig", "SonataAdminBundle:CRUD:list_array.html.twig", 11);
        // line 13
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ca963e51fcc2fb0862041a37f7a5530d0dcd9dfabadaa7b5b97988f9b96d92df->leave($__internal_ca963e51fcc2fb0862041a37f7a5530d0dcd9dfabadaa7b5b97988f9b96d92df_prof);

        
        $__internal_f95108fb4bf324f2c100ea7368ae6a23b13d3bdee1e4de6073883326d3b11486->leave($__internal_f95108fb4bf324f2c100ea7368ae6a23b13d3bdee1e4de6073883326d3b11486_prof);

    }

    // line 15
    public function block_field($context, array $blocks = array())
    {
        $__internal_4774437c2c148d6ca25f63ad9fd15046f85dd1bb42c83e21017b9c2516a73dc9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4774437c2c148d6ca25f63ad9fd15046f85dd1bb42c83e21017b9c2516a73dc9->enter($__internal_4774437c2c148d6ca25f63ad9fd15046f85dd1bb42c83e21017b9c2516a73dc9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_8e60947d51741acc5cee07f230e6e20124f298481f771c2d8b5473f655c06bc1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e60947d51741acc5cee07f230e6e20124f298481f771c2d8b5473f655c06bc1->enter($__internal_8e60947d51741acc5cee07f230e6e20124f298481f771c2d8b5473f655c06bc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 16
        echo "    ";
        echo $context["list"]->getrender_array((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), ( !$this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "inline", array(), "any", true, true) || $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "inline", array())));
        echo "
";
        
        $__internal_8e60947d51741acc5cee07f230e6e20124f298481f771c2d8b5473f655c06bc1->leave($__internal_8e60947d51741acc5cee07f230e6e20124f298481f771c2d8b5473f655c06bc1_prof);

        
        $__internal_4774437c2c148d6ca25f63ad9fd15046f85dd1bb42c83e21017b9c2516a73dc9->leave($__internal_4774437c2c148d6ca25f63ad9fd15046f85dd1bb42c83e21017b9c2516a73dc9_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_array.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  42 => 15,  32 => 13,  30 => 11,  18 => 13,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
{% import 'SonataAdminBundle:CRUD:base_array_macro.html.twig' as list %}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {{ list.render_array(value, field_description.options.inline is not defined or field_description.options.inline) }}
{% endblock %}
", "SonataAdminBundle:CRUD:list_array.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_array.html.twig");
    }
}
