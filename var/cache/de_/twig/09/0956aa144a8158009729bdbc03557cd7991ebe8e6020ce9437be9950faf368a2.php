<?php

/* SonataAdminBundle:CRUD:list_email.html.twig */
class __TwigTemplate_f813252100dcbd0113d1bd05bc425d8bb294318aa2a0a62df7f5e7d9e256c35b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_email.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3c279ad3b91a11ecde064a8fe2c2dadb1e01d2e41d16b6767ed8edd97ccf930e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3c279ad3b91a11ecde064a8fe2c2dadb1e01d2e41d16b6767ed8edd97ccf930e->enter($__internal_3c279ad3b91a11ecde064a8fe2c2dadb1e01d2e41d16b6767ed8edd97ccf930e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_email.html.twig"));

        $__internal_80bf953e823912b2d0d2c1806baf891c692fe12e2938402ec45dded7073fd023 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_80bf953e823912b2d0d2c1806baf891c692fe12e2938402ec45dded7073fd023->enter($__internal_80bf953e823912b2d0d2c1806baf891c692fe12e2938402ec45dded7073fd023_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_email.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_3c279ad3b91a11ecde064a8fe2c2dadb1e01d2e41d16b6767ed8edd97ccf930e->leave($__internal_3c279ad3b91a11ecde064a8fe2c2dadb1e01d2e41d16b6767ed8edd97ccf930e_prof);

        
        $__internal_80bf953e823912b2d0d2c1806baf891c692fe12e2938402ec45dded7073fd023->leave($__internal_80bf953e823912b2d0d2c1806baf891c692fe12e2938402ec45dded7073fd023_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_74f041baaa2856b438e55f8d606c308ad3344a0bac27d3c4a558d126d851c683 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_74f041baaa2856b438e55f8d606c308ad3344a0bac27d3c4a558d126d851c683->enter($__internal_74f041baaa2856b438e55f8d606c308ad3344a0bac27d3c4a558d126d851c683_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_43c9a39af147136b62e33dd7bb8e3bbc506a9f76f551e1ba15505adb815d722a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_43c9a39af147136b62e33dd7bb8e3bbc506a9f76f551e1ba15505adb815d722a->enter($__internal_43c9a39af147136b62e33dd7bb8e3bbc506a9f76f551e1ba15505adb815d722a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $this->loadTemplate("SonataAdminBundle:CRUD:_email_link.html.twig", "SonataAdminBundle:CRUD:list_email.html.twig", 15)->display($context);
        
        $__internal_43c9a39af147136b62e33dd7bb8e3bbc506a9f76f551e1ba15505adb815d722a->leave($__internal_43c9a39af147136b62e33dd7bb8e3bbc506a9f76f551e1ba15505adb815d722a_prof);

        
        $__internal_74f041baaa2856b438e55f8d606c308ad3344a0bac27d3c4a558d126d851c683->leave($__internal_74f041baaa2856b438e55f8d606c308ad3344a0bac27d3c4a558d126d851c683_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% include 'SonataAdminBundle:CRUD:_email_link.html.twig' %}
{% endblock %}
", "SonataAdminBundle:CRUD:list_email.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_email.html.twig");
    }
}
