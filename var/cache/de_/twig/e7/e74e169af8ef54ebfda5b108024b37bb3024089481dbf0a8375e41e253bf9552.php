<?php

/* AppBundle:CRUD:list_batch.html.twig */
class __TwigTemplate_2ec4ca2f47de5956ae21e09d27a22bde2217567c6ae0e2dbce4c2a115494148b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 4
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "AppBundle:CRUD:list_batch.html.twig", 4);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f02b67ef79402125a35db76bace9bd1609f3e1930abaf8dee41cd21de186b6c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f02b67ef79402125a35db76bace9bd1609f3e1930abaf8dee41cd21de186b6c3->enter($__internal_f02b67ef79402125a35db76bace9bd1609f3e1930abaf8dee41cd21de186b6c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:CRUD:list_batch.html.twig"));

        $__internal_ff318565615ec9e00ebed6ebec6087ef9d71ef4f731ec5fac0e618e4c7fb721b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff318565615ec9e00ebed6ebec6087ef9d71ef4f731ec5fac0e618e4c7fb721b->enter($__internal_ff318565615ec9e00ebed6ebec6087ef9d71ef4f731ec5fac0e618e4c7fb721b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "AppBundle:CRUD:list_batch.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f02b67ef79402125a35db76bace9bd1609f3e1930abaf8dee41cd21de186b6c3->leave($__internal_f02b67ef79402125a35db76bace9bd1609f3e1930abaf8dee41cd21de186b6c3_prof);

        
        $__internal_ff318565615ec9e00ebed6ebec6087ef9d71ef4f731ec5fac0e618e4c7fb721b->leave($__internal_ff318565615ec9e00ebed6ebec6087ef9d71ef4f731ec5fac0e618e4c7fb721b_prof);

    }

    // line 6
    public function block_field($context, array $blocks = array())
    {
        $__internal_7ef10d16f6b541b49a9684b803ec3a274f8b3bf27fb33292bc1fc16d31cedfed = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7ef10d16f6b541b49a9684b803ec3a274f8b3bf27fb33292bc1fc16d31cedfed->enter($__internal_7ef10d16f6b541b49a9684b803ec3a274f8b3bf27fb33292bc1fc16d31cedfed_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_c81cbe8aebcb46fb049eedfdfa78377d237302787bce885c3ee4e44f582dfa99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c81cbe8aebcb46fb049eedfdfa78377d237302787bce885c3ee4e44f582dfa99->enter($__internal_c81cbe8aebcb46fb049eedfdfa78377d237302787bce885c3ee4e44f582dfa99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 7
        echo "    <input type=\"checkbox\" name=\"idx[]\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
        echo "\" />

    ";
        // line 10
        echo "    <input type=\"radio\" name=\"targetId\" value=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "id", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
        echo "\" />
";
        
        $__internal_c81cbe8aebcb46fb049eedfdfa78377d237302787bce885c3ee4e44f582dfa99->leave($__internal_c81cbe8aebcb46fb049eedfdfa78377d237302787bce885c3ee4e44f582dfa99_prof);

        
        $__internal_7ef10d16f6b541b49a9684b803ec3a274f8b3bf27fb33292bc1fc16d31cedfed->leave($__internal_7ef10d16f6b541b49a9684b803ec3a274f8b3bf27fb33292bc1fc16d31cedfed_prof);

    }

    public function getTemplateName()
    {
        return "AppBundle:CRUD:list_batch.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 10,  48 => 7,  39 => 6,  18 => 4,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# src/AppBundle/Resources/views/CRUD/list__batch.html.twig #}
{# see SonataAdminBundle:CRUD:list__batch.html.twig for the current default template #}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <input type=\"checkbox\" name=\"idx[]\" value=\"{{ admin.id(object) }}\" />

    {# the new radio button #}
    <input type=\"radio\" name=\"targetId\" value=\"{{ admin.id(object) }}\" />
{% endblock %}
", "AppBundle:CRUD:list_batch.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\src\\AppBundle/Resources/views/CRUD/list_batch.html.twig");
    }
}
