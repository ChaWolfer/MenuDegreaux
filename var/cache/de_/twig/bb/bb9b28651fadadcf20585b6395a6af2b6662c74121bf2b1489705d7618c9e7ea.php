<?php

/* TwigBundle:Exception:error.json.twig */
class __TwigTemplate_2b618e64f21f4701c49be11103de79b8d1f83dbc48f9e7600219b825f7a5e8e3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1f08fb736e90775b07413a2b0f7700186624ee167a7df5de25816b12b5a1b42a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1f08fb736e90775b07413a2b0f7700186624ee167a7df5de25816b12b5a1b42a->enter($__internal_1f08fb736e90775b07413a2b0f7700186624ee167a7df5de25816b12b5a1b42a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        $__internal_54b32d2ac76110f09703ababb401c34e65e3f15e65b3824fef6d967735f4c353 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_54b32d2ac76110f09703ababb401c34e65e3f15e65b3824fef6d967735f4c353->enter($__internal_54b32d2ac76110f09703ababb401c34e65e3f15e65b3824fef6d967735f4c353_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")))));
        echo "
";
        
        $__internal_1f08fb736e90775b07413a2b0f7700186624ee167a7df5de25816b12b5a1b42a->leave($__internal_1f08fb736e90775b07413a2b0f7700186624ee167a7df5de25816b12b5a1b42a_prof);

        
        $__internal_54b32d2ac76110f09703ababb401c34e65e3f15e65b3824fef6d967735f4c353->leave($__internal_54b32d2ac76110f09703ababb401c34e65e3f15e65b3824fef6d967735f4c353_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text } }|json_encode|raw }}
", "TwigBundle:Exception:error.json.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/error.json.twig");
    }
}
