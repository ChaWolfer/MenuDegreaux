<?php

/* @SonataBlock/Block/block_core_action.html.twig */
class __TwigTemplate_e829e0440d8c5dbb35e3b8c194769dbb2023f5c220c155bdae470c2fadde414d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "@SonataBlock/Block/block_core_action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e04a6c8d3c785eb67a603b278a7cedf674aea50477d2ab46a38bb9cad1829b8c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e04a6c8d3c785eb67a603b278a7cedf674aea50477d2ab46a38bb9cad1829b8c->enter($__internal_e04a6c8d3c785eb67a603b278a7cedf674aea50477d2ab46a38bb9cad1829b8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_core_action.html.twig"));

        $__internal_95c4dd40c6c24c8141423adaf499a98238ba6ffab4c15d23895fa14806446f29 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_95c4dd40c6c24c8141423adaf499a98238ba6ffab4c15d23895fa14806446f29->enter($__internal_95c4dd40c6c24c8141423adaf499a98238ba6ffab4c15d23895fa14806446f29_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_core_action.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e04a6c8d3c785eb67a603b278a7cedf674aea50477d2ab46a38bb9cad1829b8c->leave($__internal_e04a6c8d3c785eb67a603b278a7cedf674aea50477d2ab46a38bb9cad1829b8c_prof);

        
        $__internal_95c4dd40c6c24c8141423adaf499a98238ba6ffab4c15d23895fa14806446f29->leave($__internal_95c4dd40c6c24c8141423adaf499a98238ba6ffab4c15d23895fa14806446f29_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_ddb90692005a723247fb2f634e07b37f53453de7d9517c57089f8d01ee8b67be = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ddb90692005a723247fb2f634e07b37f53453de7d9517c57089f8d01ee8b67be->enter($__internal_ddb90692005a723247fb2f634e07b37f53453de7d9517c57089f8d01ee8b67be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_ed158c754eda0d5c97885c0911447fffb98f806371243a5c7a58dd3db4704c13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed158c754eda0d5c97885c0911447fffb98f806371243a5c7a58dd3db4704c13->enter($__internal_ed158c754eda0d5c97885c0911447fffb98f806371243a5c7a58dd3db4704c13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo (isset($context["content"]) ? $context["content"] : $this->getContext($context, "content"));
        echo "
";
        
        $__internal_ed158c754eda0d5c97885c0911447fffb98f806371243a5c7a58dd3db4704c13->leave($__internal_ed158c754eda0d5c97885c0911447fffb98f806371243a5c7a58dd3db4704c13_prof);

        
        $__internal_ddb90692005a723247fb2f634e07b37f53453de7d9517c57089f8d01ee8b67be->leave($__internal_ddb90692005a723247fb2f634e07b37f53453de7d9517c57089f8d01ee8b67be_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_core_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ content|raw }}
{% endblock %}
", "@SonataBlock/Block/block_core_action.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_core_action.html.twig");
    }
}
