<?php

/* SonataBlockBundle:Block:block_template.html.twig */
class __TwigTemplate_7c23df5e0fd45edf5c66446a7c6a59bd8ae02d01a16137a369673f581f11b87e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_template.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_99b5160d3aec828c04037fbe7fab817d9150e88ca88509626b71d9165045d3e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_99b5160d3aec828c04037fbe7fab817d9150e88ca88509626b71d9165045d3e3->enter($__internal_99b5160d3aec828c04037fbe7fab817d9150e88ca88509626b71d9165045d3e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_template.html.twig"));

        $__internal_c38f4eb703ce09f00de8a914b78fb046556e0b60dcc4db8ae6ec4a73d4a64141 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c38f4eb703ce09f00de8a914b78fb046556e0b60dcc4db8ae6ec4a73d4a64141->enter($__internal_c38f4eb703ce09f00de8a914b78fb046556e0b60dcc4db8ae6ec4a73d4a64141_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_template.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_99b5160d3aec828c04037fbe7fab817d9150e88ca88509626b71d9165045d3e3->leave($__internal_99b5160d3aec828c04037fbe7fab817d9150e88ca88509626b71d9165045d3e3_prof);

        
        $__internal_c38f4eb703ce09f00de8a914b78fb046556e0b60dcc4db8ae6ec4a73d4a64141->leave($__internal_c38f4eb703ce09f00de8a914b78fb046556e0b60dcc4db8ae6ec4a73d4a64141_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_932cbb84f1498f6c558835ef365e10fbf41b50a7d3103e08b9db5e99e6ea9041 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_932cbb84f1498f6c558835ef365e10fbf41b50a7d3103e08b9db5e99e6ea9041->enter($__internal_932cbb84f1498f6c558835ef365e10fbf41b50a7d3103e08b9db5e99e6ea9041_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_6cbebe18fb23ec76872edc45483751274e458569dd4717c8741576e3bf40fcb3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cbebe18fb23ec76872edc45483751274e458569dd4717c8741576e3bf40fcb3->enter($__internal_6cbebe18fb23ec76872edc45483751274e458569dd4717c8741576e3bf40fcb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>";
        // line 33
        echo "{# file: 'MyBundle:Block:my_block_feature_1.html.twig' #}
{% extends sonata_block.templates.block_base %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}";
        echo "</pre>

    And then call it from a template with the <code>sonata_block_render</code> helper:

    <pre>";
        // line 43
        echo "{{ sonata_block_render({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:my_block_feature_1.html.twig',
}) }}";
        echo "</pre>
";
        
        $__internal_6cbebe18fb23ec76872edc45483751274e458569dd4717c8741576e3bf40fcb3->leave($__internal_6cbebe18fb23ec76872edc45483751274e458569dd4717c8741576e3bf40fcb3_prof);

        
        $__internal_932cbb84f1498f6c558835ef365e10fbf41b50a7d3103e08b9db5e99e6ea9041->leave($__internal_932cbb84f1498f6c558835ef365e10fbf41b50a7d3103e08b9db5e99e6ea9041_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 43,  53 => 33,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>
        {%- verbatim -%}
{# file: 'MyBundle:Block:my_block_feature_1.html.twig' #}
{% extends sonata_block.templates.block_base %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}
        {%- endverbatim -%}
    </pre>

    And then call it from a template with the <code>sonata_block_render</code> helper:

    <pre>
{%- verbatim -%}
{{ sonata_block_render({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:my_block_feature_1.html.twig',
}) }}
{%- endverbatim -%}
    </pre>
{% endblock %}
", "SonataBlockBundle:Block:block_template.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_template.html.twig");
    }
}
