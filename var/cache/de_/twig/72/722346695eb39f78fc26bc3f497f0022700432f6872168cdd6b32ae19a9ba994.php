<?php

/* TwigBundle::layout.html.twig */
class __TwigTemplate_e54c01f1ab11170c03e2a5f2fedac9ebbcbc673a5199c36f04bd3a0d57e456bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_224642cabe49b4d26ed1540b8b12e4ee81a9df9d6ada4c42c266c435954d07f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_224642cabe49b4d26ed1540b8b12e4ee81a9df9d6ada4c42c266c435954d07f5->enter($__internal_224642cabe49b4d26ed1540b8b12e4ee81a9df9d6ada4c42c266c435954d07f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        $__internal_c68ced8f13b2b75f00be71f739423834d6b977f21a25415dec7f74903d5b17d2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c68ced8f13b2b75f00be71f739423834d6b977f21a25415dec7f74903d5b17d2->enter($__internal_c68ced8f13b2b75f00be71f739423834d6b977f21a25415dec7f74903d5b17d2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle::layout.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        <link rel=\"icon\" type=\"image/png\" href=\"";
        // line 8
        echo twig_include($this->env, $context, "@Twig/images/favicon.png.base64");
        echo "\">
        <style>";
        // line 9
        echo twig_include($this->env, $context, "@Twig/exception.css.twig");
        echo "</style>
        ";
        // line 10
        $this->displayBlock('head', $context, $blocks);
        // line 11
        echo "    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">";
        // line 15
        echo twig_include($this->env, $context, "@Twig/images/symfony-logo.svg");
        echo " Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">";
        // line 19
        echo twig_include($this->env, $context, "@Twig/images/icon-book.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">";
        // line 26
        echo twig_include($this->env, $context, "@Twig/images/icon-support.svg");
        echo "</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        ";
        // line 33
        $this->displayBlock('body', $context, $blocks);
        // line 34
        echo "        ";
        echo twig_include($this->env, $context, "@Twig/base_js.html.twig");
        echo "
    </body>
</html>
";
        
        $__internal_224642cabe49b4d26ed1540b8b12e4ee81a9df9d6ada4c42c266c435954d07f5->leave($__internal_224642cabe49b4d26ed1540b8b12e4ee81a9df9d6ada4c42c266c435954d07f5_prof);

        
        $__internal_c68ced8f13b2b75f00be71f739423834d6b977f21a25415dec7f74903d5b17d2->leave($__internal_c68ced8f13b2b75f00be71f739423834d6b977f21a25415dec7f74903d5b17d2_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_71436096d29cf645068022c022a41f9078de393e7c23dc289250a5b00661b105 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71436096d29cf645068022c022a41f9078de393e7c23dc289250a5b00661b105->enter($__internal_71436096d29cf645068022c022a41f9078de393e7c23dc289250a5b00661b105_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        $__internal_a076f9538cbe194d11e11f3828196808c0a41bff5a6c3efa90750f103456f0f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a076f9538cbe194d11e11f3828196808c0a41bff5a6c3efa90750f103456f0f0->enter($__internal_a076f9538cbe194d11e11f3828196808c0a41bff5a6c3efa90750f103456f0f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_a076f9538cbe194d11e11f3828196808c0a41bff5a6c3efa90750f103456f0f0->leave($__internal_a076f9538cbe194d11e11f3828196808c0a41bff5a6c3efa90750f103456f0f0_prof);

        
        $__internal_71436096d29cf645068022c022a41f9078de393e7c23dc289250a5b00661b105->leave($__internal_71436096d29cf645068022c022a41f9078de393e7c23dc289250a5b00661b105_prof);

    }

    // line 10
    public function block_head($context, array $blocks = array())
    {
        $__internal_8794a7fa7bdec61bbf4a2656be12a91325b76e3286f40769d0d9ef77c46628f4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8794a7fa7bdec61bbf4a2656be12a91325b76e3286f40769d0d9ef77c46628f4->enter($__internal_8794a7fa7bdec61bbf4a2656be12a91325b76e3286f40769d0d9ef77c46628f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        $__internal_7acdc14240cf852f13d1f531bddbe6300b8ee889878df3b949ed409db9696b8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7acdc14240cf852f13d1f531bddbe6300b8ee889878df3b949ed409db9696b8c->enter($__internal_7acdc14240cf852f13d1f531bddbe6300b8ee889878df3b949ed409db9696b8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        
        $__internal_7acdc14240cf852f13d1f531bddbe6300b8ee889878df3b949ed409db9696b8c->leave($__internal_7acdc14240cf852f13d1f531bddbe6300b8ee889878df3b949ed409db9696b8c_prof);

        
        $__internal_8794a7fa7bdec61bbf4a2656be12a91325b76e3286f40769d0d9ef77c46628f4->leave($__internal_8794a7fa7bdec61bbf4a2656be12a91325b76e3286f40769d0d9ef77c46628f4_prof);

    }

    // line 33
    public function block_body($context, array $blocks = array())
    {
        $__internal_d1d8648d7a04861534accbc338b8c0514ad64f7f4407d142a0ace9c54e5c6ded = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1d8648d7a04861534accbc338b8c0514ad64f7f4407d142a0ace9c54e5c6ded->enter($__internal_d1d8648d7a04861534accbc338b8c0514ad64f7f4407d142a0ace9c54e5c6ded_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_5c46fe881077a78e5bddf35a5ee3f84ae29a68cc6158d79cc8a45545e9a028e9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c46fe881077a78e5bddf35a5ee3f84ae29a68cc6158d79cc8a45545e9a028e9->enter($__internal_5c46fe881077a78e5bddf35a5ee3f84ae29a68cc6158d79cc8a45545e9a028e9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_5c46fe881077a78e5bddf35a5ee3f84ae29a68cc6158d79cc8a45545e9a028e9->leave($__internal_5c46fe881077a78e5bddf35a5ee3f84ae29a68cc6158d79cc8a45545e9a028e9_prof);

        
        $__internal_d1d8648d7a04861534accbc338b8c0514ad64f7f4407d142a0ace9c54e5c6ded->leave($__internal_d1d8648d7a04861534accbc338b8c0514ad64f7f4407d142a0ace9c54e5c6ded_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle::layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 33,  120 => 10,  103 => 7,  88 => 34,  86 => 33,  76 => 26,  66 => 19,  59 => 15,  53 => 11,  51 => 10,  47 => 9,  43 => 8,  39 => 7,  33 => 4,  28 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"{{ _charset }}\" />
        <meta name=\"robots\" content=\"noindex,nofollow\" />
        <meta name=\"viewport\" content=\"width=device-width,initial-scale=1\" />
        <title>{% block title %}{% endblock %}</title>
        <link rel=\"icon\" type=\"image/png\" href=\"{{ include('@Twig/images/favicon.png.base64') }}\">
        <style>{{ include('@Twig/exception.css.twig') }}</style>
        {% block head %}{% endblock %}
    </head>
    <body>
        <header>
            <div class=\"container\">
                <h1 class=\"logo\">{{ include('@Twig/images/symfony-logo.svg') }} Symfony Exception</h1>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/doc\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-book.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Docs
                    </a>
                </div>

                <div class=\"help-link\">
                    <a href=\"https://symfony.com/support\">
                        <span class=\"icon\">{{ include('@Twig/images/icon-support.svg') }}</span>
                        <span class=\"hidden-xs-down\">Symfony</span> Support
                    </a>
                </div>
            </div>
        </header>

        {% block body %}{% endblock %}
        {{ include('@Twig/base_js.html.twig') }}
    </body>
</html>
", "TwigBundle::layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/layout.html.twig");
    }
}
