<?php

/* :menu:resident.html.twig */
class __TwigTemplate_9dfe2ee93ff95e1840d92945aeb63a68dbdbe75d51abf48f81e5d4377fa49aea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":menu:resident.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2ee59566cdf822edf395a8f8b884a18f63a30ff53729262cbc5b1847dfd6f605 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ee59566cdf822edf395a8f8b884a18f63a30ff53729262cbc5b1847dfd6f605->enter($__internal_2ee59566cdf822edf395a8f8b884a18f63a30ff53729262cbc5b1847dfd6f605_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:resident.html.twig"));

        $__internal_5db2e6d719bda2f40c278342f1f485a8293598e06cd8f841a40d598e039bbd7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5db2e6d719bda2f40c278342f1f485a8293598e06cd8f841a40d598e039bbd7c->enter($__internal_5db2e6d719bda2f40c278342f1f485a8293598e06cd8f841a40d598e039bbd7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:resident.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2ee59566cdf822edf395a8f8b884a18f63a30ff53729262cbc5b1847dfd6f605->leave($__internal_2ee59566cdf822edf395a8f8b884a18f63a30ff53729262cbc5b1847dfd6f605_prof);

        
        $__internal_5db2e6d719bda2f40c278342f1f485a8293598e06cd8f841a40d598e039bbd7c->leave($__internal_5db2e6d719bda2f40c278342f1f485a8293598e06cd8f841a40d598e039bbd7c_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_f624f41e53b85966ab401e13cf240198e77cd6dddc9bcc1f9605c960d447b2e0 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f624f41e53b85966ab401e13cf240198e77cd6dddc9bcc1f9605c960d447b2e0->enter($__internal_f624f41e53b85966ab401e13cf240198e77cd6dddc9bcc1f9605c960d447b2e0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_e344d51e1c0fe3a383bb062a649a503532008230d188566080950fce11015fd3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e344d51e1c0fe3a383bb062a649a503532008230d188566080950fce11015fd3->enter($__internal_e344d51e1c0fe3a383bb062a649a503532008230d188566080950fce11015fd3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_resident";
        
        $__internal_e344d51e1c0fe3a383bb062a649a503532008230d188566080950fce11015fd3->leave($__internal_e344d51e1c0fe3a383bb062a649a503532008230d188566080950fce11015fd3_prof);

        
        $__internal_f624f41e53b85966ab401e13cf240198e77cd6dddc9bcc1f9605c960d447b2e0->leave($__internal_f624f41e53b85966ab401e13cf240198e77cd6dddc9bcc1f9605c960d447b2e0_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_55787af3cc07f6743652ee8362a86e74001f09dad5d4fbc29921c99a7e48a791 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_55787af3cc07f6743652ee8362a86e74001f09dad5d4fbc29921c99a7e48a791->enter($__internal_55787af3cc07f6743652ee8362a86e74001f09dad5d4fbc29921c99a7e48a791_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_f5d92fa7406fa362fc14e3c6a20c123ac7d9f0ef019fd40f282ab1ed75b1fbe3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5d92fa7406fa362fc14e3c6a20c123ac7d9f0ef019fd40f282ab1ed75b1fbe3->enter($__internal_f5d92fa7406fa362fc14e3c6a20c123ac7d9f0ef019fd40f282ab1ed75b1fbe3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "<h1> Menu Résident</h1>
<p class=\"subtitle\"> Menu du midi et du soir</p>


    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Lundi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "lundiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 29
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 30
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 32
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "lundiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>


    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Mardi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 51
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mardiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 58
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 59
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 60
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 61
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mardiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Mercredi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 76
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 77
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 86
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 87
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 88
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 89
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>
    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Jeudi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 103
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "JeudiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 104
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "JeudiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 105
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "JeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 106
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "JeudiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 113
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "JeudiEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "JeudiPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 115
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "JeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 116
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "JeudiDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Vendredi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 131
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 132
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 133
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 134
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 141
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 142
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 143
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 144
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Samedi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 159
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 160
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 161
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 162
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "samediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">";
        // line 169
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "samediEntree", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 170
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "samediPlat", array()), "html", null, true);
        echo "</div>
                <div class=\"text\">";
        // line 171
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "samediAccompagnement", array()), "html", null, true);
        echo "</div>
                <div class=\" text\">";
        // line 172
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuSoir"]) ? $context["menuSoir"] : $this->getContext($context, "menuSoir")), "samediDessert", array()), "html", null, true);
        echo "</div>
            </div>
        </div>

    </div>
    <div class=\"row ligne\">

    <div class=\"col-md-2 col-xs-12\">
        <div class=\"titre\">Dimanche</div>
    </div>

    <div class=\"col-md-5 col-xs-12\">
        <div class=\"sous-titre\">Midi</div>
        <div class=\"menu\">
            <div class=\" text\">";
        // line 186
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheEntree", array()), "html", null, true);
        echo "</div>
            <div class=\" text\">";
        // line 187
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimanchePlat", array()), "html", null, true);
        echo "</div>
            <div class=\"text\">";
        // line 188
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheAccompagnement", array()), "html", null, true);
        echo "</div>
            <div class=\" text\">";
        // line 189
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuMidi"]) ? $context["menuMidi"] : $this->getContext($context, "menuMidi")), "dimancheDessert", array()), "html", null, true);
        echo "</div>
        </div>
    </div>
    <div class=\"col-md-5 col-xs-12\">
        <div class=\"sous-titre\">Soir</div>
        <div class=\"menu\">
            <div class=\" text\">Pas de menu le dimanche soir</div>
        </div>
    </div>

</div>


";
        
        $__internal_f5d92fa7406fa362fc14e3c6a20c123ac7d9f0ef019fd40f282ab1ed75b1fbe3->leave($__internal_f5d92fa7406fa362fc14e3c6a20c123ac7d9f0ef019fd40f282ab1ed75b1fbe3_prof);

        
        $__internal_55787af3cc07f6743652ee8362a86e74001f09dad5d4fbc29921c99a7e48a791->leave($__internal_55787af3cc07f6743652ee8362a86e74001f09dad5d4fbc29921c99a7e48a791_prof);

    }

    // line 204
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_2d9bd81d2ed43fa8aec20380e96650e623a4c5839de1f32053f313f558873c24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2d9bd81d2ed43fa8aec20380e96650e623a4c5839de1f32053f313f558873c24->enter($__internal_2d9bd81d2ed43fa8aec20380e96650e623a4c5839de1f32053f313f558873c24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_818ec2e77816e28903e9360c7516f076fa3d3386b8aaf1e40e2ed9ce57f050b0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_818ec2e77816e28903e9360c7516f076fa3d3386b8aaf1e40e2ed9ce57f050b0->enter($__internal_818ec2e77816e28903e9360c7516f076fa3d3386b8aaf1e40e2ed9ce57f050b0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 205
        echo "<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"";
        // line 209
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"";
        // line 214
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"";
        // line 219
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
        Résident
    </a>
</div>



";
        
        $__internal_818ec2e77816e28903e9360c7516f076fa3d3386b8aaf1e40e2ed9ce57f050b0->leave($__internal_818ec2e77816e28903e9360c7516f076fa3d3386b8aaf1e40e2ed9ce57f050b0_prof);

        
        $__internal_2d9bd81d2ed43fa8aec20380e96650e623a4c5839de1f32053f313f558873c24->leave($__internal_2d9bd81d2ed43fa8aec20380e96650e623a4c5839de1f32053f313f558873c24_prof);

    }

    public function getTemplateName()
    {
        return ":menu:resident.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  462 => 219,  454 => 214,  446 => 209,  440 => 205,  431 => 204,  407 => 189,  403 => 188,  399 => 187,  395 => 186,  378 => 172,  374 => 171,  370 => 170,  366 => 169,  356 => 162,  352 => 161,  348 => 160,  344 => 159,  326 => 144,  322 => 143,  318 => 142,  314 => 141,  304 => 134,  300 => 133,  296 => 132,  292 => 131,  274 => 116,  270 => 115,  266 => 114,  262 => 113,  252 => 106,  248 => 105,  244 => 104,  240 => 103,  223 => 89,  219 => 88,  215 => 87,  211 => 86,  201 => 79,  197 => 78,  193 => 77,  189 => 76,  171 => 61,  167 => 60,  163 => 59,  159 => 58,  149 => 51,  145 => 50,  141 => 49,  137 => 48,  118 => 32,  114 => 31,  110 => 30,  106 => 29,  96 => 22,  92 => 21,  88 => 20,  84 => 19,  69 => 6,  60 => 5,  42 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'menu_resident' %}

{% block main %}
<h1> Menu Résident</h1>
<p class=\"subtitle\"> Menu du midi et du soir</p>


    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Lundi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuMidi.lundiEntree }}</div>
                <div class=\" text\">{{ menuMidi.lundiPlat }}</div>
                <div class=\"text\">{{ menuMidi.lundiAccompagnement }}</div>
                <div class=\" text\">{{ menuMidi.lundiDessert }}</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuSoir.lundiEntree }}</div>
                <div class=\" text\">{{ menuSoir.lundiPlat }}</div>
                <div class=\"text\">{{ menuSoir.lundiAccompagnement }}</div>
                <div class=\" text\">{{ menuSoir.lundiDessert }}</div>
            </div>
        </div>

    </div>


    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Mardi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuMidi.mardiEntree }}</div>
                <div class=\" text\">{{ menuMidi.mardiPlat }}</div>
                <div class=\"text\">{{ menuMidi.mardiAccompagnement }}</div>
                <div class=\" text\">{{ menuMidi.mardiDessert }}</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuSoir.mardiEntree }}</div>
                <div class=\" text\">{{ menuSoir.mardiPlat }}</div>
                <div class=\"text\">{{ menuSoir.mardiAccompagnement }}</div>
                <div class=\" text\">{{ menuSoir.mardiDessert }}</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Mercredi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuMidi.mercrediEntree }}</div>
                <div class=\" text\">{{ menuMidi.mercrediPlat }}</div>
                <div class=\"text\">{{ menuMidi.mercrediAccompagnement }}</div>
                <div class=\" text\">{{ menuMidi.mercrediDessert }}</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuSoir.mercrediEntree }}</div>
                <div class=\" text\">{{ menuSoir.mercrediPlat }}</div>
                <div class=\"text\">{{ menuSoir.mercrediAccompagnement }}</div>
                <div class=\" text\">{{ menuSoir.mercrediDessert }}</div>
            </div>
        </div>

    </div>
    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Jeudi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuMidi.JeudiEntree }}</div>
                <div class=\" text\">{{ menuMidi.JeudiPlat }}</div>
                <div class=\"text\">{{ menuMidi.JeudiAccompagnement }}</div>
                <div class=\" text\">{{ menuMidi.JeudiDessert }}</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuSoir.JeudiEntree }}</div>
                <div class=\" text\">{{ menuSoir.JeudiPlat }}</div>
                <div class=\"text\">{{ menuSoir.JeudiAccompagnement }}</div>
                <div class=\" text\">{{ menuSoir.JeudiDessert }}</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Vendredi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuMidi.vendrediEntree }}</div>
                <div class=\" text\">{{ menuMidi.vendrediPlat }}</div>
                <div class=\"text\">{{ menuMidi.vendrediAccompagnement }}</div>
                <div class=\" text\">{{ menuMidi.vendrediDessert }}</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuSoir.vendrediEntree }}</div>
                <div class=\" text\">{{ menuSoir.vendrediPlat }}</div>
                <div class=\"text\">{{ menuSoir.vendrediAccompagnement }}</div>
                <div class=\" text\">{{ menuSoir.vendrediDessert }}</div>
            </div>
        </div>

    </div>

    <div class=\"row ligne\">

        <div class=\"col-md-2 col-xs-12\">
            <div class=\"titre\">Samedi</div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Midi</div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuMidi.samediEntree }}</div>
                <div class=\" text\">{{ menuMidi.samediPlat }}</div>
                <div class=\"text\">{{ menuMidi.samediAccompagnement }}</div>
                <div class=\" text\">{{ menuMidi.samediDessert }}</div>
            </div>
        </div>

        <div class=\"col-md-5 col-xs-12\">
            <div class=\"sous-titre\">Soir </div>
            <div class=\"menu\">
                <div class=\" text\">{{ menuSoir.samediEntree }}</div>
                <div class=\" text\">{{ menuSoir.samediPlat }}</div>
                <div class=\"text\">{{ menuSoir.samediAccompagnement }}</div>
                <div class=\" text\">{{ menuSoir.samediDessert }}</div>
            </div>
        </div>

    </div>
    <div class=\"row ligne\">

    <div class=\"col-md-2 col-xs-12\">
        <div class=\"titre\">Dimanche</div>
    </div>

    <div class=\"col-md-5 col-xs-12\">
        <div class=\"sous-titre\">Midi</div>
        <div class=\"menu\">
            <div class=\" text\">{{ menuMidi.dimancheEntree }}</div>
            <div class=\" text\">{{ menuMidi.dimanchePlat }}</div>
            <div class=\"text\">{{ menuMidi.dimancheAccompagnement }}</div>
            <div class=\" text\">{{ menuMidi.dimancheDessert }}</div>
        </div>
    </div>
    <div class=\"col-md-5 col-xs-12\">
        <div class=\"sous-titre\">Soir</div>
        <div class=\"menu\">
            <div class=\" text\">Pas de menu le dimanche soir</div>
        </div>
    </div>

</div>


{% endblock %}

{% block sidebar %}
<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"{{ path('externe') }}\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"{{ path('scolaire') }}\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"{{ path('resident') }}\" >
        Résident
    </a>
</div>



{% endblock %}
", ":menu:resident.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/menu/resident.html.twig");
    }
}
