<?php

/* @Twig/Exception/exception.js.twig */
class __TwigTemplate_c69adbc85cd2f87f735c694fdb7ebfe676b97f590bc29ff2e445887ebf396681 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b66dccec944ec06927164cdfe4bc1e10f5bd5b3d27c1caf4120ecd1411475665 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b66dccec944ec06927164cdfe4bc1e10f5bd5b3d27c1caf4120ecd1411475665->enter($__internal_b66dccec944ec06927164cdfe4bc1e10f5bd5b3d27c1caf4120ecd1411475665_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        $__internal_7f3eab970c634e4ee6fe034fcddd340f5e00c8876b92b2cf8cb3a4911926d489 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f3eab970c634e4ee6fe034fcddd340f5e00c8876b92b2cf8cb3a4911926d489->enter($__internal_7f3eab970c634e4ee6fe034fcddd340f5e00c8876b92b2cf8cb3a4911926d489_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_b66dccec944ec06927164cdfe4bc1e10f5bd5b3d27c1caf4120ecd1411475665->leave($__internal_b66dccec944ec06927164cdfe4bc1e10f5bd5b3d27c1caf4120ecd1411475665_prof);

        
        $__internal_7f3eab970c634e4ee6fe034fcddd340f5e00c8876b92b2cf8cb3a4911926d489->leave($__internal_7f3eab970c634e4ee6fe034fcddd340f5e00c8876b92b2cf8cb3a4911926d489_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "@Twig/Exception/exception.js.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception.js.twig");
    }
}
