<?php

/* @SonataBlock/Block/block_core_text.html.twig */
class __TwigTemplate_e9aa8e01da36cf724a2b210e7c07c61b9443f8a7db56355571039bae7e0aa18e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "@SonataBlock/Block/block_core_text.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_39c4da65596b1aed17963f4ecf7458c49b9bdae8bdf5aec2db8e3ef73fc3fd12 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_39c4da65596b1aed17963f4ecf7458c49b9bdae8bdf5aec2db8e3ef73fc3fd12->enter($__internal_39c4da65596b1aed17963f4ecf7458c49b9bdae8bdf5aec2db8e3ef73fc3fd12_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_core_text.html.twig"));

        $__internal_ed953c7854140623f44b4bb1701f77c3794cfdc4dd5b8f2ec925b21d44d36884 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed953c7854140623f44b4bb1701f77c3794cfdc4dd5b8f2ec925b21d44d36884->enter($__internal_ed953c7854140623f44b4bb1701f77c3794cfdc4dd5b8f2ec925b21d44d36884_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_core_text.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_39c4da65596b1aed17963f4ecf7458c49b9bdae8bdf5aec2db8e3ef73fc3fd12->leave($__internal_39c4da65596b1aed17963f4ecf7458c49b9bdae8bdf5aec2db8e3ef73fc3fd12_prof);

        
        $__internal_ed953c7854140623f44b4bb1701f77c3794cfdc4dd5b8f2ec925b21d44d36884->leave($__internal_ed953c7854140623f44b4bb1701f77c3794cfdc4dd5b8f2ec925b21d44d36884_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_475282913ce0e139e9efed03168b957925ec7d7a77b5450e84c3735669dbed92 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_475282913ce0e139e9efed03168b957925ec7d7a77b5450e84c3735669dbed92->enter($__internal_475282913ce0e139e9efed03168b957925ec7d7a77b5450e84c3735669dbed92_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_9ee4b34210d218b7fb8ee63d3a12a642f47a28e25d05d45ddd79e22068d8c924 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9ee4b34210d218b7fb8ee63d3a12a642f47a28e25d05d45ddd79e22068d8c924->enter($__internal_9ee4b34210d218b7fb8ee63d3a12a642f47a28e25d05d45ddd79e22068d8c924_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo $this->getAttribute((isset($context["settings"]) ? $context["settings"] : $this->getContext($context, "settings")), "content", array());
        echo "
";
        
        $__internal_9ee4b34210d218b7fb8ee63d3a12a642f47a28e25d05d45ddd79e22068d8c924->leave($__internal_9ee4b34210d218b7fb8ee63d3a12a642f47a28e25d05d45ddd79e22068d8c924_prof);

        
        $__internal_475282913ce0e139e9efed03168b957925ec7d7a77b5450e84c3735669dbed92->leave($__internal_475282913ce0e139e9efed03168b957925ec7d7a77b5450e84c3735669dbed92_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_core_text.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ settings.content|raw }}
{% endblock %}
", "@SonataBlock/Block/block_core_text.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_core_text.html.twig");
    }
}
