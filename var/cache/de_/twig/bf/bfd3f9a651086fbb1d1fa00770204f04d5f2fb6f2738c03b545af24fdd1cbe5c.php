<?php

/* SonataDoctrineORMAdminBundle:CRUD:list_orm_many_to_many.html.twig */
class __TwigTemplate_2b559732371bf37f6e9e1c3a567d68c4a5f4583c1ad1a3dce6ed1890f204a81c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
            'relation_link' => array($this, 'block_relation_link'),
            'relation_value' => array($this, 'block_relation_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataDoctrineORMAdminBundle:CRUD:list_orm_many_to_many.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_784bce9b537b614e54b4585f5b7adc7d3b9d708c0efea1ddf602ccf12c5d16c3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_784bce9b537b614e54b4585f5b7adc7d3b9d708c0efea1ddf602ccf12c5d16c3->enter($__internal_784bce9b537b614e54b4585f5b7adc7d3b9d708c0efea1ddf602ccf12c5d16c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:list_orm_many_to_many.html.twig"));

        $__internal_9a9e489878050a011b95beb3a1ef834e25d43faadefbd4093ee70d1bee3bb110 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a9e489878050a011b95beb3a1ef834e25d43faadefbd4093ee70d1bee3bb110->enter($__internal_9a9e489878050a011b95beb3a1ef834e25d43faadefbd4093ee70d1bee3bb110_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataDoctrineORMAdminBundle:CRUD:list_orm_many_to_many.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_784bce9b537b614e54b4585f5b7adc7d3b9d708c0efea1ddf602ccf12c5d16c3->leave($__internal_784bce9b537b614e54b4585f5b7adc7d3b9d708c0efea1ddf602ccf12c5d16c3_prof);

        
        $__internal_9a9e489878050a011b95beb3a1ef834e25d43faadefbd4093ee70d1bee3bb110->leave($__internal_9a9e489878050a011b95beb3a1ef834e25d43faadefbd4093ee70d1bee3bb110_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_1fb4097001de13faf5cde657602d3b3831b61ab8a23c442e62173ccfd11e7d6f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1fb4097001de13faf5cde657602d3b3831b61ab8a23c442e62173ccfd11e7d6f->enter($__internal_1fb4097001de13faf5cde657602d3b3831b61ab8a23c442e62173ccfd11e7d6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_6e76bfb175f4d7d9f5061fc51b914979526a9665bd4bcf0238da1fddddc9aaa4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e76bfb175f4d7d9f5061fc51b914979526a9665bd4bcf0238da1fddddc9aaa4->enter($__internal_6e76bfb175f4d7d9f5061fc51b914979526a9665bd4bcf0238da1fddddc9aaa4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["route_name"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if (($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "hasassociationadmin", array()) && $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "hasRoute", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name"))), "method"))) {
            // line 17
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 18
                if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "hasAccess", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name")), 1 => $context["element"]), "method")) {
                    // line 19
                    $this->displayBlock("relation_link", $context, $blocks);
                } else {
                    // line 21
                    $this->displayBlock("relation_value", $context, $blocks);
                }
                // line 23
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "    ";
        } else {
            // line 26
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 27
                echo "            ";
                $this->displayBlock("relation_value", $context, $blocks);
                echo "
            ";
                // line 28
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 29
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "    ";
        }
        
        $__internal_6e76bfb175f4d7d9f5061fc51b914979526a9665bd4bcf0238da1fddddc9aaa4->leave($__internal_6e76bfb175f4d7d9f5061fc51b914979526a9665bd4bcf0238da1fddddc9aaa4_prof);

        
        $__internal_1fb4097001de13faf5cde657602d3b3831b61ab8a23c442e62173ccfd11e7d6f->leave($__internal_1fb4097001de13faf5cde657602d3b3831b61ab8a23c442e62173ccfd11e7d6f_prof);

    }

    // line 33
    public function block_relation_link($context, array $blocks = array())
    {
        $__internal_45005ec1985448059e1b15aa21d7fa5a3a343f7b1e4ca8822e1ee84052bb21fd = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45005ec1985448059e1b15aa21d7fa5a3a343f7b1e4ca8822e1ee84052bb21fd->enter($__internal_45005ec1985448059e1b15aa21d7fa5a3a343f7b1e4ca8822e1ee84052bb21fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_link"));

        $__internal_1f2ea0041a07cecbfa0ac502c2018962f16e61090397585a0eb0bd641d4f491c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f2ea0041a07cecbfa0ac502c2018962f16e61090397585a0eb0bd641d4f491c->enter($__internal_1f2ea0041a07cecbfa0ac502c2018962f16e61090397585a0eb0bd641d4f491c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_link"));

        // line 34
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name")), 1 => (isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), 2 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
        echo "\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), (isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description"))), "html", null, true);
        // line 36
        echo "</a>";
        
        $__internal_1f2ea0041a07cecbfa0ac502c2018962f16e61090397585a0eb0bd641d4f491c->leave($__internal_1f2ea0041a07cecbfa0ac502c2018962f16e61090397585a0eb0bd641d4f491c_prof);

        
        $__internal_45005ec1985448059e1b15aa21d7fa5a3a343f7b1e4ca8822e1ee84052bb21fd->leave($__internal_45005ec1985448059e1b15aa21d7fa5a3a343f7b1e4ca8822e1ee84052bb21fd_prof);

    }

    // line 39
    public function block_relation_value($context, array $blocks = array())
    {
        $__internal_3f526640d95c234556145bfc66527c5230433300fe6124432c1155b667030ce5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3f526640d95c234556145bfc66527c5230433300fe6124432c1155b667030ce5->enter($__internal_3f526640d95c234556145bfc66527c5230433300fe6124432c1155b667030ce5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_value"));

        $__internal_2f6ad52f94cb12471d3c38a1fcc72ba14fa5f6ebe23673bf3541fc079ca5624f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2f6ad52f94cb12471d3c38a1fcc72ba14fa5f6ebe23673bf3541fc079ca5624f->enter($__internal_2f6ad52f94cb12471d3c38a1fcc72ba14fa5f6ebe23673bf3541fc079ca5624f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_value"));

        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), (isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description"))), "html", null, true);
        
        $__internal_2f6ad52f94cb12471d3c38a1fcc72ba14fa5f6ebe23673bf3541fc079ca5624f->leave($__internal_2f6ad52f94cb12471d3c38a1fcc72ba14fa5f6ebe23673bf3541fc079ca5624f_prof);

        
        $__internal_3f526640d95c234556145bfc66527c5230433300fe6124432c1155b667030ce5->leave($__internal_3f526640d95c234556145bfc66527c5230433300fe6124432c1155b667030ce5_prof);

    }

    public function getTemplateName()
    {
        return "SonataDoctrineORMAdminBundle:CRUD:list_orm_many_to_many.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 40,  180 => 39,  170 => 36,  168 => 35,  164 => 34,  155 => 33,  144 => 30,  130 => 29,  126 => 28,  121 => 27,  103 => 26,  100 => 25,  86 => 24,  82 => 23,  79 => 21,  76 => 19,  74 => 18,  56 => 17,  53 => 16,  50 => 15,  41 => 14,  20 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% set route_name = field_description.options.route.name %}
    {% if field_description.hasassociationadmin and field_description.associationadmin.hasRoute(route_name) %}
        {% for element in value %}
            {%- if field_description.associationadmin.hasAccess(route_name, element) -%}
                {{ block('relation_link') }}
            {%- else -%}
                {{ block('relation_value') }}
            {%- endif -%}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% else %}
        {% for element in value%}
            {{ block('relation_value') }}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% endif %}
{% endblock %}

{%- block relation_link -%}
    <a href=\"{{ field_description.associationadmin.generateObjectUrl(route_name, element, field_description.options.route.parameters) }}\">
        {{- element|render_relation_element(field_description) -}}
    </a>
{%- endblock -%}

{%- block relation_value -%}
    {{- element|render_relation_element(field_description) -}}
{%- endblock -%}
", "SonataDoctrineORMAdminBundle:CRUD:list_orm_many_to_many.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\doctrine-orm-admin-bundle/Resources/views/CRUD/list_orm_many_to_many.html.twig");
    }
}
