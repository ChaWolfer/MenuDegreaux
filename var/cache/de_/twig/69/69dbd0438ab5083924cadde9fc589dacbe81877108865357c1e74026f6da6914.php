<?php

/* @Framework/Form/email_widget.html.php */
class __TwigTemplate_e1464819f3421e8cfc7303d79085e6a513893d90f14cfd26ce860bf88d5ed06d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e88ca8c0114f8a26f7844fe053e6b9b39f8cb3c44432cdea378b964c8accb317 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e88ca8c0114f8a26f7844fe053e6b9b39f8cb3c44432cdea378b964c8accb317->enter($__internal_e88ca8c0114f8a26f7844fe053e6b9b39f8cb3c44432cdea378b964c8accb317_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        $__internal_cd3a2d0cdbd8b3f88b82ce12bad69dd15ee6701662dfff0473e54b29e6748c83 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd3a2d0cdbd8b3f88b82ce12bad69dd15ee6701662dfff0473e54b29e6748c83->enter($__internal_cd3a2d0cdbd8b3f88b82ce12bad69dd15ee6701662dfff0473e54b29e6748c83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/email_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
";
        
        $__internal_e88ca8c0114f8a26f7844fe053e6b9b39f8cb3c44432cdea378b964c8accb317->leave($__internal_e88ca8c0114f8a26f7844fe053e6b9b39f8cb3c44432cdea378b964c8accb317_prof);

        
        $__internal_cd3a2d0cdbd8b3f88b82ce12bad69dd15ee6701662dfff0473e54b29e6748c83->leave($__internal_cd3a2d0cdbd8b3f88b82ce12bad69dd15ee6701662dfff0473e54b29e6748c83_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/email_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'email')) ?>
", "@Framework/Form/email_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\email_widget.html.php");
    }
}
