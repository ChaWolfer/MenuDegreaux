<?php

/* @Framework/Form/datetime_widget.html.php */
class __TwigTemplate_e39172f0e2294876d5ebb4c2bfa6ce5dba5ca86c8c07303e98e97319672d0724 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_45109a8a05f5aff3b74318c1f3de90783d9610b73b822b567070306d8b4d933d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_45109a8a05f5aff3b74318c1f3de90783d9610b73b822b567070306d8b4d933d->enter($__internal_45109a8a05f5aff3b74318c1f3de90783d9610b73b822b567070306d8b4d933d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        $__internal_748f8518f3963443be1078c4c6f78f6c66d968b5d19c88e9935eaecfe009d5f4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_748f8518f3963443be1078c4c6f78f6c66d968b5d19c88e9935eaecfe009d5f4->enter($__internal_748f8518f3963443be1078c4c6f78f6c66d968b5d19c88e9935eaecfe009d5f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/datetime_widget.html.php"));

        // line 1
        echo "<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
";
        
        $__internal_45109a8a05f5aff3b74318c1f3de90783d9610b73b822b567070306d8b4d933d->leave($__internal_45109a8a05f5aff3b74318c1f3de90783d9610b73b822b567070306d8b4d933d_prof);

        
        $__internal_748f8518f3963443be1078c4c6f78f6c66d968b5d19c88e9935eaecfe009d5f4->leave($__internal_748f8518f3963443be1078c4c6f78f6c66d968b5d19c88e9935eaecfe009d5f4_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/datetime_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$widget == 'single_text'): ?>
    <?php echo \$view['form']->block(\$form, 'form_widget_simple'); ?>
<?php else: ?>
    <div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
        <?php echo \$view['form']->widget(\$form['date']).' '.\$view['form']->widget(\$form['time']) ?>
    </div>
<?php endif ?>
", "@Framework/Form/datetime_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\datetime_widget.html.php");
    }
}
