<?php

/* FOSUserBundle:Registration:confirmed.html.twig */
class __TwigTemplate_b1797eb6954588194ab4b69a5755972f29b6c545badfd46e3c0d7b73293a3fd8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:confirmed.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d45e1efe0f34189cd9487bc17c7e3b26ff31e8c2a31bc38185ec1ed64c8e913c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d45e1efe0f34189cd9487bc17c7e3b26ff31e8c2a31bc38185ec1ed64c8e913c->enter($__internal_d45e1efe0f34189cd9487bc17c7e3b26ff31e8c2a31bc38185ec1ed64c8e913c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $__internal_88a4ab13bfcdb087a99b3257aaa2505af52da562489bbf622ca0a20ea5e2e10f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88a4ab13bfcdb087a99b3257aaa2505af52da562489bbf622ca0a20ea5e2e10f->enter($__internal_88a4ab13bfcdb087a99b3257aaa2505af52da562489bbf622ca0a20ea5e2e10f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:confirmed.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_d45e1efe0f34189cd9487bc17c7e3b26ff31e8c2a31bc38185ec1ed64c8e913c->leave($__internal_d45e1efe0f34189cd9487bc17c7e3b26ff31e8c2a31bc38185ec1ed64c8e913c_prof);

        
        $__internal_88a4ab13bfcdb087a99b3257aaa2505af52da562489bbf622ca0a20ea5e2e10f->leave($__internal_88a4ab13bfcdb087a99b3257aaa2505af52da562489bbf622ca0a20ea5e2e10f_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_3002f8ec70e96656df7de41e4b19817c0db1c3914d094e0e1e10cf6bd8a2cd78 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3002f8ec70e96656df7de41e4b19817c0db1c3914d094e0e1e10cf6bd8a2cd78->enter($__internal_3002f8ec70e96656df7de41e4b19817c0db1c3914d094e0e1e10cf6bd8a2cd78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_97381197fef5dabfac4e53429dad0cb9809f61e689278e835e5c91bce8a48878 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_97381197fef5dabfac4e53429dad0cb9809f61e689278e835e5c91bce8a48878->enter($__internal_97381197fef5dabfac4e53429dad0cb9809f61e689278e835e5c91bce8a48878_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.confirmed", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
    ";
        // line 7
        if ((isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl"))) {
            // line 8
            echo "    <p><a href=\"";
            echo twig_escape_filter($this->env, (isset($context["targetUrl"]) ? $context["targetUrl"] : $this->getContext($context, "targetUrl")), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.back", array(), "FOSUserBundle"), "html", null, true);
            echo "</a></p>
    ";
        }
        
        $__internal_97381197fef5dabfac4e53429dad0cb9809f61e689278e835e5c91bce8a48878->leave($__internal_97381197fef5dabfac4e53429dad0cb9809f61e689278e835e5c91bce8a48878_prof);

        
        $__internal_3002f8ec70e96656df7de41e4b19817c0db1c3914d094e0e1e10cf6bd8a2cd78->leave($__internal_3002f8ec70e96656df7de41e4b19817c0db1c3914d094e0e1e10cf6bd8a2cd78_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:confirmed.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 8,  54 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.confirmed'|trans({'%username%': user.username}) }}</p>
    {% if targetUrl %}
    <p><a href=\"{{ targetUrl }}\">{{ 'registration.back'|trans }}</a></p>
    {% endif %}
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:confirmed.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/confirmed.html.twig");
    }
}
