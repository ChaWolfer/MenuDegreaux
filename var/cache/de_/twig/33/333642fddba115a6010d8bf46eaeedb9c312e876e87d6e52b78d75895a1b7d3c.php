<?php

/* SonataAdminBundle:Pager:simple_pager_results.html.twig */
class __TwigTemplate_5e6867ff07577e83189d5cd8b98f5dca383f88f82f414d7a7b7d67efe1df8a9a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:Pager:base_results.html.twig", "SonataAdminBundle:Pager:simple_pager_results.html.twig", 12);
        $this->blocks = array(
            'num_results' => array($this, 'block_num_results'),
            'num_pages' => array($this, 'block_num_pages'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Pager:base_results.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8f3c30a010f63cb6bf3f2279795698fc870e37747af89e17bcb78c1d9e6b4168 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f3c30a010f63cb6bf3f2279795698fc870e37747af89e17bcb78c1d9e6b4168->enter($__internal_8f3c30a010f63cb6bf3f2279795698fc870e37747af89e17bcb78c1d9e6b4168_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:simple_pager_results.html.twig"));

        $__internal_ce6605f7478e302b8cfd8d2d69a946b27dc80f71be90ef2940b7a9faeaab6423 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce6605f7478e302b8cfd8d2d69a946b27dc80f71be90ef2940b7a9faeaab6423->enter($__internal_ce6605f7478e302b8cfd8d2d69a946b27dc80f71be90ef2940b7a9faeaab6423_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Pager:simple_pager_results.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_8f3c30a010f63cb6bf3f2279795698fc870e37747af89e17bcb78c1d9e6b4168->leave($__internal_8f3c30a010f63cb6bf3f2279795698fc870e37747af89e17bcb78c1d9e6b4168_prof);

        
        $__internal_ce6605f7478e302b8cfd8d2d69a946b27dc80f71be90ef2940b7a9faeaab6423->leave($__internal_ce6605f7478e302b8cfd8d2d69a946b27dc80f71be90ef2940b7a9faeaab6423_prof);

    }

    // line 14
    public function block_num_results($context, array $blocks = array())
    {
        $__internal_f65db339067f8d5cc037f22240fc8d55be33146e6d5ef036e6a6a134a66f5320 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f65db339067f8d5cc037f22240fc8d55be33146e6d5ef036e6a6a134a66f5320->enter($__internal_f65db339067f8d5cc037f22240fc8d55be33146e6d5ef036e6a6a134a66f5320_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        $__internal_0607900ea120a820e384bffba54d5ce296a52bb7e44286273431b5090845af46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0607900ea120a820e384bffba54d5ce296a52bb7e44286273431b5090845af46->enter($__internal_0607900ea120a820e384bffba54d5ce296a52bb7e44286273431b5090845af46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_results"));

        // line 15
        echo "    ";
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "lastPage", array()) != $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "page", array()))) {
            // line 16
            echo "        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("list_results_count_prefix", array(), "SonataAdminBundle"), "html", null, true);
            echo "
    ";
        }
        // line 18
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->getTranslator()->transChoice("list_results_count", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "nbresults", array()), array("%count%" => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "nbresults", array())), "SonataAdminBundle");
        // line 19
        echo "    &nbsp;-&nbsp;
";
        
        $__internal_0607900ea120a820e384bffba54d5ce296a52bb7e44286273431b5090845af46->leave($__internal_0607900ea120a820e384bffba54d5ce296a52bb7e44286273431b5090845af46_prof);

        
        $__internal_f65db339067f8d5cc037f22240fc8d55be33146e6d5ef036e6a6a134a66f5320->leave($__internal_f65db339067f8d5cc037f22240fc8d55be33146e6d5ef036e6a6a134a66f5320_prof);

    }

    // line 22
    public function block_num_pages($context, array $blocks = array())
    {
        $__internal_2f0e3415a97f31a0c74e4307449d6e64d56355a6f2f9cfd2bc2e984d52a69940 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f0e3415a97f31a0c74e4307449d6e64d56355a6f2f9cfd2bc2e984d52a69940->enter($__internal_2f0e3415a97f31a0c74e4307449d6e64d56355a6f2f9cfd2bc2e984d52a69940_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        $__internal_9daabbdc6556f80a111446af7b2730a16321252f60ff96086ffda7ba66e69093 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9daabbdc6556f80a111446af7b2730a16321252f60ff96086ffda7ba66e69093->enter($__internal_9daabbdc6556f80a111446af7b2730a16321252f60ff96086ffda7ba66e69093_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "num_pages"));

        // line 23
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "page", array()), "html", null, true);
        echo "
    /
    ";
        // line 25
        if (($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "lastPage", array()) != $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "page", array()))) {
            // line 26
            echo "        ?
    ";
        } else {
            // line 28
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "datagrid", array()), "pager", array()), "lastpage", array()), "html", null, true);
            echo "
    ";
        }
        // line 30
        echo "    &nbsp;-&nbsp;
";
        
        $__internal_9daabbdc6556f80a111446af7b2730a16321252f60ff96086ffda7ba66e69093->leave($__internal_9daabbdc6556f80a111446af7b2730a16321252f60ff96086ffda7ba66e69093_prof);

        
        $__internal_2f0e3415a97f31a0c74e4307449d6e64d56355a6f2f9cfd2bc2e984d52a69940->leave($__internal_2f0e3415a97f31a0c74e4307449d6e64d56355a6f2f9cfd2bc2e984d52a69940_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Pager:simple_pager_results.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 30,  94 => 28,  90 => 26,  88 => 25,  82 => 23,  73 => 22,  62 => 19,  59 => 18,  53 => 16,  50 => 15,  41 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:Pager:base_results.html.twig' %}

{% block num_results %}
    {% if admin.datagrid.pager.lastPage != admin.datagrid.pager.page %}
        {{ 'list_results_count_prefix'|trans({}, 'SonataAdminBundle') }}
    {% endif %}
    {% transchoice admin.datagrid.pager.nbresults with {'%count%': admin.datagrid.pager.nbresults} from 'SonataAdminBundle' %}list_results_count{% endtranschoice %}
    &nbsp;-&nbsp;
{% endblock %}

{% block num_pages %}
    {{ admin.datagrid.pager.page }}
    /
    {% if admin.datagrid.pager.lastPage != admin.datagrid.pager.page %}
        ?
    {% else %}
        {{ admin.datagrid.pager.lastpage }}
    {% endif %}
    &nbsp;-&nbsp;
{% endblock %}
", "SonataAdminBundle:Pager:simple_pager_results.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/Pager/simple_pager_results.html.twig");
    }
}
