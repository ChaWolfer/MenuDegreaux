<?php

/* @SonataBlock/Block/block_template.html.twig */
class __TwigTemplate_3fe09fbed0b77506b81c16779e81ef9067fd9b386ac2f330a664140b09bba177 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "@SonataBlock/Block/block_template.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_95673b01036844d7e2c3738f45f76d44bbfbc63d32845ff8a729002d60f46d8f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95673b01036844d7e2c3738f45f76d44bbfbc63d32845ff8a729002d60f46d8f->enter($__internal_95673b01036844d7e2c3738f45f76d44bbfbc63d32845ff8a729002d60f46d8f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_template.html.twig"));

        $__internal_fb5cc06d743f7e49e7dbc438ecab7d0cc90f1a86fc531bd7581fcad3e4e1c989 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb5cc06d743f7e49e7dbc438ecab7d0cc90f1a86fc531bd7581fcad3e4e1c989->enter($__internal_fb5cc06d743f7e49e7dbc438ecab7d0cc90f1a86fc531bd7581fcad3e4e1c989_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_template.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_95673b01036844d7e2c3738f45f76d44bbfbc63d32845ff8a729002d60f46d8f->leave($__internal_95673b01036844d7e2c3738f45f76d44bbfbc63d32845ff8a729002d60f46d8f_prof);

        
        $__internal_fb5cc06d743f7e49e7dbc438ecab7d0cc90f1a86fc531bd7581fcad3e4e1c989->leave($__internal_fb5cc06d743f7e49e7dbc438ecab7d0cc90f1a86fc531bd7581fcad3e4e1c989_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_f9b0e2104afd293be1b362cc6b42f2fd98cb677c6b6a08296b531466b17c19aa = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9b0e2104afd293be1b362cc6b42f2fd98cb677c6b6a08296b531466b17c19aa->enter($__internal_f9b0e2104afd293be1b362cc6b42f2fd98cb677c6b6a08296b531466b17c19aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_a142595f0ee98532def7da143cb2c0dea0aa4a798f674a592ce608c1f3124d67 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a142595f0ee98532def7da143cb2c0dea0aa4a798f674a592ce608c1f3124d67->enter($__internal_a142595f0ee98532def7da143cb2c0dea0aa4a798f674a592ce608c1f3124d67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>";
        // line 33
        echo "{# file: 'MyBundle:Block:my_block_feature_1.html.twig' #}
{% extends sonata_block.templates.block_base %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}";
        echo "</pre>

    And then call it from a template with the <code>sonata_block_render</code> helper:

    <pre>";
        // line 43
        echo "{{ sonata_block_render({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:my_block_feature_1.html.twig',
}) }}";
        echo "</pre>
";
        
        $__internal_a142595f0ee98532def7da143cb2c0dea0aa4a798f674a592ce608c1f3124d67->leave($__internal_a142595f0ee98532def7da143cb2c0dea0aa4a798f674a592ce608c1f3124d67_prof);

        
        $__internal_f9b0e2104afd293be1b362cc6b42f2fd98cb677c6b6a08296b531466b17c19aa->leave($__internal_f9b0e2104afd293be1b362cc6b42f2fd98cb677c6b6a08296b531466b17c19aa_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 43,  53 => 33,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <h3>Sonata Block Template</h3>
    If you want to use the <code>sonata.block.template</code> block type, you need to create a template :

    <pre>
        {%- verbatim -%}
{# file: 'MyBundle:Block:my_block_feature_1.html.twig' #}
{% extends sonata_block.templates.block_base %}

{% block block %}
    &lt;h3&gt;The block title&lt;/h3&gt;
    &lt;p&gt;
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam vel turpis at lacus
        vehicula fringilla at eu lectus. Duis vitae arcu congue, porttitor nisi sit amet,
        mattis metus. Nunc mollis elit ut lectus cursus luctus. Aliquam eu magna sit amet
        massa volutpat auctor.
    &lt;/p&gt;
{% endblock %}
        {%- endverbatim -%}
    </pre>

    And then call it from a template with the <code>sonata_block_render</code> helper:

    <pre>
{%- verbatim -%}
{{ sonata_block_render({ 'type': 'sonata.block.service.template' }, {
    'template': 'MyBundle:Block:my_block_feature_1.html.twig',
}) }}
{%- endverbatim -%}
    </pre>
{% endblock %}
", "@SonataBlock/Block/block_template.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_template.html.twig");
    }
}
