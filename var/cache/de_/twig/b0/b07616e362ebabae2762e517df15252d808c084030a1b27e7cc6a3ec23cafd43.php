<?php

/* @Framework/Form/percent_widget.html.php */
class __TwigTemplate_3b886c110b3879a492113cb538c991d6974aff1a7f67cb007b7ef84de210179c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d1d67f7eb74551be6e3483ec0e0d0316092533785a962c7d1192ba3538e632b1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1d67f7eb74551be6e3483ec0e0d0316092533785a962c7d1192ba3538e632b1->enter($__internal_d1d67f7eb74551be6e3483ec0e0d0316092533785a962c7d1192ba3538e632b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        $__internal_a3172d4a5bf4bb6306755dc8e4aad155990ba8d1955837a0262824996c5f1594 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3172d4a5bf4bb6306755dc8e4aad155990ba8d1955837a0262824996c5f1594->enter($__internal_a3172d4a5bf4bb6306755dc8e4aad155990ba8d1955837a0262824996c5f1594_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/percent_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
";
        
        $__internal_d1d67f7eb74551be6e3483ec0e0d0316092533785a962c7d1192ba3538e632b1->leave($__internal_d1d67f7eb74551be6e3483ec0e0d0316092533785a962c7d1192ba3538e632b1_prof);

        
        $__internal_a3172d4a5bf4bb6306755dc8e4aad155990ba8d1955837a0262824996c5f1594->leave($__internal_a3172d4a5bf4bb6306755dc8e4aad155990ba8d1955837a0262824996c5f1594_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/percent_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?> %
", "@Framework/Form/percent_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\percent_widget.html.php");
    }
}
