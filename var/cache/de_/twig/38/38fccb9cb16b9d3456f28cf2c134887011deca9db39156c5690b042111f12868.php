<?php

/* @Framework/Form/form_widget_simple.html.php */
class __TwigTemplate_e70c75528f2f0c7330d26b0d11fbabf1c8f10ffb529288d57d7901762aa61f7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64a71339e289ca46669777f4526108aa891670313c4be696404247ae72141946 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64a71339e289ca46669777f4526108aa891670313c4be696404247ae72141946->enter($__internal_64a71339e289ca46669777f4526108aa891670313c4be696404247ae72141946_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        $__internal_9e8d9bfec7d09d4b90d68a30e7fdd29ab38f541410eeb6c8f96bdc34e934eb04 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e8d9bfec7d09d4b90d68a30e7fdd29ab38f541410eeb6c8f96bdc34e934eb04->enter($__internal_9e8d9bfec7d09d4b90d68a30e7fdd29ab38f541410eeb6c8f96bdc34e934eb04_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_simple.html.php"));

        // line 1
        echo "<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
";
        
        $__internal_64a71339e289ca46669777f4526108aa891670313c4be696404247ae72141946->leave($__internal_64a71339e289ca46669777f4526108aa891670313c4be696404247ae72141946_prof);

        
        $__internal_9e8d9bfec7d09d4b90d68a30e7fdd29ab38f541410eeb6c8f96bdc34e934eb04->leave($__internal_9e8d9bfec7d09d4b90d68a30e7fdd29ab38f541410eeb6c8f96bdc34e934eb04_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_simple.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<input type=\"<?php echo isset(\$type) ? \$view->escape(\$type) : 'text' ?>\" <?php echo \$view['form']->block(\$form, 'widget_attributes') ?><?php if (!empty(\$value) || is_numeric(\$value)): ?> value=\"<?php echo \$view->escape(\$value) ?>\"<?php endif ?> />
", "@Framework/Form/form_widget_simple.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget_simple.html.php");
    }
}
