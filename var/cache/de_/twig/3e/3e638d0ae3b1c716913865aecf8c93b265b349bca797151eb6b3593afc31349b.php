<?php

/* @Twig/Exception/error.css.twig */
class __TwigTemplate_7c16459f6cfd0035365aaf45685c5922e1702f519baaa03ca3f13c3ff3e6c0c3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c872737f69d1bd79becb8b52671a738e15efbe61db4c8945d28898b81c41f2c2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c872737f69d1bd79becb8b52671a738e15efbe61db4c8945d28898b81c41f2c2->enter($__internal_c872737f69d1bd79becb8b52671a738e15efbe61db4c8945d28898b81c41f2c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        $__internal_d1ac244f19c4110521a109836e2d8e89800ac2f91a28117a1ba77d6d5f9ffc39 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1ac244f19c4110521a109836e2d8e89800ac2f91a28117a1ba77d6d5f9ffc39->enter($__internal_d1ac244f19c4110521a109836e2d8e89800ac2f91a28117a1ba77d6d5f9ffc39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "css", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "css", null, true);
        echo "

*/
";
        
        $__internal_c872737f69d1bd79becb8b52671a738e15efbe61db4c8945d28898b81c41f2c2->leave($__internal_c872737f69d1bd79becb8b52671a738e15efbe61db4c8945d28898b81c41f2c2_prof);

        
        $__internal_d1ac244f19c4110521a109836e2d8e89800ac2f91a28117a1ba77d6d5f9ffc39->leave($__internal_d1ac244f19c4110521a109836e2d8e89800ac2f91a28117a1ba77d6d5f9ffc39_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ status_code }} {{ status_text }}

*/
", "@Twig/Exception/error.css.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.css.twig");
    }
}
