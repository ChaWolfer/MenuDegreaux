<?php

/* @SonataBlock/Block/block_base.html.twig */
class __TwigTemplate_ac11871643aa50f3bb91c480ad3e25a6183226798f41e7dbc9f197e1ff318405 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_79ff8b360dfbea9ef557dc323b89adeb1a133407f8d32fe0e053ee207f317e48 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79ff8b360dfbea9ef557dc323b89adeb1a133407f8d32fe0e053ee207f317e48->enter($__internal_79ff8b360dfbea9ef557dc323b89adeb1a133407f8d32fe0e053ee207f317e48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_base.html.twig"));

        $__internal_976a9dc43c6f353d27a82abaa596e1ba7459e62463429fd1422c24b91f5fe237 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_976a9dc43c6f353d27a82abaa596e1ba7459e62463429fd1422c24b91f5fe237->enter($__internal_976a9dc43c6f353d27a82abaa596e1ba7459e62463429fd1422c24b91f5fe237_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_base.html.twig"));

        // line 11
        echo "<div id=\"cms-block-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "id", array()), "html", null, true);
        echo "\" class=\"cms-block cms-block-element\">
    ";
        // line 12
        $this->displayBlock('block', $context, $blocks);
        // line 13
        echo "</div>
";
        
        $__internal_79ff8b360dfbea9ef557dc323b89adeb1a133407f8d32fe0e053ee207f317e48->leave($__internal_79ff8b360dfbea9ef557dc323b89adeb1a133407f8d32fe0e053ee207f317e48_prof);

        
        $__internal_976a9dc43c6f353d27a82abaa596e1ba7459e62463429fd1422c24b91f5fe237->leave($__internal_976a9dc43c6f353d27a82abaa596e1ba7459e62463429fd1422c24b91f5fe237_prof);

    }

    // line 12
    public function block_block($context, array $blocks = array())
    {
        $__internal_f6f9d493e7621288482dce9fdff229bd105f15e9bf60f31048d0725128fc1bfc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6f9d493e7621288482dce9fdff229bd105f15e9bf60f31048d0725128fc1bfc->enter($__internal_f6f9d493e7621288482dce9fdff229bd105f15e9bf60f31048d0725128fc1bfc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_d93d313cc8339c9f7eb6d4ff9ff44ff6d863803cc90feacf4429fb97e292ccaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d93d313cc8339c9f7eb6d4ff9ff44ff6d863803cc90feacf4429fb97e292ccaf->enter($__internal_d93d313cc8339c9f7eb6d4ff9ff44ff6d863803cc90feacf4429fb97e292ccaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        echo "EMPTY CONTENT";
        
        $__internal_d93d313cc8339c9f7eb6d4ff9ff44ff6d863803cc90feacf4429fb97e292ccaf->leave($__internal_d93d313cc8339c9f7eb6d4ff9ff44ff6d863803cc90feacf4429fb97e292ccaf_prof);

        
        $__internal_f6f9d493e7621288482dce9fdff229bd105f15e9bf60f31048d0725128fc1bfc->leave($__internal_f6f9d493e7621288482dce9fdff229bd105f15e9bf60f31048d0725128fc1bfc_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 12,  33 => 13,  31 => 12,  26 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<div id=\"cms-block-{{ block.id }}\" class=\"cms-block cms-block-element\">
    {% block block %}EMPTY CONTENT{% endblock %}
</div>
", "@SonataBlock/Block/block_base.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_base.html.twig");
    }
}
