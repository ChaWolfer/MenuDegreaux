<?php

/* SonataAdminBundle:CRUD:show_email.html.twig */
class __TwigTemplate_3097de1ee10a494c29d612865fd00ad9def3b30406201e00fe21799d792d5e1f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_email.html.twig", 1);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9bf3fd4b359199e0a2e8f791bfe18ac9791cc041af35ee00ce4b5da6bdb51d1b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9bf3fd4b359199e0a2e8f791bfe18ac9791cc041af35ee00ce4b5da6bdb51d1b->enter($__internal_9bf3fd4b359199e0a2e8f791bfe18ac9791cc041af35ee00ce4b5da6bdb51d1b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_email.html.twig"));

        $__internal_1793d1b955421445fbb025605de9629d3c07cf61bb273a0e7e50ebf4a2b22994 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1793d1b955421445fbb025605de9629d3c07cf61bb273a0e7e50ebf4a2b22994->enter($__internal_1793d1b955421445fbb025605de9629d3c07cf61bb273a0e7e50ebf4a2b22994_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9bf3fd4b359199e0a2e8f791bfe18ac9791cc041af35ee00ce4b5da6bdb51d1b->leave($__internal_9bf3fd4b359199e0a2e8f791bfe18ac9791cc041af35ee00ce4b5da6bdb51d1b_prof);

        
        $__internal_1793d1b955421445fbb025605de9629d3c07cf61bb273a0e7e50ebf4a2b22994->leave($__internal_1793d1b955421445fbb025605de9629d3c07cf61bb273a0e7e50ebf4a2b22994_prof);

    }

    // line 3
    public function block_field($context, array $blocks = array())
    {
        $__internal_d09c2781e5a265a054c55488989057c65a3e2d29672308448c7ec855c3c32a35 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d09c2781e5a265a054c55488989057c65a3e2d29672308448c7ec855c3c32a35->enter($__internal_d09c2781e5a265a054c55488989057c65a3e2d29672308448c7ec855c3c32a35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_7904cf3d75f33860bac90d9863fede8f91fab939406b39a5548f67388bfb3fb7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7904cf3d75f33860bac90d9863fede8f91fab939406b39a5548f67388bfb3fb7->enter($__internal_7904cf3d75f33860bac90d9863fede8f91fab939406b39a5548f67388bfb3fb7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 4
        echo "    ";
        $this->loadTemplate("SonataAdminBundle:CRUD:_email_link.html.twig", "SonataAdminBundle:CRUD:show_email.html.twig", 4)->display($context);
        
        $__internal_7904cf3d75f33860bac90d9863fede8f91fab939406b39a5548f67388bfb3fb7->leave($__internal_7904cf3d75f33860bac90d9863fede8f91fab939406b39a5548f67388bfb3fb7_prof);

        
        $__internal_d09c2781e5a265a054c55488989057c65a3e2d29672308448c7ec855c3c32a35->leave($__internal_d09c2781e5a265a054c55488989057c65a3e2d29672308448c7ec855c3c32a35_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {% include 'SonataAdminBundle:CRUD:_email_link.html.twig' %}
{% endblock %}
", "SonataAdminBundle:CRUD:show_email.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_email.html.twig");
    }
}
