<?php

/* SonataAdminBundle:CRUD:list.html.twig */
class __TwigTemplate_bf904b9d0de6b20e83dd45351d541390a3c82b6021e5104302d4c2651cd89a4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_list.html.twig", "SonataAdminBundle:CRUD:list.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_list.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_edcf411e47100541688ff67ed493a4f2a799dd8fa94bcec48ecf13c2f411dc5c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_edcf411e47100541688ff67ed493a4f2a799dd8fa94bcec48ecf13c2f411dc5c->enter($__internal_edcf411e47100541688ff67ed493a4f2a799dd8fa94bcec48ecf13c2f411dc5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list.html.twig"));

        $__internal_fcc3c1ac8b364673b8aaa2ad4fe8be59642f97e83b12c85fa895f938f2080905 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcc3c1ac8b364673b8aaa2ad4fe8be59642f97e83b12c85fa895f938f2080905->enter($__internal_fcc3c1ac8b364673b8aaa2ad4fe8be59642f97e83b12c85fa895f938f2080905_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_edcf411e47100541688ff67ed493a4f2a799dd8fa94bcec48ecf13c2f411dc5c->leave($__internal_edcf411e47100541688ff67ed493a4f2a799dd8fa94bcec48ecf13c2f411dc5c_prof);

        
        $__internal_fcc3c1ac8b364673b8aaa2ad4fe8be59642f97e83b12c85fa895f938f2080905->leave($__internal_fcc3c1ac8b364673b8aaa2ad4fe8be59642f97e83b12c85fa895f938f2080905_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_list.html.twig' %}
", "SonataAdminBundle:CRUD:list.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list.html.twig");
    }
}
