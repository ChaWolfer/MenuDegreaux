<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_e7c2ce31db08c48a490aaabea69aa7a9954e2194d39628af210daaf9ac8d667e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a1b0541068ff6369e7f4afcddd3a4355384d4b13a084166c879398ab84b9151 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7a1b0541068ff6369e7f4afcddd3a4355384d4b13a084166c879398ab84b9151->enter($__internal_7a1b0541068ff6369e7f4afcddd3a4355384d4b13a084166c879398ab84b9151_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        $__internal_ccc0e6290f7cd2e3e690831a227458ed1ea8b56325875deb247ec2d7eeab0606 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ccc0e6290f7cd2e3e690831a227458ed1ea8b56325875deb247ec2d7eeab0606->enter($__internal_ccc0e6290f7cd2e3e690831a227458ed1ea8b56325875deb247ec2d7eeab0606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_7a1b0541068ff6369e7f4afcddd3a4355384d4b13a084166c879398ab84b9151->leave($__internal_7a1b0541068ff6369e7f4afcddd3a4355384d4b13a084166c879398ab84b9151_prof);

        
        $__internal_ccc0e6290f7cd2e3e690831a227458ed1ea8b56325875deb247ec2d7eeab0606->leave($__internal_ccc0e6290f7cd2e3e690831a227458ed1ea8b56325875deb247ec2d7eeab0606_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
