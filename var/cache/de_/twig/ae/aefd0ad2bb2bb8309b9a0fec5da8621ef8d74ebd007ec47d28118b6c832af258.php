<?php

/* SonataAdminBundle:CRUD:base_show_compare.html.twig */
class __TwigTemplate_0d8e07aa6c74273070015b9113025be61a77fcc6a4d7882617629fde3d9b3923 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show.html.twig", "SonataAdminBundle:CRUD:base_show_compare.html.twig", 12);
        $this->blocks = array(
            'show_field' => array($this, 'block_show_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5db0637073b27df3eb6756ebdddcc0b7d4df60659612853dc1b5e9251a6c54f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a5db0637073b27df3eb6756ebdddcc0b7d4df60659612853dc1b5e9251a6c54f->enter($__internal_a5db0637073b27df3eb6756ebdddcc0b7d4df60659612853dc1b5e9251a6c54f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_compare.html.twig"));

        $__internal_8d605cd23fcdbc6533a3ebe0839c6cbbcaf939ddf294a516e0e70279158e828e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d605cd23fcdbc6533a3ebe0839c6cbbcaf939ddf294a516e0e70279158e828e->enter($__internal_8d605cd23fcdbc6533a3ebe0839c6cbbcaf939ddf294a516e0e70279158e828e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_compare.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a5db0637073b27df3eb6756ebdddcc0b7d4df60659612853dc1b5e9251a6c54f->leave($__internal_a5db0637073b27df3eb6756ebdddcc0b7d4df60659612853dc1b5e9251a6c54f_prof);

        
        $__internal_8d605cd23fcdbc6533a3ebe0839c6cbbcaf939ddf294a516e0e70279158e828e->leave($__internal_8d605cd23fcdbc6533a3ebe0839c6cbbcaf939ddf294a516e0e70279158e828e_prof);

    }

    // line 14
    public function block_show_field($context, array $blocks = array())
    {
        $__internal_9ea54e90e9190f2987a0e78182f2d6437f8a00d98f644a3cebfdcdd966d22e2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9ea54e90e9190f2987a0e78182f2d6437f8a00d98f644a3cebfdcdd966d22e2c->enter($__internal_9ea54e90e9190f2987a0e78182f2d6437f8a00d98f644a3cebfdcdd966d22e2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        $__internal_1f20d19a27f70bab2721c132afd0a3be665bdffdd5021919d81dabadf9c22746 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1f20d19a27f70bab2721c132afd0a3be665bdffdd5021919d81dabadf9c22746->enter($__internal_1f20d19a27f70bab2721c132afd0a3be665bdffdd5021919d81dabadf9c22746_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        // line 15
        echo "    <tr class=\"sonata-ba-view-container history-audit-compare\">
        ";
        // line 16
        if ($this->getAttribute((isset($context["elements"]) ? $context["elements"] : null), (isset($context["field_name"]) ? $context["field_name"] : $this->getContext($context, "field_name")), array(), "array", true, true)) {
            // line 17
            echo "            ";
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElementCompare($this->env, $this->getAttribute((isset($context["elements"]) ? $context["elements"] : $this->getContext($context, "elements")), (isset($context["field_name"]) ? $context["field_name"] : $this->getContext($context, "field_name")), array(), "array"), (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")), (isset($context["object_compare"]) ? $context["object_compare"] : $this->getContext($context, "object_compare")));
            echo "
        ";
        }
        // line 19
        echo "    </tr>
";
        
        $__internal_1f20d19a27f70bab2721c132afd0a3be665bdffdd5021919d81dabadf9c22746->leave($__internal_1f20d19a27f70bab2721c132afd0a3be665bdffdd5021919d81dabadf9c22746_prof);

        
        $__internal_9ea54e90e9190f2987a0e78182f2d6437f8a00d98f644a3cebfdcdd966d22e2c->leave($__internal_9ea54e90e9190f2987a0e78182f2d6437f8a00d98f644a3cebfdcdd966d22e2c_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_show_compare.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 19,  54 => 17,  52 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show.html.twig' %}

{% block show_field %}
    <tr class=\"sonata-ba-view-container history-audit-compare\">
        {% if elements[field_name] is defined %}
            {{ elements[field_name]|render_view_element_compare(object, object_compare) }}
        {% endif %}
    </tr>
{% endblock %}
", "SonataAdminBundle:CRUD:base_show_compare.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_show_compare.html.twig");
    }
}
