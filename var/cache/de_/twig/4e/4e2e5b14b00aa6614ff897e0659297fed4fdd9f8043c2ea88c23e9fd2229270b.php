<?php

/* SonataBlockBundle:Block:block_base.html.twig */
class __TwigTemplate_65b98b2abf6dc170a3d10f7bf82152058e9f5fdd2cabed289a1b4ed5ac949625 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c8a091003a0510e099f8faffd464eb2b6c152a07bb01d78b237663df6221ae5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4c8a091003a0510e099f8faffd464eb2b6c152a07bb01d78b237663df6221ae5->enter($__internal_4c8a091003a0510e099f8faffd464eb2b6c152a07bb01d78b237663df6221ae5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_base.html.twig"));

        $__internal_2cf0cb264d3fa28234db42d4c529b7c0d9bcdde34860c3d37ac00c0d5f64477f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2cf0cb264d3fa28234db42d4c529b7c0d9bcdde34860c3d37ac00c0d5f64477f->enter($__internal_2cf0cb264d3fa28234db42d4c529b7c0d9bcdde34860c3d37ac00c0d5f64477f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_base.html.twig"));

        // line 11
        echo "<div id=\"cms-block-";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "id", array()), "html", null, true);
        echo "\" class=\"cms-block cms-block-element\">
    ";
        // line 12
        $this->displayBlock('block', $context, $blocks);
        // line 13
        echo "</div>
";
        
        $__internal_4c8a091003a0510e099f8faffd464eb2b6c152a07bb01d78b237663df6221ae5->leave($__internal_4c8a091003a0510e099f8faffd464eb2b6c152a07bb01d78b237663df6221ae5_prof);

        
        $__internal_2cf0cb264d3fa28234db42d4c529b7c0d9bcdde34860c3d37ac00c0d5f64477f->leave($__internal_2cf0cb264d3fa28234db42d4c529b7c0d9bcdde34860c3d37ac00c0d5f64477f_prof);

    }

    // line 12
    public function block_block($context, array $blocks = array())
    {
        $__internal_79ee5da6578b0c61d24e1250b65df12afc76bd0c2073cbe981f53019a54c4e30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_79ee5da6578b0c61d24e1250b65df12afc76bd0c2073cbe981f53019a54c4e30->enter($__internal_79ee5da6578b0c61d24e1250b65df12afc76bd0c2073cbe981f53019a54c4e30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_bfff5346ce887fc5e4e890cb3da7f70f94c25b740c5c4cb9b64111455d921304 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bfff5346ce887fc5e4e890cb3da7f70f94c25b740c5c4cb9b64111455d921304->enter($__internal_bfff5346ce887fc5e4e890cb3da7f70f94c25b740c5c4cb9b64111455d921304_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        echo "EMPTY CONTENT";
        
        $__internal_bfff5346ce887fc5e4e890cb3da7f70f94c25b740c5c4cb9b64111455d921304->leave($__internal_bfff5346ce887fc5e4e890cb3da7f70f94c25b740c5c4cb9b64111455d921304_prof);

        
        $__internal_79ee5da6578b0c61d24e1250b65df12afc76bd0c2073cbe981f53019a54c4e30->leave($__internal_79ee5da6578b0c61d24e1250b65df12afc76bd0c2073cbe981f53019a54c4e30_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 12,  33 => 13,  31 => 12,  26 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}
<div id=\"cms-block-{{ block.id }}\" class=\"cms-block cms-block-element\">
    {% block block %}EMPTY CONTENT{% endblock %}
</div>
", "SonataBlockBundle:Block:block_base.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_base.html.twig");
    }
}
