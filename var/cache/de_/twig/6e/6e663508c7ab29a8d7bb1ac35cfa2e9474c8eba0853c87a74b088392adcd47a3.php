<?php

/* FOSUserBundle:Registration:check_email.html.twig */
class __TwigTemplate_60495ebf2ea746f7d8f0c5e9450fa280fa92265bb312787f82b44e0e1fe14d76 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Registration:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a6ed4ce14f50c4efa2ad8a1f37d1c281c859a132abfc6d9cfdf045a386f03958 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a6ed4ce14f50c4efa2ad8a1f37d1c281c859a132abfc6d9cfdf045a386f03958->enter($__internal_a6ed4ce14f50c4efa2ad8a1f37d1c281c859a132abfc6d9cfdf045a386f03958_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $__internal_0967007436ba5f9b14437a267da5dfa4044ea4ae2ab1bfaea89b959b02634872 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0967007436ba5f9b14437a267da5dfa4044ea4ae2ab1bfaea89b959b02634872->enter($__internal_0967007436ba5f9b14437a267da5dfa4044ea4ae2ab1bfaea89b959b02634872_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a6ed4ce14f50c4efa2ad8a1f37d1c281c859a132abfc6d9cfdf045a386f03958->leave($__internal_a6ed4ce14f50c4efa2ad8a1f37d1c281c859a132abfc6d9cfdf045a386f03958_prof);

        
        $__internal_0967007436ba5f9b14437a267da5dfa4044ea4ae2ab1bfaea89b959b02634872->leave($__internal_0967007436ba5f9b14437a267da5dfa4044ea4ae2ab1bfaea89b959b02634872_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_704600be436cdfca218201bd6c0b56f2f6b9e3793439be56a90a45189db9d854 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_704600be436cdfca218201bd6c0b56f2f6b9e3793439be56a90a45189db9d854->enter($__internal_704600be436cdfca218201bd6c0b56f2f6b9e3793439be56a90a45189db9d854_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_56b10e12ee13cf2265172c356bc531a4ed024e9e665e6d5212be7d109f564aea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56b10e12ee13cf2265172c356bc531a4ed024e9e665e6d5212be7d109f564aea->enter($__internal_56b10e12ee13cf2265172c356bc531a4ed024e9e665e6d5212be7d109f564aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "    <p>";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.check_email", array("%email%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "email", array())), "FOSUserBundle"), "html", null, true);
        echo "</p>
";
        
        $__internal_56b10e12ee13cf2265172c356bc531a4ed024e9e665e6d5212be7d109f564aea->leave($__internal_56b10e12ee13cf2265172c356bc531a4ed024e9e665e6d5212be7d109f564aea_prof);

        
        $__internal_704600be436cdfca218201bd6c0b56f2f6b9e3793439be56a90a45189db9d854->leave($__internal_704600be436cdfca218201bd6c0b56f2f6b9e3793439be56a90a45189db9d854_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
    <p>{{ 'registration.check_email'|trans({'%email%': user.email}) }}</p>
{% endblock fos_user_content %}
", "FOSUserBundle:Registration:check_email.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/check_email.html.twig");
    }
}
