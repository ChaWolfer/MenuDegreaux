<?php

/* @Framework/Form/hidden_row.html.php */
class __TwigTemplate_66d40f99bc9812441ee1bf8a5f4f7c5b7c006bf8ffd89e9ffc21e5e3d651a3ee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c88ae74feedab27d08921fd28d93a2f9d7b697724bf0ea44be209b78681a385a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c88ae74feedab27d08921fd28d93a2f9d7b697724bf0ea44be209b78681a385a->enter($__internal_c88ae74feedab27d08921fd28d93a2f9d7b697724bf0ea44be209b78681a385a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        $__internal_c0df3b55512c6a89a9d6a4770ff8e7fef0b6aec960ffd52362aff98ba83e0c9a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0df3b55512c6a89a9d6a4770ff8e7fef0b6aec960ffd52362aff98ba83e0c9a->enter($__internal_c0df3b55512c6a89a9d6a4770ff8e7fef0b6aec960ffd52362aff98ba83e0c9a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->widget(\$form) ?>
";
        
        $__internal_c88ae74feedab27d08921fd28d93a2f9d7b697724bf0ea44be209b78681a385a->leave($__internal_c88ae74feedab27d08921fd28d93a2f9d7b697724bf0ea44be209b78681a385a_prof);

        
        $__internal_c0df3b55512c6a89a9d6a4770ff8e7fef0b6aec960ffd52362aff98ba83e0c9a->leave($__internal_c0df3b55512c6a89a9d6a4770ff8e7fef0b6aec960ffd52362aff98ba83e0c9a_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->widget(\$form) ?>
", "@Framework/Form/hidden_row.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_row.html.php");
    }
}
