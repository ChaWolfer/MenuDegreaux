<?php

/* SonataAdminBundle:CRUD:list_string.html.twig */
class __TwigTemplate_9be2134103533aa91beb7a2ed5904bd68c7ee34c423fcbb671b21a415fda4cb0 extends Twig_Template
{
    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_string.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6518134a8222aed9b202841bc116bacaa4ecc0a044e391cdfc943d9368bcf513 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6518134a8222aed9b202841bc116bacaa4ecc0a044e391cdfc943d9368bcf513->enter($__internal_6518134a8222aed9b202841bc116bacaa4ecc0a044e391cdfc943d9368bcf513_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_string.html.twig"));

        $__internal_76bb7a30bf2c5a46acb4c69fe450b0493bcfd197eeb9a3e94ca825b249baacaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76bb7a30bf2c5a46acb4c69fe450b0493bcfd197eeb9a3e94ca825b249baacaf->enter($__internal_76bb7a30bf2c5a46acb4c69fe450b0493bcfd197eeb9a3e94ca825b249baacaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_string.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6518134a8222aed9b202841bc116bacaa4ecc0a044e391cdfc943d9368bcf513->leave($__internal_6518134a8222aed9b202841bc116bacaa4ecc0a044e391cdfc943d9368bcf513_prof);

        
        $__internal_76bb7a30bf2c5a46acb4c69fe450b0493bcfd197eeb9a3e94ca825b249baacaf->leave($__internal_76bb7a30bf2c5a46acb4c69fe450b0493bcfd197eeb9a3e94ca825b249baacaf_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_string.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  9 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}
", "SonataAdminBundle:CRUD:list_string.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_string.html.twig");
    }
}
