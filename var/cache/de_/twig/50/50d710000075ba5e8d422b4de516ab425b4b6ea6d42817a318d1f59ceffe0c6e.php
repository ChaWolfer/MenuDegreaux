<?php

/* @Framework/FormTable/form_widget_compound.html.php */
class __TwigTemplate_85f059208850ed5c156012d74446ba3600e5912dbf38b32e425222ae6394babb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9b16e6669104a519a87dc89bd2838b8e41df088254b9cd9cecb3570b6a66d406 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b16e6669104a519a87dc89bd2838b8e41df088254b9cd9cecb3570b6a66d406->enter($__internal_9b16e6669104a519a87dc89bd2838b8e41df088254b9cd9cecb3570b6a66d406_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        $__internal_7f78423059f5245eceb8cba9342aa24d09e39d73b93c46eed45270c3ac3a7c38 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7f78423059f5245eceb8cba9342aa24d09e39d73b93c46eed45270c3ac3a7c38->enter($__internal_7f78423059f5245eceb8cba9342aa24d09e39d73b93c46eed45270c3ac3a7c38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/FormTable/form_widget_compound.html.php"));

        // line 1
        echo "<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
";
        
        $__internal_9b16e6669104a519a87dc89bd2838b8e41df088254b9cd9cecb3570b6a66d406->leave($__internal_9b16e6669104a519a87dc89bd2838b8e41df088254b9cd9cecb3570b6a66d406_prof);

        
        $__internal_7f78423059f5245eceb8cba9342aa24d09e39d73b93c46eed45270c3ac3a7c38->leave($__internal_7f78423059f5245eceb8cba9342aa24d09e39d73b93c46eed45270c3ac3a7c38_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/FormTable/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<table <?php echo \$view['form']->block(\$form, 'widget_container_attributes'); ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <tr>
        <td colspan=\"2\">
            <?php echo \$view['form']->errors(\$form); ?>
        </td>
    </tr>
    <?php endif; ?>
    <?php echo \$view['form']->block(\$form, 'form_rows'); ?>
    <?php echo \$view['form']->rest(\$form); ?>
</table>
", "@Framework/FormTable/form_widget_compound.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\FormTable\\form_widget_compound.html.php");
    }
}
