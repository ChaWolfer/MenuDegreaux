<?php

/* TwigBundle:Exception:error403.html.twig */
class __TwigTemplate_9917e29d2a8b970c1eb07fb1042fa276dccc33a6e3ba8e079b4ad8cfca692842 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 11
        $this->parent = $this->loadTemplate("base.html.twig", "TwigBundle:Exception:error403.html.twig", 11);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce5a2013cfd0498c45b14bb961e464a04ff1731c23a7ce001dc7e0326305132b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce5a2013cfd0498c45b14bb961e464a04ff1731c23a7ce001dc7e0326305132b->enter($__internal_ce5a2013cfd0498c45b14bb961e464a04ff1731c23a7ce001dc7e0326305132b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error403.html.twig"));

        $__internal_a7fc78df783a2013060d1c078fcf4f51a72989a54a5226d89aadd5176cbd82bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a7fc78df783a2013060d1c078fcf4f51a72989a54a5226d89aadd5176cbd82bd->enter($__internal_a7fc78df783a2013060d1c078fcf4f51a72989a54a5226d89aadd5176cbd82bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:error403.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce5a2013cfd0498c45b14bb961e464a04ff1731c23a7ce001dc7e0326305132b->leave($__internal_ce5a2013cfd0498c45b14bb961e464a04ff1731c23a7ce001dc7e0326305132b_prof);

        
        $__internal_a7fc78df783a2013060d1c078fcf4f51a72989a54a5226d89aadd5176cbd82bd->leave($__internal_a7fc78df783a2013060d1c078fcf4f51a72989a54a5226d89aadd5176cbd82bd_prof);

    }

    // line 13
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_df81e1975ef559951585f74f0849a2f61a83234fcd7513652c7a508ff6ac5a0e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df81e1975ef559951585f74f0849a2f61a83234fcd7513652c7a508ff6ac5a0e->enter($__internal_df81e1975ef559951585f74f0849a2f61a83234fcd7513652c7a508ff6ac5a0e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_4daf30aff19e5f85c806b26b4c6630f2dcc5c68b8d66497b17b885eb6ccee35a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4daf30aff19e5f85c806b26b4c6630f2dcc5c68b8d66497b17b885eb6ccee35a->enter($__internal_4daf30aff19e5f85c806b26b4c6630f2dcc5c68b8d66497b17b885eb6ccee35a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "error";
        
        $__internal_4daf30aff19e5f85c806b26b4c6630f2dcc5c68b8d66497b17b885eb6ccee35a->leave($__internal_4daf30aff19e5f85c806b26b4c6630f2dcc5c68b8d66497b17b885eb6ccee35a_prof);

        
        $__internal_df81e1975ef559951585f74f0849a2f61a83234fcd7513652c7a508ff6ac5a0e->leave($__internal_df81e1975ef559951585f74f0849a2f61a83234fcd7513652c7a508ff6ac5a0e_prof);

    }

    // line 15
    public function block_main($context, array $blocks = array())
    {
        $__internal_e078d76b56c9cbec7355deba71d316cbffa5450f02be9b0aab2ed0e3956387a8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e078d76b56c9cbec7355deba71d316cbffa5450f02be9b0aab2ed0e3956387a8->enter($__internal_e078d76b56c9cbec7355deba71d316cbffa5450f02be9b0aab2ed0e3956387a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_3fd080b8f1031fe02fb6194de2482e10cec11096431e64427582964949de503d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3fd080b8f1031fe02fb6194de2482e10cec11096431e64427582964949de503d->enter($__internal_3fd080b8f1031fe02fb6194de2482e10cec11096431e64427582964949de503d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 16
        echo "    <h1 class=\"text-danger\"><i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error.name", array("%status_code%" => 403)), "html", null, true);
        echo "</h1>

    <p class=\"lead\">
        ";
        // line 19
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error_403.description"), "html", null, true);
        echo "
    </p>
    <p>
        ";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("http_error_403.suggestion"), "html", null, true);
        echo "
    </p>
";
        
        $__internal_3fd080b8f1031fe02fb6194de2482e10cec11096431e64427582964949de503d->leave($__internal_3fd080b8f1031fe02fb6194de2482e10cec11096431e64427582964949de503d_prof);

        
        $__internal_e078d76b56c9cbec7355deba71d316cbffa5450f02be9b0aab2ed0e3956387a8->leave($__internal_e078d76b56c9cbec7355deba71d316cbffa5450f02be9b0aab2ed0e3956387a8_prof);

    }

    // line 26
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_26781f56b8c2e40b8e91df3ff413d7b91117f9988fc24d114419de328ca166c8 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_26781f56b8c2e40b8e91df3ff413d7b91117f9988fc24d114419de328ca166c8->enter($__internal_26781f56b8c2e40b8e91df3ff413d7b91117f9988fc24d114419de328ca166c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_c508ad3514bad417138bfb2292435d98ed267252d4cebdf2f75cc7927f482b89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c508ad3514bad417138bfb2292435d98ed267252d4cebdf2f75cc7927f482b89->enter($__internal_c508ad3514bad417138bfb2292435d98ed267252d4cebdf2f75cc7927f482b89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 27
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "

    ";
        // line 29
        echo $this->env->getExtension('CodeExplorerBundle\Twig\SourceCodeExtension')->showSourceCode($this->env, $this);
        echo "
";
        
        $__internal_c508ad3514bad417138bfb2292435d98ed267252d4cebdf2f75cc7927f482b89->leave($__internal_c508ad3514bad417138bfb2292435d98ed267252d4cebdf2f75cc7927f482b89_prof);

        
        $__internal_26781f56b8c2e40b8e91df3ff413d7b91117f9988fc24d114419de328ca166c8->leave($__internal_26781f56b8c2e40b8e91df3ff413d7b91117f9988fc24d114419de328ca166c8_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:error403.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 29,  104 => 27,  95 => 26,  82 => 22,  76 => 19,  69 => 16,  60 => 15,  42 => 13,  11 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
    This template is used to render errors of type HTTP 403 (Forbidden)

    This is the simplest way to customize error pages in Symfony applications.
    In case you need it, you can also hook into the internal exception handling
    made by Symfony. This allows you to perform advanced tasks and even recover
    your application from some errors.
    See http://symfony.com/doc/current/cookbook/controller/error_pages.html
#}

{% extends 'base.html.twig' %}

{% block body_id 'error' %}

{% block main %}
    <h1 class=\"text-danger\"><i class=\"fa fa-unlock-alt\" aria-hidden=\"true\"></i> {{ 'http_error.name'|trans({ '%status_code%': 403 }) }}</h1>

    <p class=\"lead\">
        {{ 'http_error_403.description'|trans }}
    </p>
    <p>
        {{ 'http_error_403.suggestion'|trans }}
    </p>
{% endblock %}

{% block sidebar %}
    {{ parent() }}

    {{ show_source_code(_self) }}
{% endblock %}
", "TwigBundle:Exception:error403.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources/TwigBundle/views/Exception/error403.html.twig");
    }
}
