<?php

/* SonataBlockBundle:Block:block_core_action.html.twig */
class __TwigTemplate_ae8fdebfb2db3156e70358dc09acfc910be17e6539230796429c5a40b0d5bb85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "SonataBlockBundle:Block:block_core_action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b8152de6f25a8bd3fda0917eb7c5794e5d96fcf2a13a7c2a9f30df9926c7492e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b8152de6f25a8bd3fda0917eb7c5794e5d96fcf2a13a7c2a9f30df9926c7492e->enter($__internal_b8152de6f25a8bd3fda0917eb7c5794e5d96fcf2a13a7c2a9f30df9926c7492e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_action.html.twig"));

        $__internal_75cc3dc8b5b3c9d6dc0171444366337b4558dcd5c1bcecf10424e3bcbdaf881a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75cc3dc8b5b3c9d6dc0171444366337b4558dcd5c1bcecf10424e3bcbdaf881a->enter($__internal_75cc3dc8b5b3c9d6dc0171444366337b4558dcd5c1bcecf10424e3bcbdaf881a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataBlockBundle:Block:block_core_action.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b8152de6f25a8bd3fda0917eb7c5794e5d96fcf2a13a7c2a9f30df9926c7492e->leave($__internal_b8152de6f25a8bd3fda0917eb7c5794e5d96fcf2a13a7c2a9f30df9926c7492e_prof);

        
        $__internal_75cc3dc8b5b3c9d6dc0171444366337b4558dcd5c1bcecf10424e3bcbdaf881a->leave($__internal_75cc3dc8b5b3c9d6dc0171444366337b4558dcd5c1bcecf10424e3bcbdaf881a_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_6fda8a089c508cc01006a68d7fcc3a7b6edf480f16afbfe9f4b6e0c21ef29ac5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6fda8a089c508cc01006a68d7fcc3a7b6edf480f16afbfe9f4b6e0c21ef29ac5->enter($__internal_6fda8a089c508cc01006a68d7fcc3a7b6edf480f16afbfe9f4b6e0c21ef29ac5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_c0ffd3f62cc6e3b0f6fa7bdae81f30fb3d84b648baefef3fbdbc233f76917ab1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c0ffd3f62cc6e3b0f6fa7bdae81f30fb3d84b648baefef3fbdbc233f76917ab1->enter($__internal_c0ffd3f62cc6e3b0f6fa7bdae81f30fb3d84b648baefef3fbdbc233f76917ab1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    ";
        echo (isset($context["content"]) ? $context["content"] : $this->getContext($context, "content"));
        echo "
";
        
        $__internal_c0ffd3f62cc6e3b0f6fa7bdae81f30fb3d84b648baefef3fbdbc233f76917ab1->leave($__internal_c0ffd3f62cc6e3b0f6fa7bdae81f30fb3d84b648baefef3fbdbc233f76917ab1_prof);

        
        $__internal_6fda8a089c508cc01006a68d7fcc3a7b6edf480f16afbfe9f4b6e0c21ef29ac5->leave($__internal_6fda8a089c508cc01006a68d7fcc3a7b6edf480f16afbfe9f4b6e0c21ef29ac5_prof);

    }

    public function getTemplateName()
    {
        return "SonataBlockBundle:Block:block_core_action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    {{ content|raw }}
{% endblock %}
", "SonataBlockBundle:Block:block_core_action.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle/Resources/views/Block/block_core_action.html.twig");
    }
}
