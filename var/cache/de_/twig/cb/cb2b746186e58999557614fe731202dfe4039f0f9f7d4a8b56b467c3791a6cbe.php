<?php

/* WebProfilerBundle:Collector:ajax.html.twig */
class __TwigTemplate_1a4fe8e402614f9e7a793bca4e8ad772cf05fca647c3a19cf0e03e58dcdfa19c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:ajax.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ba2458a0ffccfd2c6f1a17af401a83f2bdc85879200fdc26f14e3b7aa13be704 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ba2458a0ffccfd2c6f1a17af401a83f2bdc85879200fdc26f14e3b7aa13be704->enter($__internal_ba2458a0ffccfd2c6f1a17af401a83f2bdc85879200fdc26f14e3b7aa13be704_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $__internal_1ce94beb5175c387fc0691ed2d83c9055ce5bd29ed671df2a29af44acb922af2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1ce94beb5175c387fc0691ed2d83c9055ce5bd29ed671df2a29af44acb922af2->enter($__internal_1ce94beb5175c387fc0691ed2d83c9055ce5bd29ed671df2a29af44acb922af2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:ajax.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ba2458a0ffccfd2c6f1a17af401a83f2bdc85879200fdc26f14e3b7aa13be704->leave($__internal_ba2458a0ffccfd2c6f1a17af401a83f2bdc85879200fdc26f14e3b7aa13be704_prof);

        
        $__internal_1ce94beb5175c387fc0691ed2d83c9055ce5bd29ed671df2a29af44acb922af2->leave($__internal_1ce94beb5175c387fc0691ed2d83c9055ce5bd29ed671df2a29af44acb922af2_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_69762c0c1c39c22838c55ff88146b798edc120b43d42404effbd23647cd78d1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_69762c0c1c39c22838c55ff88146b798edc120b43d42404effbd23647cd78d1f->enter($__internal_69762c0c1c39c22838c55ff88146b798edc120b43d42404effbd23647cd78d1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_12a7062ff2ec3c9f1ffd072843935ad382e354ec6e62c40febde4fd32d1cd440 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12a7062ff2ec3c9f1ffd072843935ad382e354ec6e62c40febde4fd32d1cd440->enter($__internal_12a7062ff2ec3c9f1ffd072843935ad382e354ec6e62c40febde4fd32d1cd440_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@WebProfiler/Icon/ajax.svg");
        echo "
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 8
        echo "
    ";
        // line 9
        $context["text"] = ('' === $tmp = "        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    ") ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 29
        echo "
    ";
        // line 30
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => false));
        echo "
";
        
        $__internal_12a7062ff2ec3c9f1ffd072843935ad382e354ec6e62c40febde4fd32d1cd440->leave($__internal_12a7062ff2ec3c9f1ffd072843935ad382e354ec6e62c40febde4fd32d1cd440_prof);

        
        $__internal_69762c0c1c39c22838c55ff88146b798edc120b43d42404effbd23647cd78d1f->leave($__internal_69762c0c1c39c22838c55ff88146b798edc120b43d42404effbd23647cd78d1f_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:ajax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 30,  82 => 29,  62 => 9,  59 => 8,  52 => 5,  49 => 4,  40 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {{ include('@WebProfiler/Icon/ajax.svg') }}
        <span class=\"sf-toolbar-value sf-toolbar-ajax-request-counter\">0</span>
    {% endset %}

    {% set text %}
        <div class=\"sf-toolbar-info-piece\">
            <b class=\"sf-toolbar-ajax-info\"></b>
        </div>
        <div class=\"sf-toolbar-info-piece\">
            <table class=\"sf-toolbar-ajax-requests\">
                <thead>
                    <tr>
                        <th>Method</th>
                        <th>Type</th>
                        <th>Status</th>
                        <th>URL</th>
                        <th>Time</th>
                        <th>Profile</th>
                    </tr>
                </thead>
                <tbody class=\"sf-toolbar-ajax-request-list\"></tbody>
            </table>
        </div>
    {% endset %}

    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: false }) }}
{% endblock %}
", "WebProfilerBundle:Collector:ajax.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/ajax.html.twig");
    }
}
