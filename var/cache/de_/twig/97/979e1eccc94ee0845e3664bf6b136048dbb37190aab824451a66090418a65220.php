<?php

/* @SonataBlock/Block/block_side_menu_template.html.twig */
class __TwigTemplate_9d9dea6335dc6d76dd4912c26b36a73d4b5060e6a25445eefb1ab6856a279632 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("knp_menu.html.twig", "@SonataBlock/Block/block_side_menu_template.html.twig", 12);
        $this->blocks = array(
            'list' => array($this, 'block_list'),
            'item' => array($this, 'block_item'),
            'linkElement' => array($this, 'block_linkElement'),
            'spanElement' => array($this, 'block_spanElement'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "knp_menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_71cc16df9eda38333973f49dbe68f87ec76a3bcf271ece45defcd9b6c8df2654 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_71cc16df9eda38333973f49dbe68f87ec76a3bcf271ece45defcd9b6c8df2654->enter($__internal_71cc16df9eda38333973f49dbe68f87ec76a3bcf271ece45defcd9b6c8df2654_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_side_menu_template.html.twig"));

        $__internal_76c514352ac7818f3e6cc48b024ae9958b2337b983c1044fa8d338448e04b436 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76c514352ac7818f3e6cc48b024ae9958b2337b983c1044fa8d338448e04b436->enter($__internal_76c514352ac7818f3e6cc48b024ae9958b2337b983c1044fa8d338448e04b436_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_side_menu_template.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_71cc16df9eda38333973f49dbe68f87ec76a3bcf271ece45defcd9b6c8df2654->leave($__internal_71cc16df9eda38333973f49dbe68f87ec76a3bcf271ece45defcd9b6c8df2654_prof);

        
        $__internal_76c514352ac7818f3e6cc48b024ae9958b2337b983c1044fa8d338448e04b436->leave($__internal_76c514352ac7818f3e6cc48b024ae9958b2337b983c1044fa8d338448e04b436_prof);

    }

    // line 14
    public function block_list($context, array $blocks = array())
    {
        $__internal_5afa7da3ca8c6651909efc18dee66f89df17dc6e3697fe8642f6305c01ac8f1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5afa7da3ca8c6651909efc18dee66f89df17dc6e3697fe8642f6305c01ac8f1f->enter($__internal_5afa7da3ca8c6651909efc18dee66f89df17dc6e3697fe8642f6305c01ac8f1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        $__internal_c290286eb24e0419da787462afce46a45de0446820772ab19203db6c5496baa8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c290286eb24e0419da787462afce46a45de0446820772ab19203db6c5496baa8->enter($__internal_c290286eb24e0419da787462afce46a45de0446820772ab19203db6c5496baa8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "list"));

        // line 15
        $context["macros"] = $this->loadTemplate("knp_menu.html.twig", "@SonataBlock/Block/block_side_menu_template.html.twig", 15);
        // line 16
        echo "    ";
        if ((($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "hasChildren", array()) &&  !($this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "depth", array()) === 0)) && $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "displayChildren", array()))) {
            // line 17
            echo "        <div";
            echo $context["macros"]->getattributes((isset($context["listAttributes"]) ? $context["listAttributes"] : $this->getContext($context, "listAttributes")));
            echo ">
            ";
            // line 18
            $this->displayBlock("children", $context, $blocks);
            echo "
        </div>
    ";
        }
        
        $__internal_c290286eb24e0419da787462afce46a45de0446820772ab19203db6c5496baa8->leave($__internal_c290286eb24e0419da787462afce46a45de0446820772ab19203db6c5496baa8_prof);

        
        $__internal_5afa7da3ca8c6651909efc18dee66f89df17dc6e3697fe8642f6305c01ac8f1f->leave($__internal_5afa7da3ca8c6651909efc18dee66f89df17dc6e3697fe8642f6305c01ac8f1f_prof);

    }

    // line 23
    public function block_item($context, array $blocks = array())
    {
        $__internal_6194370ae5516d1977943f8e3b768e4fd47222ff82a20b867d085116686b878a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6194370ae5516d1977943f8e3b768e4fd47222ff82a20b867d085116686b878a->enter($__internal_6194370ae5516d1977943f8e3b768e4fd47222ff82a20b867d085116686b878a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item"));

        $__internal_759c7f1198ac6d99b27cf80ae8694eb41977197ddd49fb2d1ec02bfce2fe2bb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_759c7f1198ac6d99b27cf80ae8694eb41977197ddd49fb2d1ec02bfce2fe2bb6->enter($__internal_759c7f1198ac6d99b27cf80ae8694eb41977197ddd49fb2d1ec02bfce2fe2bb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "item"));

        // line 24
        $context["macros"] = $this->loadTemplate("knp_menu.html.twig", "@SonataBlock/Block/block_side_menu_template.html.twig", 24);
        // line 25
        echo "    ";
        if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "displayed", array())) {
            // line 26
            echo "        ";
            // line 27
            $context["classes"] = (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attribute", array(0 => "class"), "method"))) ? (array(0 => $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attribute", array(0 => "class"), "method"))) : (array()));
            // line 28
            if ($this->getAttribute((isset($context["matcher"]) ? $context["matcher"] : $this->getContext($context, "matcher")), "isCurrent", array(0 => (isset($context["item"]) ? $context["item"] : $this->getContext($context, "item"))), "method")) {
                // line 29
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "currentClass", array())));
            } elseif ($this->getAttribute(            // line 30
(isset($context["matcher"]) ? $context["matcher"] : $this->getContext($context, "matcher")), "isAncestor", array(0 => (isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), 1 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "depth", array())), "method")) {
                // line 31
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "ancestorClass", array())));
            }
            // line 33
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "actsLikeFirst", array())) {
                // line 34
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "firstClass", array())));
            }
            // line 36
            if ($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "actsLikeLast", array())) {
                // line 37
                $context["classes"] = twig_array_merge((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), array(0 => $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "lastClass", array())));
            }
            // line 39
            $context["attributes"] = $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attributes", array());
            // line 40
            if ( !twig_test_empty((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")))) {
                // line 41
                $context["attributes"] = twig_array_merge((isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")), array("class" => twig_join_filter((isset($context["classes"]) ? $context["classes"] : $this->getContext($context, "classes")), " ")));
            }
            // line 43
            echo "        ";
            // line 44
            if (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "uri", array())) && ( !$this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "current", array()) || $this->getAttribute((isset($context["options"]) ? $context["options"] : $this->getContext($context, "options")), "currentAsLink", array())))) {
                // line 45
                echo "            ";
                $this->displayBlock("linkElement", $context, $blocks);
            } else {
                // line 47
                echo "            ";
                $this->displayBlock("spanElement", $context, $blocks);
            }
            // line 49
            echo "        ";
            // line 50
            $context["childrenClasses"] = (( !twig_test_empty($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttribute", array(0 => "class"), "method"))) ? (array(0 => $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttribute", array(0 => "class"), "method"))) : (array()));
            // line 51
            $context["childrenClasses"] = twig_array_merge((isset($context["childrenClasses"]) ? $context["childrenClasses"] : $this->getContext($context, "childrenClasses")), array(0 => ("menu_level_" . $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "level", array()))));
            // line 52
            $context["listAttributes"] = twig_array_merge($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "childrenAttributes", array()), array("class" => twig_join_filter((isset($context["childrenClasses"]) ? $context["childrenClasses"] : $this->getContext($context, "childrenClasses")), " ")));
            // line 53
            echo "        ";
            $this->displayBlock("list", $context, $blocks);
            echo "
    ";
        }
        
        $__internal_759c7f1198ac6d99b27cf80ae8694eb41977197ddd49fb2d1ec02bfce2fe2bb6->leave($__internal_759c7f1198ac6d99b27cf80ae8694eb41977197ddd49fb2d1ec02bfce2fe2bb6_prof);

        
        $__internal_6194370ae5516d1977943f8e3b768e4fd47222ff82a20b867d085116686b878a->leave($__internal_6194370ae5516d1977943f8e3b768e4fd47222ff82a20b867d085116686b878a_prof);

    }

    // line 57
    public function block_linkElement($context, array $blocks = array())
    {
        $__internal_03c5eaad6bf529d19aaa5370b9fda7728ad030b6e76cd830918aa9fd85cde1af = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_03c5eaad6bf529d19aaa5370b9fda7728ad030b6e76cd830918aa9fd85cde1af->enter($__internal_03c5eaad6bf529d19aaa5370b9fda7728ad030b6e76cd830918aa9fd85cde1af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "linkElement"));

        $__internal_b2e01a4c4e03a0d381744ec33a9177197c5a324f2c72a8c022d316dcd048fd57 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b2e01a4c4e03a0d381744ec33a9177197c5a324f2c72a8c022d316dcd048fd57->enter($__internal_b2e01a4c4e03a0d381744ec33a9177197c5a324f2c72a8c022d316dcd048fd57_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "linkElement"));

        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "uri", array()), "html", null, true);
        echo "\"";
        echo $this->getAttribute((isset($context["macros"]) ? $context["macros"] : $this->getContext($context, "macros")), "attributes", array(0 => twig_array_merge(twig_array_merge($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attributes", array()), $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "linkAttributes", array())), (isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")))), "method");
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</a>";
        
        $__internal_b2e01a4c4e03a0d381744ec33a9177197c5a324f2c72a8c022d316dcd048fd57->leave($__internal_b2e01a4c4e03a0d381744ec33a9177197c5a324f2c72a8c022d316dcd048fd57_prof);

        
        $__internal_03c5eaad6bf529d19aaa5370b9fda7728ad030b6e76cd830918aa9fd85cde1af->leave($__internal_03c5eaad6bf529d19aaa5370b9fda7728ad030b6e76cd830918aa9fd85cde1af_prof);

    }

    // line 59
    public function block_spanElement($context, array $blocks = array())
    {
        $__internal_0fc49a8f62d222af1c36ed51ca1b9d5b67d298a733a80b88bb14852287a0a668 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0fc49a8f62d222af1c36ed51ca1b9d5b67d298a733a80b88bb14852287a0a668->enter($__internal_0fc49a8f62d222af1c36ed51ca1b9d5b67d298a733a80b88bb14852287a0a668_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "spanElement"));

        $__internal_5a61122bac357ea04542899c266478362965570ade3bfa6f2adbbcfbac7352f9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5a61122bac357ea04542899c266478362965570ade3bfa6f2adbbcfbac7352f9->enter($__internal_5a61122bac357ea04542899c266478362965570ade3bfa6f2adbbcfbac7352f9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "spanElement"));

        echo "<div";
        echo $this->getAttribute((isset($context["macros"]) ? $context["macros"] : $this->getContext($context, "macros")), "attributes", array(0 => twig_array_merge(twig_array_merge($this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "attributes", array()), $this->getAttribute((isset($context["item"]) ? $context["item"] : $this->getContext($context, "item")), "labelAttributes", array())), (isset($context["attributes"]) ? $context["attributes"] : $this->getContext($context, "attributes")))), "method");
        echo "><h4 class=\"list-group-item-heading\">";
        $this->displayBlock("label", $context, $blocks);
        echo "</h4></div>";
        
        $__internal_5a61122bac357ea04542899c266478362965570ade3bfa6f2adbbcfbac7352f9->leave($__internal_5a61122bac357ea04542899c266478362965570ade3bfa6f2adbbcfbac7352f9_prof);

        
        $__internal_0fc49a8f62d222af1c36ed51ca1b9d5b67d298a733a80b88bb14852287a0a668->leave($__internal_0fc49a8f62d222af1c36ed51ca1b9d5b67d298a733a80b88bb14852287a0a668_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_side_menu_template.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 59,  154 => 57,  140 => 53,  138 => 52,  136 => 51,  134 => 50,  132 => 49,  128 => 47,  124 => 45,  122 => 44,  120 => 43,  117 => 41,  115 => 40,  113 => 39,  110 => 37,  108 => 36,  105 => 34,  103 => 33,  100 => 31,  98 => 30,  96 => 29,  94 => 28,  92 => 27,  90 => 26,  87 => 25,  85 => 24,  76 => 23,  62 => 18,  57 => 17,  54 => 16,  52 => 15,  43 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'knp_menu.html.twig' %}

{% block list %}
{% import 'knp_menu.html.twig' as macros %}
    {% if item.hasChildren and options.depth is not same as(0) and item.displayChildren %}
        <div{{ macros.attributes(listAttributes) }}>
            {{ block('children') }}
        </div>
    {% endif %}
{% endblock %}

{% block item %}
{% import 'knp_menu.html.twig' as macros %}
    {% if item.displayed %}
        {# building the class of the item #}
        {%- set classes = item.attribute('class') is not empty ? [item.attribute('class')] : [] %}
        {%- if matcher.isCurrent(item) %}
            {%- set classes = classes|merge([options.currentClass]) %}
        {%- elseif matcher.isAncestor(item, options.depth) %}
            {%- set classes = classes|merge([options.ancestorClass]) %}
        {%- endif %}
        {%- if item.actsLikeFirst %}
            {%- set classes = classes|merge([options.firstClass]) %}
        {%- endif %}
        {%- if item.actsLikeLast %}
            {%- set classes = classes|merge([options.lastClass]) %}
        {%- endif %}
        {%- set attributes = item.attributes %}
        {%- if classes is not empty %}
            {%- set attributes = attributes|merge({'class': classes|join(' ')}) %}
        {%- endif %}
        {# displaying the item #}
        {%- if item.uri is not empty and (not item.current or options.currentAsLink) %}
            {{ block('linkElement') }}
        {%- else %}
            {{ block('spanElement') }}
        {%- endif %}
        {# render the list of children#}
        {%- set childrenClasses = item.childrenAttribute('class') is not empty ? [item.childrenAttribute('class')] : [] %}
        {%- set childrenClasses = childrenClasses|merge(['menu_level_' ~ item.level]) %}
        {%- set listAttributes = item.childrenAttributes|merge({'class': childrenClasses|join(' ') }) %}
        {{ block('list') }}
    {% endif %}
{% endblock %}

{% block linkElement %}<a href=\"{{ item.uri }}\"{{ macros.attributes(item.attributes|merge(item.linkAttributes)|merge(attributes)) }}>{{ block('label') }}</a>{% endblock %}

{% block spanElement %}<div{{ macros.attributes(item.attributes|merge(item.labelAttributes)|merge(attributes)) }}><h4 class=\"list-group-item-heading\">{{ block('label') }}</h4></div>{% endblock %}
", "@SonataBlock/Block/block_side_menu_template.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_side_menu_template.html.twig");
    }
}
