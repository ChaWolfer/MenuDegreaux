<?php

/* FOSUserBundle:Registration:email.txt.twig */
class __TwigTemplate_6c9e63d6998b249bddce8ded68a7bd4df8b702e1d6ae33a0a29a4da94b6bf795 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3636e10bbed161ae42f54f5d54c48b167fefab5f962d33dfecb67e16a7e43fbf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3636e10bbed161ae42f54f5d54c48b167fefab5f962d33dfecb67e16a7e43fbf->enter($__internal_3636e10bbed161ae42f54f5d54c48b167fefab5f962d33dfecb67e16a7e43fbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        $__internal_364b8e29013b1033e8f753febbd1e9dd21ff8abd9f80d727c148139a7b273c13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_364b8e29013b1033e8f753febbd1e9dd21ff8abd9f80d727c148139a7b273c13->enter($__internal_364b8e29013b1033e8f753febbd1e9dd21ff8abd9f80d727c148139a7b273c13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Registration:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_3636e10bbed161ae42f54f5d54c48b167fefab5f962d33dfecb67e16a7e43fbf->leave($__internal_3636e10bbed161ae42f54f5d54c48b167fefab5f962d33dfecb67e16a7e43fbf_prof);

        
        $__internal_364b8e29013b1033e8f753febbd1e9dd21ff8abd9f80d727c148139a7b273c13->leave($__internal_364b8e29013b1033e8f753febbd1e9dd21ff8abd9f80d727c148139a7b273c13_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_2ceae57c72fbc1ad32f17a1c7fdee3ea48026660d2331a5967bb966e4d1799c4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2ceae57c72fbc1ad32f17a1c7fdee3ea48026660d2331a5967bb966e4d1799c4->enter($__internal_2ceae57c72fbc1ad32f17a1c7fdee3ea48026660d2331a5967bb966e4d1799c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_e1aa11e38ce7a435d2f3015483f11e20913f50a3fd48ca493f19349055614276 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1aa11e38ce7a435d2f3015483f11e20913f50a3fd48ca493f19349055614276->enter($__internal_e1aa11e38ce7a435d2f3015483f11e20913f50a3fd48ca493f19349055614276_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        
        $__internal_e1aa11e38ce7a435d2f3015483f11e20913f50a3fd48ca493f19349055614276->leave($__internal_e1aa11e38ce7a435d2f3015483f11e20913f50a3fd48ca493f19349055614276_prof);

        
        $__internal_2ceae57c72fbc1ad32f17a1c7fdee3ea48026660d2331a5967bb966e4d1799c4->leave($__internal_2ceae57c72fbc1ad32f17a1c7fdee3ea48026660d2331a5967bb966e4d1799c4_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_95d08bbb97c2de636e1dc76513620983832375b5e637cae3c05b4a7b5335642f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_95d08bbb97c2de636e1dc76513620983832375b5e637cae3c05b4a7b5335642f->enter($__internal_95d08bbb97c2de636e1dc76513620983832375b5e637cae3c05b4a7b5335642f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_d959b9aaebff97e2851b0387bdefda1fb1c3c39ba66e632fa3c0cd1c60cc96fc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d959b9aaebff97e2851b0387bdefda1fb1c3c39ba66e632fa3c0cd1c60cc96fc->enter($__internal_d959b9aaebff97e2851b0387bdefda1fb1c3c39ba66e632fa3c0cd1c60cc96fc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("registration.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_d959b9aaebff97e2851b0387bdefda1fb1c3c39ba66e632fa3c0cd1c60cc96fc->leave($__internal_d959b9aaebff97e2851b0387bdefda1fb1c3c39ba66e632fa3c0cd1c60cc96fc_prof);

        
        $__internal_95d08bbb97c2de636e1dc76513620983832375b5e637cae3c05b4a7b5335642f->leave($__internal_95d08bbb97c2de636e1dc76513620983832375b5e637cae3c05b4a7b5335642f_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_d85eb9d4f3533cd4b532ce944831e48c3d39ff68aaee94ea6e4dce6e373c1c74 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d85eb9d4f3533cd4b532ce944831e48c3d39ff68aaee94ea6e4dce6e373c1c74->enter($__internal_d85eb9d4f3533cd4b532ce944831e48c3d39ff68aaee94ea6e4dce6e373c1c74_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_be33702d7fd240b98a6ba5ee84d13a999084485d239e0b3412e7eb751490c3b4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_be33702d7fd240b98a6ba5ee84d13a999084485d239e0b3412e7eb751490c3b4->enter($__internal_be33702d7fd240b98a6ba5ee84d13a999084485d239e0b3412e7eb751490c3b4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_be33702d7fd240b98a6ba5ee84d13a999084485d239e0b3412e7eb751490c3b4->leave($__internal_be33702d7fd240b98a6ba5ee84d13a999084485d239e0b3412e7eb751490c3b4_prof);

        
        $__internal_d85eb9d4f3533cd4b532ce944831e48c3d39ff68aaee94ea6e4dce6e373c1c74->leave($__internal_d85eb9d4f3533cd4b532ce944831e48c3d39ff68aaee94ea6e4dce6e373c1c74_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Registration:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'registration.email.subject'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'registration.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Registration:email.txt.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Registration/email.txt.twig");
    }
}
