<?php

/* form/fields.html.twig */
class __TwigTemplate_0183c6960996a5a33103b7f15c9a33e132af5b84efeba0f55050c9710cc8e4d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'tags_input_widget' => array($this, 'block_tags_input_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58362a45e2d096034971d088bb1bc537756850a6e3aa8514d4d9e27f80f04e81 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58362a45e2d096034971d088bb1bc537756850a6e3aa8514d4d9e27f80f04e81->enter($__internal_58362a45e2d096034971d088bb1bc537756850a6e3aa8514d4d9e27f80f04e81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        $__internal_9efc79f173ff6a43a546e71b77754a667a13347e925fc1a4a92ec2f6699e8e98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9efc79f173ff6a43a546e71b77754a667a13347e925fc1a4a92ec2f6699e8e98->enter($__internal_9efc79f173ff6a43a546e71b77754a667a13347e925fc1a4a92ec2f6699e8e98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        // line 1
        echo "



";
        // line 5
        $this->displayBlock('tags_input_widget', $context, $blocks);
        
        $__internal_58362a45e2d096034971d088bb1bc537756850a6e3aa8514d4d9e27f80f04e81->leave($__internal_58362a45e2d096034971d088bb1bc537756850a6e3aa8514d4d9e27f80f04e81_prof);

        
        $__internal_9efc79f173ff6a43a546e71b77754a667a13347e925fc1a4a92ec2f6699e8e98->leave($__internal_9efc79f173ff6a43a546e71b77754a667a13347e925fc1a4a92ec2f6699e8e98_prof);

    }

    public function block_tags_input_widget($context, array $blocks = array())
    {
        $__internal_d1f9ad7a1fc7adb260aae1373e70b9d47d2b49b1e8649758acda1ff0738180d9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1f9ad7a1fc7adb260aae1373e70b9d47d2b49b1e8649758acda1ff0738180d9->enter($__internal_d1f9ad7a1fc7adb260aae1373e70b9d47d2b49b1e8649758acda1ff0738180d9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        $__internal_07049988ed327ee794b8c50013505bc5f136a5c302afbfe323f87f4d272bfea4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_07049988ed327ee794b8c50013505bc5f136a5c302afbfe323f87f4d272bfea4->enter($__internal_07049988ed327ee794b8c50013505bc5f136a5c302afbfe323f87f4d272bfea4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tags_input_widget"));

        // line 6
        echo "    <div class=\"input-group\">
        ";
        // line 7
        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget', array("attr" => array("data-toggle" => "tagsinput", "data-tags" => twig_jsonencode_filter((isset($context["tags"]) ? $context["tags"] : $this->getContext($context, "tags"))))));
        echo "
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
";
        
        $__internal_07049988ed327ee794b8c50013505bc5f136a5c302afbfe323f87f4d272bfea4->leave($__internal_07049988ed327ee794b8c50013505bc5f136a5c302afbfe323f87f4d272bfea4_prof);

        
        $__internal_d1f9ad7a1fc7adb260aae1373e70b9d47d2b49b1e8649758acda1ff0738180d9->leave($__internal_d1f9ad7a1fc7adb260aae1373e70b9d47d2b49b1e8649758acda1ff0738180d9_prof);

    }

    public function getTemplateName()
    {
        return "form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  53 => 7,  50 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("



{% block tags_input_widget %}
    <div class=\"input-group\">
        {{ form_widget(form, {'attr': {'data-toggle': 'tagsinput', 'data-tags': tags|json_encode}}) }}
        <span class=\"input-group-addon\">
            <span class=\"fa fa-tags\" aria-hidden=\"true\"></span>
        </span>
    </div>
{% endblock %}
", "form/fields.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app\\Resources\\views\\form\\fields.html.twig");
    }
}
