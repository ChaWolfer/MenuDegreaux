<?php

/* TwigBundle:Exception:exception.js.twig */
class __TwigTemplate_7060d99ab4ad01b8d85e722e91f2d622f593ec8c451dac3aa06ea84ec01cb366 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_36d0c636def7153490b0482d920f56999947b38549831ffa2923c752c2c8fa3c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_36d0c636def7153490b0482d920f56999947b38549831ffa2923c752c2c8fa3c->enter($__internal_36d0c636def7153490b0482d920f56999947b38549831ffa2923c752c2c8fa3c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        $__internal_c8ee18b83df3b74796b004004e02645af173b6a69cf17e9ae272fc8e60502d35 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c8ee18b83df3b74796b004004e02645af173b6a69cf17e9ae272fc8e60502d35->enter($__internal_c8ee18b83df3b74796b004004e02645af173b6a69cf17e9ae272fc8e60502d35_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.js.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_36d0c636def7153490b0482d920f56999947b38549831ffa2923c752c2c8fa3c->leave($__internal_36d0c636def7153490b0482d920f56999947b38549831ffa2923c752c2c8fa3c_prof);

        
        $__internal_c8ee18b83df3b74796b004004e02645af173b6a69cf17e9ae272fc8e60502d35->leave($__internal_c8ee18b83df3b74796b004004e02645af173b6a69cf17e9ae272fc8e60502d35_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.js.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.js.twig");
    }
}
