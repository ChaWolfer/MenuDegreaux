<?php

/* SonataAdminBundle:CRUD:delete.html.twig */
class __TwigTemplate_999c06892d3f95fcf8c9790f7e3d5c505a8cca963a021fdf4f3cdf033d96fa70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'actions' => array($this, 'block_actions'),
            'tab_menu' => array($this, 'block_tab_menu'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:delete.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e809e02c071019446c6df334ad7176fb430402b085245349f92116f3074a0c1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e809e02c071019446c6df334ad7176fb430402b085245349f92116f3074a0c1a->enter($__internal_e809e02c071019446c6df334ad7176fb430402b085245349f92116f3074a0c1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:delete.html.twig"));

        $__internal_397f30c476e242117fcaaee201e3ff828c25ffe0064224bfbaec78261106f8a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_397f30c476e242117fcaaee201e3ff828c25ffe0064224bfbaec78261106f8a3->enter($__internal_397f30c476e242117fcaaee201e3ff828c25ffe0064224bfbaec78261106f8a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:delete.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e809e02c071019446c6df334ad7176fb430402b085245349f92116f3074a0c1a->leave($__internal_e809e02c071019446c6df334ad7176fb430402b085245349f92116f3074a0c1a_prof);

        
        $__internal_397f30c476e242117fcaaee201e3ff828c25ffe0064224bfbaec78261106f8a3->leave($__internal_397f30c476e242117fcaaee201e3ff828c25ffe0064224bfbaec78261106f8a3_prof);

    }

    // line 14
    public function block_actions($context, array $blocks = array())
    {
        $__internal_b3497ac51338ce455b4816a82e768d9958d8dd363ee07d2468c10ad9bfa53011 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b3497ac51338ce455b4816a82e768d9958d8dd363ee07d2468c10ad9bfa53011->enter($__internal_b3497ac51338ce455b4816a82e768d9958d8dd363ee07d2468c10ad9bfa53011_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        $__internal_25ec3d6ef2849d66c8007f7f850d8bdd89854890699d5838cdf1a580a114608e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25ec3d6ef2849d66c8007f7f850d8bdd89854890699d5838cdf1a580a114608e->enter($__internal_25ec3d6ef2849d66c8007f7f850d8bdd89854890699d5838cdf1a580a114608e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "actions"));

        // line 15
        $this->loadTemplate("SonataAdminBundle:CRUD:action_buttons.html.twig", "SonataAdminBundle:CRUD:delete.html.twig", 15)->display($context);
        
        $__internal_25ec3d6ef2849d66c8007f7f850d8bdd89854890699d5838cdf1a580a114608e->leave($__internal_25ec3d6ef2849d66c8007f7f850d8bdd89854890699d5838cdf1a580a114608e_prof);

        
        $__internal_b3497ac51338ce455b4816a82e768d9958d8dd363ee07d2468c10ad9bfa53011->leave($__internal_b3497ac51338ce455b4816a82e768d9958d8dd363ee07d2468c10ad9bfa53011_prof);

    }

    // line 18
    public function block_tab_menu($context, array $blocks = array())
    {
        $__internal_5e9371365b7d7c89aba23eb4d7921ef75f28eda585999af55a90fe4ab624da1f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5e9371365b7d7c89aba23eb4d7921ef75f28eda585999af55a90fe4ab624da1f->enter($__internal_5e9371365b7d7c89aba23eb4d7921ef75f28eda585999af55a90fe4ab624da1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        $__internal_8aaa0698b27c13483c8d74d521ba064bd2f53a32cb7a8337d8cb8eb1348d8ba4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8aaa0698b27c13483c8d74d521ba064bd2f53a32cb7a8337d8cb8eb1348d8ba4->enter($__internal_8aaa0698b27c13483c8d74d521ba064bd2f53a32cb7a8337d8cb8eb1348d8ba4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu"));

        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "sidemenu", array(0 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action"))), "method"), array("currentClass" => "active", "template" => $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "tab_menu_template"), "method")), "twig");
        
        $__internal_8aaa0698b27c13483c8d74d521ba064bd2f53a32cb7a8337d8cb8eb1348d8ba4->leave($__internal_8aaa0698b27c13483c8d74d521ba064bd2f53a32cb7a8337d8cb8eb1348d8ba4_prof);

        
        $__internal_5e9371365b7d7c89aba23eb4d7921ef75f28eda585999af55a90fe4ab624da1f->leave($__internal_5e9371365b7d7c89aba23eb4d7921ef75f28eda585999af55a90fe4ab624da1f_prof);

    }

    // line 20
    public function block_content($context, array $blocks = array())
    {
        $__internal_178b46683daca6368f4dd7f3d601a99ecdeed13054cb1d3938c7282aba741f00 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_178b46683daca6368f4dd7f3d601a99ecdeed13054cb1d3938c7282aba741f00->enter($__internal_178b46683daca6368f4dd7f3d601a99ecdeed13054cb1d3938c7282aba741f00_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        $__internal_7df45f494c8af5330c3c144deae6ea44216ec7492a9dd966744c62045f1ad04a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7df45f494c8af5330c3c144deae6ea44216ec7492a9dd966744c62045f1ad04a->enter($__internal_7df45f494c8af5330c3c144deae6ea44216ec7492a9dd966744c62045f1ad04a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "content"));

        // line 21
        echo "    <div class=\"sonata-ba-delete\">

        <div class=\"box box-danger\">
            <div class=\"box-header\">
                <h3 class=\"box-title\">";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title_delete", array(), "SonataAdminBundle"), "html", null, true);
        echo "</h3>
            </div>
            <div class=\"box-body\">
                ";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("message_delete_confirmation", array("%object%" => $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "toString", array(0 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method")), "SonataAdminBundle"), "html", null, true);
        echo "
            </div>
            <div class=\"box-footer clearfix\">
                <form method=\"POST\" action=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateObjectUrl", array(0 => "delete", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
        echo "\">
                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                    <input type=\"hidden\" name=\"_sonata_csrf_token\" value=\"";
        // line 33
        echo twig_escape_filter($this->env, (isset($context["csrf_token"]) ? $context["csrf_token"] : $this->getContext($context, "csrf_token")), "html", null, true);
        echo "\">

                    <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> ";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("btn_delete", array(), "SonataAdminBundle"), "html", null, true);
        echo "</button>
                    ";
        // line 36
        if (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasRoute", array(0 => "edit"), "method") && $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "hasAccess", array(0 => "edit", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"))) {
            // line 37
            echo "                        ";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("delete_or", array(), "SonataAdminBundle"), "html", null, true);
            echo "

                        <a class=\"btn btn-success\" href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateObjectUrl", array(0 => "edit", 1 => (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object"))), "method"), "html", null, true);
            echo "\">
                            <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
                            ";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("link_action_edit", array(), "SonataAdminBundle"), "html", null, true);
            echo "</a>
                    ";
        }
        // line 43
        echo "                </form>
            </div>
        </div>
    </div>
";
        
        $__internal_7df45f494c8af5330c3c144deae6ea44216ec7492a9dd966744c62045f1ad04a->leave($__internal_7df45f494c8af5330c3c144deae6ea44216ec7492a9dd966744c62045f1ad04a_prof);

        
        $__internal_178b46683daca6368f4dd7f3d601a99ecdeed13054cb1d3938c7282aba741f00->leave($__internal_178b46683daca6368f4dd7f3d601a99ecdeed13054cb1d3938c7282aba741f00_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:delete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 43,  132 => 41,  127 => 39,  121 => 37,  119 => 36,  115 => 35,  110 => 33,  105 => 31,  99 => 28,  93 => 25,  87 => 21,  78 => 20,  60 => 18,  50 => 15,  41 => 14,  20 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{%- block actions -%}
    {% include 'SonataAdminBundle:CRUD:action_buttons.html.twig' %}
{%- endblock -%}

{% block tab_menu %}{{ knp_menu_render(admin.sidemenu(action), {'currentClass' : 'active', 'template': sonata_admin.adminPool.getTemplate('tab_menu_template')}, 'twig') }}{% endblock %}

{% block content %}
    <div class=\"sonata-ba-delete\">

        <div class=\"box box-danger\">
            <div class=\"box-header\">
                <h3 class=\"box-title\">{{ 'title_delete'|trans({}, 'SonataAdminBundle') }}</h3>
            </div>
            <div class=\"box-body\">
                {{ 'message_delete_confirmation'|trans({'%object%': admin.toString(object)}, 'SonataAdminBundle') }}
            </div>
            <div class=\"box-footer clearfix\">
                <form method=\"POST\" action=\"{{ admin.generateObjectUrl('delete', object) }}\">
                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                    <input type=\"hidden\" name=\"_sonata_csrf_token\" value=\"{{ csrf_token }}\">

                    <button type=\"submit\" class=\"btn btn-danger\"><i class=\"fa fa-trash\" aria-hidden=\"true\"></i> {{ 'btn_delete'|trans({}, 'SonataAdminBundle') }}</button>
                    {% if admin.hasRoute('edit') and admin.hasAccess('edit', object) %}
                        {{ 'delete_or'|trans({}, 'SonataAdminBundle') }}

                        <a class=\"btn btn-success\" href=\"{{ admin.generateObjectUrl('edit', object) }}\">
                            <i class=\"fa fa-pencil\" aria-hidden=\"true\"></i>
                            {{ 'link_action_edit'|trans({}, 'SonataAdminBundle') }}</a>
                    {% endif %}
                </form>
            </div>
        </div>
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:delete.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/delete.html.twig");
    }
}
