<?php

/* :menu:scolaire.html.twig */
class __TwigTemplate_12d3a50e8815d64794a6a7b4bb6164384320f9d44a6ab7da907d039e1ddcdc58 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":menu:scolaire.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e607ab59bec621de9235bce682c002113f803b218bf354cd88962542f8667106 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e607ab59bec621de9235bce682c002113f803b218bf354cd88962542f8667106->enter($__internal_e607ab59bec621de9235bce682c002113f803b218bf354cd88962542f8667106_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:scolaire.html.twig"));

        $__internal_71474fb18c425feb300d66be23a5973e9795dbfbab20ad256d310cb42bf40c1a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_71474fb18c425feb300d66be23a5973e9795dbfbab20ad256d310cb42bf40c1a->enter($__internal_71474fb18c425feb300d66be23a5973e9795dbfbab20ad256d310cb42bf40c1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":menu:scolaire.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e607ab59bec621de9235bce682c002113f803b218bf354cd88962542f8667106->leave($__internal_e607ab59bec621de9235bce682c002113f803b218bf354cd88962542f8667106_prof);

        
        $__internal_71474fb18c425feb300d66be23a5973e9795dbfbab20ad256d310cb42bf40c1a->leave($__internal_71474fb18c425feb300d66be23a5973e9795dbfbab20ad256d310cb42bf40c1a_prof);

    }

    // line 5
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_21b7b629800d08feb8ea5da175cc8957ecd5249f0ec5a75c6fb7e823ee4ea619 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_21b7b629800d08feb8ea5da175cc8957ecd5249f0ec5a75c6fb7e823ee4ea619->enter($__internal_21b7b629800d08feb8ea5da175cc8957ecd5249f0ec5a75c6fb7e823ee4ea619_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_2c142b3c1bc96a646de6020dc7a109c3b97cf43d8d70c3f9e7f5ab4d73ef1dd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2c142b3c1bc96a646de6020dc7a109c3b97cf43d8d70c3f9e7f5ab4d73ef1dd2->enter($__internal_2c142b3c1bc96a646de6020dc7a109c3b97cf43d8d70c3f9e7f5ab4d73ef1dd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "menu_scolaire";
        
        $__internal_2c142b3c1bc96a646de6020dc7a109c3b97cf43d8d70c3f9e7f5ab4d73ef1dd2->leave($__internal_2c142b3c1bc96a646de6020dc7a109c3b97cf43d8d70c3f9e7f5ab4d73ef1dd2_prof);

        
        $__internal_21b7b629800d08feb8ea5da175cc8957ecd5249f0ec5a75c6fb7e823ee4ea619->leave($__internal_21b7b629800d08feb8ea5da175cc8957ecd5249f0ec5a75c6fb7e823ee4ea619_prof);

    }

    // line 7
    public function block_main($context, array $blocks = array())
    {
        $__internal_2e8ffb94a51dd4e7889b6b4161355915a8eb555ba25646a5d7481f81cadad8b5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2e8ffb94a51dd4e7889b6b4161355915a8eb555ba25646a5d7481f81cadad8b5->enter($__internal_2e8ffb94a51dd4e7889b6b4161355915a8eb555ba25646a5d7481f81cadad8b5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        $__internal_810fe60cd2a8cc1ee5b5da613b34ff52217d04206fea5af6490497f9925ca467 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_810fe60cd2a8cc1ee5b5da613b34ff52217d04206fea5af6490497f9925ca467->enter($__internal_810fe60cd2a8cc1ee5b5da613b34ff52217d04206fea5af6490497f9925ca467_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 8
        echo "<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 19
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "lundiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 33
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 35
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mardiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 47
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 50
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "mercrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 64
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 65
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 66
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 67
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "jeudiDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediEntree", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 79
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediPlat", array()), "html", null, true);
        echo "</div>
                            <div class=\"text\">";
        // line 80
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediAccompagnement", array()), "html", null, true);
        echo "</div>
                            <div class=\" text\">";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["menuScolaire"]) ? $context["menuScolaire"] : $this->getContext($context, "menuScolaire")), "vendrediDessert", array()), "html", null, true);
        echo "</div>
                        </div>
                    </div>
                </div>


            </div>



";
        
        $__internal_810fe60cd2a8cc1ee5b5da613b34ff52217d04206fea5af6490497f9925ca467->leave($__internal_810fe60cd2a8cc1ee5b5da613b34ff52217d04206fea5af6490497f9925ca467_prof);

        
        $__internal_2e8ffb94a51dd4e7889b6b4161355915a8eb555ba25646a5d7481f81cadad8b5->leave($__internal_2e8ffb94a51dd4e7889b6b4161355915a8eb555ba25646a5d7481f81cadad8b5_prof);

    }

    // line 93
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_f480c9ed9b3d9b8b2778b01a4743074a21fd83113782afaff68ace7b3d82f397 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f480c9ed9b3d9b8b2778b01a4743074a21fd83113782afaff68ace7b3d82f397->enter($__internal_f480c9ed9b3d9b8b2778b01a4743074a21fd83113782afaff68ace7b3d82f397_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        $__internal_2dab2c95d046076914f4439529c98a1d077e668f3d8fe25c5a68da2502db6fd8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2dab2c95d046076914f4439529c98a1d077e668f3d8fe25c5a68da2502db6fd8->enter($__internal_2dab2c95d046076914f4439529c98a1d077e668f3d8fe25c5a68da2502db6fd8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 94
        echo "<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"";
        // line 98
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("externe");
        echo "\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"";
        // line 103
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("scolaire");
        echo "\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"";
        // line 108
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("resident");
        echo "\" >
        Résident
    </a>
</div>



";
        
        $__internal_2dab2c95d046076914f4439529c98a1d077e668f3d8fe25c5a68da2502db6fd8->leave($__internal_2dab2c95d046076914f4439529c98a1d077e668f3d8fe25c5a68da2502db6fd8_prof);

        
        $__internal_f480c9ed9b3d9b8b2778b01a4743074a21fd83113782afaff68ace7b3d82f397->leave($__internal_f480c9ed9b3d9b8b2778b01a4743074a21fd83113782afaff68ace7b3d82f397_prof);

    }

    public function getTemplateName()
    {
        return ":menu:scolaire.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  253 => 108,  245 => 103,  237 => 98,  231 => 94,  222 => 93,  201 => 81,  197 => 80,  193 => 79,  189 => 78,  175 => 67,  171 => 66,  167 => 65,  163 => 64,  146 => 50,  142 => 49,  138 => 48,  134 => 47,  120 => 36,  116 => 35,  112 => 34,  108 => 33,  94 => 22,  90 => 21,  86 => 20,  82 => 19,  69 => 8,  60 => 7,  42 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}



{% block body_id 'menu_scolaire' %}

{% block main %}
<h1> Menu Scolaire</h1>
<p class=\"subtitle\"> Menu du midi uniquement</p>

            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Lundi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.lundiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.lundiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.lundiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Mardi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mardiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mardiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mardiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Mercredi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.mercrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.mercrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.mercrediDessert }}</div>
                        </div>
                    </div>
                </div>

            </div>
            <div class=\"row ligne\">

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\"col-xs-12\">
                        <div class=\"titre\">Jeudi </div>
                    </div>
                    <div class=\"col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.jeudiEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.jeudiAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.jeudiDessert }}</div>
                        </div>
                    </div>
                </div>

                <div class=\"col-md-4 col-xs-12\">
                    <div class=\" col-xs-12\">
                        <div class=\"titre\">Vendredi </div>
                    </div>
                    <div class=\"col- col-xs-12\">
                        <div class=\"menu\">
                            <div class=\" text\">{{ menuScolaire.vendrediEntree }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediPlat }}</div>
                            <div class=\"text\">{{ menuScolaire.vendrediAccompagnement }}</div>
                            <div class=\" text\">{{ menuScolaire.vendrediDessert }}</div>
                        </div>
                    </div>
                </div>


            </div>



{% endblock %}

{% block sidebar %}
<p class=\"titre\">
    Voir le menu :
</p>
<div>
    <a href=\"{{ path('externe') }}\" >
        Externe
    </a>
</div>
<div>
    <a  href=\"{{ path('scolaire') }}\">
        Scolaire
    </a>
</div>
<div>
    <a href=\"{{ path('resident') }}\" >
        Résident
    </a>
</div>



{% endblock %}
", ":menu:scolaire.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/menu/scolaire.html.twig");
    }
}
