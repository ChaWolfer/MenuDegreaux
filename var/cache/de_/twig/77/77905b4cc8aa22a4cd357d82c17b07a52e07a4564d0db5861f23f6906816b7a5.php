<?php

/* SonataAdminBundle:CRUD:list__action.html.twig */
class __TwigTemplate_088131af092e891b49d2a178f009f052c1812b614c3c836178586251dce692b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list__action.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a406fa3ece01abac5ba5a307922d91d99f245f2ef6956d3481114eb799136ff9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a406fa3ece01abac5ba5a307922d91d99f245f2ef6956d3481114eb799136ff9->enter($__internal_a406fa3ece01abac5ba5a307922d91d99f245f2ef6956d3481114eb799136ff9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__action.html.twig"));

        $__internal_641071818b74b8431ea80c32ae9038fd8cea6be17f9bc1aa6ad5d95cdc37367e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_641071818b74b8431ea80c32ae9038fd8cea6be17f9bc1aa6ad5d95cdc37367e->enter($__internal_641071818b74b8431ea80c32ae9038fd8cea6be17f9bc1aa6ad5d95cdc37367e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__action.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a406fa3ece01abac5ba5a307922d91d99f245f2ef6956d3481114eb799136ff9->leave($__internal_a406fa3ece01abac5ba5a307922d91d99f245f2ef6956d3481114eb799136ff9_prof);

        
        $__internal_641071818b74b8431ea80c32ae9038fd8cea6be17f9bc1aa6ad5d95cdc37367e->leave($__internal_641071818b74b8431ea80c32ae9038fd8cea6be17f9bc1aa6ad5d95cdc37367e_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_f132b4281289e2b2a27014a7673fc785a101bb101b63f8b153c5f1fbe63726db = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f132b4281289e2b2a27014a7673fc785a101bb101b63f8b153c5f1fbe63726db->enter($__internal_f132b4281289e2b2a27014a7673fc785a101bb101b63f8b153c5f1fbe63726db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_529545c6d5d14a815fb7f6d376b5a4e2a864592199189f0bb4987392f02f7739 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_529545c6d5d14a815fb7f6d376b5a4e2a864592199189f0bb4987392f02f7739->enter($__internal_529545c6d5d14a815fb7f6d376b5a4e2a864592199189f0bb4987392f02f7739_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <div class=\"btn-group\">
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "actions", array()));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["actions"]) {
            // line 17
            echo "            ";
            $this->loadTemplate($this->getAttribute($context["actions"], "template", array()), "SonataAdminBundle:CRUD:list__action.html.twig", 17)->display($context);
            // line 18
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['actions'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 19
        echo "    </div>
";
        
        $__internal_529545c6d5d14a815fb7f6d376b5a4e2a864592199189f0bb4987392f02f7739->leave($__internal_529545c6d5d14a815fb7f6d376b5a4e2a864592199189f0bb4987392f02f7739_prof);

        
        $__internal_f132b4281289e2b2a27014a7673fc785a101bb101b63f8b153c5f1fbe63726db->leave($__internal_f132b4281289e2b2a27014a7673fc785a101bb101b63f8b153c5f1fbe63726db_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list__action.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  85 => 19,  71 => 18,  68 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <div class=\"btn-group\">
        {% for actions in field_description.options.actions %}
            {% include actions.template %}
        {% endfor %}
    </div>
{% endblock %}
", "SonataAdminBundle:CRUD:list__action.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list__action.html.twig");
    }
}
