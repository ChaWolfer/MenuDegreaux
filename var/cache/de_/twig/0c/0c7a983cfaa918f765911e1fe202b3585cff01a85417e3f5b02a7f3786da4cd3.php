<?php

/* @Framework/Form/choice_widget_expanded.html.php */
class __TwigTemplate_b8764b107cabcf46024d9fcbcae18fecb897035688f914ed4ec443181345ef68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8fa0cd8e244edb82a7faee13f4ce7e9dc5d57d872972c30c6d445cfba89510e3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8fa0cd8e244edb82a7faee13f4ce7e9dc5d57d872972c30c6d445cfba89510e3->enter($__internal_8fa0cd8e244edb82a7faee13f4ce7e9dc5d57d872972c30c6d445cfba89510e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        $__internal_22b73db09e0ed84d7b2a31e5f2e82210707a01ff573d3b42dcfc5b96884503aa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_22b73db09e0ed84d7b2a31e5f2e82210707a01ff573d3b42dcfc5b96884503aa->enter($__internal_22b73db09e0ed84d7b2a31e5f2e82210707a01ff573d3b42dcfc5b96884503aa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget_expanded.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
";
        
        $__internal_8fa0cd8e244edb82a7faee13f4ce7e9dc5d57d872972c30c6d445cfba89510e3->leave($__internal_8fa0cd8e244edb82a7faee13f4ce7e9dc5d57d872972c30c6d445cfba89510e3_prof);

        
        $__internal_22b73db09e0ed84d7b2a31e5f2e82210707a01ff573d3b42dcfc5b96884503aa->leave($__internal_22b73db09e0ed84d7b2a31e5f2e82210707a01ff573d3b42dcfc5b96884503aa_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget_expanded.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
<?php foreach (\$form as \$child): ?>
    <?php echo \$view['form']->widget(\$child) ?>
    <?php echo \$view['form']->label(\$child, null, array('translation_domain' => \$choice_translation_domain)) ?>
<?php endforeach ?>
</div>
", "@Framework/Form/choice_widget_expanded.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_widget_expanded.html.php");
    }
}
