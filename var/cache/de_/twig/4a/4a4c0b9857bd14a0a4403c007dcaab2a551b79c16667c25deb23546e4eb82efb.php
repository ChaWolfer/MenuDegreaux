<?php

/* SonataAdminBundle:CRUD/Association:list_many_to_many.html.twig */
class __TwigTemplate_3277d8befa9487e0b40870def741f92dd63c1273f8eaaf52ac6507a0cf5456ea extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
            'relation_link' => array($this, 'block_relation_link'),
            'relation_value' => array($this, 'block_relation_value'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD/Association:list_many_to_many.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_58d2901b07635aa3b253de57555ccb72a266c8a81df5903d9f54a70c13f30670 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_58d2901b07635aa3b253de57555ccb72a266c8a81df5903d9f54a70c13f30670->enter($__internal_58d2901b07635aa3b253de57555ccb72a266c8a81df5903d9f54a70c13f30670_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:list_many_to_many.html.twig"));

        $__internal_7851b65456913ca170780b59f96fa20857787cd23da12c51bd1fd5dc437b9c4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7851b65456913ca170780b59f96fa20857787cd23da12c51bd1fd5dc437b9c4b->enter($__internal_7851b65456913ca170780b59f96fa20857787cd23da12c51bd1fd5dc437b9c4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD/Association:list_many_to_many.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_58d2901b07635aa3b253de57555ccb72a266c8a81df5903d9f54a70c13f30670->leave($__internal_58d2901b07635aa3b253de57555ccb72a266c8a81df5903d9f54a70c13f30670_prof);

        
        $__internal_7851b65456913ca170780b59f96fa20857787cd23da12c51bd1fd5dc437b9c4b->leave($__internal_7851b65456913ca170780b59f96fa20857787cd23da12c51bd1fd5dc437b9c4b_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_e5efd2c8532af30913b83ebacefb2cafb9a70f407b7a812d45b953cb078da350 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e5efd2c8532af30913b83ebacefb2cafb9a70f407b7a812d45b953cb078da350->enter($__internal_e5efd2c8532af30913b83ebacefb2cafb9a70f407b7a812d45b953cb078da350_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_25e98bfe0676eade56cf5867d1aefd9bb926d792f2a8c413acebb8b65660f701 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25e98bfe0676eade56cf5867d1aefd9bb926d792f2a8c413acebb8b65660f701->enter($__internal_25e98bfe0676eade56cf5867d1aefd9bb926d792f2a8c413acebb8b65660f701_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        $context["route_name"] = $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "route", array()), "name", array());
        // line 16
        echo "    ";
        if (($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "hasassociationadmin", array()) && $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "hasRoute", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name"))), "method"))) {
            // line 17
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 18
                if ($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "hasAccess", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name")), 1 => $context["element"]), "method")) {
                    // line 19
                    $this->displayBlock("relation_link", $context, $blocks);
                } else {
                    // line 21
                    $this->displayBlock("relation_value", $context, $blocks);
                }
                // line 23
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 24
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 25
            echo "    ";
        } else {
            // line 26
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 27
                echo "            ";
                $this->displayBlock("relation_value", $context, $blocks);
                echo "
            ";
                // line 28
                if ( !$this->getAttribute($context["loop"], "last", array())) {
                    echo ", ";
                }
                // line 29
                echo "        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 30
            echo "    ";
        }
        
        $__internal_25e98bfe0676eade56cf5867d1aefd9bb926d792f2a8c413acebb8b65660f701->leave($__internal_25e98bfe0676eade56cf5867d1aefd9bb926d792f2a8c413acebb8b65660f701_prof);

        
        $__internal_e5efd2c8532af30913b83ebacefb2cafb9a70f407b7a812d45b953cb078da350->leave($__internal_e5efd2c8532af30913b83ebacefb2cafb9a70f407b7a812d45b953cb078da350_prof);

    }

    // line 33
    public function block_relation_link($context, array $blocks = array())
    {
        $__internal_04ad687e64d716480e6c4318dcd471635442748a8b8ce30b3e90ca875a874ae5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_04ad687e64d716480e6c4318dcd471635442748a8b8ce30b3e90ca875a874ae5->enter($__internal_04ad687e64d716480e6c4318dcd471635442748a8b8ce30b3e90ca875a874ae5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_link"));

        $__internal_d4e60b2aa5d8826981d69fbd401760c7279b08409654b066aabc4b65543489a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4e60b2aa5d8826981d69fbd401760c7279b08409654b066aabc4b65543489a8->enter($__internal_d4e60b2aa5d8826981d69fbd401760c7279b08409654b066aabc4b65543489a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_link"));

        // line 34
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "associationadmin", array()), "generateObjectUrl", array(0 => (isset($context["route_name"]) ? $context["route_name"] : $this->getContext($context, "route_name")), 1 => (isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), 2 => $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "route", array()), "parameters", array())), "method"), "html", null, true);
        echo "\">";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), (isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description"))), "html", null, true);
        // line 36
        echo "</a>";
        
        $__internal_d4e60b2aa5d8826981d69fbd401760c7279b08409654b066aabc4b65543489a8->leave($__internal_d4e60b2aa5d8826981d69fbd401760c7279b08409654b066aabc4b65543489a8_prof);

        
        $__internal_04ad687e64d716480e6c4318dcd471635442748a8b8ce30b3e90ca875a874ae5->leave($__internal_04ad687e64d716480e6c4318dcd471635442748a8b8ce30b3e90ca875a874ae5_prof);

    }

    // line 39
    public function block_relation_value($context, array $blocks = array())
    {
        $__internal_73a4b44a083ba7db5d4e3fa4fa809b30f95f98e2e03f8b850feb0ad533e02b80 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73a4b44a083ba7db5d4e3fa4fa809b30f95f98e2e03f8b850feb0ad533e02b80->enter($__internal_73a4b44a083ba7db5d4e3fa4fa809b30f95f98e2e03f8b850feb0ad533e02b80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_value"));

        $__internal_181806fa6e270d804c43126943f1c015ed362d180b88d7322273b3be789dbe7b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_181806fa6e270d804c43126943f1c015ed362d180b88d7322273b3be789dbe7b->enter($__internal_181806fa6e270d804c43126943f1c015ed362d180b88d7322273b3be789dbe7b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "relation_value"));

        // line 40
        echo twig_escape_filter($this->env, $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderRelationElement((isset($context["element"]) ? $context["element"] : $this->getContext($context, "element")), (isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description"))), "html", null, true);
        
        $__internal_181806fa6e270d804c43126943f1c015ed362d180b88d7322273b3be789dbe7b->leave($__internal_181806fa6e270d804c43126943f1c015ed362d180b88d7322273b3be789dbe7b_prof);

        
        $__internal_73a4b44a083ba7db5d4e3fa4fa809b30f95f98e2e03f8b850feb0ad533e02b80->leave($__internal_73a4b44a083ba7db5d4e3fa4fa809b30f95f98e2e03f8b850feb0ad533e02b80_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD/Association:list_many_to_many.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 40,  180 => 39,  170 => 36,  168 => 35,  164 => 34,  155 => 33,  144 => 30,  130 => 29,  126 => 28,  121 => 27,  103 => 26,  100 => 25,  86 => 24,  82 => 23,  79 => 21,  76 => 19,  74 => 18,  56 => 17,  53 => 16,  50 => 15,  41 => 14,  20 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% set route_name = field_description.options.route.name %}
    {% if field_description.hasassociationadmin and field_description.associationadmin.hasRoute(route_name) %}
        {% for element in value %}
            {%- if field_description.associationadmin.hasAccess(route_name, element) -%}
                {{ block('relation_link') }}
            {%- else -%}
                {{ block('relation_value') }}
            {%- endif -%}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% else %}
        {% for element in value%}
            {{ block('relation_value') }}
            {% if not loop.last %}, {% endif %}
        {% endfor %}
    {% endif %}
{% endblock %}

{%- block relation_link -%}
    <a href=\"{{ field_description.associationadmin.generateObjectUrl(route_name, element, field_description.options.route.parameters) }}\">
        {{- element|render_relation_element(field_description) -}}
    </a>
{%- endblock -%}

{%- block relation_value -%}
    {{- element|render_relation_element(field_description) -}}
{%- endblock -%}
", "SonataAdminBundle:CRUD/Association:list_many_to_many.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/Association/list_many_to_many.html.twig");
    }
}
