<?php

/* FOSUserBundle:Resetting:email.txt.twig */
class __TwigTemplate_c9378338e9453e7f9bc260e00bb54a92aca4828fa3b20e2c4796090703019db0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'subject' => array($this, 'block_subject'),
            'body_text' => array($this, 'block_body_text'),
            'body_html' => array($this, 'block_body_html'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e76a73f60bb4c8f65ea627d2a9172580d0638a8d730e3e2df7db7b0a9fae3a99 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e76a73f60bb4c8f65ea627d2a9172580d0638a8d730e3e2df7db7b0a9fae3a99->enter($__internal_e76a73f60bb4c8f65ea627d2a9172580d0638a8d730e3e2df7db7b0a9fae3a99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        $__internal_a1b0b30ce3e3b4fb2699fca5c953d2c72b20995ae36449ce8f774c2e9c731380 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a1b0b30ce3e3b4fb2699fca5c953d2c72b20995ae36449ce8f774c2e9c731380->enter($__internal_a1b0b30ce3e3b4fb2699fca5c953d2c72b20995ae36449ce8f774c2e9c731380_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:email.txt.twig"));

        // line 2
        $this->displayBlock('subject', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('body_text', $context, $blocks);
        // line 13
        $this->displayBlock('body_html', $context, $blocks);
        
        $__internal_e76a73f60bb4c8f65ea627d2a9172580d0638a8d730e3e2df7db7b0a9fae3a99->leave($__internal_e76a73f60bb4c8f65ea627d2a9172580d0638a8d730e3e2df7db7b0a9fae3a99_prof);

        
        $__internal_a1b0b30ce3e3b4fb2699fca5c953d2c72b20995ae36449ce8f774c2e9c731380->leave($__internal_a1b0b30ce3e3b4fb2699fca5c953d2c72b20995ae36449ce8f774c2e9c731380_prof);

    }

    // line 2
    public function block_subject($context, array $blocks = array())
    {
        $__internal_7cf8bc7f9f8c5c6376c1f67a8800513c4beab87a0f4d14165cdbc44246ddca90 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7cf8bc7f9f8c5c6376c1f67a8800513c4beab87a0f4d14165cdbc44246ddca90->enter($__internal_7cf8bc7f9f8c5c6376c1f67a8800513c4beab87a0f4d14165cdbc44246ddca90_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        $__internal_6450e8afd7f82dbb013d4fbc93451ab34131abfc7f59484b65dbf2202357d2a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6450e8afd7f82dbb013d4fbc93451ab34131abfc7f59484b65dbf2202357d2a4->enter($__internal_6450e8afd7f82dbb013d4fbc93451ab34131abfc7f59484b65dbf2202357d2a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "subject"));

        // line 4
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.subject", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array())), "FOSUserBundle");
        
        $__internal_6450e8afd7f82dbb013d4fbc93451ab34131abfc7f59484b65dbf2202357d2a4->leave($__internal_6450e8afd7f82dbb013d4fbc93451ab34131abfc7f59484b65dbf2202357d2a4_prof);

        
        $__internal_7cf8bc7f9f8c5c6376c1f67a8800513c4beab87a0f4d14165cdbc44246ddca90->leave($__internal_7cf8bc7f9f8c5c6376c1f67a8800513c4beab87a0f4d14165cdbc44246ddca90_prof);

    }

    // line 8
    public function block_body_text($context, array $blocks = array())
    {
        $__internal_ac310a0a1b3b3d63f43270f163e0e5d48c62b17cc85e58fb50d6e6c7f8c28748 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac310a0a1b3b3d63f43270f163e0e5d48c62b17cc85e58fb50d6e6c7f8c28748->enter($__internal_ac310a0a1b3b3d63f43270f163e0e5d48c62b17cc85e58fb50d6e6c7f8c28748_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        $__internal_a44f0527b0afa3294d7fbf4ac390227e0635e199b1d02e824170cc972b2c4d64 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a44f0527b0afa3294d7fbf4ac390227e0635e199b1d02e824170cc972b2c4d64->enter($__internal_a44f0527b0afa3294d7fbf4ac390227e0635e199b1d02e824170cc972b2c4d64_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_text"));

        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.email.message", array("%username%" => $this->getAttribute((isset($context["user"]) ? $context["user"] : $this->getContext($context, "user")), "username", array()), "%confirmationUrl%" => (isset($context["confirmationUrl"]) ? $context["confirmationUrl"] : $this->getContext($context, "confirmationUrl"))), "FOSUserBundle");
        echo "
";
        
        $__internal_a44f0527b0afa3294d7fbf4ac390227e0635e199b1d02e824170cc972b2c4d64->leave($__internal_a44f0527b0afa3294d7fbf4ac390227e0635e199b1d02e824170cc972b2c4d64_prof);

        
        $__internal_ac310a0a1b3b3d63f43270f163e0e5d48c62b17cc85e58fb50d6e6c7f8c28748->leave($__internal_ac310a0a1b3b3d63f43270f163e0e5d48c62b17cc85e58fb50d6e6c7f8c28748_prof);

    }

    // line 13
    public function block_body_html($context, array $blocks = array())
    {
        $__internal_ef4d42ac56e25d9b12b5776eb3850a89c806036ecd6457991bd1e05d674b8f37 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ef4d42ac56e25d9b12b5776eb3850a89c806036ecd6457991bd1e05d674b8f37->enter($__internal_ef4d42ac56e25d9b12b5776eb3850a89c806036ecd6457991bd1e05d674b8f37_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        $__internal_3f6e6260f279520ef6ed5334735daecea91e86ceef17d37265f88a240d8c2946 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f6e6260f279520ef6ed5334735daecea91e86ceef17d37265f88a240d8c2946->enter($__internal_3f6e6260f279520ef6ed5334735daecea91e86ceef17d37265f88a240d8c2946_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_html"));

        
        $__internal_3f6e6260f279520ef6ed5334735daecea91e86ceef17d37265f88a240d8c2946->leave($__internal_3f6e6260f279520ef6ed5334735daecea91e86ceef17d37265f88a240d8c2946_prof);

        
        $__internal_ef4d42ac56e25d9b12b5776eb3850a89c806036ecd6457991bd1e05d674b8f37->leave($__internal_ef4d42ac56e25d9b12b5776eb3850a89c806036ecd6457991bd1e05d674b8f37_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:email.txt.twig";
    }

    public function getDebugInfo()
    {
        return array (  85 => 13,  73 => 10,  64 => 8,  54 => 4,  45 => 2,  35 => 13,  33 => 8,  30 => 7,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% trans_default_domain 'FOSUserBundle' %}
{% block subject %}
{%- autoescape false -%}
{{ 'resetting.email.subject'|trans({'%username%': user.username}) }}
{%- endautoescape -%}
{% endblock %}

{% block body_text %}
{% autoescape false %}
{{ 'resetting.email.message'|trans({'%username%': user.username, '%confirmationUrl%': confirmationUrl}) }}
{% endautoescape %}
{% endblock %}
{% block body_html %}{% endblock %}
", "FOSUserBundle:Resetting:email.txt.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/email.txt.twig");
    }
}
