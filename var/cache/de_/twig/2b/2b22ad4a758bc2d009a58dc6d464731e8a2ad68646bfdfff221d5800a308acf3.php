<?php

/* SonataAdminBundle::standard_layout.html.twig */
class __TwigTemplate_897224c25afebb8f0849530d646a1e355daecf8a36f3b30586338d8207958e71 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'html_attributes' => array($this, 'block_html_attributes'),
            'meta_tags' => array($this, 'block_meta_tags'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
            'sonata_javascript_config' => array($this, 'block_sonata_javascript_config'),
            'sonata_javascript_pool' => array($this, 'block_sonata_javascript_pool'),
            'sonata_head_title' => array($this, 'block_sonata_head_title'),
            'body_attributes' => array($this, 'block_body_attributes'),
            'sonata_header' => array($this, 'block_sonata_header'),
            'sonata_header_noscript_warning' => array($this, 'block_sonata_header_noscript_warning'),
            'logo' => array($this, 'block_logo'),
            'sonata_nav' => array($this, 'block_sonata_nav'),
            'sonata_breadcrumb' => array($this, 'block_sonata_breadcrumb'),
            'sonata_top_nav_menu' => array($this, 'block_sonata_top_nav_menu'),
            'sonata_top_nav_menu_add_block' => array($this, 'block_sonata_top_nav_menu_add_block'),
            'sonata_top_nav_menu_user_block' => array($this, 'block_sonata_top_nav_menu_user_block'),
            'sonata_wrapper' => array($this, 'block_sonata_wrapper'),
            'sonata_left_side' => array($this, 'block_sonata_left_side'),
            'sonata_side_nav' => array($this, 'block_sonata_side_nav'),
            'sonata_sidebar_search' => array($this, 'block_sonata_sidebar_search'),
            'side_bar_before_nav' => array($this, 'block_side_bar_before_nav'),
            'side_bar_nav' => array($this, 'block_side_bar_nav'),
            'side_bar_after_nav' => array($this, 'block_side_bar_after_nav'),
            'side_bar_after_nav_content' => array($this, 'block_side_bar_after_nav_content'),
            'sonata_page_content' => array($this, 'block_sonata_page_content'),
            'sonata_page_content_header' => array($this, 'block_sonata_page_content_header'),
            'sonata_page_content_nav' => array($this, 'block_sonata_page_content_nav'),
            'tab_menu_navbar_header' => array($this, 'block_tab_menu_navbar_header'),
            'sonata_admin_content_actions_wrappers' => array($this, 'block_sonata_admin_content_actions_wrappers'),
            'sonata_admin_content' => array($this, 'block_sonata_admin_content'),
            'notice' => array($this, 'block_notice'),
            'bootlint' => array($this, 'block_bootlint'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d2ca9f05ea0dba500f536f021de989ca359b0f66eea78e28fbb23fbf74cfb446 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d2ca9f05ea0dba500f536f021de989ca359b0f66eea78e28fbb23fbf74cfb446->enter($__internal_d2ca9f05ea0dba500f536f021de989ca359b0f66eea78e28fbb23fbf74cfb446_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::standard_layout.html.twig"));

        $__internal_df5740a65a6cfa585095da1b5d24a6c70432c1eabff01b83c83c0114ec2b70f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_df5740a65a6cfa585095da1b5d24a6c70432c1eabff01b83c83c0114ec2b70f0->enter($__internal_df5740a65a6cfa585095da1b5d24a6c70432c1eabff01b83c83c0114ec2b70f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle::standard_layout.html.twig"));

        // line 11
        echo "
";
        // line 12
        $context["_preview"] = ((        $this->hasBlock("preview", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("preview", $context, $blocks))) : (null));
        // line 13
        $context["_form"] = ((        $this->hasBlock("form", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("form", $context, $blocks))) : (null));
        // line 14
        $context["_show"] = ((        $this->hasBlock("show", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("show", $context, $blocks))) : (null));
        // line 15
        $context["_list_table"] = ((        $this->hasBlock("list_table", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_table", $context, $blocks))) : (null));
        // line 16
        $context["_list_filters"] = ((        $this->hasBlock("list_filters", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters", $context, $blocks))) : (null));
        // line 17
        $context["_tab_menu"] = ((        $this->hasBlock("tab_menu", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("tab_menu", $context, $blocks))) : (null));
        // line 18
        $context["_content"] = ((        $this->hasBlock("content", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("content", $context, $blocks))) : (null));
        // line 19
        $context["_title"] = ((        $this->hasBlock("title", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("title", $context, $blocks))) : (null));
        // line 20
        $context["_breadcrumb"] = ((        $this->hasBlock("breadcrumb", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("breadcrumb", $context, $blocks))) : (null));
        // line 21
        $context["_actions"] = ((        $this->hasBlock("actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("actions", $context, $blocks))) : (null));
        // line 22
        $context["_navbar_title"] = ((        $this->hasBlock("navbar_title", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("navbar_title", $context, $blocks))) : (null));
        // line 23
        $context["_list_filters_actions"] = ((        $this->hasBlock("list_filters_actions", $context, $blocks)) ? (twig_trim_filter(        $this->renderBlock("list_filters_actions", $context, $blocks))) : (null));
        // line 24
        echo "
<!DOCTYPE html>
<html ";
        // line 26
        $this->displayBlock('html_attributes', $context, $blocks);
        echo ">
    <head>
        ";
        // line 28
        $this->displayBlock('meta_tags', $context, $blocks);
        // line 33
        echo "
        ";
        // line 34
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 39
        echo "
        ";
        // line 40
        $this->displayBlock('javascripts', $context, $blocks);
        // line 88
        echo "
        <title>
        ";
        // line 90
        $this->displayBlock('sonata_head_title', $context, $blocks);
        // line 116
        echo "        </title>
    </head>
    <body ";
        // line 118
        $this->displayBlock('body_attributes', $context, $blocks);
        echo ">

    <div class=\"wrapper\">

        ";
        // line 122
        $this->displayBlock('sonata_header', $context, $blocks);
        // line 221
        echo "
        ";
        // line 222
        $this->displayBlock('sonata_wrapper', $context, $blocks);
        // line 355
        echo "    </div>

    ";
        // line 357
        if ($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "use_bootlint"), "method")) {
            // line 358
            echo "        ";
            $this->displayBlock('bootlint', $context, $blocks);
            // line 364
            echo "    ";
        }
        // line 365
        echo "
    </body>
</html>
";
        
        $__internal_d2ca9f05ea0dba500f536f021de989ca359b0f66eea78e28fbb23fbf74cfb446->leave($__internal_d2ca9f05ea0dba500f536f021de989ca359b0f66eea78e28fbb23fbf74cfb446_prof);

        
        $__internal_df5740a65a6cfa585095da1b5d24a6c70432c1eabff01b83c83c0114ec2b70f0->leave($__internal_df5740a65a6cfa585095da1b5d24a6c70432c1eabff01b83c83c0114ec2b70f0_prof);

    }

    // line 26
    public function block_html_attributes($context, array $blocks = array())
    {
        $__internal_73ab1f1628fc9a4cf9c8dc16f85201e23f39aace792f159ebd4f55e7652c7418 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_73ab1f1628fc9a4cf9c8dc16f85201e23f39aace792f159ebd4f55e7652c7418->enter($__internal_73ab1f1628fc9a4cf9c8dc16f85201e23f39aace792f159ebd4f55e7652c7418_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "html_attributes"));

        $__internal_c65072c7049820f713cb0af4764a140ab328a1f4aa2dda0e88a5d1bca25efa23 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c65072c7049820f713cb0af4764a140ab328a1f4aa2dda0e88a5d1bca25efa23->enter($__internal_c65072c7049820f713cb0af4764a140ab328a1f4aa2dda0e88a5d1bca25efa23_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "html_attributes"));

        echo "class=\"no-js\"";
        
        $__internal_c65072c7049820f713cb0af4764a140ab328a1f4aa2dda0e88a5d1bca25efa23->leave($__internal_c65072c7049820f713cb0af4764a140ab328a1f4aa2dda0e88a5d1bca25efa23_prof);

        
        $__internal_73ab1f1628fc9a4cf9c8dc16f85201e23f39aace792f159ebd4f55e7652c7418->leave($__internal_73ab1f1628fc9a4cf9c8dc16f85201e23f39aace792f159ebd4f55e7652c7418_prof);

    }

    // line 28
    public function block_meta_tags($context, array $blocks = array())
    {
        $__internal_541ef0cd5c3c1d66c80a91b255dfb6002654044f78c43dc4eef2f5d379447a08 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_541ef0cd5c3c1d66c80a91b255dfb6002654044f78c43dc4eef2f5d379447a08->enter($__internal_541ef0cd5c3c1d66c80a91b255dfb6002654044f78c43dc4eef2f5d379447a08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta_tags"));

        $__internal_fbf44910d2c95904a9f3ba9bff1893e6ecaaf2a52ee9a54cd299c2d5ebe9d386 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fbf44910d2c95904a9f3ba9bff1893e6ecaaf2a52ee9a54cd299c2d5ebe9d386->enter($__internal_fbf44910d2c95904a9f3ba9bff1893e6ecaaf2a52ee9a54cd299c2d5ebe9d386_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "meta_tags"));

        // line 29
        echo "            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta charset=\"UTF-8\">
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        ";
        
        $__internal_fbf44910d2c95904a9f3ba9bff1893e6ecaaf2a52ee9a54cd299c2d5ebe9d386->leave($__internal_fbf44910d2c95904a9f3ba9bff1893e6ecaaf2a52ee9a54cd299c2d5ebe9d386_prof);

        
        $__internal_541ef0cd5c3c1d66c80a91b255dfb6002654044f78c43dc4eef2f5d379447a08->leave($__internal_541ef0cd5c3c1d66c80a91b255dfb6002654044f78c43dc4eef2f5d379447a08_prof);

    }

    // line 34
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_64ee7aae080f2df4cfea499f2a62897c9d7543cc07d3319973ce6a43318fa10c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_64ee7aae080f2df4cfea499f2a62897c9d7543cc07d3319973ce6a43318fa10c->enter($__internal_64ee7aae080f2df4cfea499f2a62897c9d7543cc07d3319973ce6a43318fa10c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        $__internal_5803a001508ea2703ab6fa883507d7b66bd8919ee828aabbd1c045495da1a7c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5803a001508ea2703ab6fa883507d7b66bd8919ee828aabbd1c045495da1a7c3->enter($__internal_5803a001508ea2703ab6fa883507d7b66bd8919ee828aabbd1c045495da1a7c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 35
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "stylesheets", 1 => array()), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["stylesheet"]) {
            // line 36
            echo "                <link rel=\"stylesheet\" href=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["stylesheet"]), "html", null, true);
            echo "\">
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['stylesheet'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "        ";
        
        $__internal_5803a001508ea2703ab6fa883507d7b66bd8919ee828aabbd1c045495da1a7c3->leave($__internal_5803a001508ea2703ab6fa883507d7b66bd8919ee828aabbd1c045495da1a7c3_prof);

        
        $__internal_64ee7aae080f2df4cfea499f2a62897c9d7543cc07d3319973ce6a43318fa10c->leave($__internal_64ee7aae080f2df4cfea499f2a62897c9d7543cc07d3319973ce6a43318fa10c_prof);

    }

    // line 40
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e53376ba14da838083a3ec74c9938ce619c9d251c98f124ccbd12d1743e4ea1a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e53376ba14da838083a3ec74c9938ce619c9d251c98f124ccbd12d1743e4ea1a->enter($__internal_e53376ba14da838083a3ec74c9938ce619c9d251c98f124ccbd12d1743e4ea1a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        $__internal_fcece3ef7f7c8256c2c594a9b1025f3698e8eb1eccbfa365070cbc77916b2994 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fcece3ef7f7c8256c2c594a9b1025f3698e8eb1eccbfa365070cbc77916b2994->enter($__internal_fcece3ef7f7c8256c2c594a9b1025f3698e8eb1eccbfa365070cbc77916b2994_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 41
        echo "            ";
        $this->displayBlock('sonata_javascript_config', $context, $blocks);
        // line 61
        echo "
            ";
        // line 62
        $this->displayBlock('sonata_javascript_pool', $context, $blocks);
        // line 67
        echo "
            ";
        // line 68
        $context["locale"] = $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "locale", array());
        // line 69
        echo "            ";
        // line 70
        echo "            ";
        if ((twig_slice($this->env, (isset($context["locale"]) ? $context["locale"] : $this->getContext($context, "locale")), 0, 2) != "en")) {
            // line 71
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("bundles/sonatacore/vendor/moment/locale/" . twig_replace_filter(twig_lower_filter($this->env,             // line 73
(isset($context["locale"]) ? $context["locale"] : $this->getContext($context, "locale"))), array("_" => "-"))) . ".js")), "html", null, true);
            // line 75
            echo "\"></script>
            ";
        }
        // line 77
        echo "
            ";
        // line 79
        echo "            ";
        if ($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "use_select2"), "method")) {
            // line 80
            echo "                ";
            if (((isset($context["locale"]) ? $context["locale"] : $this->getContext($context, "locale")) == "pt")) {
                $context["locale"] = "pt_PT";
            }
            // line 81
            echo "
                ";
            // line 83
            echo "                ";
            if ((twig_slice($this->env, (isset($context["locale"]) ? $context["locale"] : $this->getContext($context, "locale")), 0, 2) != "en")) {
                // line 84
                echo "                    <script src=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl((("bundles/sonatacore/vendor/select2/select2_locale_" . twig_replace_filter((isset($context["locale"]) ? $context["locale"] : $this->getContext($context, "locale")), array("_" => "-"))) . ".js")), "html", null, true);
                echo "\"></script>
                ";
            }
            // line 86
            echo "            ";
        }
        // line 87
        echo "        ";
        
        $__internal_fcece3ef7f7c8256c2c594a9b1025f3698e8eb1eccbfa365070cbc77916b2994->leave($__internal_fcece3ef7f7c8256c2c594a9b1025f3698e8eb1eccbfa365070cbc77916b2994_prof);

        
        $__internal_e53376ba14da838083a3ec74c9938ce619c9d251c98f124ccbd12d1743e4ea1a->leave($__internal_e53376ba14da838083a3ec74c9938ce619c9d251c98f124ccbd12d1743e4ea1a_prof);

    }

    // line 41
    public function block_sonata_javascript_config($context, array $blocks = array())
    {
        $__internal_ce336b3a0aef38fc1ac98a42f0a4f39cc8a18a56b7539d1567566f914821ede1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ce336b3a0aef38fc1ac98a42f0a4f39cc8a18a56b7539d1567566f914821ede1->enter($__internal_ce336b3a0aef38fc1ac98a42f0a4f39cc8a18a56b7539d1567566f914821ede1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_javascript_config"));

        $__internal_435d1f0f00a744e4c65466fd0efd07109267c9f1e54c2ea9ecf2692a21c2ffb8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_435d1f0f00a744e4c65466fd0efd07109267c9f1e54c2ea9ecf2692a21c2ffb8->enter($__internal_435d1f0f00a744e4c65466fd0efd07109267c9f1e54c2ea9ecf2692a21c2ffb8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_javascript_config"));

        // line 42
        echo "                <script>
                    window.SONATA_CONFIG = {
                        CONFIRM_EXIT: ";
        // line 44
        if ($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "confirm_exit"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USE_SELECT2: ";
        // line 45
        if ($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "use_select2"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USE_ICHECK: ";
        // line 46
        if ($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "use_icheck"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        echo ",
                        USE_STICKYFORMS: ";
        // line 47
        if ($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "use_stickyforms"), "method")) {
            echo "true";
        } else {
            echo "false";
        }
        // line 48
        echo "                    };
                    window.SONATA_TRANSLATIONS = {
                        CONFIRM_EXIT: '";
        // line 50
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("confirm_exit", array(), "SonataAdminBundle"), "js"), "html", null, true);
        echo "'
                    };

                    // http://getbootstrap.com/getting-started/#support-ie10-width
                    if (navigator.userAgent.match(/IEMobile\\/10\\.0/)) {
                        var msViewportStyle = document.createElement('style');
                        msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
                        document.querySelector('head').appendChild(msViewportStyle);
                    }
                </script>
            ";
        
        $__internal_435d1f0f00a744e4c65466fd0efd07109267c9f1e54c2ea9ecf2692a21c2ffb8->leave($__internal_435d1f0f00a744e4c65466fd0efd07109267c9f1e54c2ea9ecf2692a21c2ffb8_prof);

        
        $__internal_ce336b3a0aef38fc1ac98a42f0a4f39cc8a18a56b7539d1567566f914821ede1->leave($__internal_ce336b3a0aef38fc1ac98a42f0a4f39cc8a18a56b7539d1567566f914821ede1_prof);

    }

    // line 62
    public function block_sonata_javascript_pool($context, array $blocks = array())
    {
        $__internal_262bf5bb40cca9533fd7caf13dfcfc7367ca74710b319394bc24f2ac2d2a77e6 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_262bf5bb40cca9533fd7caf13dfcfc7367ca74710b319394bc24f2ac2d2a77e6->enter($__internal_262bf5bb40cca9533fd7caf13dfcfc7367ca74710b319394bc24f2ac2d2a77e6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_javascript_pool"));

        $__internal_32d09823c320b82f3e9c6676dab2b6d1615a5a35ebaac4843d56956c0f833e70 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_32d09823c320b82f3e9c6676dab2b6d1615a5a35ebaac4843d56956c0f833e70->enter($__internal_32d09823c320b82f3e9c6676dab2b6d1615a5a35ebaac4843d56956c0f833e70_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_javascript_pool"));

        // line 63
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "javascripts", 1 => array()), "method"));
        foreach ($context['_seq'] as $context["_key"] => $context["javascript"]) {
            // line 64
            echo "                    <script src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($context["javascript"]), "html", null, true);
            echo "\"></script>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['javascript'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 66
        echo "            ";
        
        $__internal_32d09823c320b82f3e9c6676dab2b6d1615a5a35ebaac4843d56956c0f833e70->leave($__internal_32d09823c320b82f3e9c6676dab2b6d1615a5a35ebaac4843d56956c0f833e70_prof);

        
        $__internal_262bf5bb40cca9533fd7caf13dfcfc7367ca74710b319394bc24f2ac2d2a77e6->leave($__internal_262bf5bb40cca9533fd7caf13dfcfc7367ca74710b319394bc24f2ac2d2a77e6_prof);

    }

    // line 90
    public function block_sonata_head_title($context, array $blocks = array())
    {
        $__internal_b66ddfb600a8a76858982b5067950947a6154ea772c889faf19b686f3f6b3461 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b66ddfb600a8a76858982b5067950947a6154ea772c889faf19b686f3f6b3461->enter($__internal_b66ddfb600a8a76858982b5067950947a6154ea772c889faf19b686f3f6b3461_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_head_title"));

        $__internal_9c6d5c0dfd46ee4034c350c33f4cc560826532ed0883de2c1a19e2911cd934b7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9c6d5c0dfd46ee4034c350c33f4cc560826532ed0883de2c1a19e2911cd934b7->enter($__internal_9c6d5c0dfd46ee4034c350c33f4cc560826532ed0883de2c1a19e2911cd934b7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_head_title"));

        // line 91
        echo "            ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Admin", array(), "SonataAdminBundle"), "html", null, true);
        echo "

            ";
        // line 93
        if ( !twig_test_empty((isset($context["_title"]) ? $context["_title"] : $this->getContext($context, "_title")))) {
            // line 94
            echo "                ";
            echo strip_tags((isset($context["_title"]) ? $context["_title"] : $this->getContext($context, "_title")));
            echo "
            ";
        } else {
            // line 96
            echo "                ";
            if (array_key_exists("action", $context)) {
                // line 97
                echo "                    -
                    ";
                // line 98
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["breadcrumbs_builder"]) ? $context["breadcrumbs_builder"] : $this->getContext($context, "breadcrumbs_builder")), "breadcrumbs", array(0 => (isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), 1 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action"))), "method"));
                $context['loop'] = array(
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                );
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                    // line 99
                    echo "                        ";
                    if ( !$this->getAttribute($context["loop"], "first", array())) {
                        // line 100
                        echo "                            ";
                        if (($this->getAttribute($context["loop"], "index", array()) != 2)) {
                            // line 101
                            echo "                                &gt;
                            ";
                        }
                        // line 104
                        $context["translation_domain"] = $this->getAttribute($context["menu"], "extra", array(0 => "translation_domain", 1 => "messages"), "method");
                        // line 105
                        $context["label"] = $this->getAttribute($context["menu"], "label", array());
                        // line 106
                        if ( !((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) {
                            // line 107
                            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), $this->getAttribute($context["menu"], "extra", array(0 => "translation_params", 1 => array()), "method"), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
                        }
                        // line 110
                        echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
                        echo "
                        ";
                    }
                    // line 112
                    echo "                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 113
                echo "                ";
            }
            // line 114
            echo "            ";
        }
        // line 115
        echo "        ";
        
        $__internal_9c6d5c0dfd46ee4034c350c33f4cc560826532ed0883de2c1a19e2911cd934b7->leave($__internal_9c6d5c0dfd46ee4034c350c33f4cc560826532ed0883de2c1a19e2911cd934b7_prof);

        
        $__internal_b66ddfb600a8a76858982b5067950947a6154ea772c889faf19b686f3f6b3461->leave($__internal_b66ddfb600a8a76858982b5067950947a6154ea772c889faf19b686f3f6b3461_prof);

    }

    // line 118
    public function block_body_attributes($context, array $blocks = array())
    {
        $__internal_8f245a34b571646d4155627e88779403dadb511a823f7207ebcd2b1a4bf9466c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8f245a34b571646d4155627e88779403dadb511a823f7207ebcd2b1a4bf9466c->enter($__internal_8f245a34b571646d4155627e88779403dadb511a823f7207ebcd2b1a4bf9466c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_attributes"));

        $__internal_7a89b16e874e058d53fbed84f02ef5f2378473ae6fe84c99f0c03611a2ac0d65 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7a89b16e874e058d53fbed84f02ef5f2378473ae6fe84c99f0c03611a2ac0d65->enter($__internal_7a89b16e874e058d53fbed84f02ef5f2378473ae6fe84c99f0c03611a2ac0d65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_attributes"));

        echo "class=\"sonata-bc skin-black fixed\"";
        
        $__internal_7a89b16e874e058d53fbed84f02ef5f2378473ae6fe84c99f0c03611a2ac0d65->leave($__internal_7a89b16e874e058d53fbed84f02ef5f2378473ae6fe84c99f0c03611a2ac0d65_prof);

        
        $__internal_8f245a34b571646d4155627e88779403dadb511a823f7207ebcd2b1a4bf9466c->leave($__internal_8f245a34b571646d4155627e88779403dadb511a823f7207ebcd2b1a4bf9466c_prof);

    }

    // line 122
    public function block_sonata_header($context, array $blocks = array())
    {
        $__internal_8203d8a76dfc86848b51ed2ad41195e2e163d33466788d871d9b8f16a4725e86 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8203d8a76dfc86848b51ed2ad41195e2e163d33466788d871d9b8f16a4725e86->enter($__internal_8203d8a76dfc86848b51ed2ad41195e2e163d33466788d871d9b8f16a4725e86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header"));

        $__internal_e07369c812c4c054b0ba5c9ff9e28c68a3ae31a0253aefa711ce188b8a9e241d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e07369c812c4c054b0ba5c9ff9e28c68a3ae31a0253aefa711ce188b8a9e241d->enter($__internal_e07369c812c4c054b0ba5c9ff9e28c68a3ae31a0253aefa711ce188b8a9e241d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header"));

        // line 123
        echo "            <header class=\"main-header\">
                ";
        // line 124
        $this->displayBlock('sonata_header_noscript_warning', $context, $blocks);
        // line 131
        echo "                ";
        $this->displayBlock('logo', $context, $blocks);
        // line 143
        echo "                ";
        $this->displayBlock('sonata_nav', $context, $blocks);
        // line 219
        echo "            </header>
        ";
        
        $__internal_e07369c812c4c054b0ba5c9ff9e28c68a3ae31a0253aefa711ce188b8a9e241d->leave($__internal_e07369c812c4c054b0ba5c9ff9e28c68a3ae31a0253aefa711ce188b8a9e241d_prof);

        
        $__internal_8203d8a76dfc86848b51ed2ad41195e2e163d33466788d871d9b8f16a4725e86->leave($__internal_8203d8a76dfc86848b51ed2ad41195e2e163d33466788d871d9b8f16a4725e86_prof);

    }

    // line 124
    public function block_sonata_header_noscript_warning($context, array $blocks = array())
    {
        $__internal_5ebd8ac68473edd8f8fc6450bfa61e73029fd6f923b940e5329edd8e0f3c686d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5ebd8ac68473edd8f8fc6450bfa61e73029fd6f923b940e5329edd8e0f3c686d->enter($__internal_5ebd8ac68473edd8f8fc6450bfa61e73029fd6f923b940e5329edd8e0f3c686d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header_noscript_warning"));

        $__internal_327ae8a1304fb05815aa98acb5a3ac30227b0c7873421c1220edc848a3b29dce = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_327ae8a1304fb05815aa98acb5a3ac30227b0c7873421c1220edc848a3b29dce->enter($__internal_327ae8a1304fb05815aa98acb5a3ac30227b0c7873421c1220edc848a3b29dce_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_header_noscript_warning"));

        // line 125
        echo "                    <noscript>
                        <div class=\"noscript-warning\">
                            ";
        // line 127
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("noscript_warning", array(), "SonataAdminBundle"), "html", null, true);
        echo "
                        </div>
                    </noscript>
                ";
        
        $__internal_327ae8a1304fb05815aa98acb5a3ac30227b0c7873421c1220edc848a3b29dce->leave($__internal_327ae8a1304fb05815aa98acb5a3ac30227b0c7873421c1220edc848a3b29dce_prof);

        
        $__internal_5ebd8ac68473edd8f8fc6450bfa61e73029fd6f923b940e5329edd8e0f3c686d->leave($__internal_5ebd8ac68473edd8f8fc6450bfa61e73029fd6f923b940e5329edd8e0f3c686d_prof);

    }

    // line 131
    public function block_logo($context, array $blocks = array())
    {
        $__internal_133a899221a747860dfd05c1aae674e938c465cf49ce133e167798689408b32d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_133a899221a747860dfd05c1aae674e938c465cf49ce133e167798689408b32d->enter($__internal_133a899221a747860dfd05c1aae674e938c465cf49ce133e167798689408b32d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "logo"));

        $__internal_362555bfef3059002548daf3b995d53a52a04c95bc82803a6dc7e7b5a054a485 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_362555bfef3059002548daf3b995d53a52a04c95bc82803a6dc7e7b5a054a485->enter($__internal_362555bfef3059002548daf3b995d53a52a04c95bc82803a6dc7e7b5a054a485_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "logo"));

        // line 132
        echo "                    ";
        ob_start();
        // line 133
        echo "                        <a class=\"logo\" href=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonata_admin_dashboard");
        echo "\">
                            ";
        // line 134
        if ((("single_image" == $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "title_mode"), "method")) || ("both" == $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "title_mode"), "method")))) {
            // line 135
            echo "                                <img src=\"";
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "titlelogo", array())), "html", null, true);
            echo "\" alt=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "title", array()), "html", null, true);
            echo "\">
                            ";
        }
        // line 137
        echo "                            ";
        if ((("single_text" == $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "title_mode"), "method")) || ("both" == $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getOption", array(0 => "title_mode"), "method")))) {
            // line 138
            echo "                                <span>";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "title", array()), "html", null, true);
            echo "</span>
                            ";
        }
        // line 140
        echo "                        </a>
                    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        // line 142
        echo "                ";
        
        $__internal_362555bfef3059002548daf3b995d53a52a04c95bc82803a6dc7e7b5a054a485->leave($__internal_362555bfef3059002548daf3b995d53a52a04c95bc82803a6dc7e7b5a054a485_prof);

        
        $__internal_133a899221a747860dfd05c1aae674e938c465cf49ce133e167798689408b32d->leave($__internal_133a899221a747860dfd05c1aae674e938c465cf49ce133e167798689408b32d_prof);

    }

    // line 143
    public function block_sonata_nav($context, array $blocks = array())
    {
        $__internal_4a7c7c4450d34719d01ba05118d5682f44fd64d27eb626201f1b304680443b1e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4a7c7c4450d34719d01ba05118d5682f44fd64d27eb626201f1b304680443b1e->enter($__internal_4a7c7c4450d34719d01ba05118d5682f44fd64d27eb626201f1b304680443b1e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_nav"));

        $__internal_05f7b94a6a442ca98d04ae5451ecf51a90989f5df34413970c847291adc05f24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_05f7b94a6a442ca98d04ae5451ecf51a90989f5df34413970c847291adc05f24->enter($__internal_05f7b94a6a442ca98d04ae5451ecf51a90989f5df34413970c847291adc05f24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_nav"));

        // line 144
        echo "                    <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                            <span class=\"sr-only\">Toggle navigation</span>
                        </a>

                        <div class=\"navbar-left\">
                            ";
        // line 150
        $this->displayBlock('sonata_breadcrumb', $context, $blocks);
        // line 189
        echo "                        </div>

                        ";
        // line 191
        $this->displayBlock('sonata_top_nav_menu', $context, $blocks);
        // line 217
        echo "                    </nav>
                ";
        
        $__internal_05f7b94a6a442ca98d04ae5451ecf51a90989f5df34413970c847291adc05f24->leave($__internal_05f7b94a6a442ca98d04ae5451ecf51a90989f5df34413970c847291adc05f24_prof);

        
        $__internal_4a7c7c4450d34719d01ba05118d5682f44fd64d27eb626201f1b304680443b1e->leave($__internal_4a7c7c4450d34719d01ba05118d5682f44fd64d27eb626201f1b304680443b1e_prof);

    }

    // line 150
    public function block_sonata_breadcrumb($context, array $blocks = array())
    {
        $__internal_1c24462e9fb91441b4e39687dee15979a1c37d5248048f634a2c75648c20af2c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1c24462e9fb91441b4e39687dee15979a1c37d5248048f634a2c75648c20af2c->enter($__internal_1c24462e9fb91441b4e39687dee15979a1c37d5248048f634a2c75648c20af2c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_breadcrumb"));

        $__internal_835d47e17f194cc5dcf79473231cc2c65e78603d9de70a6db30b7a878ff930a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_835d47e17f194cc5dcf79473231cc2c65e78603d9de70a6db30b7a878ff930a0->enter($__internal_835d47e17f194cc5dcf79473231cc2c65e78603d9de70a6db30b7a878ff930a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_breadcrumb"));

        // line 151
        echo "                                <div class=\"hidden-xs\">
                                    ";
        // line 152
        if (( !twig_test_empty((isset($context["_breadcrumb"]) ? $context["_breadcrumb"] : $this->getContext($context, "_breadcrumb"))) || array_key_exists("action", $context))) {
            // line 153
            echo "                                        <ol class=\"nav navbar-top-links breadcrumb\">
                                            ";
            // line 154
            if (twig_test_empty((isset($context["_breadcrumb"]) ? $context["_breadcrumb"] : $this->getContext($context, "_breadcrumb")))) {
                // line 155
                echo "                                                ";
                if (array_key_exists("action", $context)) {
                    // line 156
                    echo "                                                    ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["breadcrumbs_builder"]) ? $context["breadcrumbs_builder"] : $this->getContext($context, "breadcrumbs_builder")), "breadcrumbs", array(0 => (isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), 1 => (isset($context["action"]) ? $context["action"] : $this->getContext($context, "action"))), "method"));
                    $context['loop'] = array(
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    );
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["menu"]) {
                        // line 157
                        $context["translation_domain"] = $this->getAttribute($context["menu"], "extra", array(0 => "translation_domain", 1 => "messages"), "method");
                        // line 158
                        $context["label"] = $this->getAttribute($context["menu"], "label", array());
                        // line 159
                        if ( !((isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")) === false)) {
                            // line 160
                            $context["label"] = $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans((isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), $this->getAttribute($context["menu"], "extra", array(0 => "translation_params", 1 => array()), "method"), (isset($context["translation_domain"]) ? $context["translation_domain"] : $this->getContext($context, "translation_domain")));
                        }
                        // line 163
                        if ( !$this->getAttribute($context["loop"], "last", array())) {
                            // line 164
                            echo "                                                            <li>
                                                                ";
                            // line 165
                            if ( !twig_test_empty($this->getAttribute($context["menu"], "uri", array()))) {
                                // line 166
                                echo "                                                                    <a href=\"";
                                echo twig_escape_filter($this->env, $this->getAttribute($context["menu"], "uri", array()), "html", null, true);
                                echo "\">
                                                                        ";
                                // line 167
                                if ($this->getAttribute($context["menu"], "extra", array(0 => "safe_label", 1 => true), "method")) {
                                    // line 168
                                    echo (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label"));
                                } else {
                                    // line 170
                                    echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
                                }
                                // line 172
                                echo "                                                                    </a>
                                                                ";
                            } else {
                                // line 174
                                echo "                                                                    <span>";
                                echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
                                echo "</span>
                                                                ";
                            }
                            // line 176
                            echo "                                                            </li>
                                                        ";
                        } else {
                            // line 178
                            echo "                                                            <li class=\"active\"><span>";
                            echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : $this->getContext($context, "label")), "html", null, true);
                            echo "</span></li>
                                                        ";
                        }
                        // line 180
                        echo "                                                    ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['menu'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 181
                    echo "                                                ";
                }
                // line 182
                echo "                                            ";
            } else {
                // line 183
                echo "                                                ";
                echo (isset($context["_breadcrumb"]) ? $context["_breadcrumb"] : $this->getContext($context, "_breadcrumb"));
                echo "
                                            ";
            }
            // line 185
            echo "                                        </ol>
                                    ";
        }
        // line 187
        echo "                                </div>
                            ";
        
        $__internal_835d47e17f194cc5dcf79473231cc2c65e78603d9de70a6db30b7a878ff930a0->leave($__internal_835d47e17f194cc5dcf79473231cc2c65e78603d9de70a6db30b7a878ff930a0_prof);

        
        $__internal_1c24462e9fb91441b4e39687dee15979a1c37d5248048f634a2c75648c20af2c->leave($__internal_1c24462e9fb91441b4e39687dee15979a1c37d5248048f634a2c75648c20af2c_prof);

    }

    // line 191
    public function block_sonata_top_nav_menu($context, array $blocks = array())
    {
        $__internal_4345dbdfb1eb3e8dd5e4aa73b892046809f5b1a70eeef99a444ba3f33c82ccc1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4345dbdfb1eb3e8dd5e4aa73b892046809f5b1a70eeef99a444ba3f33c82ccc1->enter($__internal_4345dbdfb1eb3e8dd5e4aa73b892046809f5b1a70eeef99a444ba3f33c82ccc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_top_nav_menu"));

        $__internal_a3e117329cf081b7d179528a68992b6c45546d042cd360e9b27484774dc1ca9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3e117329cf081b7d179528a68992b6c45546d042cd360e9b27484774dc1ca9b->enter($__internal_a3e117329cf081b7d179528a68992b6c45546d042cd360e9b27484774dc1ca9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_top_nav_menu"));

        // line 192
        echo "                            ";
        if (($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "user", array()) && $this->env->getExtension('Symfony\Bridge\Twig\Extension\SecurityExtension')->isGranted("ROLE_SONATA_ADMIN"))) {
            // line 193
            echo "                                <div class=\"navbar-custom-menu\">
                                    <ul class=\"nav navbar-nav\">
                                        ";
            // line 195
            $this->displayBlock('sonata_top_nav_menu_add_block', $context, $blocks);
            // line 203
            echo "                                        ";
            $this->displayBlock('sonata_top_nav_menu_user_block', $context, $blocks);
            // line 213
            echo "                                    </ul>
                                </div>
                            ";
        }
        // line 216
        echo "                        ";
        
        $__internal_a3e117329cf081b7d179528a68992b6c45546d042cd360e9b27484774dc1ca9b->leave($__internal_a3e117329cf081b7d179528a68992b6c45546d042cd360e9b27484774dc1ca9b_prof);

        
        $__internal_4345dbdfb1eb3e8dd5e4aa73b892046809f5b1a70eeef99a444ba3f33c82ccc1->leave($__internal_4345dbdfb1eb3e8dd5e4aa73b892046809f5b1a70eeef99a444ba3f33c82ccc1_prof);

    }

    // line 195
    public function block_sonata_top_nav_menu_add_block($context, array $blocks = array())
    {
        $__internal_9b2fec3bbbb2bb3b23696bffa09698862a1f2a30f9829386c55acd6ed54b1f0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9b2fec3bbbb2bb3b23696bffa09698862a1f2a30f9829386c55acd6ed54b1f0c->enter($__internal_9b2fec3bbbb2bb3b23696bffa09698862a1f2a30f9829386c55acd6ed54b1f0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_top_nav_menu_add_block"));

        $__internal_6cec39702df39f4027e552fa9326eacc664b38b96642760b0543cd54c9cf2612 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6cec39702df39f4027e552fa9326eacc664b38b96642760b0543cd54c9cf2612->enter($__internal_6cec39702df39f4027e552fa9326eacc664b38b96642760b0543cd54c9cf2612_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_top_nav_menu_add_block"));

        // line 196
        echo "                                            <li class=\"dropdown\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-plus-square fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                ";
        // line 200
        $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "add_block"), "method"), "SonataAdminBundle::standard_layout.html.twig", 200)->display($context);
        // line 201
        echo "                                            </li>
                                        ";
        
        $__internal_6cec39702df39f4027e552fa9326eacc664b38b96642760b0543cd54c9cf2612->leave($__internal_6cec39702df39f4027e552fa9326eacc664b38b96642760b0543cd54c9cf2612_prof);

        
        $__internal_9b2fec3bbbb2bb3b23696bffa09698862a1f2a30f9829386c55acd6ed54b1f0c->leave($__internal_9b2fec3bbbb2bb3b23696bffa09698862a1f2a30f9829386c55acd6ed54b1f0c_prof);

    }

    // line 203
    public function block_sonata_top_nav_menu_user_block($context, array $blocks = array())
    {
        $__internal_8e820b06926361680d85e29d86ce4efa2eec7e279da650220437d863153da8f5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8e820b06926361680d85e29d86ce4efa2eec7e279da650220437d863153da8f5->enter($__internal_8e820b06926361680d85e29d86ce4efa2eec7e279da650220437d863153da8f5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_top_nav_menu_user_block"));

        $__internal_bb0ba848e1416bee87ce2e610b5c2bcd9bf97c452b8f6a6faadde251d4ea24f1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb0ba848e1416bee87ce2e610b5c2bcd9bf97c452b8f6a6faadde251d4ea24f1->enter($__internal_bb0ba848e1416bee87ce2e610b5c2bcd9bf97c452b8f6a6faadde251d4ea24f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_top_nav_menu_user_block"));

        // line 204
        echo "                                            <li class=\"dropdown user-menu\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-user fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                <ul class=\"dropdown-menu dropdown-user\">
                                                    ";
        // line 209
        $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "user_block"), "method"), "SonataAdminBundle::standard_layout.html.twig", 209)->display($context);
        // line 210
        echo "                                                </ul>
                                            </li>
                                        ";
        
        $__internal_bb0ba848e1416bee87ce2e610b5c2bcd9bf97c452b8f6a6faadde251d4ea24f1->leave($__internal_bb0ba848e1416bee87ce2e610b5c2bcd9bf97c452b8f6a6faadde251d4ea24f1_prof);

        
        $__internal_8e820b06926361680d85e29d86ce4efa2eec7e279da650220437d863153da8f5->leave($__internal_8e820b06926361680d85e29d86ce4efa2eec7e279da650220437d863153da8f5_prof);

    }

    // line 222
    public function block_sonata_wrapper($context, array $blocks = array())
    {
        $__internal_eadb367d5d0a1ffb287e69b597018799aadf5fdf99853e0e501eba8800b99848 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_eadb367d5d0a1ffb287e69b597018799aadf5fdf99853e0e501eba8800b99848->enter($__internal_eadb367d5d0a1ffb287e69b597018799aadf5fdf99853e0e501eba8800b99848_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_wrapper"));

        $__internal_dee56eb439a303da1cd2cd625d5cfdd9045a991ffc173a7ce05253b893121dd9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dee56eb439a303da1cd2cd625d5cfdd9045a991ffc173a7ce05253b893121dd9->enter($__internal_dee56eb439a303da1cd2cd625d5cfdd9045a991ffc173a7ce05253b893121dd9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_wrapper"));

        // line 223
        echo "            ";
        $this->displayBlock('sonata_left_side', $context, $blocks);
        // line 255
        echo "
            <div class=\"content-wrapper\">
                ";
        // line 257
        $this->displayBlock('sonata_page_content', $context, $blocks);
        // line 353
        echo "            </div>
        ";
        
        $__internal_dee56eb439a303da1cd2cd625d5cfdd9045a991ffc173a7ce05253b893121dd9->leave($__internal_dee56eb439a303da1cd2cd625d5cfdd9045a991ffc173a7ce05253b893121dd9_prof);

        
        $__internal_eadb367d5d0a1ffb287e69b597018799aadf5fdf99853e0e501eba8800b99848->leave($__internal_eadb367d5d0a1ffb287e69b597018799aadf5fdf99853e0e501eba8800b99848_prof);

    }

    // line 223
    public function block_sonata_left_side($context, array $blocks = array())
    {
        $__internal_08c2929a32a9395d249a6daad7fedb915ee85dc0ae14dbb6fe978d31642c8854 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_08c2929a32a9395d249a6daad7fedb915ee85dc0ae14dbb6fe978d31642c8854->enter($__internal_08c2929a32a9395d249a6daad7fedb915ee85dc0ae14dbb6fe978d31642c8854_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_left_side"));

        $__internal_fa3f82eb9fe5337572c51567a0c62443de2e569c1b2fd14d2d661df8928b22c6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fa3f82eb9fe5337572c51567a0c62443de2e569c1b2fd14d2d661df8928b22c6->enter($__internal_fa3f82eb9fe5337572c51567a0c62443de2e569c1b2fd14d2d661df8928b22c6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_left_side"));

        // line 224
        echo "                <aside class=\"main-sidebar\">
                    <section class=\"sidebar\">
                        ";
        // line 226
        $this->displayBlock('sonata_side_nav', $context, $blocks);
        // line 252
        echo "                    </section>
                </aside>
            ";
        
        $__internal_fa3f82eb9fe5337572c51567a0c62443de2e569c1b2fd14d2d661df8928b22c6->leave($__internal_fa3f82eb9fe5337572c51567a0c62443de2e569c1b2fd14d2d661df8928b22c6_prof);

        
        $__internal_08c2929a32a9395d249a6daad7fedb915ee85dc0ae14dbb6fe978d31642c8854->leave($__internal_08c2929a32a9395d249a6daad7fedb915ee85dc0ae14dbb6fe978d31642c8854_prof);

    }

    // line 226
    public function block_sonata_side_nav($context, array $blocks = array())
    {
        $__internal_a1d635f88e7264924da6901bbbc470ac88745659985daf46b43b243ff209470f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a1d635f88e7264924da6901bbbc470ac88745659985daf46b43b243ff209470f->enter($__internal_a1d635f88e7264924da6901bbbc470ac88745659985daf46b43b243ff209470f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_side_nav"));

        $__internal_3f7fcf1ebf55d95ae4bdb7b2322de9ffceabdf29123f262210fc341003c0a9e3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3f7fcf1ebf55d95ae4bdb7b2322de9ffceabdf29123f262210fc341003c0a9e3->enter($__internal_3f7fcf1ebf55d95ae4bdb7b2322de9ffceabdf29123f262210fc341003c0a9e3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_side_nav"));

        // line 227
        echo "                            ";
        $this->displayBlock('sonata_sidebar_search', $context, $blocks);
        // line 239
        echo "
                            ";
        // line 240
        $this->displayBlock('side_bar_before_nav', $context, $blocks);
        // line 241
        echo "                            ";
        $this->displayBlock('side_bar_nav', $context, $blocks);
        // line 244
        echo "                            ";
        $this->displayBlock('side_bar_after_nav', $context, $blocks);
        // line 251
        echo "                        ";
        
        $__internal_3f7fcf1ebf55d95ae4bdb7b2322de9ffceabdf29123f262210fc341003c0a9e3->leave($__internal_3f7fcf1ebf55d95ae4bdb7b2322de9ffceabdf29123f262210fc341003c0a9e3_prof);

        
        $__internal_a1d635f88e7264924da6901bbbc470ac88745659985daf46b43b243ff209470f->leave($__internal_a1d635f88e7264924da6901bbbc470ac88745659985daf46b43b243ff209470f_prof);

    }

    // line 227
    public function block_sonata_sidebar_search($context, array $blocks = array())
    {
        $__internal_2c9ea0b9549450eb42c870ed61184f7a1bf683f90725f0f0cf5bfb4521ec6728 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2c9ea0b9549450eb42c870ed61184f7a1bf683f90725f0f0cf5bfb4521ec6728->enter($__internal_2c9ea0b9549450eb42c870ed61184f7a1bf683f90725f0f0cf5bfb4521ec6728_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_sidebar_search"));

        $__internal_bcc8d6dc478ef47fe4fd904ed5f71684329fbd58d6159401342e4e8ba0df2e98 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcc8d6dc478ef47fe4fd904ed5f71684329fbd58d6159401342e4e8ba0df2e98->enter($__internal_bcc8d6dc478ef47fe4fd904ed5f71684329fbd58d6159401342e4e8ba0df2e98_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_sidebar_search"));

        // line 228
        echo "                                <form action=\"";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("sonata_admin_search");
        echo "\" method=\"GET\" class=\"sidebar-form\" role=\"search\">
                                    <div class=\"input-group custom-search-form\">
                                        <input type=\"text\" name=\"q\" value=\"";
        // line 230
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "get", array(0 => "q"), "method"), "html", null, true);
        echo "\" class=\"form-control\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("search_placeholder", array(), "SonataAdminBundle"), "html", null, true);
        echo "\">
                                        <span class=\"input-group-btn\">
                                            <button class=\"btn btn-flat\" type=\"submit\">
                                                <i class=\"fa fa-search\" aria-hidden=\"true\"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            ";
        
        $__internal_bcc8d6dc478ef47fe4fd904ed5f71684329fbd58d6159401342e4e8ba0df2e98->leave($__internal_bcc8d6dc478ef47fe4fd904ed5f71684329fbd58d6159401342e4e8ba0df2e98_prof);

        
        $__internal_2c9ea0b9549450eb42c870ed61184f7a1bf683f90725f0f0cf5bfb4521ec6728->leave($__internal_2c9ea0b9549450eb42c870ed61184f7a1bf683f90725f0f0cf5bfb4521ec6728_prof);

    }

    // line 240
    public function block_side_bar_before_nav($context, array $blocks = array())
    {
        $__internal_2043e0521ba89acf9dfb22ff3d1148c096c319234269b8a96cc0a9c2abd3e456 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2043e0521ba89acf9dfb22ff3d1148c096c319234269b8a96cc0a9c2abd3e456->enter($__internal_2043e0521ba89acf9dfb22ff3d1148c096c319234269b8a96cc0a9c2abd3e456_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_before_nav"));

        $__internal_56d9a2008d60037f98d7ac6923840b62445d00be7abaf09d3f3724694d017444 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56d9a2008d60037f98d7ac6923840b62445d00be7abaf09d3f3724694d017444->enter($__internal_56d9a2008d60037f98d7ac6923840b62445d00be7abaf09d3f3724694d017444_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_before_nav"));

        echo " ";
        
        $__internal_56d9a2008d60037f98d7ac6923840b62445d00be7abaf09d3f3724694d017444->leave($__internal_56d9a2008d60037f98d7ac6923840b62445d00be7abaf09d3f3724694d017444_prof);

        
        $__internal_2043e0521ba89acf9dfb22ff3d1148c096c319234269b8a96cc0a9c2abd3e456->leave($__internal_2043e0521ba89acf9dfb22ff3d1148c096c319234269b8a96cc0a9c2abd3e456_prof);

    }

    // line 241
    public function block_side_bar_nav($context, array $blocks = array())
    {
        $__internal_d1a74171815f56635571eb47fb29525c937dabce300dbd4762f955cec383ede1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d1a74171815f56635571eb47fb29525c937dabce300dbd4762f955cec383ede1->enter($__internal_d1a74171815f56635571eb47fb29525c937dabce300dbd4762f955cec383ede1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_nav"));

        $__internal_8a7054ae3f81acfdd72538efbd6e9100ef2ef4787aaaadb3eecffb6c3f017f78 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8a7054ae3f81acfdd72538efbd6e9100ef2ef4787aaaadb3eecffb6c3f017f78->enter($__internal_8a7054ae3f81acfdd72538efbd6e9100ef2ef4787aaaadb3eecffb6c3f017f78_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_nav"));

        // line 242
        echo "                                ";
        echo $this->env->getExtension('Knp\Menu\Twig\MenuExtension')->render("sonata_admin_sidebar", array("template" => $this->getAttribute($this->getAttribute((isset($context["sonata_admin"]) ? $context["sonata_admin"] : $this->getContext($context, "sonata_admin")), "adminPool", array()), "getTemplate", array(0 => "knp_menu_template"), "method")));
        echo "
                            ";
        
        $__internal_8a7054ae3f81acfdd72538efbd6e9100ef2ef4787aaaadb3eecffb6c3f017f78->leave($__internal_8a7054ae3f81acfdd72538efbd6e9100ef2ef4787aaaadb3eecffb6c3f017f78_prof);

        
        $__internal_d1a74171815f56635571eb47fb29525c937dabce300dbd4762f955cec383ede1->leave($__internal_d1a74171815f56635571eb47fb29525c937dabce300dbd4762f955cec383ede1_prof);

    }

    // line 244
    public function block_side_bar_after_nav($context, array $blocks = array())
    {
        $__internal_f9fa34e4b513472e988618e5a479669a1d2fcc5f7674b00a3096f7ff34a3d959 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f9fa34e4b513472e988618e5a479669a1d2fcc5f7674b00a3096f7ff34a3d959->enter($__internal_f9fa34e4b513472e988618e5a479669a1d2fcc5f7674b00a3096f7ff34a3d959_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_after_nav"));

        $__internal_63c7040b5baab09fd13c94ffce92a0d54ec502b344fe0048691049f769feeff8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_63c7040b5baab09fd13c94ffce92a0d54ec502b344fe0048691049f769feeff8->enter($__internal_63c7040b5baab09fd13c94ffce92a0d54ec502b344fe0048691049f769feeff8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_after_nav"));

        // line 245
        echo "                                <p class=\"text-center small\" style=\"border-top: 1px solid #444444; padding-top: 10px\">
                                    ";
        // line 246
        $this->displayBlock('side_bar_after_nav_content', $context, $blocks);
        // line 249
        echo "                                </p>
                            ";
        
        $__internal_63c7040b5baab09fd13c94ffce92a0d54ec502b344fe0048691049f769feeff8->leave($__internal_63c7040b5baab09fd13c94ffce92a0d54ec502b344fe0048691049f769feeff8_prof);

        
        $__internal_f9fa34e4b513472e988618e5a479669a1d2fcc5f7674b00a3096f7ff34a3d959->leave($__internal_f9fa34e4b513472e988618e5a479669a1d2fcc5f7674b00a3096f7ff34a3d959_prof);

    }

    // line 246
    public function block_side_bar_after_nav_content($context, array $blocks = array())
    {
        $__internal_657403d66d84060754f35c5846eaf3ebe82f26d6086d01fa01a7084832e5bc97 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_657403d66d84060754f35c5846eaf3ebe82f26d6086d01fa01a7084832e5bc97->enter($__internal_657403d66d84060754f35c5846eaf3ebe82f26d6086d01fa01a7084832e5bc97_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_after_nav_content"));

        $__internal_d4a7bc0c8eb19501e0de675b48716e5cbe1148792a8bcd0e030acc72151005fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d4a7bc0c8eb19501e0de675b48716e5cbe1148792a8bcd0e030acc72151005fd->enter($__internal_d4a7bc0c8eb19501e0de675b48716e5cbe1148792a8bcd0e030acc72151005fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "side_bar_after_nav_content"));

        // line 247
        echo "                                        <a href=\"https://sonata-project.org\" rel=\"noreferrer\" target=\"_blank\">sonata project</a>
                                    ";
        
        $__internal_d4a7bc0c8eb19501e0de675b48716e5cbe1148792a8bcd0e030acc72151005fd->leave($__internal_d4a7bc0c8eb19501e0de675b48716e5cbe1148792a8bcd0e030acc72151005fd_prof);

        
        $__internal_657403d66d84060754f35c5846eaf3ebe82f26d6086d01fa01a7084832e5bc97->leave($__internal_657403d66d84060754f35c5846eaf3ebe82f26d6086d01fa01a7084832e5bc97_prof);

    }

    // line 257
    public function block_sonata_page_content($context, array $blocks = array())
    {
        $__internal_6c2106de0fc84a9752ae6d7864f4f2a11bc2f4c6b6012a89b0f126c936c981cf = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6c2106de0fc84a9752ae6d7864f4f2a11bc2f4c6b6012a89b0f126c936c981cf->enter($__internal_6c2106de0fc84a9752ae6d7864f4f2a11bc2f4c6b6012a89b0f126c936c981cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content"));

        $__internal_51a360aa8611661aa791d295ce132d72532824fe37d05a7930f31bf8d39f25e1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_51a360aa8611661aa791d295ce132d72532824fe37d05a7930f31bf8d39f25e1->enter($__internal_51a360aa8611661aa791d295ce132d72532824fe37d05a7930f31bf8d39f25e1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content"));

        // line 258
        echo "                    <section class=\"content-header\">

                        ";
        // line 260
        $this->displayBlock('sonata_page_content_header', $context, $blocks);
        // line 314
        echo "                    </section>

                    <section class=\"content\">
                        ";
        // line 317
        $this->displayBlock('sonata_admin_content', $context, $blocks);
        // line 351
        echo "                    </section>
                ";
        
        $__internal_51a360aa8611661aa791d295ce132d72532824fe37d05a7930f31bf8d39f25e1->leave($__internal_51a360aa8611661aa791d295ce132d72532824fe37d05a7930f31bf8d39f25e1_prof);

        
        $__internal_6c2106de0fc84a9752ae6d7864f4f2a11bc2f4c6b6012a89b0f126c936c981cf->leave($__internal_6c2106de0fc84a9752ae6d7864f4f2a11bc2f4c6b6012a89b0f126c936c981cf_prof);

    }

    // line 260
    public function block_sonata_page_content_header($context, array $blocks = array())
    {
        $__internal_8be6b6b93e150950578f9aa48daea034cb25924b4b87efab617fe0b6a23ed94b = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8be6b6b93e150950578f9aa48daea034cb25924b4b87efab617fe0b6a23ed94b->enter($__internal_8be6b6b93e150950578f9aa48daea034cb25924b4b87efab617fe0b6a23ed94b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content_header"));

        $__internal_f8de6fea17d53da0c490c9c6e53f7db3535cd44168e578cfe712b9cd2a1dd8c7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f8de6fea17d53da0c490c9c6e53f7db3535cd44168e578cfe712b9cd2a1dd8c7->enter($__internal_f8de6fea17d53da0c490c9c6e53f7db3535cd44168e578cfe712b9cd2a1dd8c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content_header"));

        // line 261
        echo "                            ";
        $this->displayBlock('sonata_page_content_nav', $context, $blocks);
        // line 313
        echo "                        ";
        
        $__internal_f8de6fea17d53da0c490c9c6e53f7db3535cd44168e578cfe712b9cd2a1dd8c7->leave($__internal_f8de6fea17d53da0c490c9c6e53f7db3535cd44168e578cfe712b9cd2a1dd8c7_prof);

        
        $__internal_8be6b6b93e150950578f9aa48daea034cb25924b4b87efab617fe0b6a23ed94b->leave($__internal_8be6b6b93e150950578f9aa48daea034cb25924b4b87efab617fe0b6a23ed94b_prof);

    }

    // line 261
    public function block_sonata_page_content_nav($context, array $blocks = array())
    {
        $__internal_f76ce25c97c4f2d362cd119382ba768a870610760e2c1374d3be66c7383871ae = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f76ce25c97c4f2d362cd119382ba768a870610760e2c1374d3be66c7383871ae->enter($__internal_f76ce25c97c4f2d362cd119382ba768a870610760e2c1374d3be66c7383871ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content_nav"));

        $__internal_f11cc78a7d1810a1b510033fd67f13b10cf12392143482d5751c86f3b150eddf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f11cc78a7d1810a1b510033fd67f13b10cf12392143482d5751c86f3b150eddf->enter($__internal_f11cc78a7d1810a1b510033fd67f13b10cf12392143482d5751c86f3b150eddf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_page_content_nav"));

        // line 262
        echo "                                ";
        if ((( !twig_test_empty((isset($context["_tab_menu"]) ? $context["_tab_menu"] : $this->getContext($context, "_tab_menu"))) ||  !twig_test_empty((isset($context["_actions"]) ? $context["_actions"] : $this->getContext($context, "_actions")))) ||  !twig_test_empty((isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : $this->getContext($context, "_list_filters_actions"))))) {
            // line 263
            echo "                                    <nav class=\"navbar navbar-default\" role=\"navigation\">
                                        <div class=\"container-fluid\">
                                            ";
            // line 265
            $this->displayBlock('tab_menu_navbar_header', $context, $blocks);
            // line 272
            echo "
                                            <div class=\"navbar-collapse\">
                                                ";
            // line 274
            if ( !twig_test_empty((isset($context["_tab_menu"]) ? $context["_tab_menu"] : $this->getContext($context, "_tab_menu")))) {
                // line 275
                echo "                                                    <div class=\"navbar-left\">
                                                        ";
                // line 276
                echo (isset($context["_tab_menu"]) ? $context["_tab_menu"] : $this->getContext($context, "_tab_menu"));
                echo "
                                                    </div>
                                                ";
            }
            // line 279
            echo "
                                                ";
            // line 280
            if ((((array_key_exists("admin", $context) && array_key_exists("action", $context)) && ((isset($context["action"]) ? $context["action"] : $this->getContext($context, "action")) == "list")) && (twig_length_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "listModes", array())) > 1))) {
                // line 281
                echo "                                                    <div class=\"nav navbar-right btn-group\">
                                                        ";
                // line 282
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "listModes", array()));
                foreach ($context['_seq'] as $context["mode"] => $context["settings"]) {
                    // line 283
                    echo "                                                            <a href=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateUrl", array(0 => "list", 1 => twig_array_merge($this->getAttribute($this->getAttribute($this->getAttribute((isset($context["app"]) ? $context["app"] : $this->getContext($context, "app")), "request", array()), "query", array()), "all", array()), array("_list_mode" => $context["mode"]))), "method"), "html", null, true);
                    echo "\" class=\"btn btn-default navbar-btn btn-sm";
                    if (($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getListMode", array(), "method") == $context["mode"])) {
                        echo " active";
                    }
                    echo "\"><i class=\"";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["settings"], "class", array()), "html", null, true);
                    echo "\"></i></a>
                                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['mode'], $context['settings'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 285
                echo "                                                    </div>
                                                ";
            }
            // line 287
            echo "
                                                ";
            // line 288
            $this->displayBlock('sonata_admin_content_actions_wrappers', $context, $blocks);
            // line 304
            echo "
                                                ";
            // line 305
            if ( !twig_test_empty((isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : $this->getContext($context, "_list_filters_actions")))) {
                // line 306
                echo "                                                    ";
                echo (isset($context["_list_filters_actions"]) ? $context["_list_filters_actions"] : $this->getContext($context, "_list_filters_actions"));
                echo "
                                                ";
            }
            // line 308
            echo "                                            </div>
                                        </div>
                                    </nav>
                                ";
        }
        // line 312
        echo "                            ";
        
        $__internal_f11cc78a7d1810a1b510033fd67f13b10cf12392143482d5751c86f3b150eddf->leave($__internal_f11cc78a7d1810a1b510033fd67f13b10cf12392143482d5751c86f3b150eddf_prof);

        
        $__internal_f76ce25c97c4f2d362cd119382ba768a870610760e2c1374d3be66c7383871ae->leave($__internal_f76ce25c97c4f2d362cd119382ba768a870610760e2c1374d3be66c7383871ae_prof);

    }

    // line 265
    public function block_tab_menu_navbar_header($context, array $blocks = array())
    {
        $__internal_dfe4df310092180fc5d0cc9907f62714a449941d95eb275bf11408793067ac28 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_dfe4df310092180fc5d0cc9907f62714a449941d95eb275bf11408793067ac28->enter($__internal_dfe4df310092180fc5d0cc9907f62714a449941d95eb275bf11408793067ac28_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu_navbar_header"));

        $__internal_bf816c252dce2647d62e2e9c8fab9f9c31943f90d38b9ed61a99a980f768518f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bf816c252dce2647d62e2e9c8fab9f9c31943f90d38b9ed61a99a980f768518f->enter($__internal_bf816c252dce2647d62e2e9c8fab9f9c31943f90d38b9ed61a99a980f768518f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tab_menu_navbar_header"));

        // line 266
        echo "                                                ";
        if ( !twig_test_empty((isset($context["_navbar_title"]) ? $context["_navbar_title"] : $this->getContext($context, "_navbar_title")))) {
            // line 267
            echo "                                                    <div class=\"navbar-header\">
                                                        <a class=\"navbar-brand\" href=\"#\">";
            // line 268
            echo (isset($context["_navbar_title"]) ? $context["_navbar_title"] : $this->getContext($context, "_navbar_title"));
            echo "</a>
                                                    </div>
                                                ";
        }
        // line 271
        echo "                                            ";
        
        $__internal_bf816c252dce2647d62e2e9c8fab9f9c31943f90d38b9ed61a99a980f768518f->leave($__internal_bf816c252dce2647d62e2e9c8fab9f9c31943f90d38b9ed61a99a980f768518f_prof);

        
        $__internal_dfe4df310092180fc5d0cc9907f62714a449941d95eb275bf11408793067ac28->leave($__internal_dfe4df310092180fc5d0cc9907f62714a449941d95eb275bf11408793067ac28_prof);

    }

    // line 288
    public function block_sonata_admin_content_actions_wrappers($context, array $blocks = array())
    {
        $__internal_ac1556e22cbbef170d5b28e5f77d32a3bcd6240c19aa97f638f76af54b93820e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ac1556e22cbbef170d5b28e5f77d32a3bcd6240c19aa97f638f76af54b93820e->enter($__internal_ac1556e22cbbef170d5b28e5f77d32a3bcd6240c19aa97f638f76af54b93820e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_admin_content_actions_wrappers"));

        $__internal_0e39a9fc3b87bf1ed1c3419efa53b21a18147aec4e5e3c920b806c35679706ec = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e39a9fc3b87bf1ed1c3419efa53b21a18147aec4e5e3c920b806c35679706ec->enter($__internal_0e39a9fc3b87bf1ed1c3419efa53b21a18147aec4e5e3c920b806c35679706ec_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_admin_content_actions_wrappers"));

        // line 289
        echo "                                                    ";
        if ( !twig_test_empty(twig_trim_filter(twig_replace_filter((isset($context["_actions"]) ? $context["_actions"] : $this->getContext($context, "_actions")), array("<li>" => "", "</li>" => ""))))) {
            // line 290
            echo "                                                        <ul class=\"nav navbar-nav navbar-right\">
                                                        ";
            // line 291
            if ((twig_length_filter($this->env, twig_split_filter($this->env, (isset($context["_actions"]) ? $context["_actions"] : $this->getContext($context, "_actions")), "</a>")) > 2)) {
                // line 292
                echo "                                                            <li class=\"dropdown sonata-actions\">
                                                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">";
                // line 293
                echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("link_actions", array(), "SonataAdminBundle"), "html", null, true);
                echo " <b class=\"caret\"></b></a>
                                                                <ul class=\"dropdown-menu\" role=\"menu\">
                                                                    ";
                // line 295
                echo (isset($context["_actions"]) ? $context["_actions"] : $this->getContext($context, "_actions"));
                echo "
                                                                </ul>
                                                            </li>
                                                        ";
            } else {
                // line 299
                echo "                                                            ";
                echo (isset($context["_actions"]) ? $context["_actions"] : $this->getContext($context, "_actions"));
                echo "
                                                        ";
            }
            // line 301
            echo "                                                        </ul>
                                                    ";
        }
        // line 303
        echo "                                                ";
        
        $__internal_0e39a9fc3b87bf1ed1c3419efa53b21a18147aec4e5e3c920b806c35679706ec->leave($__internal_0e39a9fc3b87bf1ed1c3419efa53b21a18147aec4e5e3c920b806c35679706ec_prof);

        
        $__internal_ac1556e22cbbef170d5b28e5f77d32a3bcd6240c19aa97f638f76af54b93820e->leave($__internal_ac1556e22cbbef170d5b28e5f77d32a3bcd6240c19aa97f638f76af54b93820e_prof);

    }

    // line 317
    public function block_sonata_admin_content($context, array $blocks = array())
    {
        $__internal_de7c69b2245b2ddf341467b63dd8b56d94661a1c6a985d00c66940d3b6ab2455 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_de7c69b2245b2ddf341467b63dd8b56d94661a1c6a985d00c66940d3b6ab2455->enter($__internal_de7c69b2245b2ddf341467b63dd8b56d94661a1c6a985d00c66940d3b6ab2455_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_admin_content"));

        $__internal_fd49dd92a8f64a38db2d34891091ce61fd070773d113f5fc16b97a75066ff9bd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd49dd92a8f64a38db2d34891091ce61fd070773d113f5fc16b97a75066ff9bd->enter($__internal_fd49dd92a8f64a38db2d34891091ce61fd070773d113f5fc16b97a75066ff9bd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sonata_admin_content"));

        // line 318
        echo "
                            ";
        // line 319
        $this->displayBlock('notice', $context, $blocks);
        // line 322
        echo "
                            ";
        // line 323
        if ( !twig_test_empty((isset($context["_preview"]) ? $context["_preview"] : $this->getContext($context, "_preview")))) {
            // line 324
            echo "                                <div class=\"sonata-ba-preview\">";
            echo (isset($context["_preview"]) ? $context["_preview"] : $this->getContext($context, "_preview"));
            echo "</div>
                            ";
        }
        // line 326
        echo "
                            ";
        // line 327
        if ( !twig_test_empty((isset($context["_content"]) ? $context["_content"] : $this->getContext($context, "_content")))) {
            // line 328
            echo "                                <div class=\"sonata-ba-content\">";
            echo (isset($context["_content"]) ? $context["_content"] : $this->getContext($context, "_content"));
            echo "</div>
                            ";
        }
        // line 330
        echo "
                            ";
        // line 331
        if ( !twig_test_empty((isset($context["_show"]) ? $context["_show"] : $this->getContext($context, "_show")))) {
            // line 332
            echo "                                <div class=\"sonata-ba-show\">";
            echo (isset($context["_show"]) ? $context["_show"] : $this->getContext($context, "_show"));
            echo "</div>
                            ";
        }
        // line 334
        echo "
                            ";
        // line 335
        if ( !twig_test_empty((isset($context["_form"]) ? $context["_form"] : $this->getContext($context, "_form")))) {
            // line 336
            echo "                                <div class=\"sonata-ba-form\">";
            echo (isset($context["_form"]) ? $context["_form"] : $this->getContext($context, "_form"));
            echo "</div>
                            ";
        }
        // line 338
        echo "
                            ";
        // line 339
        if ( !twig_test_empty((isset($context["_list_filters"]) ? $context["_list_filters"] : $this->getContext($context, "_list_filters")))) {
            // line 340
            echo "                                <div class=\"row\">
                                    ";
            // line 341
            echo (isset($context["_list_filters"]) ? $context["_list_filters"] : $this->getContext($context, "_list_filters"));
            echo "
                                </div>
                            ";
        }
        // line 344
        echo "
                            ";
        // line 345
        if ( !twig_test_empty((isset($context["_list_table"]) ? $context["_list_table"] : $this->getContext($context, "_list_table")))) {
            // line 346
            echo "                                <div class=\"row\">
                                    ";
            // line 347
            echo (isset($context["_list_table"]) ? $context["_list_table"] : $this->getContext($context, "_list_table"));
            echo "
                                </div>
                            ";
        }
        // line 350
        echo "                        ";
        
        $__internal_fd49dd92a8f64a38db2d34891091ce61fd070773d113f5fc16b97a75066ff9bd->leave($__internal_fd49dd92a8f64a38db2d34891091ce61fd070773d113f5fc16b97a75066ff9bd_prof);

        
        $__internal_de7c69b2245b2ddf341467b63dd8b56d94661a1c6a985d00c66940d3b6ab2455->leave($__internal_de7c69b2245b2ddf341467b63dd8b56d94661a1c6a985d00c66940d3b6ab2455_prof);

    }

    // line 319
    public function block_notice($context, array $blocks = array())
    {
        $__internal_b78a64b0ab480c0b100047c7c63ae0ef57f834e665200e4801a0252cc3f278ca = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b78a64b0ab480c0b100047c7c63ae0ef57f834e665200e4801a0252cc3f278ca->enter($__internal_b78a64b0ab480c0b100047c7c63ae0ef57f834e665200e4801a0252cc3f278ca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "notice"));

        $__internal_8e555b0f4c88517785665add26a9db4b05b90604c359cfd625a2654527d9455f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8e555b0f4c88517785665add26a9db4b05b90604c359cfd625a2654527d9455f->enter($__internal_8e555b0f4c88517785665add26a9db4b05b90604c359cfd625a2654527d9455f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "notice"));

        // line 320
        echo "                                ";
        $this->loadTemplate("SonataCoreBundle:FlashMessage:render.html.twig", "SonataAdminBundle::standard_layout.html.twig", 320)->display($context);
        // line 321
        echo "                            ";
        
        $__internal_8e555b0f4c88517785665add26a9db4b05b90604c359cfd625a2654527d9455f->leave($__internal_8e555b0f4c88517785665add26a9db4b05b90604c359cfd625a2654527d9455f_prof);

        
        $__internal_b78a64b0ab480c0b100047c7c63ae0ef57f834e665200e4801a0252cc3f278ca->leave($__internal_b78a64b0ab480c0b100047c7c63ae0ef57f834e665200e4801a0252cc3f278ca_prof);

    }

    // line 358
    public function block_bootlint($context, array $blocks = array())
    {
        $__internal_2fc64814718d14bac33da257acac3a915bdc88a118b219b506aec8b24c93e65f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2fc64814718d14bac33da257acac3a915bdc88a118b219b506aec8b24c93e65f->enter($__internal_2fc64814718d14bac33da257acac3a915bdc88a118b219b506aec8b24c93e65f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bootlint"));

        $__internal_b0af07bc57f5ee10396dc4e8c3fe3afb2512fca93c7504194d7c7a8c63f145bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0af07bc57f5ee10396dc4e8c3fe3afb2512fca93c7504194d7c7a8c63f145bc->enter($__internal_b0af07bc57f5ee10396dc4e8c3fe3afb2512fca93c7504194d7c7a8c63f145bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "bootlint"));

        // line 359
        echo "            ";
        // line 360
        echo "            <script type=\"text/javascript\">
                javascript:(function(){var s=document.createElement(\"script\");s.onload=function(){bootlint.showLintReportForCurrentDocument([], {hasProblems: false, problemFree: false});};s.src=\"https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js\";document.body.appendChild(s)})();
            </script>
        ";
        
        $__internal_b0af07bc57f5ee10396dc4e8c3fe3afb2512fca93c7504194d7c7a8c63f145bc->leave($__internal_b0af07bc57f5ee10396dc4e8c3fe3afb2512fca93c7504194d7c7a8c63f145bc_prof);

        
        $__internal_2fc64814718d14bac33da257acac3a915bdc88a118b219b506aec8b24c93e65f->leave($__internal_2fc64814718d14bac33da257acac3a915bdc88a118b219b506aec8b24c93e65f_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle::standard_layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1458 => 360,  1456 => 359,  1447 => 358,  1437 => 321,  1434 => 320,  1425 => 319,  1415 => 350,  1409 => 347,  1406 => 346,  1404 => 345,  1401 => 344,  1395 => 341,  1392 => 340,  1390 => 339,  1387 => 338,  1381 => 336,  1379 => 335,  1376 => 334,  1370 => 332,  1368 => 331,  1365 => 330,  1359 => 328,  1357 => 327,  1354 => 326,  1348 => 324,  1346 => 323,  1343 => 322,  1341 => 319,  1338 => 318,  1329 => 317,  1319 => 303,  1315 => 301,  1309 => 299,  1302 => 295,  1297 => 293,  1294 => 292,  1292 => 291,  1289 => 290,  1286 => 289,  1277 => 288,  1267 => 271,  1261 => 268,  1258 => 267,  1255 => 266,  1246 => 265,  1236 => 312,  1230 => 308,  1224 => 306,  1222 => 305,  1219 => 304,  1217 => 288,  1214 => 287,  1210 => 285,  1195 => 283,  1191 => 282,  1188 => 281,  1186 => 280,  1183 => 279,  1177 => 276,  1174 => 275,  1172 => 274,  1168 => 272,  1166 => 265,  1162 => 263,  1159 => 262,  1150 => 261,  1140 => 313,  1137 => 261,  1128 => 260,  1117 => 351,  1115 => 317,  1110 => 314,  1108 => 260,  1104 => 258,  1095 => 257,  1084 => 247,  1075 => 246,  1064 => 249,  1062 => 246,  1059 => 245,  1050 => 244,  1037 => 242,  1028 => 241,  1010 => 240,  989 => 230,  983 => 228,  974 => 227,  964 => 251,  961 => 244,  958 => 241,  956 => 240,  953 => 239,  950 => 227,  941 => 226,  929 => 252,  927 => 226,  923 => 224,  914 => 223,  903 => 353,  901 => 257,  897 => 255,  894 => 223,  885 => 222,  873 => 210,  871 => 209,  864 => 204,  855 => 203,  844 => 201,  842 => 200,  836 => 196,  827 => 195,  817 => 216,  812 => 213,  809 => 203,  807 => 195,  803 => 193,  800 => 192,  791 => 191,  780 => 187,  776 => 185,  770 => 183,  767 => 182,  764 => 181,  750 => 180,  744 => 178,  740 => 176,  734 => 174,  730 => 172,  727 => 170,  724 => 168,  722 => 167,  717 => 166,  715 => 165,  712 => 164,  710 => 163,  707 => 160,  705 => 159,  703 => 158,  701 => 157,  683 => 156,  680 => 155,  678 => 154,  675 => 153,  673 => 152,  670 => 151,  661 => 150,  650 => 217,  648 => 191,  644 => 189,  642 => 150,  634 => 144,  625 => 143,  615 => 142,  611 => 140,  605 => 138,  602 => 137,  594 => 135,  592 => 134,  587 => 133,  584 => 132,  575 => 131,  561 => 127,  557 => 125,  548 => 124,  537 => 219,  534 => 143,  531 => 131,  529 => 124,  526 => 123,  517 => 122,  499 => 118,  489 => 115,  486 => 114,  483 => 113,  469 => 112,  464 => 110,  461 => 107,  459 => 106,  457 => 105,  455 => 104,  451 => 101,  448 => 100,  445 => 99,  428 => 98,  425 => 97,  422 => 96,  416 => 94,  414 => 93,  408 => 91,  399 => 90,  389 => 66,  380 => 64,  375 => 63,  366 => 62,  345 => 50,  341 => 48,  335 => 47,  327 => 46,  319 => 45,  311 => 44,  307 => 42,  298 => 41,  288 => 87,  285 => 86,  279 => 84,  276 => 83,  273 => 81,  268 => 80,  265 => 79,  262 => 77,  258 => 75,  256 => 73,  254 => 71,  251 => 70,  249 => 69,  247 => 68,  244 => 67,  242 => 62,  239 => 61,  236 => 41,  227 => 40,  217 => 38,  208 => 36,  203 => 35,  194 => 34,  181 => 29,  172 => 28,  154 => 26,  141 => 365,  138 => 364,  135 => 358,  133 => 357,  129 => 355,  127 => 222,  124 => 221,  122 => 122,  115 => 118,  111 => 116,  109 => 90,  105 => 88,  103 => 40,  100 => 39,  98 => 34,  95 => 33,  93 => 28,  88 => 26,  84 => 24,  82 => 23,  80 => 22,  78 => 21,  76 => 20,  74 => 19,  72 => 18,  70 => 17,  68 => 16,  66 => 15,  64 => 14,  62 => 13,  60 => 12,  57 => 11,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% set _preview = block('preview') is defined ? block('preview')|trim : null %}
{% set _form = block('form') is defined ? block('form')|trim : null %}
{% set _show = block('show') is defined ? block('show')|trim : null %}
{% set _list_table = block('list_table') is defined ? block('list_table')|trim : null %}
{% set _list_filters = block('list_filters') is defined ? block('list_filters')|trim : null %}
{% set _tab_menu = block('tab_menu') is defined ? block('tab_menu')|trim : null %}
{% set _content = block('content') is defined ? block('content')|trim : null %}
{% set _title = block('title') is defined ? block('title')|trim : null %}
{% set _breadcrumb = block('breadcrumb') is defined ? block('breadcrumb')|trim : null %}
{% set _actions = block('actions') is defined ? block('actions')|trim : null %}
{% set _navbar_title = block('navbar_title') is defined ? block('navbar_title')|trim : null %}
{% set _list_filters_actions = block('list_filters_actions') is defined ? block('list_filters_actions')|trim : null %}

<!DOCTYPE html>
<html {% block html_attributes %}class=\"no-js\"{% endblock %}>
    <head>
        {% block meta_tags %}
            <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
            <meta charset=\"UTF-8\">
            <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        {% endblock %}

        {% block stylesheets %}
            {% for stylesheet in sonata_admin.adminPool.getOption('stylesheets', []) %}
                <link rel=\"stylesheet\" href=\"{{ asset(stylesheet) }}\">
            {% endfor %}
        {% endblock %}

        {% block javascripts %}
            {% block sonata_javascript_config %}
                <script>
                    window.SONATA_CONFIG = {
                        CONFIRM_EXIT: {% if sonata_admin.adminPool.getOption('confirm_exit') %}true{% else %}false{% endif %},
                        USE_SELECT2: {% if sonata_admin.adminPool.getOption('use_select2') %}true{% else %}false{% endif %},
                        USE_ICHECK: {% if sonata_admin.adminPool.getOption('use_icheck') %}true{% else %}false{% endif %},
                        USE_STICKYFORMS: {% if sonata_admin.adminPool.getOption('use_stickyforms') %}true{% else %}false{% endif %}
                    };
                    window.SONATA_TRANSLATIONS = {
                        CONFIRM_EXIT: '{{ 'confirm_exit'|trans({}, 'SonataAdminBundle')|escape('js') }}'
                    };

                    // http://getbootstrap.com/getting-started/#support-ie10-width
                    if (navigator.userAgent.match(/IEMobile\\/10\\.0/)) {
                        var msViewportStyle = document.createElement('style');
                        msViewportStyle.appendChild(document.createTextNode('@-ms-viewport{width:auto!important}'));
                        document.querySelector('head').appendChild(msViewportStyle);
                    }
                </script>
            {% endblock %}

            {% block sonata_javascript_pool %}
                {% for javascript in sonata_admin.adminPool.getOption('javascripts', []) %}
                    <script src=\"{{ asset(javascript) }}\"></script>
                {% endfor %}
            {% endblock %}

            {% set locale = app.request.locale %}
            {# localize moment #}
            {% if locale[:2] != 'en' %}
                <script src=\"{{ asset(
                    'bundles/sonatacore/vendor/moment/locale/' ~
                    locale|lower|replace({'_':'-'}) ~
                    '.js'
                ) }}\"></script>
            {% endif %}

            {# localize select2 #}
            {% if sonata_admin.adminPool.getOption('use_select2') %}
                {% if locale == 'pt' %}{% set locale = 'pt_PT' %}{% endif %}

                {# omit default EN locale #}
                {% if locale[:2] != 'en' %}
                    <script src=\"{{ asset('bundles/sonatacore/vendor/select2/select2_locale_' ~ locale|replace({'_':'-'}) ~ '.js') }}\"></script>
                {% endif %}
            {% endif %}
        {% endblock %}

        <title>
        {% block sonata_head_title %}
            {{ 'Admin'|trans({}, 'SonataAdminBundle') }}

            {% if _title is not empty %}
                {{ _title|striptags|raw }}
            {% else %}
                {% if action is defined %}
                    -
                    {% for menu in breadcrumbs_builder.breadcrumbs(admin, action) %}
                        {% if not loop.first %}
                            {% if loop.index != 2 %}
                                &gt;
                            {% endif %}

                            {%- set translation_domain = menu.extra('translation_domain', 'messages') -%}
                            {%- set label = menu.label -%}
                            {%- if translation_domain is not same as(false) -%}
                                {%- set label = label|trans(menu.extra('translation_params', {}), translation_domain) -%}
                            {%- endif -%}

                            {{ label }}
                        {% endif %}
                    {% endfor %}
                {% endif %}
            {% endif %}
        {% endblock %}
        </title>
    </head>
    <body {% block body_attributes %}class=\"sonata-bc skin-black fixed\"{% endblock %}>

    <div class=\"wrapper\">

        {% block sonata_header %}
            <header class=\"main-header\">
                {% block sonata_header_noscript_warning %}
                    <noscript>
                        <div class=\"noscript-warning\">
                            {{ 'noscript_warning'|trans({}, 'SonataAdminBundle') }}
                        </div>
                    </noscript>
                {% endblock %}
                {% block logo %}
                    {% spaceless %}
                        <a class=\"logo\" href=\"{{ path('sonata_admin_dashboard') }}\">
                            {% if 'single_image' == sonata_admin.adminPool.getOption('title_mode') or 'both' == sonata_admin.adminPool.getOption('title_mode') %}
                                <img src=\"{{ asset(sonata_admin.adminPool.titlelogo) }}\" alt=\"{{ sonata_admin.adminPool.title }}\">
                            {% endif %}
                            {% if 'single_text' == sonata_admin.adminPool.getOption('title_mode') or 'both' == sonata_admin.adminPool.getOption('title_mode') %}
                                <span>{{ sonata_admin.adminPool.title }}</span>
                            {% endif %}
                        </a>
                    {% endspaceless %}
                {% endblock %}
                {% block sonata_nav %}
                    <nav class=\"navbar navbar-static-top\" role=\"navigation\">
                        <a href=\"#\" class=\"sidebar-toggle\" data-toggle=\"offcanvas\" role=\"button\">
                            <span class=\"sr-only\">Toggle navigation</span>
                        </a>

                        <div class=\"navbar-left\">
                            {% block sonata_breadcrumb %}
                                <div class=\"hidden-xs\">
                                    {% if _breadcrumb is not empty or action is defined %}
                                        <ol class=\"nav navbar-top-links breadcrumb\">
                                            {% if _breadcrumb is empty %}
                                                {% if action is defined %}
                                                    {% for menu in breadcrumbs_builder.breadcrumbs(admin, action) %}
                                                        {%- set translation_domain = menu.extra('translation_domain', 'messages') -%}
                                                        {%- set label = menu.label -%}
                                                        {%- if translation_domain is not same as(false) -%}
                                                            {%- set label = label|trans(menu.extra('translation_params', {}), translation_domain) -%}
                                                        {%- endif -%}

                                                        {% if not loop.last %}
                                                            <li>
                                                                {% if menu.uri is not empty %}
                                                                    <a href=\"{{ menu.uri }}\">
                                                                        {% if menu.extra('safe_label', true) %}
                                                                            {{- label|raw -}}
                                                                        {% else %}
                                                                            {{- label -}}
                                                                        {% endif %}
                                                                    </a>
                                                                {% else %}
                                                                    <span>{{ label }}</span>
                                                                {% endif %}
                                                            </li>
                                                        {% else %}
                                                            <li class=\"active\"><span>{{ label }}</span></li>
                                                        {% endif %}
                                                    {% endfor %}
                                                {% endif %}
                                            {% else %}
                                                {{ _breadcrumb|raw }}
                                            {% endif %}
                                        </ol>
                                    {% endif %}
                                </div>
                            {% endblock sonata_breadcrumb %}
                        </div>

                        {% block sonata_top_nav_menu %}
                            {% if app.user and is_granted('ROLE_SONATA_ADMIN') %}
                                <div class=\"navbar-custom-menu\">
                                    <ul class=\"nav navbar-nav\">
                                        {% block sonata_top_nav_menu_add_block %}
                                            <li class=\"dropdown\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-plus-square fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                {% include sonata_admin.adminPool.getTemplate('add_block') %}
                                            </li>
                                        {% endblock %}
                                        {% block sonata_top_nav_menu_user_block %}
                                            <li class=\"dropdown user-menu\">
                                                <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                                                    <i class=\"fa fa-user fa-fw\" aria-hidden=\"true\"></i> <i class=\"fa fa-caret-down\" aria-hidden=\"true\"></i>
                                                </a>
                                                <ul class=\"dropdown-menu dropdown-user\">
                                                    {% include sonata_admin.adminPool.getTemplate('user_block') %}
                                                </ul>
                                            </li>
                                        {% endblock %}
                                    </ul>
                                </div>
                            {% endif %}
                        {% endblock %}
                    </nav>
                {% endblock sonata_nav %}
            </header>
        {% endblock sonata_header %}

        {% block sonata_wrapper %}
            {% block sonata_left_side %}
                <aside class=\"main-sidebar\">
                    <section class=\"sidebar\">
                        {% block sonata_side_nav %}
                            {% block sonata_sidebar_search %}
                                <form action=\"{{ path('sonata_admin_search') }}\" method=\"GET\" class=\"sidebar-form\" role=\"search\">
                                    <div class=\"input-group custom-search-form\">
                                        <input type=\"text\" name=\"q\" value=\"{{ app.request.get('q') }}\" class=\"form-control\" placeholder=\"{{ 'search_placeholder'|trans({}, 'SonataAdminBundle') }}\">
                                        <span class=\"input-group-btn\">
                                            <button class=\"btn btn-flat\" type=\"submit\">
                                                <i class=\"fa fa-search\" aria-hidden=\"true\"></i>
                                            </button>
                                        </span>
                                    </div>
                                </form>
                            {% endblock sonata_sidebar_search %}

                            {% block side_bar_before_nav %} {% endblock %}
                            {% block side_bar_nav %}
                                {{ knp_menu_render('sonata_admin_sidebar', {template: sonata_admin.adminPool.getTemplate('knp_menu_template')}) }}
                            {% endblock side_bar_nav %}
                            {% block side_bar_after_nav %}
                                <p class=\"text-center small\" style=\"border-top: 1px solid #444444; padding-top: 10px\">
                                    {% block side_bar_after_nav_content %}
                                        <a href=\"https://sonata-project.org\" rel=\"noreferrer\" target=\"_blank\">sonata project</a>
                                    {% endblock %}
                                </p>
                            {% endblock %}
                        {% endblock sonata_side_nav %}
                    </section>
                </aside>
            {% endblock sonata_left_side %}

            <div class=\"content-wrapper\">
                {% block sonata_page_content %}
                    <section class=\"content-header\">

                        {% block sonata_page_content_header %}
                            {% block sonata_page_content_nav %}
                                {% if _tab_menu is not empty or _actions is not empty or _list_filters_actions is not empty %}
                                    <nav class=\"navbar navbar-default\" role=\"navigation\">
                                        <div class=\"container-fluid\">
                                            {% block tab_menu_navbar_header %}
                                                {% if _navbar_title is not empty %}
                                                    <div class=\"navbar-header\">
                                                        <a class=\"navbar-brand\" href=\"#\">{{ _navbar_title|raw }}</a>
                                                    </div>
                                                {% endif %}
                                            {% endblock %}

                                            <div class=\"navbar-collapse\">
                                                {% if _tab_menu is not empty %}
                                                    <div class=\"navbar-left\">
                                                        {{ _tab_menu|raw }}
                                                    </div>
                                                {% endif %}

                                                {% if admin is defined and action is defined and action == 'list' and admin.listModes|length > 1 %}
                                                    <div class=\"nav navbar-right btn-group\">
                                                        {% for mode, settings in admin.listModes %}
                                                            <a href=\"{{ admin.generateUrl('list', app.request.query.all|merge({_list_mode: mode})) }}\" class=\"btn btn-default navbar-btn btn-sm{% if admin.getListMode() == mode %} active{% endif %}\"><i class=\"{{ settings.class }}\"></i></a>
                                                        {% endfor %}
                                                    </div>
                                                {% endif %}

                                                {% block sonata_admin_content_actions_wrappers %}
                                                    {% if _actions|replace({ '<li>': '', '</li>': '' })|trim is not empty %}
                                                        <ul class=\"nav navbar-nav navbar-right\">
                                                        {% if _actions|split('</a>')|length > 2 %}
                                                            <li class=\"dropdown sonata-actions\">
                                                                <a href=\"#\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">{{ 'link_actions'|trans({}, 'SonataAdminBundle') }} <b class=\"caret\"></b></a>
                                                                <ul class=\"dropdown-menu\" role=\"menu\">
                                                                    {{ _actions|raw }}
                                                                </ul>
                                                            </li>
                                                        {% else %}
                                                            {{ _actions|raw }}
                                                        {% endif %}
                                                        </ul>
                                                    {% endif %}
                                                {% endblock sonata_admin_content_actions_wrappers %}

                                                {% if _list_filters_actions is not empty %}
                                                    {{ _list_filters_actions|raw }}
                                                {% endif %}
                                            </div>
                                        </div>
                                    </nav>
                                {% endif %}
                            {% endblock sonata_page_content_nav %}
                        {% endblock sonata_page_content_header %}
                    </section>

                    <section class=\"content\">
                        {% block sonata_admin_content %}

                            {% block notice %}
                                {% include 'SonataCoreBundle:FlashMessage:render.html.twig' %}
                            {% endblock notice %}

                            {% if _preview is not empty %}
                                <div class=\"sonata-ba-preview\">{{ _preview|raw }}</div>
                            {% endif %}

                            {% if _content is not empty %}
                                <div class=\"sonata-ba-content\">{{ _content|raw }}</div>
                            {% endif %}

                            {% if _show is not empty %}
                                <div class=\"sonata-ba-show\">{{ _show|raw }}</div>
                            {% endif %}

                            {% if _form is not empty %}
                                <div class=\"sonata-ba-form\">{{ _form|raw }}</div>
                            {% endif %}

                            {% if _list_filters is not empty %}
                                <div class=\"row\">
                                    {{ _list_filters|raw }}
                                </div>
                            {% endif %}

                            {% if _list_table is not empty %}
                                <div class=\"row\">
                                    {{ _list_table|raw }}
                                </div>
                            {% endif %}
                        {% endblock sonata_admin_content %}
                    </section>
                {% endblock sonata_page_content %}
            </div>
        {% endblock sonata_wrapper %}
    </div>

    {% if sonata_admin.adminPool.getOption('use_bootlint') %}
        {% block bootlint %}
            {# Bootlint - https://github.com/twbs/bootlint#in-the-browser #}
            <script type=\"text/javascript\">
                javascript:(function(){var s=document.createElement(\"script\");s.onload=function(){bootlint.showLintReportForCurrentDocument([], {hasProblems: false, problemFree: false});};s.src=\"https://maxcdn.bootstrapcdn.com/bootlint/latest/bootlint.min.js\";document.body.appendChild(s)})();
            </script>
        {% endblock %}
    {% endif %}

    </body>
</html>
", "SonataAdminBundle::standard_layout.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/standard_layout.html.twig");
    }
}
