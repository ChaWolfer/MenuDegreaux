<?php

/* SonataAdminBundle:CRUD:show_compare.html.twig */
class __TwigTemplate_2c794360a8c7382fa6d9766e6f71b9d5acb6c8ce60ac883e635a201bfbf33a66 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_compare.html.twig", "SonataAdminBundle:CRUD:show_compare.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_compare.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0abea4d1d2eaa6faf109d8eecbe876d953024e4c8574f1a7b3ffcf129d6ad83a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0abea4d1d2eaa6faf109d8eecbe876d953024e4c8574f1a7b3ffcf129d6ad83a->enter($__internal_0abea4d1d2eaa6faf109d8eecbe876d953024e4c8574f1a7b3ffcf129d6ad83a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_compare.html.twig"));

        $__internal_640a8ee6e001e2eddd35ee5bf454b34bfe617af6415388d4bf704486bc993c4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_640a8ee6e001e2eddd35ee5bf454b34bfe617af6415388d4bf704486bc993c4a->enter($__internal_640a8ee6e001e2eddd35ee5bf454b34bfe617af6415388d4bf704486bc993c4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_compare.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0abea4d1d2eaa6faf109d8eecbe876d953024e4c8574f1a7b3ffcf129d6ad83a->leave($__internal_0abea4d1d2eaa6faf109d8eecbe876d953024e4c8574f1a7b3ffcf129d6ad83a_prof);

        
        $__internal_640a8ee6e001e2eddd35ee5bf454b34bfe617af6415388d4bf704486bc993c4a->leave($__internal_640a8ee6e001e2eddd35ee5bf454b34bfe617af6415388d4bf704486bc993c4a_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_compare.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_compare.html.twig' %}
", "SonataAdminBundle:CRUD:show_compare.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_compare.html.twig");
    }
}
