<?php

/* @Framework/Form/form_rows.html.php */
class __TwigTemplate_d897928cf0c476b3d5bff3522d7e175198ebeeee9e95f551b397c38ee4ddc082 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a9e9a36da0514133af3fc7975bc347a47ad9245db2bab6d5493157719b195e6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a9e9a36da0514133af3fc7975bc347a47ad9245db2bab6d5493157719b195e6c->enter($__internal_a9e9a36da0514133af3fc7975bc347a47ad9245db2bab6d5493157719b195e6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        $__internal_c4f94d258751c5f269a17f5ef376cf4d694fc8464b1886949ed7b2d50348f1e2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4f94d258751c5f269a17f5ef376cf4d694fc8464b1886949ed7b2d50348f1e2->enter($__internal_c4f94d258751c5f269a17f5ef376cf4d694fc8464b1886949ed7b2d50348f1e2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_rows.html.php"));

        // line 1
        echo "<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
";
        
        $__internal_a9e9a36da0514133af3fc7975bc347a47ad9245db2bab6d5493157719b195e6c->leave($__internal_a9e9a36da0514133af3fc7975bc347a47ad9245db2bab6d5493157719b195e6c_prof);

        
        $__internal_c4f94d258751c5f269a17f5ef376cf4d694fc8464b1886949ed7b2d50348f1e2->leave($__internal_c4f94d258751c5f269a17f5ef376cf4d694fc8464b1886949ed7b2d50348f1e2_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_rows.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php foreach (\$form as \$child) : ?>
    <?php echo \$view['form']->row(\$child) ?>
<?php endforeach; ?>
", "@Framework/Form/form_rows.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_rows.html.php");
    }
}
