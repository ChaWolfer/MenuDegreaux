<?php

/* FOSUserBundle:Resetting:check_email.html.twig */
class __TwigTemplate_b9703eb8f86d355ce9852f040f98b112bad097ecd7c73feba1a3918a280c3943 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@FOSUser/layout.html.twig", "FOSUserBundle:Resetting:check_email.html.twig", 1);
        $this->blocks = array(
            'fos_user_content' => array($this, 'block_fos_user_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@FOSUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_be5d3d63f81a69424226e521c9c65245003d5c007d0588217252bca4cb17347f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_be5d3d63f81a69424226e521c9c65245003d5c007d0588217252bca4cb17347f->enter($__internal_be5d3d63f81a69424226e521c9c65245003d5c007d0588217252bca4cb17347f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $__internal_5cb2aa4e9f6e6dc863ccac114d8fb8e621d85cd0ea5aac38b7b057b392154cd2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5cb2aa4e9f6e6dc863ccac114d8fb8e621d85cd0ea5aac38b7b057b392154cd2->enter($__internal_5cb2aa4e9f6e6dc863ccac114d8fb8e621d85cd0ea5aac38b7b057b392154cd2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "FOSUserBundle:Resetting:check_email.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_be5d3d63f81a69424226e521c9c65245003d5c007d0588217252bca4cb17347f->leave($__internal_be5d3d63f81a69424226e521c9c65245003d5c007d0588217252bca4cb17347f_prof);

        
        $__internal_5cb2aa4e9f6e6dc863ccac114d8fb8e621d85cd0ea5aac38b7b057b392154cd2->leave($__internal_5cb2aa4e9f6e6dc863ccac114d8fb8e621d85cd0ea5aac38b7b057b392154cd2_prof);

    }

    // line 5
    public function block_fos_user_content($context, array $blocks = array())
    {
        $__internal_2f5a30a539eae0edc85349b85c9b5e67147f3e9c554b6fb1e541826f4bfac37e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_2f5a30a539eae0edc85349b85c9b5e67147f3e9c554b6fb1e541826f4bfac37e->enter($__internal_2f5a30a539eae0edc85349b85c9b5e67147f3e9c554b6fb1e541826f4bfac37e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        $__internal_e3f4b98ae9d8c048757fd3a4984d4c11cb3b42e2d8b80d7f0299404813d9a759 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e3f4b98ae9d8c048757fd3a4984d4c11cb3b42e2d8b80d7f0299404813d9a759->enter($__internal_e3f4b98ae9d8c048757fd3a4984d4c11cb3b42e2d8b80d7f0299404813d9a759_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "fos_user_content"));

        // line 6
        echo "<p>
";
        // line 7
        echo nl2br(twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("resetting.check_email", array("%tokenLifetime%" => (isset($context["tokenLifetime"]) ? $context["tokenLifetime"] : $this->getContext($context, "tokenLifetime"))), "FOSUserBundle"), "html", null, true));
        echo "
</p>
";
        
        $__internal_e3f4b98ae9d8c048757fd3a4984d4c11cb3b42e2d8b80d7f0299404813d9a759->leave($__internal_e3f4b98ae9d8c048757fd3a4984d4c11cb3b42e2d8b80d7f0299404813d9a759_prof);

        
        $__internal_2f5a30a539eae0edc85349b85c9b5e67147f3e9c554b6fb1e541826f4bfac37e->leave($__internal_2f5a30a539eae0edc85349b85c9b5e67147f3e9c554b6fb1e541826f4bfac37e_prof);

    }

    public function getTemplateName()
    {
        return "FOSUserBundle:Resetting:check_email.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 7,  49 => 6,  40 => 5,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"@FOSUser/layout.html.twig\" %}

{% trans_default_domain 'FOSUserBundle' %}

{% block fos_user_content %}
<p>
{{ 'resetting.check_email'|trans({'%tokenLifetime%': tokenLifetime})|nl2br }}
</p>
{% endblock %}
", "FOSUserBundle:Resetting:check_email.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\friendsofsymfony\\user-bundle/Resources/views/Resetting/check_email.html.twig");
    }
}
