<?php

/* @Framework/Form/choice_options.html.php */
class __TwigTemplate_607b2384063b9b1f1f8c8ad4f6116998eb06965600a23d879e141ac3486d5993 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9eaf5c69dfbb036dab80565ef181ca2389cfb7b9f556ab907e30abff07a15518 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9eaf5c69dfbb036dab80565ef181ca2389cfb7b9f556ab907e30abff07a15518->enter($__internal_9eaf5c69dfbb036dab80565ef181ca2389cfb7b9f556ab907e30abff07a15518_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        $__internal_7135d58c74f896f59af8225013eb7d69c05c334cb37142b6854f2f986660b231 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7135d58c74f896f59af8225013eb7d69c05c334cb37142b6854f2f986660b231->enter($__internal_7135d58c74f896f59af8225013eb7d69c05c334cb37142b6854f2f986660b231_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_options.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
";
        
        $__internal_9eaf5c69dfbb036dab80565ef181ca2389cfb7b9f556ab907e30abff07a15518->leave($__internal_9eaf5c69dfbb036dab80565ef181ca2389cfb7b9f556ab907e30abff07a15518_prof);

        
        $__internal_7135d58c74f896f59af8225013eb7d69c05c334cb37142b6854f2f986660b231->leave($__internal_7135d58c74f896f59af8225013eb7d69c05c334cb37142b6854f2f986660b231_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_options.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'choice_widget_options') ?>
", "@Framework/Form/choice_options.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_options.html.php");
    }
}
