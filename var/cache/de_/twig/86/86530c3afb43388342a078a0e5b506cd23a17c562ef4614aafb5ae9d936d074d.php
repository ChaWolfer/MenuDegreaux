<?php

/* TwigBundle:Exception:exception.css.twig */
class __TwigTemplate_bca9c251af2fa4a7e8c3e4c500949d8c59ca67b947b3461ac4be6108a18a36b2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e6694938a4d23ab562f7c7432bca8dacefd254a473aa07df109cbc3f3bd7502d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_e6694938a4d23ab562f7c7432bca8dacefd254a473aa07df109cbc3f3bd7502d->enter($__internal_e6694938a4d23ab562f7c7432bca8dacefd254a473aa07df109cbc3f3bd7502d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        $__internal_e422f309833683eb80f5a4a5bb3cedc0d11523886ae1725f7ea52987ea86ea0c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e422f309833683eb80f5a4a5bb3cedc0d11523886ae1725f7ea52987ea86ea0c->enter($__internal_e422f309833683eb80f5a4a5bb3cedc0d11523886ae1725f7ea52987ea86ea0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.css.twig"));

        // line 1
        echo "/*
";
        // line 2
        echo twig_include($this->env, $context, "@Twig/Exception/exception.txt.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
*/
";
        
        $__internal_e6694938a4d23ab562f7c7432bca8dacefd254a473aa07df109cbc3f3bd7502d->leave($__internal_e6694938a4d23ab562f7c7432bca8dacefd254a473aa07df109cbc3f3bd7502d_prof);

        
        $__internal_e422f309833683eb80f5a4a5bb3cedc0d11523886ae1725f7ea52987ea86ea0c->leave($__internal_e422f309833683eb80f5a4a5bb3cedc0d11523886ae1725f7ea52987ea86ea0c_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.css.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  28 => 2,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("/*
{{ include('@Twig/Exception/exception.txt.twig', { exception: exception }) }}
*/
", "TwigBundle:Exception:exception.css.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.css.twig");
    }
}
