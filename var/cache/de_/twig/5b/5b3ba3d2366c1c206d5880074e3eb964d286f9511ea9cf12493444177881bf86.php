<?php

/* @SonataBlock/Block/block_exception.html.twig */
class __TwigTemplate_fbd69c453b800efb1c28c9471c9f1ffcd2db3b6a6315c9cf6bc3bbbbb99347aa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'block' => array($this, 'block_block'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute($this->getAttribute((isset($context["sonata_block"]) ? $context["sonata_block"] : $this->getContext($context, "sonata_block")), "templates", array()), "block_base", array()), "@SonataBlock/Block/block_exception.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0ec7f7380833523612d641d1148b3ae58abe368525de6d3b90b7021d8079c957 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0ec7f7380833523612d641d1148b3ae58abe368525de6d3b90b7021d8079c957->enter($__internal_0ec7f7380833523612d641d1148b3ae58abe368525de6d3b90b7021d8079c957_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_exception.html.twig"));

        $__internal_4a14f4e0c0ed538a1e25e2ca533bc6574cb46786ae1996a26db3df1604eeebdf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4a14f4e0c0ed538a1e25e2ca533bc6574cb46786ae1996a26db3df1604eeebdf->enter($__internal_4a14f4e0c0ed538a1e25e2ca533bc6574cb46786ae1996a26db3df1604eeebdf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@SonataBlock/Block/block_exception.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0ec7f7380833523612d641d1148b3ae58abe368525de6d3b90b7021d8079c957->leave($__internal_0ec7f7380833523612d641d1148b3ae58abe368525de6d3b90b7021d8079c957_prof);

        
        $__internal_4a14f4e0c0ed538a1e25e2ca533bc6574cb46786ae1996a26db3df1604eeebdf->leave($__internal_4a14f4e0c0ed538a1e25e2ca533bc6574cb46786ae1996a26db3df1604eeebdf_prof);

    }

    // line 14
    public function block_block($context, array $blocks = array())
    {
        $__internal_d3441fe6e1c505958b91ea934f261d363110655fb9bd14a997070b05d9273278 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_d3441fe6e1c505958b91ea934f261d363110655fb9bd14a997070b05d9273278->enter($__internal_d3441fe6e1c505958b91ea934f261d363110655fb9bd14a997070b05d9273278_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        $__internal_62b6ef85126f930b95afb66dd3e602f1ff0e5ceb6d0d5c55864380b12f9d2d25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_62b6ef85126f930b95afb66dd3e602f1ff0e5ceb6d0d5c55864380b12f9d2d25->enter($__internal_62b6ef85126f930b95afb66dd3e602f1ff0e5ceb6d0d5c55864380b12f9d2d25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "block"));

        // line 15
        echo "    <div class=\"cms-block-exception\">
        <h2>";
        // line 16
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["block"]) ? $context["block"] : $this->getContext($context, "block")), "name", array()), "html", null, true);
        echo "</h2>
        <h3>";
        // line 17
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo "</h3>
    </div>
";
        
        $__internal_62b6ef85126f930b95afb66dd3e602f1ff0e5ceb6d0d5c55864380b12f9d2d25->leave($__internal_62b6ef85126f930b95afb66dd3e602f1ff0e5ceb6d0d5c55864380b12f9d2d25_prof);

        
        $__internal_d3441fe6e1c505958b91ea934f261d363110655fb9bd14a997070b05d9273278->leave($__internal_d3441fe6e1c505958b91ea934f261d363110655fb9bd14a997070b05d9273278_prof);

    }

    public function getTemplateName()
    {
        return "@SonataBlock/Block/block_exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 17,  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends sonata_block.templates.block_base %}

{% block block %}
    <div class=\"cms-block-exception\">
        <h2>{{ block.name }}</h2>
        <h3>{{ exception.message }}</h3>
    </div>
{% endblock %}
", "@SonataBlock/Block/block_exception.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\block-bundle\\Resources\\views\\Block\\block_exception.html.twig");
    }
}
