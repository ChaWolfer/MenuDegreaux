<?php

/* SonataAdminBundle:CRUD:show_datetime.html.twig */
class __TwigTemplate_5af97f194a2f8829f09057ba56879ed1ed50e7384092b63b78217df519fdb79e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_show_field.html.twig", "SonataAdminBundle:CRUD:show_datetime.html.twig", 12);
        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_show_field.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a74677dd5ed4734c9621def34d61b5aa4e9002c2b7d77632fb09776287d722b2 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a74677dd5ed4734c9621def34d61b5aa4e9002c2b7d77632fb09776287d722b2->enter($__internal_a74677dd5ed4734c9621def34d61b5aa4e9002c2b7d77632fb09776287d722b2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_datetime.html.twig"));

        $__internal_2354dc0e573fb94ce57a23568a1aa32b8ac0eb91ae943311544c36aa3b7c500a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2354dc0e573fb94ce57a23568a1aa32b8ac0eb91ae943311544c36aa3b7c500a->enter($__internal_2354dc0e573fb94ce57a23568a1aa32b8ac0eb91ae943311544c36aa3b7c500a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:show_datetime.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a74677dd5ed4734c9621def34d61b5aa4e9002c2b7d77632fb09776287d722b2->leave($__internal_a74677dd5ed4734c9621def34d61b5aa4e9002c2b7d77632fb09776287d722b2_prof);

        
        $__internal_2354dc0e573fb94ce57a23568a1aa32b8ac0eb91ae943311544c36aa3b7c500a->leave($__internal_2354dc0e573fb94ce57a23568a1aa32b8ac0eb91ae943311544c36aa3b7c500a_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_1938e7bcd626b36c3841021a5fe85271c2a8cb418575699799a93c5de8444190 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_1938e7bcd626b36c3841021a5fe85271c2a8cb418575699799a93c5de8444190->enter($__internal_1938e7bcd626b36c3841021a5fe85271c2a8cb418575699799a93c5de8444190_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_b0e08430aa010c663597299910d3e2b28606da47c666ce21b9a61cdba7fc993d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b0e08430aa010c663597299910d3e2b28606da47c666ce21b9a61cdba7fc993d->enter($__internal_b0e08430aa010c663597299910d3e2b28606da47c666ce21b9a61cdba7fc993d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        if (twig_test_empty((isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "&nbsp;";
        } elseif ($this->getAttribute($this->getAttribute(        // line 17
(isset($context["field_description"]) ? $context["field_description"] : null), "options", array(), "any", false, true), "format", array(), "any", true, true)) {
            // line 18
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "format", array())), "html", null, true);
        } else {
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value"))), "html", null, true);
        }
        
        $__internal_b0e08430aa010c663597299910d3e2b28606da47c666ce21b9a61cdba7fc993d->leave($__internal_b0e08430aa010c663597299910d3e2b28606da47c666ce21b9a61cdba7fc993d_prof);

        
        $__internal_1938e7bcd626b36c3841021a5fe85271c2a8cb418575699799a93c5de8444190->leave($__internal_1938e7bcd626b36c3841021a5fe85271c2a8cb418575699799a93c5de8444190_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:show_datetime.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 20,  55 => 18,  53 => 17,  51 => 16,  49 => 15,  40 => 14,  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_show_field.html.twig' %}

{% block field %}
    {%- if value is empty -%}
        &nbsp;
    {%- elseif field_description.options.format is defined -%}
        {{ value|date(field_description.options.format) }}
    {%- else -%}
        {{ value|date }}
    {%- endif -%}
{% endblock %}
", "SonataAdminBundle:CRUD:show_datetime.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/show_datetime.html.twig");
    }
}
