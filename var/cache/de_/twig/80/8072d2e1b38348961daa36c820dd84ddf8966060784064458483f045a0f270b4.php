<?php

/* @Twig/Exception/error.xml.twig */
class __TwigTemplate_9f937c30c0bb770d0658e71da72c957b98e4079cc3dba6a0faa679ad00976c31 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4b4d092e92ba62478d45220e7795e19f7314756f8ab8136e65f9d8f6156d93a3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4b4d092e92ba62478d45220e7795e19f7314756f8ab8136e65f9d8f6156d93a3->enter($__internal_4b4d092e92ba62478d45220e7795e19f7314756f8ab8136e65f9d8f6156d93a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.xml.twig"));

        $__internal_5c7a45a3ce1d262279ec9da826975790277a7ec560b48e660faa302f702ddeba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5c7a45a3ce1d262279ec9da826975790277a7ec560b48e660faa302f702ddeba->enter($__internal_5c7a45a3ce1d262279ec9da826975790277a7ec560b48e660faa302f702ddeba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/error.xml.twig"));

        // line 1
        echo "<?xml version=\"1.0\" encoding=\"";
        echo twig_escape_filter($this->env, $this->env->getCharset(), "html", null, true);
        echo "\" ?>

<error code=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo "\" message=\"";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo "\" />
";
        
        $__internal_4b4d092e92ba62478d45220e7795e19f7314756f8ab8136e65f9d8f6156d93a3->leave($__internal_4b4d092e92ba62478d45220e7795e19f7314756f8ab8136e65f9d8f6156d93a3_prof);

        
        $__internal_5c7a45a3ce1d262279ec9da826975790277a7ec560b48e660faa302f702ddeba->leave($__internal_5c7a45a3ce1d262279ec9da826975790277a7ec560b48e660faa302f702ddeba_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/error.xml.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?xml version=\"1.0\" encoding=\"{{ _charset }}\" ?>

<error code=\"{{ status_code }}\" message=\"{{ status_text }}\" />
", "@Twig/Exception/error.xml.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\error.xml.twig");
    }
}
