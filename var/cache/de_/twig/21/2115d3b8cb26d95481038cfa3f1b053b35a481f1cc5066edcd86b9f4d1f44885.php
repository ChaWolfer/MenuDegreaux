<?php

/* WebProfilerBundle:Profiler:info.html.twig */
class __TwigTemplate_a97544a3323832f79b7f0e7d7e5a26af7e2eccf47a143bd55a92faf297230c9b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Profiler:info.html.twig", 1);
        $this->blocks = array(
            'summary' => array($this, 'block_summary'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cb78449d15c400dc9e5a300f013ebe44ca51aed8b5a2946311d162b88583f80c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cb78449d15c400dc9e5a300f013ebe44ca51aed8b5a2946311d162b88583f80c->enter($__internal_cb78449d15c400dc9e5a300f013ebe44ca51aed8b5a2946311d162b88583f80c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        $__internal_fc26e79900a16d68bfd4b2d977dd7e7c4b6ca5730c63551c7574daf1584ac080 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fc26e79900a16d68bfd4b2d977dd7e7c4b6ca5730c63551c7574daf1584ac080->enter($__internal_fc26e79900a16d68bfd4b2d977dd7e7c4b6ca5730c63551c7574daf1584ac080_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:info.html.twig"));

        // line 3
        $context["messages"] = array("no_token" => array("status" => "error", "title" => (((((        // line 6
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("There are no profiles") : ("Token not found")), "message" => (((((        // line 7
array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : ("")) == "latest")) ? ("No profiles found in the database.") : ((("Token \"" . ((array_key_exists("token", $context)) ? (_twig_default_filter((isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")), "")) : (""))) . "\" was not found in the database.")))));
        // line 1
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cb78449d15c400dc9e5a300f013ebe44ca51aed8b5a2946311d162b88583f80c->leave($__internal_cb78449d15c400dc9e5a300f013ebe44ca51aed8b5a2946311d162b88583f80c_prof);

        
        $__internal_fc26e79900a16d68bfd4b2d977dd7e7c4b6ca5730c63551c7574daf1584ac080->leave($__internal_fc26e79900a16d68bfd4b2d977dd7e7c4b6ca5730c63551c7574daf1584ac080_prof);

    }

    // line 11
    public function block_summary($context, array $blocks = array())
    {
        $__internal_01aa6fd39556ef85c5e98387713062859ae8f87c5e60e78471fabc0344cecb24 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_01aa6fd39556ef85c5e98387713062859ae8f87c5e60e78471fabc0344cecb24->enter($__internal_01aa6fd39556ef85c5e98387713062859ae8f87c5e60e78471fabc0344cecb24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        $__internal_a41b1596ac1d824613713562310715e24132c30e3b795e3142ae5df30ae2f7a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a41b1596ac1d824613713562310715e24132c30e3b795e3142ae5df30ae2f7a5->enter($__internal_a41b1596ac1d824613713562310715e24132c30e3b795e3142ae5df30ae2f7a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "summary"));

        // line 12
        echo "    <div class=\"status status-";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "status", array()), "html", null, true);
        echo "\">
        <div class=\"container\">
            <h2>";
        // line 14
        echo twig_escape_filter($this->env, twig_title_string_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "status", array())), "html", null, true);
        echo "</h2>
        </div>
    </div>
";
        
        $__internal_a41b1596ac1d824613713562310715e24132c30e3b795e3142ae5df30ae2f7a5->leave($__internal_a41b1596ac1d824613713562310715e24132c30e3b795e3142ae5df30ae2f7a5_prof);

        
        $__internal_01aa6fd39556ef85c5e98387713062859ae8f87c5e60e78471fabc0344cecb24->leave($__internal_01aa6fd39556ef85c5e98387713062859ae8f87c5e60e78471fabc0344cecb24_prof);

    }

    // line 19
    public function block_panel($context, array $blocks = array())
    {
        $__internal_f6514c7c8dc3b6ea798dbc776781cdb7075db4d3eb0c2e9703ff6b57d1b2b287 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6514c7c8dc3b6ea798dbc776781cdb7075db4d3eb0c2e9703ff6b57d1b2b287->enter($__internal_f6514c7c8dc3b6ea798dbc776781cdb7075db4d3eb0c2e9703ff6b57d1b2b287_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_c149ca87a12fc51ef0e258f0afccedb94c7a479ae25000d0cfd4299d2637cf7e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c149ca87a12fc51ef0e258f0afccedb94c7a479ae25000d0cfd4299d2637cf7e->enter($__internal_c149ca87a12fc51ef0e258f0afccedb94c7a479ae25000d0cfd4299d2637cf7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 20
        echo "    <h2>";
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "title", array()), "html", null, true);
        echo "</h2>
    <p>";
        // line 21
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["messages"]) ? $context["messages"] : $this->getContext($context, "messages")), (isset($context["about"]) ? $context["about"] : $this->getContext($context, "about")), array(), "array"), "message", array()), "html", null, true);
        echo "</p>
";
        
        $__internal_c149ca87a12fc51ef0e258f0afccedb94c7a479ae25000d0cfd4299d2637cf7e->leave($__internal_c149ca87a12fc51ef0e258f0afccedb94c7a479ae25000d0cfd4299d2637cf7e_prof);

        
        $__internal_f6514c7c8dc3b6ea798dbc776781cdb7075db4d3eb0c2e9703ff6b57d1b2b287->leave($__internal_f6514c7c8dc3b6ea798dbc776781cdb7075db4d3eb0c2e9703ff6b57d1b2b287_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 21,  84 => 20,  75 => 19,  61 => 14,  55 => 12,  46 => 11,  36 => 1,  34 => 7,  33 => 6,  32 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% set messages = {
    'no_token' : {
        status:  'error',
        title:   (token|default('') == 'latest') ? 'There are no profiles' : 'Token not found',
        message: (token|default('') == 'latest') ? 'No profiles found in the database.' : 'Token \"' ~ token|default('') ~ '\" was not found in the database.'
    }
} %}

{% block summary %}
    <div class=\"status status-{{ messages[about].status }}\">
        <div class=\"container\">
            <h2>{{ messages[about].status|title }}</h2>
        </div>
    </div>
{% endblock %}

{% block panel %}
    <h2>{{ messages[about].title }}</h2>
    <p>{{ messages[about].message }}</p>
{% endblock %}
", "WebProfilerBundle:Profiler:info.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/info.html.twig");
    }
}
