<?php

/* TwigBundle:Exception:exception.json.twig */
class __TwigTemplate_f3ca4b8bf40dbbacf61055713e8ec2f5814fe33275e95e60c7714deeda2eeb01 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a4c58d85231732f5e0f77e3cc441a04247440aac4a3aefa9d459487c086c5c0f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_a4c58d85231732f5e0f77e3cc441a04247440aac4a3aefa9d459487c086c5c0f->enter($__internal_a4c58d85231732f5e0f77e3cc441a04247440aac4a3aefa9d459487c086c5c0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        $__internal_c1015fad5396c48a3f6bd28526768468710370f7cff6960bd687fc1c6c923085 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c1015fad5396c48a3f6bd28526768468710370f7cff6960bd687fc1c6c923085->enter($__internal_c1015fad5396c48a3f6bd28526768468710370f7cff6960bd687fc1c6c923085_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.json.twig"));

        // line 1
        echo twig_jsonencode_filter(array("error" => array("code" => (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "message" => (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "exception" => $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "toarray", array()))));
        echo "
";
        
        $__internal_a4c58d85231732f5e0f77e3cc441a04247440aac4a3aefa9d459487c086c5c0f->leave($__internal_a4c58d85231732f5e0f77e3cc441a04247440aac4a3aefa9d459487c086c5c0f_prof);

        
        $__internal_c1015fad5396c48a3f6bd28526768468710370f7cff6960bd687fc1c6c923085->leave($__internal_c1015fad5396c48a3f6bd28526768468710370f7cff6960bd687fc1c6c923085_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.json.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ { 'error': { 'code': status_code, 'message': status_text, 'exception': exception.toarray } }|json_encode|raw }}
", "TwigBundle:Exception:exception.json.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.json.twig");
    }
}
