<?php

/* SonataAdminBundle:CRUD:acl.html.twig */
class __TwigTemplate_960793c490802f94f52aa77a5c1a7321acaeeb6acb0718b8a460d2deddfa54b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 12
        $this->parent = $this->loadTemplate("SonataAdminBundle:CRUD:base_acl.html.twig", "SonataAdminBundle:CRUD:acl.html.twig", 12);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:CRUD:base_acl.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_59dcabc3329477f83069361c2395230b318674d52358a57089a54df76acf4c30 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_59dcabc3329477f83069361c2395230b318674d52358a57089a54df76acf4c30->enter($__internal_59dcabc3329477f83069361c2395230b318674d52358a57089a54df76acf4c30_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:acl.html.twig"));

        $__internal_a6442d3353ab9017c72cbcaaf75ff6b38d099ebf3626d779dc57c0f56fef9770 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a6442d3353ab9017c72cbcaaf75ff6b38d099ebf3626d779dc57c0f56fef9770->enter($__internal_a6442d3353ab9017c72cbcaaf75ff6b38d099ebf3626d779dc57c0f56fef9770_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:acl.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_59dcabc3329477f83069361c2395230b318674d52358a57089a54df76acf4c30->leave($__internal_59dcabc3329477f83069361c2395230b318674d52358a57089a54df76acf4c30_prof);

        
        $__internal_a6442d3353ab9017c72cbcaaf75ff6b38d099ebf3626d779dc57c0f56fef9770->leave($__internal_a6442d3353ab9017c72cbcaaf75ff6b38d099ebf3626d779dc57c0f56fef9770_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:acl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends 'SonataAdminBundle:CRUD:base_acl.html.twig' %}
", "SonataAdminBundle:CRUD:acl.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/acl.html.twig");
    }
}
