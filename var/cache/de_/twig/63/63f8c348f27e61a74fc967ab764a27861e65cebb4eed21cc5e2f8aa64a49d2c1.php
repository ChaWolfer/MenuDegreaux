<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_fbb9d3bdc6cdc743f2e3b7a0ebb56560c515c2d648c8e0f9ea5ad159b3583844 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4f124bff4d3a0c39eab6fb8820eb5c2e43ec8f185b3a9a62f6776dcdc23228e7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_4f124bff4d3a0c39eab6fb8820eb5c2e43ec8f185b3a9a62f6776dcdc23228e7->enter($__internal_4f124bff4d3a0c39eab6fb8820eb5c2e43ec8f185b3a9a62f6776dcdc23228e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        $__internal_180bd3c6602b405f45d557f377b9d57191c818c6776ab55786616658fd49ab8b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_180bd3c6602b405f45d557f377b9d57191c818c6776ab55786616658fd49ab8b->enter($__internal_180bd3c6602b405f45d557f377b9d57191c818c6776ab55786616658fd49ab8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_4f124bff4d3a0c39eab6fb8820eb5c2e43ec8f185b3a9a62f6776dcdc23228e7->leave($__internal_4f124bff4d3a0c39eab6fb8820eb5c2e43ec8f185b3a9a62f6776dcdc23228e7_prof);

        
        $__internal_180bd3c6602b405f45d557f377b9d57191c818c6776ab55786616658fd49ab8b->leave($__internal_180bd3c6602b405f45d557f377b9d57191c818c6776ab55786616658fd49ab8b_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_row.html.php");
    }
}
