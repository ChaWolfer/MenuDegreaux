<?php

/* SonataAdminBundle:Core:create_button.html.twig */
class __TwigTemplate_fead113d696ac5383dbde99ef1bb3c012d084a098f57c8680f2e30fb8bf289f8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 15
        $this->parent = $this->loadTemplate("SonataAdminBundle:Button:create_button.html.twig", "SonataAdminBundle:Core:create_button.html.twig", 15);
        $this->blocks = array(
        );
    }

    protected function doGetParent(array $context)
    {
        return "SonataAdminBundle:Button:create_button.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_771252d4c68455599b843c2d9e003a360c78b5112a78ffd79682218808d49485 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_771252d4c68455599b843c2d9e003a360c78b5112a78ffd79682218808d49485->enter($__internal_771252d4c68455599b843c2d9e003a360c78b5112a78ffd79682218808d49485_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:create_button.html.twig"));

        $__internal_c7af80bc308a7e9c6d5193cbcb19c2a3d52293fcbe72dce8cfab8a6d88681eee = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7af80bc308a7e9c6d5193cbcb19c2a3d52293fcbe72dce8cfab8a6d88681eee->enter($__internal_c7af80bc308a7e9c6d5193cbcb19c2a3d52293fcbe72dce8cfab8a6d88681eee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:Core:create_button.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_771252d4c68455599b843c2d9e003a360c78b5112a78ffd79682218808d49485->leave($__internal_771252d4c68455599b843c2d9e003a360c78b5112a78ffd79682218808d49485_prof);

        
        $__internal_c7af80bc308a7e9c6d5193cbcb19c2a3d52293fcbe72dce8cfab8a6d88681eee->leave($__internal_c7af80bc308a7e9c6d5193cbcb19c2a3d52293fcbe72dce8cfab8a6d88681eee_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:Core:create_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  11 => 15,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{# DEPRECATED #}
{# This file is kept here for backward compatibility - Rather use SonataAdminBundle:Button:create_button.html.twig #}

{% extends 'SonataAdminBundle:Button:create_button.html.twig' %}
", "SonataAdminBundle:Core:create_button.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/Core/create_button.html.twig");
    }
}
