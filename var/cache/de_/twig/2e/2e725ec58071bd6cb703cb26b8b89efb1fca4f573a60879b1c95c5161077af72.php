<?php

/* SonataAdminBundle:CRUD:base_show_macro.html.twig */
class __TwigTemplate_06f9aca26a4913fe64391325c2ed13c86d5f8d10a430746cd1ab319604aed810 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'field_row' => array($this, 'block_field_row'),
            'show_title' => array($this, 'block_show_title'),
            'show_field' => array($this, 'block_show_field'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f6499b8b95b82499525b2e01c7ef6fcbb1d88ff4787840595f7e296f2acb2454 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_f6499b8b95b82499525b2e01c7ef6fcbb1d88ff4787840595f7e296f2acb2454->enter($__internal_f6499b8b95b82499525b2e01c7ef6fcbb1d88ff4787840595f7e296f2acb2454_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_macro.html.twig"));

        $__internal_c9efcf9611d5b132dce915dd59d00350310523e25d960e49332edcf89ac05eb9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c9efcf9611d5b132dce915dd59d00350310523e25d960e49332edcf89ac05eb9->enter($__internal_c9efcf9611d5b132dce915dd59d00350310523e25d960e49332edcf89ac05eb9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:base_show_macro.html.twig"));

        // line 2
        echo "
";
        // line 8
        echo "
";
        // line 9
        $this->displayBlock('field_row', $context, $blocks);
        
        $__internal_f6499b8b95b82499525b2e01c7ef6fcbb1d88ff4787840595f7e296f2acb2454->leave($__internal_f6499b8b95b82499525b2e01c7ef6fcbb1d88ff4787840595f7e296f2acb2454_prof);

        
        $__internal_c9efcf9611d5b132dce915dd59d00350310523e25d960e49332edcf89ac05eb9->leave($__internal_c9efcf9611d5b132dce915dd59d00350310523e25d960e49332edcf89ac05eb9_prof);

    }

    public function block_field_row($context, array $blocks = array())
    {
        $__internal_0159946f7078760a6db8fc73c56bd5bfcc74fe86101f1241a252d10acf679ccc = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0159946f7078760a6db8fc73c56bd5bfcc74fe86101f1241a252d10acf679ccc->enter($__internal_0159946f7078760a6db8fc73c56bd5bfcc74fe86101f1241a252d10acf679ccc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_row"));

        $__internal_bcbd27000f21922e313e9170715a77141132f445d156160ee91aa80465ad37e7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcbd27000f21922e313e9170715a77141132f445d156160ee91aa80465ad37e7->enter($__internal_bcbd27000f21922e313e9170715a77141132f445d156160ee91aa80465ad37e7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field_row"));

        // line 10
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["groups"]) ? $context["groups"] : $this->getContext($context, "groups")));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["code"]) {
            // line 11
            echo "        ";
            $context["show_group"] = $this->getAttribute($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "showgroups", array()), $context["code"], array(), "array");
            // line 12
            echo "
        <div class=\"";
            // line 13
            echo twig_escape_filter($this->env, (($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : null), "class", array(), "any", true, true)) ? (_twig_default_filter($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : null), "class", array()), "col-md-12")) : ("col-md-12")), "html", null, true);
            echo " ";
            echo (((isset($context["no_padding"]) ? $context["no_padding"] : $this->getContext($context, "no_padding"))) ? ("nopadding") : (""));
            echo "\">
            <div class=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "box_class", array()), "html", null, true);
            echo "\">
                <div class=\"box-header\">
                    <h4 class=\"box-title\">
                        ";
            // line 17
            $this->displayBlock('show_title', $context, $blocks);
            // line 20
            echo "                    </h4>
                </div>
                <div class=\"box-body table-responsive no-padding\">
                    <table class=\"table\">
                        <tbody>
                        ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "fields", array()));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["field_name"]) {
                // line 26
                echo "                            ";
                $this->displayBlock('show_field', $context, $blocks);
                // line 33
                echo "                        ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field_name'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['code'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_bcbd27000f21922e313e9170715a77141132f445d156160ee91aa80465ad37e7->leave($__internal_bcbd27000f21922e313e9170715a77141132f445d156160ee91aa80465ad37e7_prof);

        
        $__internal_0159946f7078760a6db8fc73c56bd5bfcc74fe86101f1241a252d10acf679ccc->leave($__internal_0159946f7078760a6db8fc73c56bd5bfcc74fe86101f1241a252d10acf679ccc_prof);

    }

    // line 17
    public function block_show_title($context, array $blocks = array())
    {
        $__internal_5fd98480686acb59e692636c7757fa9d0918cb1dcbe0339e830063ea83863b18 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fd98480686acb59e692636c7757fa9d0918cb1dcbe0339e830063ea83863b18->enter($__internal_5fd98480686acb59e692636c7757fa9d0918cb1dcbe0339e830063ea83863b18_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_title"));

        $__internal_89d56d4ab4fa7295ac61cd63ad2d8ece81f918143b3f524209d504ae4506a2a5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_89d56d4ab4fa7295ac61cd63ad2d8ece81f918143b3f524209d504ae4506a2a5->enter($__internal_89d56d4ab4fa7295ac61cd63ad2d8ece81f918143b3f524209d504ae4506a2a5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_title"));

        // line 18
        echo "                            ";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "label", array()), array(), (($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "translation_domain", array())) ? ($this->getAttribute((isset($context["show_group"]) ? $context["show_group"] : $this->getContext($context, "show_group")), "translation_domain", array())) : ($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "translationDomain", array())))), "html", null, true);
        echo "
                        ";
        
        $__internal_89d56d4ab4fa7295ac61cd63ad2d8ece81f918143b3f524209d504ae4506a2a5->leave($__internal_89d56d4ab4fa7295ac61cd63ad2d8ece81f918143b3f524209d504ae4506a2a5_prof);

        
        $__internal_5fd98480686acb59e692636c7757fa9d0918cb1dcbe0339e830063ea83863b18->leave($__internal_5fd98480686acb59e692636c7757fa9d0918cb1dcbe0339e830063ea83863b18_prof);

    }

    // line 26
    public function block_show_field($context, array $blocks = array())
    {
        $__internal_8acd592e4514248cef67da2912333ca7a932aaec81a352d8e6d2bd5b1aad5f39 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8acd592e4514248cef67da2912333ca7a932aaec81a352d8e6d2bd5b1aad5f39->enter($__internal_8acd592e4514248cef67da2912333ca7a932aaec81a352d8e6d2bd5b1aad5f39_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        $__internal_f5ecafc0d41511865c74f3d46504a25bd22aaad09f4b809255b4a5cbe661337a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f5ecafc0d41511865c74f3d46504a25bd22aaad09f4b809255b4a5cbe661337a->enter($__internal_f5ecafc0d41511865c74f3d46504a25bd22aaad09f4b809255b4a5cbe661337a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "show_field"));

        // line 27
        echo "                                <tr class=\"sonata-ba-view-container\">
                                    ";
        // line 28
        if ($this->getAttribute((isset($context["elements"]) ? $context["elements"] : null), (isset($context["field_name"]) ? $context["field_name"] : $this->getContext($context, "field_name")), array(), "array", true, true)) {
            // line 29
            echo "                                        ";
            echo $this->env->getExtension('Sonata\AdminBundle\Twig\Extension\SonataAdminExtension')->renderViewElement($this->env, $this->getAttribute((isset($context["elements"]) ? $context["elements"] : $this->getContext($context, "elements")), (isset($context["field_name"]) ? $context["field_name"] : $this->getContext($context, "field_name")), array(), "array"), (isset($context["object"]) ? $context["object"] : $this->getContext($context, "object")));
            echo "
                                    ";
        }
        // line 31
        echo "                                </tr>
                            ";
        
        $__internal_f5ecafc0d41511865c74f3d46504a25bd22aaad09f4b809255b4a5cbe661337a->leave($__internal_f5ecafc0d41511865c74f3d46504a25bd22aaad09f4b809255b4a5cbe661337a_prof);

        
        $__internal_8acd592e4514248cef67da2912333ca7a932aaec81a352d8e6d2bd5b1aad5f39->leave($__internal_8acd592e4514248cef67da2912333ca7a932aaec81a352d8e6d2bd5b1aad5f39_prof);

    }

    // line 3
    public function getrender_groups($__admin__ = null, $__object__ = null, $__elements__ = null, $__groups__ = null, $__has_tab__ = null, $__no_padding__ = false, ...$__varargs__)
    {
        $context = $this->env->mergeGlobals(array(
            "admin" => $__admin__,
            "object" => $__object__,
            "elements" => $__elements__,
            "groups" => $__groups__,
            "has_tab" => $__has_tab__,
            "no_padding" => $__no_padding__,
            "varargs" => $__varargs__,
        ));

        $blocks = array();

        ob_start();
        try {
            $__internal_d50e799fffbcf5c2ff482d02b782467eea7b2639e01c3e13311625ff92b813c1 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
            $__internal_d50e799fffbcf5c2ff482d02b782467eea7b2639e01c3e13311625ff92b813c1->enter($__internal_d50e799fffbcf5c2ff482d02b782467eea7b2639e01c3e13311625ff92b813c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "render_groups"));

            $__internal_328ff6ecce73ad6db8c2da76cb4ec547c54c34066b4776b44bdc9da08ebebf19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
            $__internal_328ff6ecce73ad6db8c2da76cb4ec547c54c34066b4776b44bdc9da08ebebf19->enter($__internal_328ff6ecce73ad6db8c2da76cb4ec547c54c34066b4776b44bdc9da08ebebf19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "macro", "render_groups"));

            // line 4
            echo "    <div class=\"row\">
        ";
            // line 5
            $this->displayBlock("field_row", $context, $blocks);
            echo "
    </div>
";
            
            $__internal_328ff6ecce73ad6db8c2da76cb4ec547c54c34066b4776b44bdc9da08ebebf19->leave($__internal_328ff6ecce73ad6db8c2da76cb4ec547c54c34066b4776b44bdc9da08ebebf19_prof);

            
            $__internal_d50e799fffbcf5c2ff482d02b782467eea7b2639e01c3e13311625ff92b813c1->leave($__internal_d50e799fffbcf5c2ff482d02b782467eea7b2639e01c3e13311625ff92b813c1_prof);

        } catch (Exception $e) {
            ob_end_clean();

            throw $e;
        } catch (Throwable $e) {
            ob_end_clean();

            throw $e;
        }

        return ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:base_show_macro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  237 => 5,  234 => 4,  211 => 3,  200 => 31,  194 => 29,  192 => 28,  189 => 27,  180 => 26,  167 => 18,  158 => 17,  131 => 34,  117 => 33,  114 => 26,  97 => 25,  90 => 20,  88 => 17,  82 => 14,  76 => 13,  73 => 12,  70 => 11,  52 => 10,  34 => 9,  31 => 8,  28 => 2,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# NEXT_MAJOR: remove this template #}

{% macro render_groups(admin, object, elements, groups, has_tab, no_padding = false) %}
    <div class=\"row\">
        {{ block('field_row') }}
    </div>
{% endmacro %}

{% block field_row %}
    {% for code in groups %}
        {% set show_group = admin.showgroups[code] %}

        <div class=\"{{ show_group.class|default('col-md-12') }} {{ no_padding ? 'nopadding' }}\">
            <div class=\"{{ show_group.box_class }}\">
                <div class=\"box-header\">
                    <h4 class=\"box-title\">
                        {% block show_title %}
                            {{ show_group.label|trans({}, show_group.translation_domain ?: admin.translationDomain) }}
                        {% endblock %}
                    </h4>
                </div>
                <div class=\"box-body table-responsive no-padding\">
                    <table class=\"table\">
                        <tbody>
                        {% for field_name in show_group.fields %}
                            {% block show_field %}
                                <tr class=\"sonata-ba-view-container\">
                                    {% if elements[field_name] is defined %}
                                        {{ elements[field_name]|render_view_element(object)}}
                                    {% endif %}
                                </tr>
                            {% endblock %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    {% endfor %}
{% endblock %}
", "SonataAdminBundle:CRUD:base_show_macro.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/base_show_macro.html.twig");
    }
}
