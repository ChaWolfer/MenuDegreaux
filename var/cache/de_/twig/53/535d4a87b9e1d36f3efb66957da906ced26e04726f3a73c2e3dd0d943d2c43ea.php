<?php

/* :default:homepage.html.twig */
class __TwigTemplate_f647d3f1947b13514eaf60bac36c9ab0b66232d952340b4896bf983b4f4fe6d5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":default:homepage.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'footer' => array($this, 'block_footer'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_49f398b79d92dd13ba9c91e1180d1be92e95672c68cb0028870ee133131ac183 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_49f398b79d92dd13ba9c91e1180d1be92e95672c68cb0028870ee133131ac183->enter($__internal_49f398b79d92dd13ba9c91e1180d1be92e95672c68cb0028870ee133131ac183_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:homepage.html.twig"));

        $__internal_0df78dd2e57e47a6e4c04c93a07b91067bf1154ae6aeedf090dec7397c7e0f89 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0df78dd2e57e47a6e4c04c93a07b91067bf1154ae6aeedf090dec7397c7e0f89->enter($__internal_0df78dd2e57e47a6e4c04c93a07b91067bf1154ae6aeedf090dec7397c7e0f89_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:homepage.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_49f398b79d92dd13ba9c91e1180d1be92e95672c68cb0028870ee133131ac183->leave($__internal_49f398b79d92dd13ba9c91e1180d1be92e95672c68cb0028870ee133131ac183_prof);

        
        $__internal_0df78dd2e57e47a6e4c04c93a07b91067bf1154ae6aeedf090dec7397c7e0f89->leave($__internal_0df78dd2e57e47a6e4c04c93a07b91067bf1154ae6aeedf090dec7397c7e0f89_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_df12b95d82de745361597714872f1b0b6b51d17a420964e8caf91518be2a6b0c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_df12b95d82de745361597714872f1b0b6b51d17a420964e8caf91518be2a6b0c->enter($__internal_df12b95d82de745361597714872f1b0b6b51d17a420964e8caf91518be2a6b0c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        $__internal_347e1f7b792b0313c5fec1583e637a114898205f48bed03e4dc0bb81230986a9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_347e1f7b792b0313c5fec1583e637a114898205f48bed03e4dc0bb81230986a9->enter($__internal_347e1f7b792b0313c5fec1583e637a114898205f48bed03e4dc0bb81230986a9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "homepage";
        
        $__internal_347e1f7b792b0313c5fec1583e637a114898205f48bed03e4dc0bb81230986a9->leave($__internal_347e1f7b792b0313c5fec1583e637a114898205f48bed03e4dc0bb81230986a9_prof);

        
        $__internal_df12b95d82de745361597714872f1b0b6b51d17a420964e8caf91518be2a6b0c->leave($__internal_df12b95d82de745361597714872f1b0b6b51d17a420964e8caf91518be2a6b0c_prof);

    }

    // line 6
    public function block_header($context, array $blocks = array())
    {
        $__internal_c44d5182617cad3aa627a312f559e286671c9060bab5b865faafa03995208867 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c44d5182617cad3aa627a312f559e286671c9060bab5b865faafa03995208867->enter($__internal_c44d5182617cad3aa627a312f559e286671c9060bab5b865faafa03995208867_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        $__internal_794fdcc6f975f422567afb2b2d33f2731842752dfba9f8bf56824aeb0aa20d52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_794fdcc6f975f422567afb2b2d33f2731842752dfba9f8bf56824aeb0aa20d52->enter($__internal_794fdcc6f975f422567afb2b2d33f2731842752dfba9f8bf56824aeb0aa20d52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        
        $__internal_794fdcc6f975f422567afb2b2d33f2731842752dfba9f8bf56824aeb0aa20d52->leave($__internal_794fdcc6f975f422567afb2b2d33f2731842752dfba9f8bf56824aeb0aa20d52_prof);

        
        $__internal_c44d5182617cad3aa627a312f559e286671c9060bab5b865faafa03995208867->leave($__internal_c44d5182617cad3aa627a312f559e286671c9060bab5b865faafa03995208867_prof);

    }

    // line 7
    public function block_footer($context, array $blocks = array())
    {
        $__internal_9d75afc4217cc4e94b798264b49f7e9587d2452c8e29350d4841ff212e59aa9c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_9d75afc4217cc4e94b798264b49f7e9587d2452c8e29350d4841ff212e59aa9c->enter($__internal_9d75afc4217cc4e94b798264b49f7e9587d2452c8e29350d4841ff212e59aa9c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        $__internal_19ab9d163bf6216b0bccb461e9e398ea223e6ce9b174a154926231828d783d27 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_19ab9d163bf6216b0bccb461e9e398ea223e6ce9b174a154926231828d783d27->enter($__internal_19ab9d163bf6216b0bccb461e9e398ea223e6ce9b174a154926231828d783d27_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        
        $__internal_19ab9d163bf6216b0bccb461e9e398ea223e6ce9b174a154926231828d783d27->leave($__internal_19ab9d163bf6216b0bccb461e9e398ea223e6ce9b174a154926231828d783d27_prof);

        
        $__internal_9d75afc4217cc4e94b798264b49f7e9587d2452c8e29350d4841ff212e59aa9c->leave($__internal_9d75afc4217cc4e94b798264b49f7e9587d2452c8e29350d4841ff212e59aa9c_prof);

    }

    // line 9
    public function block_body($context, array $blocks = array())
    {
        $__internal_44407c3ed582a376b6943c8c0ef4c612e7eff48d6f6661694ddd89b9f6241492 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_44407c3ed582a376b6943c8c0ef4c612e7eff48d6f6661694ddd89b9f6241492->enter($__internal_44407c3ed582a376b6943c8c0ef4c612e7eff48d6f6661694ddd89b9f6241492_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        $__internal_fac1c268fff69d3049b9bb510064127a80edeb277c695b2f297d6e6dbeffecba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fac1c268fff69d3049b9bb510064127a80edeb277c695b2f297d6e6dbeffecba->enter($__internal_fac1c268fff69d3049b9bb510064127a80edeb277c695b2f297d6e6dbeffecba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 10
        echo "    <div class=\"page-header\">
        <h1>";
        // line 11
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("title.homepage");
        echo "</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_app");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 21
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("blog_index");
        echo "\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> ";
        // line 22
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_app"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    ";
        // line 31
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("help.browse_admin");
        echo "
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"";
        // line 34
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("admin_index");
        echo "\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> ";
        // line 35
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("action.browse_admin"), "html", null, true);
        echo "
                    </a>
                </p>
            </div>
        </div>
    </div>
";
        
        $__internal_fac1c268fff69d3049b9bb510064127a80edeb277c695b2f297d6e6dbeffecba->leave($__internal_fac1c268fff69d3049b9bb510064127a80edeb277c695b2f297d6e6dbeffecba_prof);

        
        $__internal_44407c3ed582a376b6943c8c0ef4c612e7eff48d6f6661694ddd89b9f6241492->leave($__internal_44407c3ed582a376b6943c8c0ef4c612e7eff48d6f6661694ddd89b9f6241492_prof);

    }

    public function getTemplateName()
    {
        return ":default:homepage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 35,  145 => 34,  139 => 31,  127 => 22,  123 => 21,  117 => 18,  107 => 11,  104 => 10,  95 => 9,  78 => 7,  61 => 6,  43 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'homepage' %}


{% block header %}{% endblock %}
{% block footer %}{% endblock %}

{% block body %}
    <div class=\"page-header\">
        <h1>{{ 'title.homepage'|trans|raw }}</h1>
    </div>

    <div class=\"row\">
        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_app'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('blog_index') }}\">
                        <i class=\"fa fa-users\" aria-hidden=\"true\"></i> {{ 'action.browse_app'|trans }}
                    </a>
                </p>
            </div>
        </div>

        <div class=\"col-sm-12\">
            <div class=\"jumbotron\">
                <p>
                    {{ 'help.browse_admin'|trans|raw }}
                </p>
                <p>
                    <a class=\"btn btn-primary btn-lg\" href=\"{{ path('admin_index') }}\">
                        <i class=\"fa fa-lock\" aria-hidden=\"true\"></i> {{ 'action.browse_admin'|trans }}
                    </a>
                </p>
            </div>
        </div>
    </div>
{% endblock %}
", ":default:homepage.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\app/Resources\\views/default/homepage.html.twig");
    }
}
