<?php

/* TwigBundle:Exception:exception.rdf.twig */
class __TwigTemplate_d4f598edf38e5a959d553518b5cbb1018d9e8ada601168406286ea9a868e6525 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c04d92267395660bf977b9bb3bb91b5f23f6c58fe905f63c297d47f78e0c72e5 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_c04d92267395660bf977b9bb3bb91b5f23f6c58fe905f63c297d47f78e0c72e5->enter($__internal_c04d92267395660bf977b9bb3bb91b5f23f6c58fe905f63c297d47f78e0c72e5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        $__internal_21c0331703f3555d1d9f75223db49931cd4c0b1ecc01e72c98f9efaa7bac5606 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_21c0331703f3555d1d9f75223db49931cd4c0b1ecc01e72c98f9efaa7bac5606->enter($__internal_21c0331703f3555d1d9f75223db49931cd4c0b1ecc01e72c98f9efaa7bac5606_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.rdf.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_c04d92267395660bf977b9bb3bb91b5f23f6c58fe905f63c297d47f78e0c72e5->leave($__internal_c04d92267395660bf977b9bb3bb91b5f23f6c58fe905f63c297d47f78e0c72e5_prof);

        
        $__internal_21c0331703f3555d1d9f75223db49931cd4c0b1ecc01e72c98f9efaa7bac5606->leave($__internal_21c0331703f3555d1d9f75223db49931cd4c0b1ecc01e72c98f9efaa7bac5606_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.rdf.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.rdf.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.rdf.twig");
    }
}
