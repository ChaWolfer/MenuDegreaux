<?php

/* SonataAdminBundle:CRUD:edit_string.html.twig */
class __TwigTemplate_89b418ec6d684ddf0a3e56a7e6a73a5f2536801d81177a614cc725430eb49e09 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate((isset($context["base_template"]) ? $context["base_template"] : $this->getContext($context, "base_template")), "SonataAdminBundle:CRUD:edit_string.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_30a4a1ba5529104eb45188781a2af54e1edd51e1dfff1e885b35cc3d4b4ff157 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_30a4a1ba5529104eb45188781a2af54e1edd51e1dfff1e885b35cc3d4b4ff157->enter($__internal_30a4a1ba5529104eb45188781a2af54e1edd51e1dfff1e885b35cc3d4b4ff157_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_string.html.twig"));

        $__internal_4eeb2b74443933ad7c7befa8628ccdad9bcfbf9585eb8f993ba9a6967ea094d0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4eeb2b74443933ad7c7befa8628ccdad9bcfbf9585eb8f993ba9a6967ea094d0->enter($__internal_4eeb2b74443933ad7c7befa8628ccdad9bcfbf9585eb8f993ba9a6967ea094d0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:edit_string.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_30a4a1ba5529104eb45188781a2af54e1edd51e1dfff1e885b35cc3d4b4ff157->leave($__internal_30a4a1ba5529104eb45188781a2af54e1edd51e1dfff1e885b35cc3d4b4ff157_prof);

        
        $__internal_4eeb2b74443933ad7c7befa8628ccdad9bcfbf9585eb8f993ba9a6967ea094d0->leave($__internal_4eeb2b74443933ad7c7befa8628ccdad9bcfbf9585eb8f993ba9a6967ea094d0_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_aaaa40ae39c78daf62738f3ec266e63bc6fa5a8354af628c70d8e5797c029ee7 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_aaaa40ae39c78daf62738f3ec266e63bc6fa5a8354af628c70d8e5797c029ee7->enter($__internal_aaaa40ae39c78daf62738f3ec266e63bc6fa5a8354af628c70d8e5797c029ee7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_6f9ffa1414f886c924c99c6badec9907fe8d577c4de6e3a18ef59d7351aecd79 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f9ffa1414f886c924c99c6badec9907fe8d577c4de6e3a18ef59d7351aecd79->enter($__internal_6f9ffa1414f886c924c99c6badec9907fe8d577c4de6e3a18ef59d7351aecd79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        echo $this->env->getRuntime('Symfony\Bridge\Twig\Form\TwigRenderer')->searchAndRenderBlock((isset($context["field_element"]) ? $context["field_element"] : $this->getContext($context, "field_element")), 'widget', array("attr" => array("class" => "title")));
        
        $__internal_6f9ffa1414f886c924c99c6badec9907fe8d577c4de6e3a18ef59d7351aecd79->leave($__internal_6f9ffa1414f886c924c99c6badec9907fe8d577c4de6e3a18ef59d7351aecd79_prof);

        
        $__internal_aaaa40ae39c78daf62738f3ec266e63bc6fa5a8354af628c70d8e5797c029ee7->leave($__internal_aaaa40ae39c78daf62738f3ec266e63bc6fa5a8354af628c70d8e5797c029ee7_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:edit_string.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends base_template %}

{% block field %}{{ form_widget(field_element, {'attr': {'class' : 'title'}}) }}{% endblock %}
", "SonataAdminBundle:CRUD:edit_string.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/edit_string.html.twig");
    }
}
