<?php

/* TwigBundle:Exception:exception.atom.twig */
class __TwigTemplate_9b829b82b9e1b1d81f441e7b71a8907aaabc47f47e4b1262e283e0baa569f3d9 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ad199fd2d7a42f8f006c6d38deac24427b15ac9bb0a3f799fc045554fe47aa33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ad199fd2d7a42f8f006c6d38deac24427b15ac9bb0a3f799fc045554fe47aa33->enter($__internal_ad199fd2d7a42f8f006c6d38deac24427b15ac9bb0a3f799fc045554fe47aa33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        $__internal_9f3caa305d171e52459312b63b9ecf13cbf6602b7508c9054abefd5d0db2bf48 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9f3caa305d171e52459312b63b9ecf13cbf6602b7508c9054abefd5d0db2bf48->enter($__internal_9f3caa305d171e52459312b63b9ecf13cbf6602b7508c9054abefd5d0db2bf48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception.atom.twig"));

        // line 1
        echo twig_include($this->env, $context, "@Twig/Exception/exception.xml.twig", array("exception" => (isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception"))));
        echo "
";
        
        $__internal_ad199fd2d7a42f8f006c6d38deac24427b15ac9bb0a3f799fc045554fe47aa33->leave($__internal_ad199fd2d7a42f8f006c6d38deac24427b15ac9bb0a3f799fc045554fe47aa33_prof);

        
        $__internal_9f3caa305d171e52459312b63b9ecf13cbf6602b7508c9054abefd5d0db2bf48->leave($__internal_9f3caa305d171e52459312b63b9ecf13cbf6602b7508c9054abefd5d0db2bf48_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception.atom.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{{ include('@Twig/Exception/exception.xml.twig', { exception: exception }) }}
", "TwigBundle:Exception:exception.atom.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception.atom.twig");
    }
}
