<?php

/* SonataAdminBundle:CRUD:list__select.html.twig */
class __TwigTemplate_94c2401f84892bf16bea358598d9274cf665ce531e24c0445ac035288139ebfd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list__select.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_784e30ad0a8415af4bac7f6220f43fb91c5146affc2e8c0a1dfc55aedf434d10 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_784e30ad0a8415af4bac7f6220f43fb91c5146affc2e8c0a1dfc55aedf434d10->enter($__internal_784e30ad0a8415af4bac7f6220f43fb91c5146affc2e8c0a1dfc55aedf434d10_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__select.html.twig"));

        $__internal_cb542abeb438d532f03ad897e86e84bad063981ac138dea66c9bc3a1b93db2db = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb542abeb438d532f03ad897e86e84bad063981ac138dea66c9bc3a1b93db2db->enter($__internal_cb542abeb438d532f03ad897e86e84bad063981ac138dea66c9bc3a1b93db2db_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list__select.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_784e30ad0a8415af4bac7f6220f43fb91c5146affc2e8c0a1dfc55aedf434d10->leave($__internal_784e30ad0a8415af4bac7f6220f43fb91c5146affc2e8c0a1dfc55aedf434d10_prof);

        
        $__internal_cb542abeb438d532f03ad897e86e84bad063981ac138dea66c9bc3a1b93db2db->leave($__internal_cb542abeb438d532f03ad897e86e84bad063981ac138dea66c9bc3a1b93db2db_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_cf52e32a444b3c05f17c5a770878ee55613bd777966f585decfe748daf1f08a4 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_cf52e32a444b3c05f17c5a770878ee55613bd777966f585decfe748daf1f08a4->enter($__internal_cf52e32a444b3c05f17c5a770878ee55613bd777966f585decfe748daf1f08a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_5ef00e99f7a0c70f3ac0c997bb6649774085824b66cc51f768f18c42e11286d1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5ef00e99f7a0c70f3ac0c997bb6649774085824b66cc51f768f18c42e11286d1->enter($__internal_5ef00e99f7a0c70f3ac0c997bb6649774085824b66cc51f768f18c42e11286d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    <a class=\"btn btn-primary\" href=\"";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "generateUrl", array(0 => "list"), "method"), "html", null, true);
        echo "\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        ";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("list_select", array(), "SonataAdminBundle"), "html", null, true);
        echo "
    </a>
";
        
        $__internal_5ef00e99f7a0c70f3ac0c997bb6649774085824b66cc51f768f18c42e11286d1->leave($__internal_5ef00e99f7a0c70f3ac0c997bb6649774085824b66cc51f768f18c42e11286d1_prof);

        
        $__internal_cf52e32a444b3c05f17c5a770878ee55613bd777966f585decfe748daf1f08a4->leave($__internal_cf52e32a444b3c05f17c5a770878ee55613bd777966f585decfe748daf1f08a4_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list__select.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 17,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    <a class=\"btn btn-primary\" href=\"{{ admin.generateUrl('list') }}\">
        <i class=\"fa fa-check\" aria-hidden=\"true\"></i>
        {{ 'list_select'|trans({}, 'SonataAdminBundle') }}
    </a>
{% endblock %}
", "SonataAdminBundle:CRUD:list__select.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list__select.html.twig");
    }
}
