<?php

/* @Framework/Form/choice_widget.html.php */
class __TwigTemplate_2facf7e17ec73cc2d028ea99d301dbe061c26a48fb1b63ff7612d781fbb4d79d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b2fba9e5515f27e9a2a546e1f8f9915d2a2aef737ed9b61eaaa0ceb91790fa4e = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b2fba9e5515f27e9a2a546e1f8f9915d2a2aef737ed9b61eaaa0ceb91790fa4e->enter($__internal_b2fba9e5515f27e9a2a546e1f8f9915d2a2aef737ed9b61eaaa0ceb91790fa4e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        $__internal_fba30a91df4d52220ed27e0083d83495fd5732faa057ec9511d23422bec7bcf0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fba30a91df4d52220ed27e0083d83495fd5732faa057ec9511d23422bec7bcf0->enter($__internal_fba30a91df4d52220ed27e0083d83495fd5732faa057ec9511d23422bec7bcf0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/choice_widget.html.php"));

        // line 1
        echo "<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
";
        
        $__internal_b2fba9e5515f27e9a2a546e1f8f9915d2a2aef737ed9b61eaaa0ceb91790fa4e->leave($__internal_b2fba9e5515f27e9a2a546e1f8f9915d2a2aef737ed9b61eaaa0ceb91790fa4e_prof);

        
        $__internal_fba30a91df4d52220ed27e0083d83495fd5732faa057ec9511d23422bec7bcf0->leave($__internal_fba30a91df4d52220ed27e0083d83495fd5732faa057ec9511d23422bec7bcf0_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/choice_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$expanded): ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_expanded') ?>
<?php else: ?>
<?php echo \$view['form']->block(\$form, 'choice_widget_collapsed') ?>
<?php endif ?>
", "@Framework/Form/choice_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\choice_widget.html.php");
    }
}
