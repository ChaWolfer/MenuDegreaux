<?php

/* @Framework/Form/form_widget_compound.html.php */
class __TwigTemplate_9db1677d4a8b8643cd119287e378438ead7617559f04411b189034089687c494 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5fa6a907c0ca109cb14cf596bcfcdfa8c9a9819c2e1747526586a25ac6e72ca9 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5fa6a907c0ca109cb14cf596bcfcdfa8c9a9819c2e1747526586a25ac6e72ca9->enter($__internal_5fa6a907c0ca109cb14cf596bcfcdfa8c9a9819c2e1747526586a25ac6e72ca9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        $__internal_14e32d2bb9b2b4318b1dca47087bd32169f4f91de1ed11e5703541fe083de338 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_14e32d2bb9b2b4318b1dca47087bd32169f4f91de1ed11e5703541fe083de338->enter($__internal_14e32d2bb9b2b4318b1dca47087bd32169f4f91de1ed11e5703541fe083de338_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_widget_compound.html.php"));

        // line 1
        echo "<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
";
        
        $__internal_5fa6a907c0ca109cb14cf596bcfcdfa8c9a9819c2e1747526586a25ac6e72ca9->leave($__internal_5fa6a907c0ca109cb14cf596bcfcdfa8c9a9819c2e1747526586a25ac6e72ca9_prof);

        
        $__internal_14e32d2bb9b2b4318b1dca47087bd32169f4f91de1ed11e5703541fe083de338->leave($__internal_14e32d2bb9b2b4318b1dca47087bd32169f4f91de1ed11e5703541fe083de338_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_widget_compound.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div <?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>>
    <?php if (!\$form->parent && \$errors): ?>
    <?php echo \$view['form']->errors(\$form) ?>
    <?php endif ?>
    <?php echo \$view['form']->block(\$form, 'form_rows') ?>
    <?php echo \$view['form']->rest(\$form) ?>
</div>
", "@Framework/Form/form_widget_compound.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_widget_compound.html.php");
    }
}
