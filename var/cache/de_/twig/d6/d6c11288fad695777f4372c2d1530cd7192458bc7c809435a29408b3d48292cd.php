<?php

/* @Framework/Form/url_widget.html.php */
class __TwigTemplate_60878f9c9c4afc69e05d8c454ba82e9d954cfe67212d0723df4b6446d499a0ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5c5167c59f2f3f671dd131c8e9fd9200f2d635dd60569fad3ed746c54b19d645 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_5c5167c59f2f3f671dd131c8e9fd9200f2d635dd60569fad3ed746c54b19d645->enter($__internal_5c5167c59f2f3f671dd131c8e9fd9200f2d635dd60569fad3ed746c54b19d645_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        $__internal_a574ae9c5087a321b76203188b753ea72238d5c0d7c4d09c451f98d760ebeeea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a574ae9c5087a321b76203188b753ea72238d5c0d7c4d09c451f98d760ebeeea->enter($__internal_a574ae9c5087a321b76203188b753ea72238d5c0d7c4d09c451f98d760ebeeea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/url_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
";
        
        $__internal_5c5167c59f2f3f671dd131c8e9fd9200f2d635dd60569fad3ed746c54b19d645->leave($__internal_5c5167c59f2f3f671dd131c8e9fd9200f2d635dd60569fad3ed746c54b19d645_prof);

        
        $__internal_a574ae9c5087a321b76203188b753ea72238d5c0d7c4d09c451f98d760ebeeea->leave($__internal_a574ae9c5087a321b76203188b753ea72238d5c0d7c4d09c451f98d760ebeeea_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/url_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'url')) ?>
", "@Framework/Form/url_widget.html.php", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\url_widget.html.php");
    }
}
