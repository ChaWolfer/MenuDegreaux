<?php

/* SonataAdminBundle:CRUD:list_currency.html.twig */
class __TwigTemplate_4363ac400b442255df621429929996364d8bc95fa6301ff7f99f09a656c0f3b8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->blocks = array(
            'field' => array($this, 'block_field'),
        );
    }

    protected function doGetParent(array $context)
    {
        // line 12
        return $this->loadTemplate($this->getAttribute((isset($context["admin"]) ? $context["admin"] : $this->getContext($context, "admin")), "getTemplate", array(0 => "base_list_field"), "method"), "SonataAdminBundle:CRUD:list_currency.html.twig", 12);
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_265b0e5a81f50f6ca5f0f7b97dce43e11a7ad9d6bfc8e7b55f3991ebc917985c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_265b0e5a81f50f6ca5f0f7b97dce43e11a7ad9d6bfc8e7b55f3991ebc917985c->enter($__internal_265b0e5a81f50f6ca5f0f7b97dce43e11a7ad9d6bfc8e7b55f3991ebc917985c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_currency.html.twig"));

        $__internal_2177fb929973477e1ff154aaa0964f8fda4d730b8effe6efcea49b6c6879b14b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2177fb929973477e1ff154aaa0964f8fda4d730b8effe6efcea49b6c6879b14b->enter($__internal_2177fb929973477e1ff154aaa0964f8fda4d730b8effe6efcea49b6c6879b14b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "SonataAdminBundle:CRUD:list_currency.html.twig"));

        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_265b0e5a81f50f6ca5f0f7b97dce43e11a7ad9d6bfc8e7b55f3991ebc917985c->leave($__internal_265b0e5a81f50f6ca5f0f7b97dce43e11a7ad9d6bfc8e7b55f3991ebc917985c_prof);

        
        $__internal_2177fb929973477e1ff154aaa0964f8fda4d730b8effe6efcea49b6c6879b14b->leave($__internal_2177fb929973477e1ff154aaa0964f8fda4d730b8effe6efcea49b6c6879b14b_prof);

    }

    // line 14
    public function block_field($context, array $blocks = array())
    {
        $__internal_3e468541a2af7e86fe94009e11d785663ef8d63974fdbd29676a76cc09b86971 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_3e468541a2af7e86fe94009e11d785663ef8d63974fdbd29676a76cc09b86971->enter($__internal_3e468541a2af7e86fe94009e11d785663ef8d63974fdbd29676a76cc09b86971_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        $__internal_7319a8588a46abb0a9ca6f4702bb2530efd0affa92cf2a628ee14a41516a1783 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7319a8588a46abb0a9ca6f4702bb2530efd0affa92cf2a628ee14a41516a1783->enter($__internal_7319a8588a46abb0a9ca6f4702bb2530efd0affa92cf2a628ee14a41516a1783_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "field"));

        // line 15
        echo "    ";
        if ( !(null === (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")))) {
            // line 16
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["field_description"]) ? $context["field_description"] : $this->getContext($context, "field_description")), "options", array()), "currency", array()), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : $this->getContext($context, "value")), "html", null, true);
            echo "
    ";
        }
        
        $__internal_7319a8588a46abb0a9ca6f4702bb2530efd0affa92cf2a628ee14a41516a1783->leave($__internal_7319a8588a46abb0a9ca6f4702bb2530efd0affa92cf2a628ee14a41516a1783_prof);

        
        $__internal_3e468541a2af7e86fe94009e11d785663ef8d63974fdbd29676a76cc09b86971->leave($__internal_3e468541a2af7e86fe94009e11d785663ef8d63974fdbd29676a76cc09b86971_prof);

    }

    public function getTemplateName()
    {
        return "SonataAdminBundle:CRUD:list_currency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 16,  48 => 15,  39 => 14,  18 => 12,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#

This file is part of the Sonata package.

(c) Thomas Rabaix <thomas.rabaix@sonata-project.org>

For the full copyright and license information, please view the LICENSE
file that was distributed with this source code.

#}

{% extends admin.getTemplate('base_list_field') %}

{% block field %}
    {% if value is not null %}
        {{ field_description.options.currency }} {{ value }}
    {% endif %}
{% endblock %}
", "SonataAdminBundle:CRUD:list_currency.html.twig", "C:\\wamp64\\www\\MenuDegreaux\\vendor\\sonata-project\\admin-bundle/Resources/views/CRUD/list_currency.html.twig");
    }
}
